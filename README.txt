Elementalist is a turn-based RPG game made as a master's thesis. Initially, it was meant to be a realtime platformer
type game where player can combine various "element orbs"  to cast spells, but gradually evolved into it's current form.

The game can be divided into two parts. First one is combat, the base of which are the characters. Each characters has
stats that can be (de)buffed with stat modifiers. Additionally, each character has its own spells. The game also features
a simple inventory system. Some items can be used as actions, others can provide buffs.

Enemies are controlled by self-implemented Behaviour Tree AI. Thee BT can be extended by additional nodes if needed.
The structure of BT is defined within an XML file. To make writing of the XML easier, an XSD was generated.

In the non-combat part of the game, player can accept and turn in completed quests, buy talents and items and go on expeditions.

Since this project was made for master's thesis, I was working on it alone both on programing and creating graphics.
The only exception is some brainstorming when creating game mechanics and some concept art.

Link to a Windows build: https://drive.google.com/open?id=1OP7Jm9axNbR008C9vK-louVAlk_Q1FYE