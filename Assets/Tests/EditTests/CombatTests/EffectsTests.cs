using Zenject;
using NUnit.Framework;
using Elementalist.Character;
using UnityEngine;
using Elementalist.Combat.Effects;
using Elementalist;

[TestFixture]
public class EffectsTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();
    }

    private Character GetTestCharacter()
    {
        GameObject go = GameObject.Instantiate(Resources.Load<GameObject>("Tests/Effects/TestCharacterPrefab")) as GameObject;

        var characterGO = GameObject.Instantiate(go);
        var character = characterGO.GetComponent<Character>();

        Container.InjectGameObject(characterGO);

        return character;
    }

    [Test]
    public void ImmediateEffect_Damage()
    {
        var character = GetTestCharacter();

        IImmediateEffect eff = Resources.Load<DamageEffect>("Tests/Effects/DamageEffect");

        eff.Execute(character, character);

        Assert.That(character.CurrentHealth == 35);
    }

    [Test]
    public void ImmediateEffect_Heal()
    {
        var character = GetTestCharacter();
        character.TakeDamage(20, ElementType.Fire, true);

        IImmediateEffect eff = Resources.Load<HealEffect>("Tests/Effects/HealEffect");

        eff.Execute(character, character);

        Assert.That(character.CurrentHealth == 45);
    }

    [Test]
    public void ImmediateEffect_LifeSteal()
    {
        var target = GetTestCharacter();
        var source = GetTestCharacter();

        IImmediateEffect eff = Resources.Load<LifeStealEffect>("Tests/Effects/LifestealEffect");

        source.TakeDamage(20, ElementType.Fire, true);
        
        eff.Execute(target, source);

        Assert.That(target.CurrentHealth == 30 && source.CurrentHealth == 40);
    }

    [Test]
    public void TemporaryEffect_ActionEffect()
    {
        var character = GetTestCharacter();

        ITemporaryEffect eff = Resources.Load<ActionEffect>("Tests/Effects/ActionEffect");

        eff.Execute(character, character);

        Assert.That(character.CurrentHealth == 40);
    }

    [Test]
    public void TemporaryEffectHandler_ActionEffect()
    {
        var character = GetTestCharacter();

        ITemporaryEffect eff = Resources.Load<ActionEffect>("Tests/Effects/ActionEffect");
        TemporaryEffectHandler<ActionEffect> handler = new TemporaryEffectHandler<ActionEffect>(eff, character, character);
        handler.Update(1);

        Assert.That(character.CurrentHealth == 40);
    }

    [Test]
    public void TemporaryEffectHandler_ActionEffect_MultipleRounds()
    {
        var character = GetTestCharacter();

        ITemporaryEffect eff = Resources.Load<ActionEffect>("Tests/Effects/ActionEffect");
        TemporaryEffectHandler<ActionEffect> handler = new TemporaryEffectHandler<ActionEffect>(eff, character, character);
        handler.Update(2);

        Assert.That(character.CurrentHealth == 10);
    }

    [Test]
    public void TemporaryEffectHandler_ActionEffect_ExecuteWhenAdded()
    {
        var character = GetTestCharacter();

        ITemporaryEffect eff = Resources.Load<ActionEffect>("Tests/Effects/ActionEffect_ExecuteWhenAdded");
        TemporaryEffectHandler<ActionEffect> handler = new TemporaryEffectHandler<ActionEffect>(eff, character, character);

        Assert.That(character.CurrentHealth == 40);
    }
    [Test]
    public void TemporaryEffectHandler_EffectEndedEvent()
    {
        var character = GetTestCharacter();

        ITemporaryEffect eff = Resources.Load<ActionEffect>("Tests/Effects/ActionEffect");
        TemporaryEffectHandler<ActionEffect> handler = new TemporaryEffectHandler<ActionEffect>(eff, character, character);

        TemporaryEffectHandler<ActionEffect> handlerFromEvent = null;

        handler.OnEffectEnded += h => handlerFromEvent = h;

        handler.Update(2);

        Assert.NotNull(handlerFromEvent);
    }
}