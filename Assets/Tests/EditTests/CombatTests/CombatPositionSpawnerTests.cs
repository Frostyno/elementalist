using Zenject;
using NUnit.Framework;
using Elementalist.Combat.Utility;
using UnityEngine;
using Elementalist.Combat;
using System.Collections.Generic;

[TestFixture]
public class CombatPositionSpawnerTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        var cpPrefab = Resources.Load<GameObject>("Tests/Combat/CombatPosition");

        Container.Bind<SpriteRenderer>().FromComponentsInChildren().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.Bind<BoxCollider2D>().FromComponentSibling().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.BindFactory<CombatPosition, CombatPosition.CombatPositionFactory>()
            .FromNewComponentOnNewPrefab(cpPrefab);
    }

    private CombatPositionSpawner SetUpTest()
    {
        var spawner = GameObject.Instantiate(Resources.Load<CombatPositionSpawner>("Tests/Combat/Spawner")) as CombatPositionSpawner;
        Container.InjectGameObject(spawner.gameObject);
        return spawner;
    }

    [Test]
    public void CombatPositionSpawner_SpawnPosition()
    {
        var spawner = SetUpTest();

        var positionLeft = spawner.SpawnCombatPosition(CombatSide.Left);
        var positionRight = spawner.SpawnCombatPosition(CombatSide.Right);

        Assert.That(positionLeft != null && positionRight != null);
    }

    [Test]
    public void CombatPositionSpawner_SpawnLeftPositions()
    {
        var spawner = SetUpTest();

        List<CombatPosition> positions = new List<CombatPosition>();

        for (int i = 0; i < 2; i++) positions.Add(spawner.SpawnCombatPosition(CombatSide.Left));

        float width = positions[0].GetComponent<Collider2D>().bounds.size.x;


        Assert.That(positions[0].transform.position.x, 
            Is.EqualTo(-(0.25f + (width / 2))).Within(0.01f));
        Assert.That(positions[1].transform.position.x,
            Is.EqualTo(-(0.25f + (width / 2) + 0.25f + width)).Within(0.01f));
    }

    [Test]
    public void CombatPositionSpawner_SpawnRightPositions()
    {
        var spawner = SetUpTest();

        List<CombatPosition> positions = new List<CombatPosition>();

        for (int i = 0; i < 2; i++) positions.Add(spawner.SpawnCombatPosition(CombatSide.Right));

        float width = positions[0].GetComponent<Collider2D>().bounds.size.x;
        
        Assert.That(positions[0].transform.position.x,
            Is.EqualTo(0.25f + (width / 2)).Within(0.01f));
        Assert.That(positions[1].transform.position.x,
            Is.EqualTo(0.25f + (width / 2) + 0.25f + width).Within(0.01f));
    }
}