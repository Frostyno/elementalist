using Zenject;
using NUnit.Framework;
using Elementalist.Combat;
using Elementalist.Combat.Utility;
using System.Collections.Generic;
using UnityEngine;
using Elementalist.Character;

[TestFixture]
public class CombatAreaTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        var cpPrefab = Resources.Load<GameObject>("Tests/Combat/CombatPosition");

        Container.Bind<SpriteRenderer>().FromComponentsInChildren().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.Bind<BoxCollider2D>().FromComponentSibling().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.BindFactory<CombatPosition, CombatPosition.CombatPositionFactory>()
            .FromNewComponentOnNewPrefab(cpPrefab);
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentsSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();

        Container.Bind<Character>().FromComponentSibling().AsTransient()
            .WhenInjectedInto<CombatController>();
    }

    private (CombatArea, List<CombatController>, List<CombatController>) SetUpTest()
    {
        CombatParameters cParams = new CombatParameters(3, 4);
        var spawner = GameObject.Instantiate(Resources.Load<CombatPositionSpawner>("Tests/Combat/Spawner")) as CombatPositionSpawner;
        List<CombatController> leftCombatants = new List<CombatController>();
        List<CombatController> rightCombatants = new List<CombatController>();

        Container.InjectGameObject(spawner.gameObject);

        for (int i = 0; i < 2; i++)
            leftCombatants.Add(SpawnCombatant(CombatSide.Left));
        for (int i = 0; i < 3; i++)
            rightCombatants.Add(SpawnCombatant(CombatSide.Right));
        
        CombatArea cArea = new CombatArea(spawner, leftCombatants, rightCombatants, cParams);
        return (cArea, leftCombatants, rightCombatants);
    }

    private CombatController SpawnCombatant(CombatSide side)
    {
        var combatant = GameObject.Instantiate(Resources.Load<CombatController>("Tests/Combat/TestCharacterPrefab")) as CombatController;
        combatant.Team = new CombatTeam(side);
        Container.InjectGameObject(combatant.gameObject);
        return combatant;
    }

    [Test]
    public void CombatArea_SpawnPositions()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        Assert.That(area.LeftSidePositions.Count == 3, "Left side positions are not the corrrect amount.");
        Assert.That(area.RightSidePositions.Count == 4, "Right side positions are not the correct amount.");
    }

    [Test]
    public void CombatArea_EmplaceCombatants()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        var areaLeft = area.LeftSidePositions;
        var areaRight = area.RightSidePositions;

        Assert.That(
            areaLeft[0].Combatant == leftCombatants[0] &&
            areaLeft[1].Combatant == leftCombatants[1] &&
            areaLeft[2].Combatant == null,
            "Combatants in left side are not emplaced in positions correctly."
            );

        Assert.That(
            areaRight[0].Combatant == rightCombatants[0] &&
            areaRight[1].Combatant == rightCombatants[1] &&
            areaRight[2].Combatant == rightCombatants[2] &&
            areaRight[3].Combatant == null,
            "Combatants in right side are not emplaced in positions correctly."
            );
    }

    [Test]
    public void ComobatArea_GetFirstCombatant()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        var position = area.GetFirstCombatantPosition(CombatSide.Left);
        Assert.That(position.Combatant == leftCombatants[0], "First combatant incorrect on left side.");

        position = area.GetFirstCombatantPosition(CombatSide.Right);
        Assert.That(position.Combatant == rightCombatants[0], "First combatant incorrect on right side.");
    }

    [Test]
    public void CombatArea_GetCombatantPosition()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        Assert.That(area.GetCombatantPosition(leftCombatants[0]).Combatant == leftCombatants[0]);
        Assert.That(area.GetCombatantPosition(leftCombatants[1]).Combatant == leftCombatants[1]);
        Assert.That(area.GetCombatantPosition(rightCombatants[0]).Combatant == rightCombatants[0]);
        Assert.That(area.GetCombatantPosition(rightCombatants[1]).Combatant == rightCombatants[1]);
        Assert.That(area.GetCombatantPosition(rightCombatants[2]).Combatant == rightCombatants[2]);
    }

    [Test]
    public void CombatArea_GetCombatantNeighboursPositions()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        var neighbours = area.GetCombatantNeighboursPositions(rightCombatants[1]);
        Assert.That(neighbours.Count == 2, "Incorrect number of neighbours surrounded combatant.");
        Assert.That(neighbours[0].Combatant == rightCombatants[0]
            && neighbours[1].Combatant == rightCombatants[2],
            "Neighbours for surrounded combatant are incorrect.");

        neighbours = area.GetCombatantNeighboursPositions(leftCombatants[0]);
        Assert.That(neighbours.Count == 1, "Incorrect number of neighbours for combatant in the front.");
        Assert.That(neighbours[0].Combatant == leftCombatants[1],
            "Neighbours for front combatant are incorrect.");

        neighbours = area.GetCombatantNeighboursPositions(rightCombatants[2]);
        Assert.That(neighbours.Count == 1, "Incorrect number of neighbours for combatant in the back.");
        Assert.That(neighbours[0].Combatant == rightCombatants[1],
            "Neighbours for combatant in the back are incorrect.");
    }
}