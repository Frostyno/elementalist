using Zenject;
using NUnit.Framework;
using UnityEngine;
using Elementalist.Combat;
using System.Collections.Generic;
using Elementalist.Combat.Utility;
using Elementalist.Character;
using Elementalist.Combat.Actions;
using Elementalist;

[TestFixture]
public class SpellTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        // Character
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();

        // Combat
        Container.Bind<Character>().FromComponentSibling()
            .WhenInjectedInto<CombatController>();

        Container.Bind<CombatPositionSpawner>()
            .FromComponentSibling().AsSingle();

        Container.Bind<SpriteRenderer>().FromComponentsInChildren().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.Bind<BoxCollider2D>().FromComponentSibling().AsTransient()
            .WhenInjectedInto<CombatPosition>();

        //Factories
        Container.BindFactory<CombatPosition, CombatPosition.CombatPositionFactory>()
            .FromNewComponentOnNewPrefab(Resources.Load<GameObject>("Tests/Combat/CombatPosition"));
    }

    private (CombatArea, List<CombatController>, List<CombatController>) SetUpTest()
    {
        CombatParameters cParams = new CombatParameters(3, 4);
        var spawner = GameObject.Instantiate(Resources.Load<CombatPositionSpawner>("Tests/Combat/Spawner")) as CombatPositionSpawner;
        List<CombatController> leftCombatants = new List<CombatController>();
        List<CombatController> rightCombatants = new List<CombatController>();

        Container.InjectGameObject(spawner.gameObject);

        leftCombatants.Add(SpawnCombatant(Resources.Load<CombatController>("Tests/Combat/Spells/TestCharacterPrefab"), CombatSide.Left));
        rightCombatants.Add(SpawnCombatant(Resources.Load<CombatController>("Tests/Combat/Spells/EnemyCharacterPrefab_NoAP"), CombatSide.Right));
        rightCombatants.Add(SpawnCombatant(Resources.Load<CombatController>("Tests/Combat/Spells/EnemyCharacterPrefab_NoOrbs"), CombatSide.Right));

        CombatArea cArea = new CombatArea(spawner, leftCombatants, rightCombatants, cParams);
        return (cArea, leftCombatants, rightCombatants);
    }

    private CombatController SpawnCombatant(CombatController prefab, CombatSide side)
    {
        var combatant = GameObject.Instantiate(prefab) as CombatController;
        combatant.Team = new CombatTeam(side);
        Container.InjectGameObject(combatant.gameObject);
        return combatant;
    }

    [Test]
    public async void Spell_UseResources()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        var spell = Resources.Load<SpellType>("Tests/Combat/Spells/InstantSpell");

        var executed = await spell.Execute(leftCombatants[0], leftCombatants[0], area);
        var player = leftCombatants[0].Character;

        Assert.True(executed, "Spell wasn't executed.");
        Assert.That(
            player.CurrentActionPoints == 9 &&
            player.CurrentOrbs(ElementType.Fire) == 9 &&
            player.CurrentOrbs(ElementType.Water) == 8 &&
            player.CurrentOrbs(ElementType.Earth) == 9 &&
            player.CurrentOrbs(ElementType.Air) == 9,
            "Characters resources were not used correctly when casting spell.");
    }

    [Test]
    public async void Spell_Instant_NoAP()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        var spell = Resources.Load<SpellType>("Tests/Combat/Spells/InstantSpell");

        var executed = await spell.Execute(rightCombatants[0], rightCombatants[0], area);

        Assert.False(executed, "Spell executed even though character didn't have enough AP.");
    }

    [Test]
    public async void Spell_Instant_NoOrbs()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        var spell = Resources.Load<SpellType>("Tests/Combat/Spells/InstantSpell");

        var executed = spell.Execute(rightCombatants[1], rightCombatants[1], area);

        Assert.False(await executed, "Spell executed even though character didn't have enough orbs.");
    }

    [Test]
    public async void Spell_Instant_InvalidTarget()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        var spell = Resources.Load<SpellType>("Tests/Combat/Spells/InstantSpell");

        var executed = await spell.Execute(leftCombatants[0], rightCombatants[1], area);

        Assert.False(executed, "Spell executed even though character didn't have enough orbs.");
    }

    [Test]
    public async void Spell_Instant_Valid()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        var spell = Resources.Load<SpellType>("Tests/Combat/Spells/InstantSpell");

        var executed = await spell.Execute(leftCombatants[0], leftCombatants[0], area);

        Assert.True(executed, "Spell executed even though character didn't have enough orbs.");
    }

    [Test]
    public void Spell_Instant_Effects()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();

        var spell = Resources.Load<SpellType>("Tests/Combat/Spells/InstantSpell");

        leftCombatants[0].Character.TakeDamage(20d, ElementType.Fire);
        spell.Execute(leftCombatants[0], leftCombatants[0], area);

        Assert.That(leftCombatants[0].Character.CurrentHealth, Is.EqualTo(40d).Within(0.01d),
            "Spell immediate effects didn't go through properly.");
        Assert.That(leftCombatants[0].Character.ActiveActionEffectsCount == 1,
            "Spell temporary effect was not applied.");
    }
}