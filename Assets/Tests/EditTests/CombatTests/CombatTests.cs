using Zenject;
using NUnit.Framework;
using Elementalist.Combat;
using UnityEngine;
using Elementalist.Combat.Utility;
using Elementalist.Injection;
using Elementalist.Character;

[TestFixture]
public class CombatTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        // Character
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();

        // Combat
        Container.Bind<Character>().FromComponentSibling()
            .WhenInjectedInto<CombatController>();

        Container.Bind<CombatPositionSpawner>()
            .FromComponentSibling().AsSingle();

        Container.Bind<SpriteRenderer>().FromComponentsInChildren().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.Bind<BoxCollider2D>().FromComponentSibling().AsTransient()
            .WhenInjectedInto<CombatPosition>();

        //Factories
        Container.BindFactory<CombatPosition, CombatPosition.CombatPositionFactory>()
            .FromNewComponentOnNewPrefab(Resources.Load<GameObject>("Tests/Combat/CombatPosition"));

        Container.Bind<CombatantFactoryContext>().AsSingle();
        Container.BindFactory<CombatController, CombatController.CombatControllerFactory>()
            .FromFactory<CombatantFromPrefabFactory>();
    }

    private (Combat, CombatController) SetUpTest(bool localCombatEvents)
    {
        PlayerCombatController player = GameObject.Instantiate<PlayerCombatController>(
            Resources.Load<PlayerCombatController>("Tests/Combat/TestCharacterPrefab"));
        Combat combat;
        if (localCombatEvents)
            combat = GameObject.Instantiate<Combat>(Resources.Load<Combat>("Tests/Combat/Combat"));
        else
            combat = GameObject.Instantiate<Combat>(Resources.Load<Combat>("Tests/Combat/Combat_GlobalEvents"));

        Container.Bind<PlayerCombatController>().FromInstance(player).AsSingle();
        Container.Bind<Combat>().FromInstance(combat).AsSingle();

        Container.InjectGameObject(player.gameObject);
        Container.InjectGameObject(combat.gameObject);

        CombatParameters cParams = new CombatParameters(1, 2, CombatSide.Left);
        GameObject enemyPrefab = Resources.Load<GameObject>("Tests/Combat/EnemyCharacterPrefab");

        cParams.AddEnemyPrefab(enemyPrefab);
        cParams.AddEnemyPrefab(enemyPrefab);

        combat.Initialize(cParams);

        return (combat, player);
    }

    [Test]
    public void Combat_SetUpPlayer()
    {
        (var combat, var player) = SetUpTest(true);

        var leftSidePositions = combat.CombatArea.LeftSidePositions;

        Assert.That(player == leftSidePositions[0].Combatant, "Player is not set to correct position.");
        Assert.NotNull(player.Team, "Player doesn't have team set.");
        Assert.That(player.Team.Side == CombatSide.Left, "Player doesn't have correct side set");
    }

    [Test]
    public void Combat_EnemiesSetUp()
    {
        (var combat, var player) = SetUpTest(true);

        var rightSidePositions = combat.CombatArea.RightSidePositions;

        Assert.NotNull(rightSidePositions[0].Combatant);
        Assert.NotNull(rightSidePositions[1].Combatant);
    }

    [Test]
    public void Combat_TurnOrder()
    {
        //NOTE: this test depends on how turn order is made in combat, as of now left side is going before right side
        (var combat, var player) = SetUpTest(true);
        var leftPositions = combat.CombatArea.LeftSidePositions;
        var rightPositions = combat.CombatArea.RightSidePositions;

        Assert.True(leftPositions[0].Combatant.OnTurn);
        leftPositions[0].Combatant.EndTurn();
        Assert.True(rightPositions[0].Combatant.OnTurn);
        rightPositions[0].Combatant.EndTurn();
        Assert.True(rightPositions[1].Combatant.OnTurn);
        rightPositions[1].Combatant.EndTurn();
        Assert.True(leftPositions[0].Combatant.OnTurn);
    }
    
    [Test]
    public void Combat_LocalCombatEvents()
    {
        (var combat, var player) = SetUpTest(true);
        var leftPositions = combat.CombatArea.LeftSidePositions;
        var rightPositions = combat.CombatArea.RightSidePositions;
        
        leftPositions[0].Combatant.EndTurn();
        rightPositions[0].Combatant.EndTurn();

        int triggersBegin = 0;
        int triggersEnd = 0;

        leftPositions[0].Combatant.GetComponent<CharacterEvents>().OnRoundBegan += () => triggersBegin++;
        rightPositions[0].Combatant.GetComponent<CharacterEvents>().OnRoundBegan += () => triggersBegin++;
        rightPositions[1].Combatant.GetComponent<CharacterEvents>().OnRoundBegan += () => triggersBegin++;
        leftPositions[0].Combatant.GetComponent<CharacterEvents>().OnRoundEnded += () => triggersEnd++;
        rightPositions[0].Combatant.GetComponent<CharacterEvents>().OnRoundEnded += () => triggersEnd++;
        rightPositions[1].Combatant.GetComponent<CharacterEvents>().OnRoundEnded += () => triggersEnd++;

        rightPositions[1].Combatant.EndTurn();

        Assert.That(triggersBegin, Is.EqualTo(1), "Incorrect number of events triggered on round beginning.");
        Assert.That(triggersEnd, Is.EqualTo(1), "Incorrect number of events triggered on round end.");
    }

    [Test]
    public void Combat_Global()
    {
        (var combat, var player) = SetUpTest(false);
        var leftPositions = combat.CombatArea.LeftSidePositions;
        var rightPositions = combat.CombatArea.RightSidePositions;

        leftPositions[0].Combatant.EndTurn();
        rightPositions[0].Combatant.EndTurn();

        int triggersBegin = 0;
        int triggersEnd = 0;

        leftPositions[0].Combatant.GetComponent<CharacterEvents>().OnRoundBegan += () => triggersBegin++;
        rightPositions[0].Combatant.GetComponent<CharacterEvents>().OnRoundBegan += () => triggersBegin++;
        rightPositions[1].Combatant.GetComponent<CharacterEvents>().OnRoundBegan += () => triggersBegin++;
        leftPositions[0].Combatant.GetComponent<CharacterEvents>().OnRoundEnded += () => triggersEnd++;
        rightPositions[0].Combatant.GetComponent<CharacterEvents>().OnRoundEnded += () => triggersEnd++;
        rightPositions[1].Combatant.GetComponent<CharacterEvents>().OnRoundEnded += () => triggersEnd++;

        rightPositions[1].Combatant.EndTurn();


        Assert.That(triggersBegin, Is.EqualTo(3), "Incorrect number of events triggered on round beginning.");
        Assert.That(triggersEnd, Is.EqualTo(3), "Incorrect number of events triggered on round end.");
    }
}