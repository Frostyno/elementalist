using Zenject;
using NUnit.Framework;
using Elementalist.Combat;
using UnityEngine;
using Elementalist.Character;

[TestFixture]
public class CombatPositionTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        Container.Bind<SpriteRenderer>().FromComponentsInChildren().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.Bind<BoxCollider2D>().FromComponentSibling().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentsSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();

        Container.Bind<Character>().FromComponentSibling().AsTransient()
            .WhenInjectedInto<CombatController>();
    }

    private (CombatPosition, CombatController) SetUpTest()
    {
        var position = GameObject.Instantiate(Resources.Load<CombatPosition>("Tests/Combat/CombatPosition")) as CombatPosition;
        var controller = GameObject.Instantiate(Resources.Load<CombatController>("Tests/Combat/TestCharacterPrefab")) as CombatController;

        Container.InjectGameObject(position.gameObject);
        Container.InjectGameObject(controller.gameObject);

        return (position, controller);
    }

    [Test]
    public void CombatPosition_SetCombatant()
    {
        (var position, var controller) = SetUpTest();

        position.Combatant = controller;

        Assert.That(position.Combatant = controller);
    }

    [Test]
    public void CombatPosition_SetCombatantSprite()
    {
        (var position, var controller) = SetUpTest();
        var renderer = position.GetComponentInChildren<SpriteRenderer>();
        var charEssence = controller.GetComponent<CharacterEssence>();

        position.Combatant = controller;

        Assert.That(renderer.sprite == charEssence.CharacterIcon, 
            "Sprite in combat position's renderer is not equal to character's sprite");
        if (renderer.sprite == null) return;
        Assert.That(renderer.transform.position.y == renderer.sprite.bounds.extents.y,
            "Combat position's renderer's GameObject is not in correct height.");
    }

    [Test]
    public void CombatPosition_RemoveCombatant()
    {
        (var position, var controller) = SetUpTest();
        var renderer = position.GetComponentInChildren<SpriteRenderer>();

        position.Combatant = controller;
        position.Combatant = null;

        Assert.That(position.Combatant == null, "Combatant was not set to null in combat position.");
        Assert.That(renderer.sprite == null, "Combat position's renderer's sprite was not set to null");
    }
}