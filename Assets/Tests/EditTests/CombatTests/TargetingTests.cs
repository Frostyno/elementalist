using Zenject;
using NUnit.Framework;
using UnityEngine;
using Elementalist.Combat;
using Elementalist.Combat.Targeting;
using System.Collections.Generic;
using Elementalist.Combat.Utility;
using Elementalist.Character;

[TestFixture]
public class TargetingTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        var cpPrefab = Resources.Load<GameObject>("Tests/Combat/CombatPosition");

        Container.Bind<SpriteRenderer>().FromComponentsInChildren().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.Bind<BoxCollider2D>().FromComponentSibling().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.BindFactory<CombatPosition, CombatPosition.CombatPositionFactory>()
            .FromNewComponentOnNewPrefab(cpPrefab);
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentsSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();

        Container.Bind<Character>().FromComponentSibling().AsTransient()
            .WhenInjectedInto<CombatController>();
    }

    private (CombatArea, List<CombatController>, List<CombatController>) SetUpTest()
    {
        CombatParameters cParams = new CombatParameters(2, 3);
        var spawner = GameObject.Instantiate(Resources.Load<CombatPositionSpawner>("Tests/Combat/Spawner")) as CombatPositionSpawner;
        List<CombatController> leftCombatants = new List<CombatController>();
        List<CombatController> rightCombatants = new List<CombatController>();

        Container.InjectGameObject(spawner.gameObject);

        for (int i = 0; i < 2; i++)
            leftCombatants.Add(SpawnCombatant(CombatSide.Left));
        for (int i = 0; i < 3; i++)
            rightCombatants.Add(SpawnCombatant(CombatSide.Right));

        CombatArea cArea = new CombatArea(spawner, leftCombatants, rightCombatants, cParams);
        return (cArea, leftCombatants, rightCombatants);
    }

    private CombatController SpawnCombatant(CombatSide side)
    {
        var combatant = GameObject.Instantiate(Resources.Load<CombatController>("Tests/Combat/TestCharacterPrefab")) as CombatController;
        combatant.Team = new CombatTeam(side);
        Container.InjectGameObject(combatant.gameObject);
        return combatant;
    }

    [Test]
    public void Targeting_Instant_Self()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();
        ITargetingStrategy targeting = Resources.Load<InstantTargeting>("Tests/Combat/InstantTargeting_CombatTest") as ITargetingStrategy;

        targeting.MarkTargetablePositions(rightCombatants[1], area, new HashSet<ActionTargetType>() { ActionTargetType.Self });

        var rightPositions = area.GetCombatSide(CombatSide.Right);
        var leftPositions = area.GetCombatSide(CombatSide.Left);
        Assert.That(rightPositions[1].Targetable, "Targetable position wasn't marked");
        Assert.That(!rightPositions[0].Targetable && !rightPositions[2].Targetable && !leftPositions[0].Targetable && !leftPositions[1].Targetable,
            "Some positions were marked incorectly.");
    }

    [Test]
    public void Targeting_Instant_Ally()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();
        ITargetingStrategy targeting = Resources.Load<InstantTargeting>("Tests/Combat/InstantTargeting_CombatTest") as ITargetingStrategy;

        targeting.MarkTargetablePositions(rightCombatants[1], area, new HashSet<ActionTargetType>() { ActionTargetType.Ally });

        var rightPositions = area.GetCombatSide(CombatSide.Right);
        var leftPositions = area.GetCombatSide(CombatSide.Left);
        Assert.That(rightPositions[0].Targetable && rightPositions[2].Targetable, "Not all targetable positions were marked correctly");
        Assert.That(!rightPositions[1].Targetable && !leftPositions[0].Targetable && !leftPositions[1].Targetable,
            "Some positions were marked incorrectly.");
    }

    [Test]
    public void Targeting_Instant_Enemy()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();
        ITargetingStrategy targeting = Resources.Load<InstantTargeting>("Tests/Combat/InstantTargeting_CombatTest") as ITargetingStrategy;

        targeting.MarkTargetablePositions(rightCombatants[1], area, new HashSet<ActionTargetType>() { ActionTargetType.Enemy });

        var leftPositions = area.GetCombatSide(CombatSide.Left);
        var rightPositions = area.GetCombatSide(CombatSide.Right);
        Assert.That(leftPositions[0].Targetable && leftPositions[1].Targetable, "Not all targetable positions were marked correctly");
        Assert.That(!rightPositions[0].Targetable && !rightPositions[1].Targetable && !rightPositions[2].Targetable,
            "Some positions were marked incorrectly");
    }

    [Test]
    public void Targeting_Projectile_Self()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();
        ITargetingStrategy targeting = Resources.Load<ProjectileTargeting>("Tests/Combat/ProjectileTargeting_CombatTest") as ITargetingStrategy;
        
        targeting.MarkTargetablePositions(rightCombatants[1], area, new HashSet<ActionTargetType>() { ActionTargetType.Self });

        foreach (var position in area.GetCombatSide(CombatSide.Left))
            Assert.That(!position.Targetable, "Some positions were marked incorrectly");
        foreach (var position in area.GetCombatSide(CombatSide.Right))
            Assert.That(!position.Targetable, "Some positions were marked incorrectly");
    }

    [Test]
    public void Targeting_Projectile_Ally()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();
        ITargetingStrategy targeting = Resources.Load<ProjectileTargeting>("Tests/Combat/ProjectileTargeting_CombatTest") as ITargetingStrategy;

        targeting.MarkTargetablePositions(rightCombatants[1], area, new HashSet<ActionTargetType>() { ActionTargetType.Ally });

        var leftPositions = area.GetCombatSide(CombatSide.Left);
        var rightPositions = area.GetCombatSide(CombatSide.Right);
        Assert.That(rightPositions[0].Targetable && rightPositions[2].Targetable, "Not all targetable positions were marked correctly.");
        Assert.That(!rightPositions[1].Targetable && !leftPositions[0].Targetable && !leftPositions[1].Targetable,
            "Some positions were marked incorrectly");
    }

    [Test]
    public void Targeting_Projectile_Enemy()
    {
        (var area, var leftCombatants, var rightCombatants) = SetUpTest();
        ITargetingStrategy targeting = Resources.Load<ProjectileTargeting>("Tests/Combat/ProjectileTargeting_CombatTest") as ITargetingStrategy;

        targeting.MarkTargetablePositions(rightCombatants[1], area, new HashSet<ActionTargetType>() { ActionTargetType.Enemy });

        var leftPositions = area.GetCombatSide(CombatSide.Left);
        Assert.That(leftPositions[0].Targetable, "Targetable position wasn't marked correctly.");
        Assert.That(!leftPositions[1].Targetable, "Some targetable positions were marked incorrectly");
        foreach (var position in area.GetCombatSide(CombatSide.Right))
            Assert.That(!position.Targetable, "Some positions were marked incorrectly.");
    }
}