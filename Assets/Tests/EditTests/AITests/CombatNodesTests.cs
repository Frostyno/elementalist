using Zenject;
using NUnit.Framework;
using Elementalist.Character;
using Elementalist.Combat;
using Elementalist.Combat.Utility;
using UnityEngine;
using Elementalist.Injection;
using Elementalist.AI.BehaviorTree;
using Elementalist.AI.CombatNodes;
using Elementalist.Combat.Actions;
using Elementalist.Character.Talents;

[TestFixture]
public class CombatNodesTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        // Character
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();

        // Combat
        Container.Bind<Character>().FromComponentSibling()
            .WhenInjectedInto<CombatController>();

        Container.Bind<CombatPositionSpawner>()
            .FromComponentSibling().AsSingle();

        Container.Bind<SpriteRenderer>().FromComponentsInChildren().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        Container.Bind<BoxCollider2D>().FromComponentSibling().AsTransient()
            .WhenInjectedInto<CombatPosition>();
        //Factories
        Container.BindFactory<CombatPosition, CombatPosition.CombatPositionFactory>()
            .FromNewComponentOnNewPrefab(Resources.Load<GameObject>("Tests/AI/CombatPosition"));

        Container.Bind<CombatantFactoryContext>().AsSingle();
        Container.BindFactory<CombatController, CombatController.CombatControllerFactory>()
            .FromFactory<CombatantFromPrefabFactory>();
    }

    private (Combat, CombatController, BTContext) SetUpTest()
    {
        PlayerCombatController player = GameObject.Instantiate<PlayerCombatController>(
            Resources.Load<PlayerCombatController>("Tests/AI/TestCharacterPrefab"));
        var combat = GameObject.Instantiate<Combat>(Resources.Load<Combat>("Tests/AI/Combat"));

        Container.Bind<PlayerCombatController>().FromInstance(player).AsSingle();
        Container.Bind<Combat>().FromInstance(combat).AsSingle();

        Container.InjectGameObject(player.gameObject);
        Container.InjectGameObject(combat.gameObject);

        CombatParameters cParams = new CombatParameters(1, 2, CombatSide.Left);
        GameObject enemyPrefab = Resources.Load<GameObject>("Tests/AI/EnemyCharacterPrefab");

        cParams.AddEnemyPrefab(enemyPrefab);
        cParams.AddEnemyPrefab(enemyPrefab);

        combat.Initialize(cParams);

        BTContext context = new BTContext();
        CombatContext cContext = new CombatContext();
        context.SetDataItem(cContext);

        cContext.Source = player;
        cContext.Combat = combat;
        cContext.CombatArea = combat.CombatArea;

        return (combat, player, context);
    }

    [Test]
    public async void TargetFirstEnemyNode()
    {
        (var combat, var player, var context) = SetUpTest();

        TargetFirstEnemyNode node = new TargetFirstEnemyNode();
        var expectedTarget = combat.CombatArea.GetCombatSide(CombatSide.Right)[0].Combatant;
        var result = await node.Process(context);

        Assert.That(result == BTProcessResult.Success);
        Assert.That(context.GetDataItem<CombatContext>().Target == expectedTarget);
    }

    [Test]
    public async void TargetSelfNode()
    {
        (var combat, var player, var context) = SetUpTest();

        TargetSelfNode node = new TargetSelfNode();
        var result = await node.Process(context);

        Assert.That(result == BTProcessResult.Success);
        Assert.That(context.GetDataItem<CombatContext>().Target == player);
    }

    [Test]
    public async void CastSpellNode()
    {
        (var combat, var player, var context) = SetUpTest();
        context.GetDataItem<CombatContext>().Target = player;

        player.Character.GetComponent<CharacterTalents>().AddTalent(Resources.Load<Talent>("Tests/AI/Talent"));

        CastSpellNode node = new CastSpellNode(Spell.TestSpell);

        var result = await node.Process(context);

        Assert.That(result == BTProcessResult.Success);
        Assert.That(player.Character.CurrentHealth, Is.EqualTo(40d).Within(0.01d));
    }
}