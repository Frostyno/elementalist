﻿using System.Collections.Generic;
using Elementalist.AI.BehaviorTree;
using Elementalist.AI.Testing;
using NUnit.Framework;

public class BehaviorTreeTests
{
    [Test]
    public void BehaviorTree_BTContext_SetDataItem()
    {
        BTContext context = new BTContext();

        context.SetDataItem("", "key");

        Assert.That(context.ItemsCount == 1);
    }

    [Test]
    public void BehaviorTree_BTContext_GetDataItem_FoundNoKey()
    {
        BTContext context = new BTContext();

        context.SetDataItem("item");

        var item = context.GetDataItem<string>();
        Assert.That(item == "item");
    }

    [Test]
    public void BehaviorTree_BTContext_GetDataItem_FoundWithKey()
    {
        BTContext context = new BTContext();

        context.SetDataItem("item", "key");

        var item = context.GetDataItem<string>("key");
        Assert.That(item == "item");
    }

    [Test]
    public void BehaviorTree_BTContext_GetDataItem_NotFoundNoKey()
    {
        BTContext context = new BTContext();

        context.SetDataItem("item", "key");

        var item = context.GetDataItem<string>();
        Assert.Null(item);
    }

    [Test]
    public void BehaviorTree_BTContext_GetDataItem_NotFoundWithKey()
    {
        BTContext context = new BTContext();

        context.SetDataItem("item");

        var item = context.GetDataItem<string>("key");
        Assert.Null(item);
    }

    [Test]
    public async void BehaviorTree_SequenceNode_Success()
    {
        SequenceNode seq = new SequenceNode(
            new List<BTNode>()
            {
                new BTMockUpNode(BTProcessResult.Success),
                new BTMockUpNode(BTProcessResult.Success),
                new BTMockUpNode(BTProcessResult.Success)
            });

        var result = await seq.Process(null);

        Assert.That(result == BTProcessResult.Success);
    }

    [Test]
    public async void BehaviorTree_SequenceNode_Failure()
    {
        SequenceNode seq = new SequenceNode(
            new List<BTNode>()
            {
                new BTMockUpNode(BTProcessResult.Success),
                new BTMockUpNode(BTProcessResult.Failure),
                new BTMockUpNode(BTProcessResult.Success)
            });

        var result = await seq.Process(null);

        Assert.That(result == BTProcessResult.Failure);
    }

    [Test]
    public async void BehaviorTree_SelectorNode_Success()
    {
        SelectorNode sel = new SelectorNode(
            new List<BTNode>()
            {
                new BTMockUpNode(BTProcessResult.Failure),
                new BTMockUpNode(BTProcessResult.Success),
                new BTMockUpNode(BTProcessResult.Failure)
            });

        var result = await sel.Process(null);

        Assert.That(result == BTProcessResult.Success);
    }

    [Test]
    public async void BehaviorTree_SelectorNode_Failure()
    {
        SelectorNode sel = new SelectorNode(
            new List<BTNode>()
            {
                new BTMockUpNode(BTProcessResult.Failure),
                new BTMockUpNode(BTProcessResult.Failure),
                new BTMockUpNode(BTProcessResult.Failure)
            });

        var result = await sel.Process(null);

        Assert.That(result == BTProcessResult.Failure);
    }

    [Test]
    public async void BehaviorTree_InverterNode()
    {
        InverterNode invFail = new InverterNode(new BTMockUpNode(BTProcessResult.Failure));
        InverterNode invSucc = new InverterNode(new BTMockUpNode(BTProcessResult.Success));

        Assert.That(await invFail.Process(null) == BTProcessResult.Success, "Failure is not being inverted properly.");
        Assert.That(await invSucc.Process(null) == BTProcessResult.Failure, "Success is not being inverted properly.");
    }

    [Test]
    public async void BehaviorTree_SucceederNode()
    {
        SucceederNode sucFail = new SucceederNode(new BTMockUpNode(BTProcessResult.Failure));
        SucceederNode sucSucc = new SucceederNode(new BTMockUpNode(BTProcessResult.Success));

        Assert.That(await sucFail.Process(null) == BTProcessResult.Success, "Failure is not succeeded properly.");
        Assert.That(await sucSucc.Process(null) == BTProcessResult.Success, "Success is not succeeded properly.");
    }

    [Test]
    public async void BehaviorTree_ChanceSucceederNode_1Chance()
    {
        ChanceSucceederNode sucFail = new ChanceSucceederNode(new BTMockUpNode(BTProcessResult.Failure), 1d);
        ChanceSucceederNode sucSucc = new ChanceSucceederNode(new BTMockUpNode(BTProcessResult.Success), 1d);

        Assert.That(await sucFail.Process(null) == BTProcessResult.Success, "Failure is not succeeded properly.");
        Assert.That(await sucSucc.Process(null) == BTProcessResult.Success, "Success is not succeeded properly.");
    }

    [Test]
    public async void BehaviorTree_ChanceSucceederNode_0Chance()
    {
        ChanceSucceederNode sucFail = new ChanceSucceederNode(new BTMockUpNode(BTProcessResult.Failure), 0d);
        ChanceSucceederNode sucSucc = new ChanceSucceederNode(new BTMockUpNode(BTProcessResult.Success), 0d);

        Assert.That(await sucFail.Process(null) == BTProcessResult.Failure, "Failure is not succeeded properly.");
        Assert.That(await sucSucc.Process(null) == BTProcessResult.Success, "Success is not succeeded properly.");
    }
}
