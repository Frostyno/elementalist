using Zenject;
using NUnit.Framework;
using Elementalist.Character;
using Elementalist.AI.BehaviorTree;
using Elementalist.AI.CombatNodes;
using UnityEngine;
using Elementalist.Combat;
using Elementalist.Character.Stats;

[TestFixture]
public class StatConditionNodeTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();

        Container.Bind<Character>().FromComponentSibling().AsSingle()
            .WhenInjectedInto<CombatController>();
    }

    private Character GetTestCharacter()
    {
        GameObject go = GameObject.Instantiate(Resources.Load<GameObject>("Tests/AI/CharacterPrefab")) as GameObject;

        var characterGO = GameObject.Instantiate(go);
        var character = characterGO.GetComponent<Character>();

        Container.InjectGameObject(characterGO);

        return character;
    }

    private BTContext GetFilledBTContext(Character character)
    {
        BTContext context = new BTContext();
        CombatContext cContext = new CombatContext();
        cContext.Target = character.GetComponent<CombatController>();
        cContext.Source = character.GetComponent<CombatController>();

        context.SetDataItem(cContext);

        return context;
    }

    [Test]
    public async void StatConditionNode_Equal_Success()
    {
        var character = GetTestCharacter();
        var context = GetFilledBTContext(character);

        var node = new StatConditionNode(
            StatType.Health,
            50d,
            ConditionTarget.Source,
            ComparisonType.Equal
            );

        var result = await node.Process(context);
        Assert.That(result == BTProcessResult.Success);
    }

    [Test]
    public async void StatConditionNode_Equal_Failure()
    {
        var character = GetTestCharacter();
        var context = GetFilledBTContext(character);

        var node = new StatConditionNode(
            StatType.Health,
            0d,
            ConditionTarget.Source,
            ComparisonType.Equal
            );

        var result = await node.Process(context);
        Assert.That(result == BTProcessResult.Failure);
    }

    [Test]
    public async void StatConditionNode_Greater_Success()
    {
        var character = GetTestCharacter();
        var context = GetFilledBTContext(character);

        var node = new StatConditionNode(
            StatType.Health,
            20d,
            ConditionTarget.Source,
            ComparisonType.Greater
            );

        var result = await node.Process(context);
        Assert.That(result == BTProcessResult.Success);
    }

    [Test]
    public async void StatConditionNode_Greater_Failure()
    {
        var character = GetTestCharacter();
        var context = GetFilledBTContext(character);

        var node = new StatConditionNode(
            StatType.Health,
            50d,
            ConditionTarget.Source,
            ComparisonType.Greater
            );

        var result = await node.Process(context);
        Assert.That(result == BTProcessResult.Failure);
    }

    [Test]
    public async void StatConditionNode_GreaterOrEqual_Success()
    {
        var character = GetTestCharacter();
        var context = GetFilledBTContext(character);

        var node = new StatConditionNode(
            StatType.Health,
            50d,
            ConditionTarget.Source,
            ComparisonType.GreaterOrEqual
            );

        var result = await node.Process(context);
        Assert.That(result == BTProcessResult.Success);
    }

    [Test]
    public async void StatConditionNode_GreaterOrEqual_Failure()
    {
        var character = GetTestCharacter();
        var context = GetFilledBTContext(character);

        var node = new StatConditionNode(
            StatType.Health,
            51d,
            ConditionTarget.Source,
            ComparisonType.GreaterOrEqual
            );

        var result = await node.Process(context);
        Assert.That(result == BTProcessResult.Failure);
    }

    [Test]
    public async void StatConditionNode_Less_Success()
    {
        var character = GetTestCharacter();
        var context = GetFilledBTContext(character);

        var node = new StatConditionNode(
            StatType.Health,
            60d,
            ConditionTarget.Source,
            ComparisonType.Less
            );

        var result = await node.Process(context);
        Assert.That(result == BTProcessResult.Success);
    }

    [Test]
    public async void StatConditionNode_Less_Failure()
    {
        var character = GetTestCharacter();
        var context = GetFilledBTContext(character);

        var node = new StatConditionNode(
            StatType.Health,
            50d,
            ConditionTarget.Source,
            ComparisonType.Less
            );

        var result = await node.Process(context);
        Assert.That(result == BTProcessResult.Failure);
    }

    [Test]
    public async void StatConditionNode_LessOrEqual_Success()
    {
        var character = GetTestCharacter();
        var context = GetFilledBTContext(character);

        var node = new StatConditionNode(
            StatType.Health,
            50d,
            ConditionTarget.Source,
            ComparisonType.LessOrEqual
            );

        var result = await node.Process(context);
        Assert.That(result == BTProcessResult.Success);
    }

    [Test]
    public async void StatConditionNode_LessOrEqual_Failure()
    {
        var character = GetTestCharacter();
        var context = GetFilledBTContext(character);

        var node = new StatConditionNode(
            StatType.Health,
            49d,
            ConditionTarget.Source,
            ComparisonType.LessOrEqual
            );

        var result = await node.Process(context);
        Assert.That(result == BTProcessResult.Failure);
    }
}