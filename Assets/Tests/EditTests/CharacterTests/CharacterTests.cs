using Zenject;
using NUnit.Framework;
using Elementalist.Character;
using UnityEngine;
using Elementalist.Combat.Effects;
using Elementalist.Character.Talents;
using Elementalist.Character.Stats;
using Elementalist.Combat.Actions;
using Elementalist;

[TestFixture]
public class CharacterTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();
    }

    private Character GetTestCharacter()
    {
        GameObject go = GameObject.Instantiate(Resources.Load<GameObject>("Tests/Character/TestCharacterPrefab")) as GameObject;

        var characterGO = GameObject.Instantiate(go);
        var character = characterGO.GetComponent<Character>();

        Container.InjectGameObject(characterGO);

        return character;
    }

    [Test]
    public void TemporaryEffects_AddActionEffect()
    {
        var character = GetTestCharacter();

        ActionEffect effect = Resources.Load<ActionEffect>("Tests/Character/ActionEffect");

        character.AddTemporaryEffect(effect, character, character);

        Assert.That(character.ActiveActionEffectsCount == 1);
    }

    [Test]
    public void TemporaryEffects_UpdateActionEffects()
    {
        var character = GetTestCharacter();
        ActionEffect effect = Resources.Load<ActionEffect>("Tests/Character/ActionEffect");
        character.AddTemporaryEffect(effect, character, character);

        character.GetComponent<CharacterEvents>().SignalRoundBegan();

        // added action effect should deal 30 dmg to character
        Assert.That(character.CurrentHealth, Is.EqualTo(30d).Within(0.01d));
    }

    [Test]
    public void TemporaryEffects_RemoveEffectThatEnded()
    {
        var character = GetTestCharacter();
        ActionEffect effect = Resources.Load<ActionEffect>("Tests/Character/ActionEffect");
        character.AddTemporaryEffect(effect, character, character);

        var charEvents = character.GetComponent<CharacterEvents>();
        charEvents.SignalRoundBegan();
        charEvents.SignalRoundBegan();

        // added effect has the length of 2 rounds
        Assert.That(character.ActiveActionEffectsCount == 0);
    }

    [Test]
    public void TemporaryEffects_EffectsUpdateType()
    {
        var character = GetTestCharacter();
        ActionEffect effectRoundBegan = Resources.Load<ActionEffect>("Tests/Character/ActionEffect");
        ActionEffect effectRoundEnd = Resources.Load<ActionEffect>("Tests/Character/ActionEffect_RoundEnd");

        character.AddTemporaryEffect(effectRoundBegan, character, character);
        character.AddTemporaryEffect(effectRoundEnd, character, character);

        var charEvents = character.GetComponent<CharacterEvents>();
        charEvents.SignalRoundBegan();

        Assert.That(character.CurrentHealth, Is.EqualTo(30d).Within(0.01d),
            "TemporaryEffect updated when round begins didn't trigger.");

        charEvents.SignalRoundEnded();

        Assert.That(character.CurrentHealth, Is.EqualTo(15d).Within(0.01d),
            "TemporaryEffect updated when round begins didn't trigger.");
    }

    [Test]
    public void CharacterEvents_OnStatModifiersChanged()
    {
        var character = GetTestCharacter();
        var talents = character.GetComponent<CharacterTalents>();
        var charEvents = character.GetComponent<CharacterEvents>();

        StatModifier[] modifiers = null;

        charEvents.OnStatModifiersChanged += mods => modifiers = mods;

        talents.AddTalent(Resources.Load<Talent>("Tests/Character/Talent"));

        // 1 modifier from talent
        Assert.That(modifiers.Length == 1);

        character.AddTemporaryEffect(Resources.Load<ActionEffect>("Tests/Character/ActionEffect_StatModifier"), character, character);

        // 1 modifier from talent, 1 from action effect
        Assert.That(modifiers.Length == 2);

        //TODO: pridat kontrolu na statuses
    }

    [Test]
    public void CharacterEvents_OnSpellsChanged()
    {
        var character = GetTestCharacter();
        var talents = character.GetComponent<CharacterTalents>();
        var charEvents = character.GetComponent<CharacterEvents>();

        SpellType[] spells = null;

        charEvents.OnSpellsChanged += s => spells = s;

        talents.AddTalent(Resources.Load<Talent>("Tests/Character/Talent"));

        Assert.NotNull(spells);
        Assert.That(spells.Length == 1);
    }

    [Test]
    public void CharacterSpells()
    {
        var character = GetTestCharacter();
        var talents = character.GetComponent<CharacterTalents>();
        var charSpells = character.GetComponent<CharacterSpells>();
        var talent = Resources.Load<Talent>("Tests/Character/Talent");
        talents.AddTalent(talent);

        Assert.NotNull(charSpells.Spells);
        Assert.That(charSpells.Spells.Count == 1);
        Assert.True(charSpells.HasSpell(Spell.TestSpell));
        Assert.That(charSpells.GetSpell(Spell.TestSpell) == talent.Spells[0]);
    }

    [Test]
    public void Resources_OnRoundBeganRegenerate()
    {
        var character = GetTestCharacter();

        // use resources
        character.TakeDamage(20d, ElementType.Fire);
        character.UseActionPoints(2d);
        character.UseOrbs(2d, ElementType.Fire);
        character.UseOrbs(2d, ElementType.Water);
        character.UseOrbs(2d, ElementType.Earth);
        character.UseOrbs(2d, ElementType.Air);

        // resources should regenarate to full value
        character.GetComponent<CharacterEvents>().SignalRoundBegan();

        Assert.That(character.CurrentHealth, Is.EqualTo(character.MaxHealth).Within(0.01d),
            "Health is not regenerated properly.");
        Assert.That(character.CurrentBarrier, Is.EqualTo(character.MaxBarrier).Within(0.01d),
            "Barrier is not regenerated properly.");
        Assert.That(character.CurrentActionPoints, Is.EqualTo(character.MaxActionPoints).Within(0.01d),
            "Action points are not regenerated properly.");
        Assert.That(character.CurrentOrbs(ElementType.Fire), Is.EqualTo(character.MaxOrbs(ElementType.Fire)).Within(0.01d),
            "Fire orbs are not regenerated properly.");
        Assert.That(character.CurrentOrbs(ElementType.Water), Is.EqualTo(character.MaxOrbs(ElementType.Water)).Within(0.01d),
            "Fire orbs are not regenerated properly.");
        Assert.That(character.CurrentOrbs(ElementType.Earth), Is.EqualTo(character.MaxOrbs(ElementType.Earth)).Within(0.01d),
            "Fire orbs are not regenerated properly.");
        Assert.That(character.CurrentOrbs(ElementType.Air), Is.EqualTo(character.MaxOrbs(ElementType.Air)).Within(0.01d),
            "Fire orbs are not regenerated properly.");
    }
}