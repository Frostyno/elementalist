using Zenject;
using NUnit.Framework;
using Elementalist.Character;
using UnityEngine;
using Elementalist;

[TestFixture]
public class VitalityTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();
    }

    private Vitality GetTestVitality(GameObject prefab)
    {
        var characterGO = GameObject.Instantiate(prefab);

        Container.InjectGameObject(characterGO);

        return characterGO.GetComponent<Vitality>();
    }

    [Test]
    public void Vitality_TakeBarrierDamage()
    {
        var vitality = GetTestVitality(Resources.Load<GameObject>("Tests/Vitality/TestCharacterPrefab_NoResistance"));

        vitality.TakeDamage(5d, ElementType.Fire);
        Assert.That(vitality.CurrentBarrier, Is.EqualTo(5d).Within(0.01d));
    }

    [Test]
    public void Vitality_TakeBarrierDamage_Whole()
    {
        var vitality = GetTestVitality(Resources.Load<GameObject>("Tests/Vitality/TestCharacterPrefab_NoResistance"));

        vitality.TakeDamage(10d, ElementType.Fire);
        Assert.That(vitality.CurrentBarrier, Is.EqualTo(0d).Within(0.01d));
    }

    [Test]
    public void Vitality_TakeBarrierDamage_Overflow()
    {
        var vitality = GetTestVitality(Resources.Load<GameObject>("Tests/Vitality/TestCharacterPrefab_NoResistance"));

        vitality.TakeDamage(15d, ElementType.Fire);
        Assert.That(vitality.CurrentHealth, Is.EqualTo(45d).Within(0.01d));
    }

    [Test]
    public void Vitality_TakeBarrierDamage_Resistances_Overflow()
    {
        var vitality = GetTestVitality(Resources.Load<GameObject>("Tests/Vitality/TestCharacterPrefab_0_5_Resistance"));

        vitality.TakeDamage(20d, ElementType.Fire);
        Assert.That(vitality.CurrentHealth, Is.EqualTo(45d).Within(0.01d));
    }

    [Test]
    public void Vitality_TakeDamage_NoBarrier()
    {
        var vitality = GetTestVitality(Resources.Load<GameObject>("Tests/Vitality/TestCharacterPrefab_NoResistance"));

        vitality.TakeDamage(10d, ElementType.Fire); // destroy barrier
        vitality.TakeDamage(10d, ElementType.Fire);
        Assert.That(vitality.CurrentHealth, Is.EqualTo(40d).Within(0.01d));
    }

    [Test]
    public void Vitality_TakeDamage_Resistances_NoBarrier()
    {
        var vitality = GetTestVitality(Resources.Load<GameObject>("Tests/Vitality/TestCharacterPrefab_0_5_Resistance"));

        vitality.TakeDamage(10d, ElementType.Fire); // destroy barrier
        vitality.TakeDamage(25d, ElementType.Fire);
        Assert.That(vitality.CurrentHealth, Is.EqualTo(37.5d).Within(0.01d));
    }

    [Test]
    public void Vitality_TakeDamage_ResistanceHeal()
    {
        var vitality = GetTestVitality(Resources.Load<GameObject>("Tests/Vitality/TestCharacterPrefab_2FireResistance"));

        vitality.TakeDamage(10d, ElementType.Water);
        vitality.TakeDamage(25d, ElementType.Water);
        vitality.TakeDamage(15d, ElementType.Fire); // This should heal target for 15

        Assert.That(vitality.CurrentHealth, Is.EqualTo(40d).Within(0.01d));
    }

    [Test]
    public void Vitality_TakeDamage_NegativeResistance()
    {
        var vitality = GetTestVitality(Resources.Load<GameObject>("Tests/Vitality/TestCharacterPrefab_NegativeResistance"));

        vitality.TakeDamage(15d, ElementType.Fire); // should give 10 dmg to barrier, and then do double of the rest (so 10 dmg to health), because fire resist is -1

        Assert.That(vitality.CurrentHealth, Is.EqualTo(40d).Within(0.01d));
    }

    [Test]
    public void Vitality_CharacterDiesEvent()
    {
        var vitality = GetTestVitality(Resources.Load<GameObject>("Tests/Vitality/TestCharacterPrefab_NoResistance"));
        Vitality eventVitality = null;

        vitality.OnCharacterDied += vit => eventVitality = vit;

        vitality.TakeDamage(60d, ElementType.Fire);

        Assert.That(eventVitality == vitality);
    }

    [Test]
    public void Vitality_CharacterDiesEvent_Overkill()
    {
        var vitality = GetTestVitality(Resources.Load<GameObject>("Tests/Vitality/TestCharacterPrefab_NoResistance"));
        Vitality eventVitality = null;

        vitality.OnCharacterDied += vit => eventVitality = vit;

        vitality.TakeDamage(1000d, ElementType.Fire);

        Assert.That(eventVitality == vitality);
    }
}