﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Elementalist.Character;
using Elementalist.Character.Stats;
using Elementalist;

public class StatTests
{
    /// <summary>
    /// Tests if Stat that is not element based calculates value correctly.
    /// </summary>
    [Test]
    public void NonElement_Stat_Test()
    {
        Stat stat = new Stat(StatType.Health);

        CharacterType chType = Resources.Load<CharacterType>("Tests/Stats/CharacterType");
            
        var mods = new StatModifier[]
        {
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Additive_10"),
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Percentual_point25"),
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Percentual_point75"),
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Handicap")
        };

        stat.Recalculate(chType, mods);

        Assert.That(stat.Value, Is.EqualTo(20d).Within(0.01d));
    }
        
    /// <summary>
    /// Tests if element based Stat calculates value correctly.
    /// </summary>
    [Test]
    public void Element_Stat_Test()
    {
        Stat stat = new Stat(StatType.Orbs, ElementType.Fire);

        CharacterType chType = Resources.Load<CharacterType>("Tests/Stats/CharacterType");

        var mods = new StatModifier[]
        {
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Additive_10"),
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Percentual_point25"),
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Percentual_point75"),
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Handicap")
        };

        stat.Recalculate(chType, mods);

        Assert.That(stat.Value, Is.EqualTo(60d).Within(0.01d));
    }

    private Resource SetUpResourceForTest()
    {
        Resource res = new Resource(StatType.Orbs, StatType.OrbsRegen, ElementType.Fire);

        CharacterType chType = Resources.Load<CharacterType>("Tests/Stats/CharacterType");

        var mods = new StatModifier[]
        {
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Additive_10"),
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Percentual_point25"),
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Percentual_point75"),
            Resources.Load<StatModifier>("Tests/Stats/StatModifier_Handicap")
        };

        res.Recalculate(chType, mods);

        return res;
    }

    [Test]
    public void Resource_MaxValue_RecalculateTest()
    {
        Resource res = SetUpResourceForTest();
        Assert.That(res.MaxValue, Is.EqualTo(60d).Within(0.01d));
    }

    [Test]
    public void Resource_CurrentValue_AfterRecalculateTest()
    {
        Resource res = SetUpResourceForTest();
        Assert.That(res.CurrentValue, Is.EqualTo(60d).Within(0.01d));
    }

    [Test]
    public void Resource_UseTest()
    {
        Resource res = SetUpResourceForTest();

        res.Use(30d);

        Assert.That(res.CurrentValue, Is.EqualTo(30d).Within(0.01d));
    }

    [Test]
    public void Resource_RestoreTest()
    {
        Resource res = SetUpResourceForTest();

        res.Use(30d);
        res.Restore(10d);

        Assert.That(res.CurrentValue, Is.EqualTo(40d).Within(0.01d));
    }

    [Test]
    public void Resource_RegenValue_RecalculateTest()
    {
        Resource res = SetUpResourceForTest();
        Assert.That(res.Regeneration, Is.EqualTo(18d).Within(0.01d));
    }

    [Test]
    public void Resource_RegenerateTest()
    {
        Resource res = SetUpResourceForTest();

        res.Use(40d);
        res.Regenerate(2);

        Assert.That(res.CurrentValue, Is.EqualTo(56d).Within(0.01d));
    }
        
    [Test]
    public void Resource_OverRestoreTest()
    {
        Resource res = SetUpResourceForTest();

        res.Use(10d);
        res.Restore(20d);

        Assert.That(res.CurrentValue, Is.EqualTo(60d).Within(0.01d));
    }

    [Test]
    public void Resource_OverRegenerateTest()
    {
        Resource res = SetUpResourceForTest();

        res.Use(10d);
        res.Regenerate(10);

        Assert.That(res.CurrentValue, Is.EqualTo(60d).Within(0.01d));
    }
}
