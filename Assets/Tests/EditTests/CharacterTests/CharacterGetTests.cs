using Zenject;
using NUnit.Framework;
using Elementalist.Character;
using UnityEngine;
using Elementalist.Character.Stats;
using Elementalist;

[TestFixture]
public class CharacterGetTests : ZenjectUnitTestFixture
{
    [SetUp]
    public void Install()
    {
        Container.Bind<CharacterEssence>().FromComponentSibling();
        Container.Bind<CharacterEvents>().FromComponentSibling();
        Container.Bind<IStatModifierProvider>().FromComponentsSibling();
        Container.Bind<ISpellProvider>().FromComponentSibling();
        Container.Bind<Resistance>().FromComponentSibling();
        Container.Bind<Sources>().FromComponentSibling();
        Container.Bind<Vitality>().FromComponentSibling();
        Container.Bind<SpellPower>().FromComponentSibling();
        Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
        Container.Bind<CharacterSpells>().FromComponentSibling();
    }

    private Character GetTestCharacter()
    {
        GameObject go = GameObject.Instantiate(Resources.Load<GameObject>("Tests/Character/StatsTestCharacterPrefab")) as GameObject;

        var characterGO = GameObject.Instantiate(go);
        var character = characterGO.GetComponent<Character>();

        Container.InjectGameObject(characterGO);

        return character;
    }

    [Test]
    public void Character_GetMaxHealth()
    {
        var character = GetTestCharacter();

        Assert.That(character.GetStatValue(StatType.Health, maxValue: true),
            Is.EqualTo(50d).Within(0.01d));
    }

    [Test]
    public void Character_GetCurrentHealth()
    {
        var character = GetTestCharacter();

        character.TakeDamage(10d, ElementType.Air, true);

        Assert.That(character.GetStatValue(StatType.Health, maxValue: false),
            Is.EqualTo(45d).Within(0.01d));
    }

    [Test]
    public void Character_GetMaxBarrier()
    {
        var character = GetTestCharacter();

        Assert.That(character.GetStatValue(StatType.Barrier, maxValue: true),
            Is.EqualTo(30d).Within(0.01d));
    }

    [Test]
    public void Character_GetCurrentBarrier()
    {
        var character = GetTestCharacter();

        character.TakeDamage(10d, ElementType.Air);

        Assert.That(character.GetStatValue(StatType.Barrier, maxValue: false),
            Is.EqualTo(20d).Within(0.01d));
    }
    
    [Test]
    public void Character_GetMaxActionPoints()
    {
        var character = GetTestCharacter();

        Assert.That(character.GetStatValue(StatType.ActionPoints, maxValue: true),
            Is.EqualTo(6d).Within(0.01d));
    }

    [Test]
    public void Character_GetCurrentActionPoints()
    {
        var character = GetTestCharacter();

        character.UseActionPoints(2d);

        Assert.That(character.GetStatValue(StatType.ActionPoints, maxValue: false),
            Is.EqualTo(4d).Within(0.01d));
    }

    [Test]
    public void Character_GetMaxOrbs()
    {
        var character = GetTestCharacter();

        Assert.That(character.GetStatValue(StatType.Orbs, ElementType.Fire, maxValue: true),
            Is.EqualTo(4d).Within(0.01d));
    }

    [Test]
    public void Character_GetCurrentOrbs()
    {
        var character = GetTestCharacter();

        character.UseOrbs(1d, ElementType.Fire);

        Assert.That(character.GetStatValue(StatType.Orbs, ElementType.Fire, maxValue: false),
            Is.EqualTo(3d).Within(0.01d));
    }

    [Test]
    public void Character_GetOrbsRegeneration()
    {
        var character = GetTestCharacter();

        Assert.That(character.GetStatValue(StatType.OrbsRegen, ElementType.Fire),
            Is.EqualTo(1d).Within(0.01d));
    }

    [Test]
    public void Character_GetResistance()
    {
        var character = GetTestCharacter();

        Assert.That(character.GetStatValue(StatType.Resistance, ElementType.Fire),
            Is.EqualTo(0.3d).Within(0.01d));
    }

    [Test]
    public void Character_GetSpellPower()
    {
        var character = GetTestCharacter();

        Assert.That(character.GetStatValue(StatType.SpellPower, ElementType.Fire),
            Is.EqualTo(20d).Within(0.01d));
    }
}