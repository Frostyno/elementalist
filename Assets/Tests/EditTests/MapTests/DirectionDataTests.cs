﻿using Elementalist.Map;
using NUnit.Framework;

public class DirectionDataTests
{
    [Test]
    public void Enumerable_NonClassType()
    {
        DirectionData<int> data = new DirectionData<int>();
        data.SetValue(5, Direction.North);
        data.SetValue(3, Direction.West);
        data.SetValue(2, Direction.East);

        int iteratedValues = 0;
        int sum = 0;

        foreach (var dirData in data)
        {
            iteratedValues++;
            sum += dirData;
        }

        Assert.That(iteratedValues == 4, "Invalid number of iterated values.");
        Assert.That(sum == 10, "Wrong sum of iterated values.");
    }

    [Test]
    public void Enumerable_ClassType()
    {
        DirectionData<object> data = new DirectionData<object>();

        data.SetValue(new object(), Direction.South);
        data.SetValue(new object(), Direction.East);

        int iteratedValues = 0;

        foreach (var dirData in data)
        {
            iteratedValues++;
        }

        Assert.That(iteratedValues == 2);
    }

    [Test]
    public void HasValue_NonClassType()
    {
        DirectionData<int> data = new DirectionData<int>();
        data.SetValue(1, Direction.North);

        Assert.That(data.HasValue(Direction.North) && data.HasValue(Direction.South));
    }

    [Test]
    public void HasValue_ClassType()
    {
        DirectionData<object> data = new DirectionData<object>();
        data.SetValue(new object(), Direction.North);

        Assert.That(data.HasValue(Direction.North) && !data.HasValue(Direction.South));
    }
}

