﻿using Elementalist.Items;
using NUnit.Framework;
using UnityEngine;

public class ItemStackTests
{
    [Test]
    public void ItemStack_SetItem()
    {
        ItemType item = Resources.Load<ItemType>("Tests/Items/Item");
        ItemStack stack = new ItemStack();

        bool e = false;

        stack.OnItemTypeChanged += () => e = true;

        stack.ItemType = item;

        Assert.True(e);
        Assert.That(stack.ItemType == item);
    }

    [Test]
    public void ItemStack_AddItemsToStack()
    {
        ItemType item = Resources.Load<ItemType>("Tests/Items/Item");
        ItemStack stack = new ItemStack();

        var amount = 0;
        stack.OnAmountChanged += () => amount = stack.Amount;

        stack.ItemType = item;
        var overflow = stack.AddItemsToStack(3);
        
        Assert.That(stack.Amount == 3, "Incorrect amount of items in stack.");
        Assert.That(overflow == 0, "Incorrect number of not added items returned.");
        Assert.That(amount == 3, "OnAmountChanged event wasn't fired.");
    }

    [Test]
    public void ItemStack_AddItemsToStack_Overflow()
    {
        ItemType item = Resources.Load<ItemType>("Tests/Items/Item");
        ItemStack stack = new ItemStack();

        stack.ItemType = item;
        var overflow = stack.AddItemsToStack(5);

        Assert.That(stack.Amount == 3);
        Assert.That(overflow == 2);
    }

    [Test]
    public void ItemStack_AddItemsToStack_Infinite()
    {
        ItemType item = Resources.Load<ItemType>("Tests/Items/Item_infinite");
        ItemStack stack = new ItemStack();

        stack.ItemType = item;
        var overflow = stack.AddItemsToStack(10000);

        Assert.That(stack.Amount == 10000);
        Assert.That(overflow == 0);
    }

    [Test]
    public void ItemStack_RemoveItemsFromStack()
    {
        ItemType item = Resources.Load<ItemType>("Tests/Items/Item");
        ItemStack stack = new ItemStack();

        stack.ItemType = item;
        stack.AddItemsToStack(3);

        int amount = 0;
        stack.OnAmountChanged += () => amount = stack.Amount;

        var notRemoved = stack.RemoveItemsFromStack(2);

        Assert.That(stack.ItemType = item, "Item was removed from the stack.");
        Assert.That(stack.Amount == 1, "Incorrect number of items left in stack.");
        Assert.That(notRemoved == 0, "Incorrrect number of not removed items returned.");
        Assert.That(amount == 1, "OnAmountChanged event wasn't fired.");
    }

    [Test]
    public void ItemStack_RemoveItemsFromStack_All()
    {
        ItemType item = Resources.Load<ItemType>("Tests/Items/Item");
        ItemStack stack = new ItemStack();

        stack.ItemType = item;
        stack.AddItemsToStack(3);

        var notRemoved = stack.RemoveItemsFromStack(3);

        Assert.Null(stack.ItemType, "Item wasn't removed from the stack.");
        Assert.That(stack.Amount == 0, "Incorrect number of items left in stack.");
        Assert.That(notRemoved == 0, "Incorrrect number of not removed items returned.");
    }

    [Test]
    public void ItemStack_RemoveItemsFromStack_Overflow()
    {
        ItemType item = Resources.Load<ItemType>("Tests/Items/Item");
        ItemStack stack = new ItemStack();

        stack.ItemType = item;
        stack.AddItemsToStack(3);

        var notRemoved = stack.RemoveItemsFromStack(5);

        Assert.Null(stack.ItemType, "Item wasn't removed from the stack.");
        Assert.That(stack.Amount == 0, "Incorrect number of items left in stack.");
        Assert.That(notRemoved == 2, "Incorrrect number of not removed items returned.");
    }
}

