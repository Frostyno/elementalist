﻿using System.Collections;
using System.Collections.Generic;
using Elementalist.Character.Stats;
using Elementalist.Combat.Actions;
using Elementalist.Items;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class InventoryTests
{
    [Test]
    public void Inventory_AddItem()
    {
        Inventory inventory = GameObject.Instantiate<Inventory>(
            Resources.Load<Inventory>("Tests/Items/Inventory"));
        inventory.Initialize();

        Item? item = null;
        inventory.OnItemCollected += i => item = i;

        ItemType itemType = Resources.Load<ItemType>("Tests/Items/Item");

        var overflow = inventory.AddItem(itemType, 5);

        Assert.That(inventory.GetItemAmount(itemType.Item) == 5, "Incorrect number of items added to inventory.");
        Assert.That(overflow == 0, "Incorrect amount of items not added to inventory.");
        Assert.NotNull(item, "OnItemCollected event wasn't fired.");
    }

    [Test]
    public void Inventory_AddItem_Overflow()
    {
        Inventory inventory = GameObject.Instantiate<Inventory>(
            Resources.Load<Inventory>("Tests/Items/Inventory"));
        inventory.Initialize();

        Item? item = null;
        inventory.OnItemCollected += i => item = i;

        ItemType itemType = Resources.Load<ItemType>("Tests/Items/Item");

        var overflow = inventory.AddItem(itemType, 7);

        Assert.That(inventory.GetItemAmount(itemType.Item) == 6, "Incorrect number of items added to inventory.");
        Assert.That(overflow == 1, "Incorrect amount of items not added to inventory.");
        Assert.NotNull(item, "OnItemCollected event wasn't fired.");
    }

    [Test]
    public void Inventory_AddItem_Infinite()
    {
        Inventory inventory = GameObject.Instantiate<Inventory>(
            Resources.Load<Inventory>("Tests/Items/Inventory"));
        inventory.Initialize();

        Item? item = null;
        inventory.OnItemCollected += i => item = i;

        ItemType itemType = Resources.Load<ItemType>("Tests/Items/Item_infinite");

        var overflow = inventory.AddItem(itemType, 10000);

        Assert.That(inventory.GetItemAmount(itemType.Item) == 10000, "Incorrect number of items added to inventory.");
        Assert.That(overflow == 0, "Incorrect amount of items not added to inventory.");
        Assert.NotNull(item, "OnItemCollected event wasn't fired.");
    }
    
    [Test]
    public void Inventory_RemoveItem()
    {
        Inventory inventory = GameObject.Instantiate<Inventory>(
            Resources.Load<Inventory>("Tests/Items/Inventory"));
        inventory.Initialize();

        Item? item = null;
        inventory.OnItemRemoved += i => item = i;

        ItemType itemType = Resources.Load<ItemType>("Tests/Items/Item");

        var overflow = inventory.AddItem(itemType, 5);

        var removed = inventory.RemoveItem(itemType, 4);

        Assert.True(removed, "Items weren't removed from the inventory.");
        Assert.That(inventory.GetItemAmount(itemType.Item) == 1, "Incorrect number of items was removed.");
        Assert.NotNull(item, "OnItemRemoved event wasn't fired.");
    }

    [Test]
    public void Inventory_RemoveItem_NotEnoughItems()
    {
        Inventory inventory = GameObject.Instantiate<Inventory>(
            Resources.Load<Inventory>("Tests/Items/Inventory"));
        inventory.Initialize();

        Item? item = null;
        inventory.OnItemRemoved += i => item = i;

        ItemType itemType = Resources.Load<ItemType>("Tests/Items/Item");

        var overflow = inventory.AddItem(itemType, 5);

        var removed = inventory.RemoveItem(itemType, 6);

        Assert.False(removed, "Items weren't removed from the inventory.");
        Assert.That(inventory.GetItemAmount(itemType.Item) == 5, "Some items were removed.");
        Assert.Null(item, "OnItemRemoved event was fired.");
    }
    
    [Test]
    public void Inventory_GetItemAmount()
    {
        Inventory inventory = GameObject.Instantiate<Inventory>(
            Resources.Load<Inventory>("Tests/Items/Inventory"));
        inventory.Initialize();

        ItemType item = Resources.Load<ItemType>("Tests/Items/Item");

        var overflow = inventory.AddItem(item, 5);
        
        Assert.That(inventory.GetItemAmount(item.Item) == 5, "Some items were removed.");
    }

    [Test]
    public void Inventory_GetFreeSpace()
    {
        Inventory inventory = GameObject.Instantiate<Inventory>(
            Resources.Load<Inventory>("Tests/Items/Inventory"));
        inventory.Initialize();

        ItemType item = Resources.Load<ItemType>("Tests/Items/Item");

        Assert.That(inventory.GetFreeSpace(item) == 6);

        var overflow = inventory.AddItem(item, 5);

        Assert.That(inventory.GetFreeSpace(item) == 1);
    }

    [Test]
    public void Inventory_GetFreeSpace_Infinite()
    {
        Inventory inventory = GameObject.Instantiate<Inventory>(
            Resources.Load<Inventory>("Tests/Items/Inventory"));
        inventory.Initialize();

        ItemType item = Resources.Load<ItemType>("Tests/Items/Item_infinite");

        Assert.That(inventory.GetFreeSpace(item) < 0);

        var overflow = inventory.AddItem(item, 10000);

        Assert.That(inventory.GetFreeSpace(item) < 0);
    }

    [Test]
    public void Inventory_AddItem_ProviderEvents()
    {
        Inventory inventory = GameObject.Instantiate<Inventory>(
            Resources.Load<Inventory>("Tests/Items/Inventory"));
        inventory.Initialize();

        StatModifier[] mods = null;
        SpellType[] spells = null;

        inventory.OnStatModifiersChanged += () => mods = inventory.GetStatModifiers();
        inventory.OnSpellsChanged += () => spells = inventory.GetSpells();

        ItemType trinket = Resources.Load<ItemType>("Tests/Items/Trinket");

        inventory.AddItem(trinket, 1);

        Assert.NotNull(mods, "OnStatModifiersChanged event didn't fire.");
        Assert.That(mods.Length == 1, "Incorrect number of mods returned.");
        Assert.NotNull(spells, "OnSpellsChanged event didn't fire.");
        Assert.That(spells.Length == 1, "Incorrect number of spells returned.");
    }

    [Test]
    public void Inventory_RemoveItem_ProviderEvents()
    {
        Inventory inventory = GameObject.Instantiate<Inventory>(
            Resources.Load<Inventory>("Tests/Items/Inventory"));
        inventory.Initialize();

        ItemType trinket = Resources.Load<ItemType>("Tests/Items/Trinket");

        inventory.AddItem(trinket, 1);

        StatModifier[] mods = null;
        SpellType[] spells = null;

        inventory.OnStatModifiersChanged += () => mods = inventory.GetStatModifiers();
        inventory.OnSpellsChanged += () => spells = inventory.GetSpells();

        inventory.RemoveItem(trinket, 1);

        Assert.NotNull(mods, "OnStatModifiersChanged event didn't fire.");
        Assert.That(mods.Length == 0, "Incorrect number of mods returned.");
        Assert.NotNull(spells, "OnSpellsChanged event didn't fire.");
        Assert.That(spells.Length == 0, "Incorrect number of spells returned.");
    }
}
