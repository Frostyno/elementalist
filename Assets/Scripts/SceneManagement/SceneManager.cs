﻿using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using Elementalist.Utility;

namespace Elementalist.SceneManagement
{
    public static class SceneManager
    {
        private static CoroutineSlave _coroutineSlave = null;

        public static async Task LoadScene(Scenes scene)
        {
            if (_coroutineSlave == null) _coroutineSlave = new GameObject().AddComponent<CoroutineSlave>();

            TaskCompletionSource<bool> sceneLoader = new TaskCompletionSource<bool>();

            string sceneName = "";
            switch (scene)
            {
                case Scenes.CombatScene:
                    sceneName = "CombatScene";
                    break;
                case Scenes.MapScene:
                    sceneName = "MapScene";
                    break;
                default:
                    Debug.LogError($"Scene {scene} is not supported.");
                    break;
            }

            if (string.IsNullOrEmpty(sceneName)) return;
            
            _coroutineSlave.StartCoroutine(LoadSceneAsync(sceneName, LoadSceneMode.Additive, sceneLoader));

            await sceneLoader.Task;
        }

        public static async Task UnloadScene(Scenes scene)
        {
            if (_coroutineSlave == null) _coroutineSlave = new GameObject().AddComponent<CoroutineSlave>();

            TaskCompletionSource<bool> sceneUnloader = new TaskCompletionSource<bool>();

            string sceneName = "";
            switch (scene)
            {
                case Scenes.CombatScene:
                    sceneName = "CombatScene";
                    break;
                case Scenes.MapScene:
                    sceneName = "MapScene";
                    break;
                default:
                    Debug.LogError($"Scene {scene} is not supported.");
                    break;
            }

            if (string.IsNullOrEmpty(sceneName)) return;

            _coroutineSlave.StartCoroutine(UnloadSceneAsync(sceneName, sceneUnloader));

            await sceneUnloader.Task;
        }

        private static IEnumerator LoadSceneAsync(string sceneName, LoadSceneMode loadMode, TaskCompletionSource<bool> tcs)
        {
            var loadingOp = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName, loadMode);

            while (!loadingOp.isDone)
                yield return null;

            tcs.SetResult(true);
        }

        private static IEnumerator UnloadSceneAsync(string sceneName, TaskCompletionSource<bool> tcs)
        {
            var unloadingOp = UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(sceneName);

            while (!unloadingOp.isDone)
                yield return null;

            tcs.SetResult(true);
        }
    }
}