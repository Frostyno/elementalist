﻿namespace Elementalist.SceneManagement
{
    public enum Scenes
    {
        CombatScene,
        MapScene,
        CityScene,
        BaseScene,
        MenuScene
    }
}