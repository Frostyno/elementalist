﻿using Elementalist.Saving;
using UnityEngine;
using Zenject;

namespace Elementalist
{
    public class ApplicationManager : MonoBehaviour
    {
        public void QuitApplication()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}
