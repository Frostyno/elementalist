﻿using Elementalist.Utility;
using System;
using UnityEngine;
using Zenject;

namespace Elementalist.Map
{
    [RequireComponent(typeof(Collider2D))]
    public class DungeonRoomSlot : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _backgroundImage = null;
        [SerializeField] private SpriteRenderer _visitableImage = null;
        [SerializeField] private SpriteRenderer _unexploredImage = null;

        [Space]

        [SerializeField] private bool _randomBackgroundRotation = true;

        private PlayerDungeonMover _mover = null;

        public DungeonRoom Room { get; private set; }

        public event Action<DungeonRoom> OnRoomTargeted = null;

        [Inject]
        private void Inject(PlayerDungeonMover mover)
        {
            _mover = mover;
            _mover.OnRoomEntered += OnRoomEntered;
            _mover.OnRoomLeft += OnRoomLeft;
        }

        public void SetRoom(DungeonRoom room)
        {
            if (room == null)
            {
                Debug.LogError("Trying to set null room.");
                return;
            }

            Room = room;
            transform.position = new Vector2(room.X * GlobalVariables.DISTANCE_SCALE, room.Y * GlobalVariables.DISTANCE_SCALE);
            Room.OnVisitableChanged += OnRoomVisitableChanged;
            Room.OnExplored += OnRoomExplored;

            if (_randomBackgroundRotation)
                _backgroundImage.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Round(UnityEngine.Random.Range(0f, 3f)) * 90f);
        }

        private void OnMouseDown()
        {
            if (!Room.Visitable) return;
            if (Room == null)
            {
                Debug.LogError("Room is null when trying to visit room.");
                return;
            }

            OnRoomTargeted?.Invoke(Room);
        }

        private void OnRoomExplored(DungeonRoom room)
        {
            if (room != Room) return;
            if (!_unexploredImage.gameObject.activeSelf) return;

            _unexploredImage.gameObject.SetActive(false);
        }

        private void OnRoomVisitableChanged(bool state)
        {
            if (_visitableImage == null) return;
            _visitableImage.gameObject.SetActive(state);
        }

        private void OnRoomEntered(DungeonRoom room)
        {
            if (Room == room)
            {
                foreach (var r in Room.NeighbourRooms)
                    r.SetVisitable(true);
            }
        }

        private void OnRoomLeft(DungeonRoom room)
        {
            if (Room == room)
            {
                foreach (var r in Room.NeighbourRooms)
                    r.SetVisitable(false);
            }
        }

        private void OnDestroy()
        {
            if (Room == null) return;

            Room.OnVisitableChanged -= OnRoomVisitableChanged;
            Room.OnExplored -= OnRoomExplored;
            _mover.OnRoomEntered -= OnRoomEntered;
            _mover.OnRoomLeft -= OnRoomLeft;
        }

        public class DungeonRoomSlotFactory : PlaceholderFactory<DungeonRoomSlot> { }
    }
}
