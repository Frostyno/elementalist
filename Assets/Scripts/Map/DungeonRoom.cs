﻿using Elementalist.Map.Events;
using System;
using System.Threading.Tasks;
using UnityEngine;

namespace Elementalist.Map
{
    [Serializable]
    public class DungeonRoom
    {
        public int ID { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }
        public uint DistanceFromEnterance { get; set; }

        public bool Explored { get; private set; }
        public bool Visitable { get; private set; }
        
        public DirectionData<DungeonRoom> NeighbourRooms { get; private set; }

        private IDungeonEvent _roomEvent = null;

        public event Action<bool> OnVisitableChanged = null;
        public event Action<DungeonRoom> OnExplored = null;

        public DungeonRoom(int id, int x, int y, uint distanceFromEnterance = uint.MaxValue)
        {
            ID = id;
            X = x;
            Y = y;
            DistanceFromEnterance = distanceFromEnterance;
            Explored = false;
            Visitable = false;
            NeighbourRooms = new DirectionData<DungeonRoom>();
        }

        public bool SetRoomEvent(IDungeonEvent dungeonEvent)
        {
            if (_roomEvent != null) return false;

            _roomEvent = dungeonEvent;
            return true;
        }

        public void SetVisitable(bool visitable)
        {
            Visitable = visitable;
            OnVisitableChanged?.Invoke(Visitable);
        }

        public async Task VisitRoom()
        {
            if (_roomEvent == null)
            {
                Debug.LogWarning($"Room dungeon event is null when visiging room #{ID}");
                return;
            }
            
            await _roomEvent.Execute();

            if (!Explored)
            {
                Explored = true;
                OnExplored?.Invoke(this);
            }
        }
    }
}
