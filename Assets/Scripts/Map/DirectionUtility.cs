﻿namespace Elementalist.Map
{
    public static class DirectionUtility
    {
        public static Direction GetOppositeDirection(Direction dir)
        {
            switch (dir)
            {
                case Direction.North: return Direction.South;
                case Direction.South: return Direction.North;
                case Direction.East: return Direction.West;
                case Direction.West:
                default: return Direction.East;
            }
        }

        public static (int, int) GetDirectionVector(Direction dir)
        {
            switch (dir)
            {
                case Direction.North: return (0, 1);
                case Direction.South: return (0, -1);
                case Direction.East: return (1, 0);
                case Direction.West: return (-1, 0);
            }

            return (0, 0);
        }
    }
}