﻿using Elementalist.Map.Generation;
using Elementalist.Saving;
using Elementalist.Utility;
using System;
using UnityEngine;
using Zenject;

namespace Elementalist.Map
{
    public class PlayerDungeonMover : MonoBehaviour
    {
        private DungeonRoom _currentRoom = null;

        private DungeonGenerator _dungeonGenerator = null;
        private Character.Character _player = null;
        private PlayerMapMarker _playerMarker = null;
        private SaveManager _saveManager = null;

        private bool _isVisitingRoom = false;

        public event Action<DungeonRoom> OnRoomEntered = null;
        public event Action<DungeonRoom> OnRoomLeft = null;

        [Inject]
        private void Inject(
            DungeonGenerator generator,
            Character.Character player,
            PlayerMapMarker playerMarker,
            SaveManager saveManager)
        {
            _player = player;
            _dungeonGenerator = generator;
            _dungeonGenerator.OnDungeonGenerated += OnDungeonGenerated;
            _playerMarker = playerMarker;
            _saveManager = saveManager;
        }

        public void EnterDungeon(DungeonRoom enterance)
        {
            if (enterance == null)
            {
                Debug.LogError("Can't enter null room.");
                return;
            }
            if (_currentRoom != null)
            {
                Debug.LogError($"Dungeon is already entered in room #{_currentRoom.ID}.");
                return;
            }

            // TODO: think about how this could be done better
            var slots = FindObjectsOfType<DungeonRoomSlot>();
            foreach (var slot in slots)
                slot.OnRoomTargeted += OnRoomTargeted;

            MoveToRoom(enterance);
        }

        private async void MoveToRoom(DungeonRoom room)
        {
            if (!CanMoveToRoom(room))
            {
                Debug.LogError($"Can't move to room #{room.ID} from room #{_currentRoom.ID}.");
                return;
            }
            if (_isVisitingRoom) return;

            if (_currentRoom != null) OnRoomLeft.Invoke(_currentRoom);

            _isVisitingRoom = true;

            await _playerMarker.MoveToPosition(new Vector2(room.X * GlobalVariables.DISTANCE_SCALE, room.Y * GlobalVariables.DISTANCE_SCALE));

            _currentRoom = room;
            OnRoomEntered.Invoke(_currentRoom);

            await _currentRoom.VisitRoom();

            _saveManager.SaveGameState();
            _isVisitingRoom = false;
        }

        private bool CanMoveToRoom(DungeonRoom room)
        {
            if (_currentRoom == null) return true;

            var isNeighbour = false;
            foreach (var r in _currentRoom.NeighbourRooms)
            {
                if (room == r)
                {
                    isNeighbour = true;
                    break;
                }
            }

            return isNeighbour;
        }

        private void OnRoomTargeted(DungeonRoom room)
        {
            if (room == null)
            {
                Debug.LogError("Targeted room is null.");
                return;
            }

            MoveToRoom(room);
        }

        private void OnDungeonGenerated(DungeonRoom enterance)
        {
            EnterDungeon(enterance);
        }

        private void OnDestroy()
        {
            if (_dungeonGenerator == null) return;
            _dungeonGenerator.OnDungeonGenerated -= OnDungeonGenerated;
        }
    }
}
