﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Map
{
    [System.Serializable]
    public class DirectionDataDouble : DirectionData<double> { }

    [System.Serializable]
    public class DirectionData<T> : IEnumerable<T>
    {
        [SerializeField] private T _north = default;
        [SerializeField] private T _south = default;
        [SerializeField] private T _east = default;
        [SerializeField] private T _west = default;
        
        public void SetValue(T value, Direction dir)
        {
            switch (dir)
            {
                case Direction.North:
                    _north = value;
                    break;
                case Direction.South:
                    _south = value;
                    break;
                case Direction.East:
                    _east = value;
                    break;
                case Direction.West:
                    _west = value;
                    break;
            }
        }

        public T GetValue(Direction dir)
        {
            switch (dir)
            {
                case Direction.North: return _north;
                case Direction.South: return _south;
                case Direction.East: return _east;
                case Direction.West: return _west;
            }

            return default;
        }

        public bool HasValue(Direction dir)
        {
            if (typeof(T).IsClass) return GetValue(dir) != null;
            return true;
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (typeof(T).IsClass)
            {
                if (_north != null) yield return _north;
                if (_south != null) yield return _south;
                if (_east != null) yield return _east;
                if (_west != null) yield return _west;
            }
            else
            {
                yield return _north;
                yield return _south;
                yield return _east;
                yield return _west;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}