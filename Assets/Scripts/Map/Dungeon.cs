﻿using Elementalist.Combat;
using Elementalist.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Zenject;

namespace Elementalist.Map
{
    public class Dungeon : MonoBehaviour
    {
        [SerializeField, TextArea] private string _allRoomsExploredMessage = "";
        [SerializeField, TextArea] private string _playerDiedMessage = "";

        private Character.Character _player = null;
        private MessagePopup _messagePopup = null;

        public DungeonRoom Enterance { get; set; }

        private List<DungeonRoom> _rooms = new List<DungeonRoom>();
        public ReadOnlyCollection<DungeonRoom> Rooms { get; private set; }

        public event Action<bool> OnDungeonCompleted = null;

        private void Awake()
        {
            Rooms = new ReadOnlyCollection<DungeonRoom>(_rooms);
        }

        [Inject]
        private void Inject(
            Character.Character player,
            MessagePopup messagePopup)
        {
            _player = player;
            _messagePopup = messagePopup;
        }

        public void AddRoom(DungeonRoom room)
        {
            if (_rooms.Contains(room))
            {
                Debug.LogWarning("Can't add multiple instances of the same room.");
                return;
            }

            room.OnExplored += OnRoomExplored;
            _rooms.Add(room);
        }

        public void EndExpedition(bool playerDied)
        {
            _player.RefillResources();
            OnDungeonCompleted?.Invoke(playerDied);
        }

        private void OnRoomExplored(DungeonRoom room)
        {
            CheckDungeonCompletition();
        }

        private async void CheckDungeonCompletition()
        {
            bool completed = false;
            bool playerDied = false;

            if (_player.CurrentHealth <= 0d) // check if player died
            {
                completed = true;
                playerDied = true;
                await _messagePopup.DisplayMessage(_playerDiedMessage, "Expedition failed!");
            }
            else // check if all rooms were explored
            {
                completed = true;
                foreach (var r in _rooms)
                {
                    if (!r.Explored)
                    {
                        completed = false;
                        break;
                    }
                }
                if (completed) await _messagePopup.DisplayMessage(_allRoomsExploredMessage, "Expedition completed!");
            }

            if (completed)
                EndExpedition(playerDied);
        }

        private void OnDestroy()
        {
            foreach (var room in _rooms)
            {
                if (room == null) continue;
                room.OnExplored -= OnRoomExplored;
            }
        }
    }
}
