﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Elementalist.Map
{
    public class MapMover : MonoBehaviour
    {
        public class MapBounds
        {
            public float BottomBound { get; set; }
            public float UpperBound { get; set; }
            public float LeftBound { get; set; }
            public float RightBound { get; set; }
        }

        [SerializeField] private Transform _mapPivot = null;

        [Space]

        [Range(0.5f, 5f)]
        [SerializeField] private float _moveSpeedMultiplier = 2f;
        [SerializeField] private bool _invertedMovement = true;
        
        private MapBounds _mapBounds = null;
        private Vector2 _oldMousePosition = Vector2.zero;

        private bool _dragStartedOverUI = false;
        
        public void SetBounds(MapBounds bounds)
        {
            _mapBounds = bounds;

            // Moved object is not camera but map itself
            // it is therefore moved in an opposite direction so bounds need to be inverted
            _mapBounds.UpperBound *= -1;
            _mapBounds.BottomBound *= -1;
            _mapBounds.LeftBound *= -1;
            _mapBounds.RightBound *= -1;
        }

        public void OnMouseDown()
        {
            if (_mapBounds == null)
            {
                Debug.LogError("Map bounds are not defined in map mover. Map won't be able to be moved.");
                _mapBounds = new MapBounds();
            }

            _dragStartedOverUI = EventSystem.current.IsPointerOverGameObject();
            _oldMousePosition = Input.mousePosition;
        }
        
        /// <summary>
        /// Calculates where how the map should be moved based on user drag input and moves it.
        /// </summary>
        public void OnMouseDrag()
        {
            // don't move map if pointer is over ui elements
            if (_dragStartedOverUI) return;

            Vector2 dragAmount = _oldMousePosition - (Vector2)Input.mousePosition;
            if (_invertedMovement) dragAmount *= -1;

            dragAmount *= _moveSpeedMultiplier * Time.deltaTime;
           
            Vector2 newPivotPosition = (Vector2)_mapPivot.position + dragAmount;

            // Check if new position is within bounds, if not correct it
            // NOTE: comparisons are actually inverted, because object moved is not camera but map itself, therefore it is moved in an opposite direction
            if (newPivotPosition.x > _mapBounds.LeftBound) newPivotPosition.x = _mapBounds.LeftBound;
            else if (newPivotPosition.x < _mapBounds.RightBound) newPivotPosition.x = _mapBounds.RightBound;
            else if (newPivotPosition.y < _mapBounds.UpperBound) newPivotPosition.y = _mapBounds.UpperBound;
            else if (newPivotPosition.y > _mapBounds.BottomBound) newPivotPosition.y = _mapBounds.BottomBound;

            _mapPivot.position = new Vector3(newPivotPosition.x, newPivotPosition.y, _mapPivot.position.z);

            _oldMousePosition = Input.mousePosition;
        }

        public void OnMouseUp()
        {
            _oldMousePosition = Vector2.zero;
        }
    }
}
