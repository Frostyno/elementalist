﻿namespace Elementalist.Map
{
    public enum Direction
    {
        North,
        South,
        East,
        West
    }
}