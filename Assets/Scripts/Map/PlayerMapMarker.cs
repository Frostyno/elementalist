﻿using Elementalist.Character;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Elementalist.Map
{
    /// <summary>
    /// Takes care of displaying player graphics on map and moving it around.
    /// </summary>
    public class PlayerMapMarker : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _playerIcon = null;
        [SerializeField] private float _moveSpeed = 2f;

        private Character.Character _player = null;

        private TaskCompletionSource<bool> _moveWaiter = null;

        [Inject]
        private void Inject(Character.Character player)
        {
            _player = player;
            _playerIcon.sprite = _player.CharacterIcon;
        }

        public void SetPosition(Vector2 position)
        {
            transform.localPosition = position;
        }

        public async Task MoveToPosition(Vector2 targetPosition)
        {
            if (_moveWaiter != null) return;

            _moveWaiter = new TaskCompletionSource<bool>();
            StartCoroutine(ExecuteMovement(targetPosition));

            await _moveWaiter.Task;

            _moveWaiter = null;
        }

        private IEnumerator ExecuteMovement(Vector2 targetPosition)
        {
            while (transform.localPosition != (Vector3)targetPosition)
            {
                transform.localPosition = Vector2.MoveTowards(transform.localPosition, targetPosition, Time.deltaTime * _moveSpeed);
                yield return null;
            }

            _moveWaiter.SetResult(true);
        }
    }
}
