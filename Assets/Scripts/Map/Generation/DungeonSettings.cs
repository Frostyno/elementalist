﻿using UnityEngine;
using Elementalist.Utility;
using System.Collections.ObjectModel;
using System;
using Elementalist.Items;

namespace Elementalist.Map.Generation
{
    [Serializable]
    public class RarityItemGroupGenerationArgs
    {
        [SerializeField] private ItemRarity _itemRarity = ItemRarity.Common;
        [Range(0f, 1f)]
        [SerializeField] private double _probability = 0d;
        [SerializeField] private int _itemsToGenerate = 2;
        [SerializeField] private ItemGenerationArgs[] _items = new ItemGenerationArgs[0];

        public ItemRarity ItemRarity { get => _itemRarity; }
        public double Probability { get => _probability; }
        public int ItemsToGenerate { get => _itemsToGenerate; }
        public ItemGenerationArgs[] Items { get => _items; }
    }

    [Serializable]
    public class ItemGenerationArgs
    {
        [SerializeField] private ItemType _itemType = null;
        [Range(0f, 1f)]
        [SerializeField] private double _probability = 0d;
        [Range(1, 100)]
        [SerializeField] private int _minAmount = 1;
        [Range(1, 100)]
        [SerializeField] private int _maxAmount = 1;

        public ItemType ItemType { get => _itemType; }
        public double Probability { get => _probability; }
        public int MinAmount { get => _minAmount; }
        public int MaxAmount { get => _maxAmount; }
    }

    [CreateAssetMenu(fileName = "DungeonSettings", menuName = "Elementalist/Map/DungeonSettings")]
    public class DungeonSettings : ScriptableObject
    {
        [SerializeField] private string _dungeonName = "";
        [SerializeField, TextArea] private string _dungeonDescription = "";

        [Space]
        [Header("Room settings")]

        [Range(2, 100)]
        [SerializeField] private int _roomCount = 2;
        [SerializeField] private int _enterancePositionX = 0;
        [SerializeField] private int _enterancePositionY = 0;

        [Tooltip("Specifies the probability of two adjacent room connecting.")]
        [Range(0f, 1f)]
        [SerializeField] private double _roomConnectProbability = 0.3d;
        
        [Tooltip("Specifies the probability of room spawning in specified direction. Sum of these must equal 1.")]
        [SerializeField] private DirectionDataDouble _roomSpawnProbabilities = new DirectionDataDouble();

        [Space]
        [Header("Combat settings")]

        [Range(0f, 1f)]
        [SerializeField] private double _combatEventProbability = 0.4d;
        [SerializeField] private PrefabProbabilityPair[] _enemyPrefabs = new PrefabProbabilityPair[0];

        [Space]
        [Header("Loot settings")]

        [SerializeField, Range(0f, 1f)] private double _lootEventProbability = 0.25d;
        [SerializeField] private RarityItemGroupGenerationArgs[] _itemsByRarity = new RarityItemGroupGenerationArgs[0];

        public string DungeonName { get => _dungeonName; }
        public string DungeonDescription => _dungeonDescription;
        public int RoomCount { get => _roomCount; }
        public int EnterancePositionX { get => _enterancePositionX; }
        public int EnterancePositionY { get => _enterancePositionY; }
        public double RoomConnectProbability { get => _roomConnectProbability; }
        public DirectionData<double> RoomSpawnProbabilities { get => _roomSpawnProbabilities; }
        public double CombatEventProbability { get => _combatEventProbability; }
        public ReadOnlyCollection<PrefabProbabilityPair> EnemyPrefabs { get; private set; }
        public double LootEventProbability { get => _lootEventProbability; }
        public ReadOnlyCollection<RarityItemGroupGenerationArgs> ItemsByRarity { get; private set; }

        private void OnEnable()
        {
            EnemyPrefabs = new ReadOnlyCollection<PrefabProbabilityPair>(_enemyPrefabs);
            ItemsByRarity = new ReadOnlyCollection<RarityItemGroupGenerationArgs>(_itemsByRarity);

            double sum = 0d;
            foreach (var prob in _roomSpawnProbabilities)
                sum += prob;
            if (1d - sum > GlobalVariables.EPSILON)
                Debug.LogError("The sum of room spawn probabilities must be equal to 1.");

            sum = 0d;
            foreach (var prefabProbPair in _enemyPrefabs)
                sum += prefabProbPair.Probability;
            if (1d - sum > GlobalVariables.EPSILON)
                Debug.LogError("The sum of enemy prefabs probabilities must be equal to 1.");

            foreach (var rarityGroup in _itemsByRarity)
            {
                if (rarityGroup.ItemsToGenerate < 1)
                    Debug.LogError($"Invalid number of items to generate in rarity group {rarityGroup.ItemRarity}.");

                sum = 0d;
                foreach (var itemParams in rarityGroup.Items)
                {
                    if (itemParams.ItemType.ItemRarity != rarityGroup.ItemRarity)
                        Debug.LogWarning($"Item {itemParams.ItemType.ItemName} is in incorrect rarity group {rarityGroup.ItemRarity} in {name}.");

                    sum += itemParams.Probability;
                }

                if (1d - sum > GlobalVariables.EPSILON)
                    Debug.LogError($"Sum of individual ItemType probabilities must be equal to 1 in rarity group {rarityGroup.ItemRarity}.");
            }
        }
    }
}
