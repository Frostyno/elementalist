﻿using Elementalist.Combat;
using Elementalist.Items;
using Elementalist.Map.Events;
using Elementalist.UI.Items;
using Elementalist.Utility;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Elementalist.Map.Generation
{
    public class DungeonGenerator : MonoBehaviour
    {
        #region AttributesAndProperties

        [SerializeField] private Transform _roomSlotsParent = null;
        [SerializeField] private Transform _corridorsParent = null;
        [SerializeField] private GameObject _horizontalCorridorPrefab = null;
        [SerializeField] private GameObject _verticalCorridorPrefab = null;

        private DungeonRoomSlot.DungeonRoomSlotFactory _roomSlotFactory = null;
        private Dungeon _dungeon = null;
        private CombatHandler _combatHandler = null;
        private Inventory _playerInventory = null;
        private LootPopup _lootPopup = null;
        private MapMover _mapMover = null;

        #region Generators

        private System.Random _roomSelectGenerator = new System.Random();
        private System.Random _roomDirGenerator = new System.Random();
        private System.Random _roomConnectGenerator = new System.Random();
        private System.Random _combatChanceGenerator = new System.Random();
        private System.Random _enemyPrefabChoiceGenerator = new System.Random();
        private System.Random _lootChanceGenerator = new System.Random();
        private System.Random _rarityGroupChanceGenerator = new System.Random();
        private System.Random _itemChanceGenerator = new System.Random();
        private System.Random _itemAmountGenerator = new System.Random();

        #endregion

        public event Action<DungeonRoom> OnDungeonGenerated = null;

        #endregion

        [Inject]
        private void Inject(
            DungeonRoomSlot.DungeonRoomSlotFactory roomSlotFactory,
            Dungeon dungeon,
            CombatHandler combatHandler,
            Inventory playerInventory,
            LootPopup lootPopup,
            MapMover mapMover)
        {
            _roomSlotFactory = roomSlotFactory;
            _dungeon = dungeon;
            _combatHandler = combatHandler;
            _playerInventory = playerInventory;
            _lootPopup = lootPopup;
            _mapMover = mapMover;
        }

        public void GenerateDungeon(DungeonSettings settings)
        {
            GenerateRooms(settings);
            GenerateEvents(settings);

            OnDungeonGenerated?.Invoke(_dungeon.Enterance);
        }

        #region RoomsGenerationFunctions

        /// <summary>
        /// Generates and connects rooms for the dungeon.
        /// </summary>
        /// <param name="settings">Settings of the dungeon.</param>
        private void GenerateRooms(DungeonSettings settings)
        {
            Dictionary<(int, int), DungeonRoom> rooms = new Dictionary<(int, int), DungeonRoom>();

            // generate enterance
            DungeonRoom enterance = new DungeonRoom(0, settings.EnterancePositionX, settings.EnterancePositionY, 0);
            rooms[(enterance.X, enterance.Y)] = enterance;
            (float bottomBound, float upperBound, float leftBound, float rightBound) = (enterance.Y, enterance.Y, enterance.X, enterance.X);
            _dungeon.Enterance = enterance;
            _dungeon.AddRoom(enterance);
            DungeonRoomSlot enteranceSlot = _roomSlotFactory.Create();
            if (_roomSlotsParent != null) enteranceSlot.transform.SetParent(_roomSlotsParent);
            enteranceSlot.SetRoom(enterance);

            int roomID = 1;
            int generatedRooms = 1;
            while (generatedRooms < settings.RoomCount)
            {
                DungeonRoom spawnPoint = _dungeon.Rooms[_roomSelectGenerator.Next(0, _dungeon.Rooms.Count)];
                (Direction spawnDirection ,int newRoomX, int newRoomY) = GetNewRoomPosition(spawnPoint, settings);

                if (rooms.ContainsKey((newRoomX, newRoomY))) // if there is already a room spawned at position of new room
                {
                    if (_roomConnectGenerator.NextDouble() < settings.RoomConnectProbability)
                        ConnectRooms(spawnPoint, rooms[(newRoomX, newRoomY)], spawnDirection);
                    continue;
                }
                else
                {
                    DungeonRoom newRoom = new DungeonRoom(roomID++, newRoomX, newRoomY);
                    ConnectRooms(spawnPoint, newRoom, spawnDirection);
                    
                    // update bounds
                    if (newRoomX < leftBound) leftBound = newRoomX;
                    if (newRoomX > rightBound) rightBound = newRoomX;
                    if (newRoomY < bottomBound) bottomBound = newRoomY;
                    if (newRoomY > upperBound) upperBound = newRoomY;

                    DungeonRoomSlot newRoomSlot = _roomSlotFactory.Create();
                    if (_roomSlotsParent != null) newRoomSlot.transform.SetParent(_roomSlotsParent);
                    newRoomSlot.SetRoom(newRoom);

                    rooms[(newRoom.X, newRoom.Y)] = newRoom;
                    _dungeon.AddRoom(newRoom);
                    generatedRooms++;
                }
            }

            _mapMover.SetBounds(new MapMover.MapBounds()
            {
                UpperBound = upperBound,
                BottomBound = bottomBound,
                LeftBound = leftBound,
                RightBound = rightBound
            });
        }

        /// <summary>
        /// Generates position of the new room together with direction from spawn point.
        /// </summary>
        /// <param name="spawnPoint">Room next to which the next room is to be spawned.</param>
        /// <param name="settings">Settings of dungeon.</param>
        /// <returns>Direction of new room from spawn point and it's position.</returns>
        private (Direction, int, int) GetNewRoomPosition(DungeonRoom spawnPoint, DungeonSettings settings)
        {
            double roomDirChoice = _roomDirGenerator.NextDouble();
            double probSum = 0;
            Direction spawnDir = Direction.North;
            foreach (Direction dir in EnumUtility.GetValues<Direction>())
            {
                probSum += settings.RoomSpawnProbabilities.GetValue(dir);
                if (roomDirChoice < probSum)
                {
                    spawnDir = dir;
                    break;
                }
            }

            (int dirX, int dirY) = DirectionUtility.GetDirectionVector(spawnDir);

            return (spawnDir, spawnPoint.X + dirX, spawnPoint.Y + dirY);
        }

        /// <summary>
        /// Connects two adjacent rooms.
        /// </summary>
        /// <param name="firstRoom">First of the rooms to connect.</param>
        /// <param name="secondRoom">Second of the rooms to connect.</param>
        /// <param name="dirFromFirstRoom"></param>
        private void ConnectRooms(DungeonRoom firstRoom, DungeonRoom secondRoom, Direction dirFromFirstRoom)
        {
            if (Math.Abs(firstRoom.X - secondRoom.X) > 1 || Math.Abs(firstRoom.Y - secondRoom.Y) > 1)
            {
                Debug.LogError($"Can't connect rooms #{firstRoom.ID} and #{secondRoom.ID}. They are not adjacent");
                return;
            }

            // Check if there isn't already a room in given direction
            if (firstRoom.NeighbourRooms.GetValue(dirFromFirstRoom) != null) return;
            
            // Check if rooms are not connected already
            // TODO: probably remove this
            foreach (var neighbour in firstRoom.NeighbourRooms)
                if (neighbour == secondRoom) return;

            firstRoom.NeighbourRooms.SetValue(secondRoom, dirFromFirstRoom);
            secondRoom.NeighbourRooms.SetValue(firstRoom, DirectionUtility.GetOppositeDirection(dirFromFirstRoom));

            // create corridor graphics
            GameObject corridor = null;
            if (firstRoom.X == secondRoom.X) // vertical
            {
                corridor = Instantiate(_verticalCorridorPrefab, _corridorsParent);
                corridor.transform.position = new Vector3(firstRoom.X * GlobalVariables.DISTANCE_SCALE, 
                    (firstRoom.Y + secondRoom.Y) * GlobalVariables.DISTANCE_SCALE / 2, 
                    corridor.transform.position.z);
            }
            else if (firstRoom.Y == secondRoom.Y) // horizontal
            {
                corridor = Instantiate(_horizontalCorridorPrefab, _corridorsParent);
                corridor.transform.position = new Vector3((firstRoom.X + secondRoom.X) * GlobalVariables.DISTANCE_SCALE / 2,
                    firstRoom.Y * GlobalVariables.DISTANCE_SCALE, 
                    corridor.transform.position.z);
            }
            else
                Debug.LogError("Rooms cannot bee diagonal.");
            if (corridor != null) corridor.transform.localScale = new Vector3(GlobalVariables.DISTANCE_SCALE, GlobalVariables.DISTANCE_SCALE);

            // set new distance from enterance
            if (firstRoom.DistanceFromEnterance < secondRoom.DistanceFromEnterance)
                secondRoom.DistanceFromEnterance = firstRoom.DistanceFromEnterance + 1;
            else
                firstRoom.DistanceFromEnterance = secondRoom.DistanceFromEnterance + 1;
        }

        #endregion

        #region EventsGenerationFunctions

        private void GenerateEvents(DungeonSettings settings)
        {
            foreach (var room in _dungeon.Rooms)
            {
                if (room == _dungeon.Enterance)
                {
                    room.SetRoomEvent(new VisitEvent());
                    continue;
                }

                IDungeonEvent roomEvent = new VisitEvent();

                roomEvent = GenerateLootEvent(roomEvent, settings);
                roomEvent = GenerateCombatEvent(roomEvent, settings);

                room.SetRoomEvent(roomEvent);
            }
        }

        private IDungeonEvent GenerateLootEvent(IDungeonEvent roomEvent, DungeonSettings settings)
        {
            if (_lootChanceGenerator.NextDouble() >= settings.LootEventProbability)
                return roomEvent;

            List<ItemTypeAmountPair> items = new List<ItemTypeAmountPair>();

            foreach (var rarityGroup in settings.ItemsByRarity)
            {
                if (_rarityGroupChanceGenerator.NextDouble() >= rarityGroup.Probability) continue;

                for (int i = 0; i < rarityGroup.ItemsToGenerate; i++)
                {
                    double probsSum = 0d;
                    double itemProb = _itemChanceGenerator.NextDouble();
                    foreach (var args in rarityGroup.Items)
                    {
                        probsSum += args.Probability;
                        if (itemProb >= probsSum) continue;

                        var pair = new ItemTypeAmountPair()
                        {
                            ItemType = args.ItemType,
                            Amount = _itemAmountGenerator.Next(args.MinAmount, args.MaxAmount + 1)
                        };
                        items.Add(pair);
                    }
                }
            }

            return new LootEvent(roomEvent, _playerInventory, _lootPopup, items.ToArray());
        }

        /// <summary>
        /// First checks if combat event should be generated.
        /// After that chooses enemy to put in the combat event and creates it.
        /// </summary>
        /// <param name="roomEvent">Event that the newly generated combat event should decorate.</param>
        /// <param name="settings">Settings of dungeon.</param>
        /// <returns>Either passed event or generated combat event.</returns>
        private IDungeonEvent GenerateCombatEvent(IDungeonEvent roomEvent, DungeonSettings settings)
        {
            // check if combat event should be generated
            if (_combatChanceGenerator.NextDouble() >= settings.CombatEventProbability)
                return roomEvent;

            CombatParameters combatParams = new CombatParameters(1, 1);

            double enemyChoice = _enemyPrefabChoiceGenerator.NextDouble();
            double probSum = 0d;
            foreach (var prefabPair in settings.EnemyPrefabs)
            {
                probSum += prefabPair.Probability;
                if (enemyChoice < probSum)
                {
                    combatParams.AddEnemyPrefab(prefabPair.Item);
                    break;
                }
            }

            return new CombatEvent(roomEvent, _combatHandler, combatParams);
        }

        #endregion
    }
}
