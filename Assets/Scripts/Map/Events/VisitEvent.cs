﻿using System.Threading.Tasks;

namespace Elementalist.Map.Events
{
    public class VisitEvent : IDungeonEvent
    {
        public Task Execute()
        {
            return Task.CompletedTask;
        }
    }
}
