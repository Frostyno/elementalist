﻿using Elementalist.Items;
using Elementalist.UI.Items;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Elementalist.Map.Events
{
    public class LootEvent : DungeonEventDecorator
    {
        private bool _executed = false;

        private Inventory _playerInventory = null;
        private ItemTypeAmountPair[] _items = null;

        private LootPopup _lootPopup = null;

        public LootEvent(
            IDungeonEvent decoratedEvent,
            Inventory playerInventory,
            LootPopup lootPopup,
            ItemTypeAmountPair[] items) : base(decoratedEvent)
        {
            _playerInventory = playerInventory;
            _lootPopup = lootPopup;
            _items = items;
        }

        public override async Task Execute()
        {
            await base.Execute();

            if (_executed) return;

            if (_items == null || _playerInventory == null)
            {
                Debug.LogError($"Items or player inventory missing in LootEvent. Cannot execute.");
                return;
            }

            await _lootPopup.DisplayPopup(_items);

            _executed = true;
        }
    }
}
