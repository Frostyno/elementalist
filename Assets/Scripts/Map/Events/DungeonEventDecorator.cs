﻿using System.Threading.Tasks;

namespace Elementalist.Map.Events
{
    public class DungeonEventDecorator : IDungeonEvent
    {
        private IDungeonEvent _decoratedEvent = null;

        public DungeonEventDecorator(IDungeonEvent decoratedEvent)
        {
            _decoratedEvent = decoratedEvent;
        }

        public virtual async Task Execute()
        {
            await _decoratedEvent.Execute();
        }
    }
}
