﻿using Elementalist.Combat;
using System.Threading.Tasks;

namespace Elementalist.Map.Events
{
    public class CombatEvent : DungeonEventDecorator
    {
        private CombatHandler _combatHandler = null;
        private CombatParameters _combatParams = null;

        private bool _executed = false;

        public CombatEvent(
            IDungeonEvent decoratedEvent,
            CombatHandler combatHandler, 
            CombatParameters combatParams)
            : base(decoratedEvent)
        {
            _combatHandler = combatHandler;
            _combatParams = combatParams;
        }

        public override async Task Execute()
        {
            if (!_executed)
            {
                var winnerSide = await _combatHandler.HandleCombat(_combatParams);

                // TODO: give out rewards here
                _executed = true;

                if (winnerSide != _combatParams.PlayerCombatSide) return;
            }

            await base.Execute();
        }
    }
}
