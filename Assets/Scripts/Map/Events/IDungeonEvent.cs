﻿using System.Threading.Tasks;

namespace Elementalist.Map.Events
{
    public interface IDungeonEvent
    {
        Task Execute();
    }
}
