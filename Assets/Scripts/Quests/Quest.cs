﻿using System;
using System.Collections.ObjectModel;

namespace Elementalist.Quests
{
    public class Quest
    {
        [Serializable]
        public class QuestSaveData
        {
            public string QuestGUID { get; set; }
            public string QuestName { get; set; }
            public string Description { get; set; }
            public QuestGoalSaveData[] Goals { get; set; }
        }

        private IQuestGoal[] _goals;
        public ReadOnlyCollection<IQuestGoal> Goals { get; private set; }

        public string QuestName { get; private set; }
        public string Description { get; private set; }
        public bool Completed { get; private set; }
        public bool Active { get; set; }

        public QuestPrototype QuestPrototype { get; private set; }
        
        public event Action<Quest> OnQuestUpdated = null;

        public Quest(IQuestGoal[] goals, string questName, string description = null, QuestPrototype prototype = null)
        {
            QuestName = questName;
            Description = description;
            Completed = false;
            QuestPrototype = prototype;
            _goals = goals;
            Goals = new ReadOnlyCollection<IQuestGoal>(_goals);

            foreach (var goal in _goals)
            {
                goal.OnGoalUpdated += OnGoalUpdated;
            }
        }

        private void OnGoalUpdated(IQuestGoal goal)
        {
            Completed = true;
            foreach (var g in _goals)
            {
                if (!g.Completed)
                {
                    Completed = false;
                    break;
                }
            }

            OnQuestUpdated?.Invoke(this);
        }

        public QuestSaveData GetState()
        {
            QuestSaveData saveData = new QuestSaveData()
            {
                QuestGUID = QuestPrototype.QuestGUID,
                QuestName = QuestName,
                Description = Description,
                Goals = new QuestGoalSaveData[_goals.Length]
            };

            for (int i = 0; i < _goals.Length; i++) saveData.Goals[i] = _goals[i].GetState();

            return saveData;
        }
    }
}
