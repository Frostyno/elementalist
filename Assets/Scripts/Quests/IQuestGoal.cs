﻿using System;

namespace Elementalist.Quests
{
    public interface IQuestGoal
    {
        event Action<IQuestGoal> OnGoalUpdated;
        bool Completed { get; }
        string GetFormattedDescription();
        QuestGoalSaveData GetState();
    }
}