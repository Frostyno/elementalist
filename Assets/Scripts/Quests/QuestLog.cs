﻿using Elementalist.Saving;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Zenject;

namespace Elementalist.Quests
{
    public class QuestLog : MonoBehaviour, ISavable
    {
        [Serializable]
        private class QuestLogSaveData
        {
            public Quest.QuestSaveData[] Quests { get; set; }
        }

        [SerializeField] private int _maxQuests = 3;

        private QuestUtility _questUtility = null;

        private List<Quest> _quests = new List<Quest>();

        public ReadOnlyCollection<Quest> Quests { get; private set; }

        public event Action OnQuestAdded = null;
        public event Action OnQuestRemoved = null;

        [Inject]
        private void Inject(QuestUtility questUtility)
        {
            _questUtility = questUtility;
        }

        private void Awake()
        {
            Quests = new ReadOnlyCollection<Quest>(_quests);
        }

        private void Clear()
        {
            _quests.Clear();
        }

        public bool CanAddQuest() => _quests.Count < _maxQuests;

        public bool AddQuest(Quest quest)
        {
            if (quest == null) return false;
            if (!CanAddQuest()) return false;

            _quests.Add(quest);
            OnQuestAdded?.Invoke();
            return true;
        }

        public bool RemoveQuest(Quest quest)
        {
            bool removed = _quests.Remove(quest);

            if (removed) OnQuestRemoved?.Invoke();
            return removed;
        }

        public string GetKey() => "";

        public object GetState()
        {
            var saveData = new QuestLogSaveData() { Quests = new Quest.QuestSaveData[_quests.Count] };

            for (int i = 0; i < _quests.Count; i++) saveData.Quests[i] = _quests[i].GetState();

            return saveData;
        }

        public void LoadState(object state)
        {
            var saveData = state as QuestLogSaveData;
            if (saveData == null)
            {
                Debug.LogError($"Invalid save data object passed to QuestLog when trying to LoadState. Passed type: {state.GetType().ToString()}. Expected type: {typeof(QuestLogSaveData).ToString()}.");
                return;
            }

            Clear();

            foreach (var questSaveData in saveData.Quests)
            {
                _quests.Add(_questUtility.CreateQuestFromSaveData(questSaveData));
            }
        }
    }
}
