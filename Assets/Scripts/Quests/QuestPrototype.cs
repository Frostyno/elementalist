﻿using Elementalist.Combat;
using Elementalist.Items;
using System;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEditor;

namespace Elementalist.Quests
{
    [Serializable]
    public class KillGoalArgs
    {
        [SerializeField] private Enemy _targetEnemy = Enemy.TestingEnemy;
        [SerializeField] private int _targetCount = 1;
        [TextArea, SerializeField] private string _description = "";
        
        public Enemy TargetEnemy { get => _targetEnemy; }
        public int TargetCount { get => _targetCount; }
        public string Description { get => _description; }
    }

    [Serializable]
    public class CollectGoalArgs
    {
        [SerializeField] private Item _targetItem = Item.TestItem;
        [SerializeField] private int _targetCount = 1;
        [TextArea, SerializeField] private string _description = "";

        public Item TargetItem { get => _targetItem; }
        public int TargetCount { get => _targetCount; }
        public string Description { get => _description; }
    }

    [CreateAssetMenu(fileName = "QuestPrototype", menuName = "Elementalist/QuestPrototype")]
    public class QuestPrototype : ScriptableObject
    {
        [SerializeField] private string _questGUID = "";
        [SerializeField] private string _questName = "";
        [TextArea]
        [SerializeField] private string _questDescription = "";

        [Space]
        [Header("Goals")]

        [SerializeField] private KillGoalArgs[] _killGoals = new KillGoalArgs[0];
        [SerializeField] private CollectGoalArgs[] _collectGoals = new CollectGoalArgs[0];

        [Space]
        [Header("Rewards")]

        [SerializeField] private ItemTypeAmountPair[] _rewards = new ItemTypeAmountPair[0];

        public string QuestGUID { get => _questGUID; }
        public string QuestName { get => _questName; }
        public string QuestDescription { get => _questDescription; }

        public int GoalCount { get => _killGoals.Length + _collectGoals.Length; }

        public ReadOnlyCollection<KillGoalArgs> KillGoals = null;
        public ReadOnlyCollection<CollectGoalArgs> CollectGoals = null;

        public ReadOnlyCollection<ItemTypeAmountPair> Rewards = null;
        
        private void OnEnable()
        {
            KillGoals = new ReadOnlyCollection<KillGoalArgs>(_killGoals);
            CollectGoals = new ReadOnlyCollection<CollectGoalArgs>(_collectGoals);
            Rewards = new ReadOnlyCollection<ItemTypeAmountPair>(_rewards);

#if UNITY_EDITOR
            AssignGUID();
#endif
        }

#if UNITY_EDITOR
        private void AssignGUID()
        {
            if (!string.IsNullOrEmpty(_questGUID)) return;

            _questGUID = GUID.Generate().ToString();
        }
#endif
    }
}
