﻿using Elementalist.Items;
using Elementalist.UI.City;
using Elementalist.UI.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Zenject;

namespace Elementalist.Quests
{
    public class QuestGiver : MonoBehaviour
    {
        [SerializeField] private int _questToGenerateCount = 2;

        [Space]

        [SerializeField] private QuestPrototype[] _possibleQuests = new QuestPrototype[0];
        
        private GlobalEvents _globalEvents = null;
        private QuestLog _playerQuestLog = null;
        private Inventory _playerInventory = null;
        private LootPopup _lootPopup = null;

        private List<Quest> _generatedQuests = new List<Quest>();

        public ReadOnlyCollection<Quest> GeneratedQuests { get; private set; }

        public event Action OnGeneratedQuestsChanged = null;

        [Inject]
        private void Inject(
            GlobalEvents globalEvents,
            QuestLog playerQuestLog,
            Inventory playerInventory,
            LootPopup lootPopup)
        {
            _globalEvents = globalEvents;
            _globalEvents.OnExpeditionEnded += OnExpeditionEnded;

            _playerQuestLog = playerQuestLog;
            
            _playerInventory = playerInventory;
            _lootPopup = lootPopup;

            GeneratedQuests = new ReadOnlyCollection<Quest>(_generatedQuests);
        }

        private void Start()
        {
            GenerateQuests();
        }

        private void GenerateQuests()
        {
            _generatedQuests.Clear();

            if (_possibleQuests.Length == 0)
            {
                Debug.LogError($"No quest prototypes present in quest giver {name}.");
                return;
            }

            for (int i = 0; i < _questToGenerateCount; i++)
            {
                QuestPrototype prototype = _possibleQuests[UnityEngine.Random.Range(0, _possibleQuests.Length)];

                //  prevent duplicite quests
                bool skip = false;
                foreach (var q in _generatedQuests)
                {
                    if (q.QuestPrototype == prototype)
                    {
                        skip = true;
                        break;
                    }
                }
                if (skip) continue;

                List<IQuestGoal> goals = new List<IQuestGoal>(prototype.KillGoals.Count + prototype.GoalCount);

                foreach (var killGoal in prototype.KillGoals)
                    goals.Add(new KillGoal(_globalEvents, killGoal.TargetEnemy, killGoal.TargetCount, description: killGoal.Description));
                foreach (var collectGoal in prototype.CollectGoals)
                    goals.Add(
                        new CollectGoal(_globalEvents, collectGoal.TargetItem, collectGoal.TargetCount,
                        currentCount: _playerInventory.GetItemAmount(collectGoal.TargetItem),
                        description: collectGoal.Description)
                        );

                Quest quest = new Quest(goals.ToArray(), prototype.QuestName, prototype.QuestDescription, prototype);

                _generatedQuests.Add(quest);
            }

            OnGeneratedQuestsChanged?.Invoke();
        }

        private void GiveRewards(Quest quest)
        {
            var qPrototype = quest.QuestPrototype;
            if (qPrototype.Rewards.Count == 0) return;

            ItemTypeAmountPair[] rewards = new ItemTypeAmountPair[qPrototype.Rewards.Count];
            qPrototype.Rewards.CopyTo(rewards, 0);
            _lootPopup.DisplayPopup(rewards);
        }

        public void AddQuestToPlayer(Quest quest)
        {
            if (!_playerQuestLog.CanAddQuest())
            {
                Debug.LogWarning($"Cannot add quest {quest.QuestName} to player quest log. Player's quest log is full.");
                return;
            }
            if (!_generatedQuests.Contains(quest))
            {
                Debug.LogWarning($"Cannot add quest {quest.QuestName} to player quest log. No such quest in {name}'s generated quests.");
                return;
            }

            quest.Active = true;
            _playerQuestLog.AddQuest(quest);
            _generatedQuests.Remove(quest);
            OnGeneratedQuestsChanged?.Invoke();
        }

        public void CompleteQuest(Quest quest)
        {
            if (!quest.Completed)
            {
                Debug.LogError($"Cannot complete quest that doesn't have all goals finished. Quest: {quest.QuestName}.");
                return;
            }
            if (!_playerQuestLog.RemoveQuest(quest))
            {
                Debug.LogError($"Cannot complete quest that isn't in players quest log. Quest: {quest.QuestName}.");
                return;
            }
            
            foreach (var goal in quest.Goals)
            {
                // TODO: consider how to do this without type check
                CollectGoal cGoal = goal as CollectGoal;
                if (cGoal == null) continue;
                
                bool removed = _playerInventory.RemoveItem(cGoal.TargetItem, cGoal.TargetCount);
                if (!removed)
                    Debug.LogError($"Tried to remove items from collect goal from player's inventory but they were not present. Quest: {quest.QuestName}. Item: {cGoal.TargetItem}.");
            }

            GiveRewards(quest);
        }

        private void OnExpeditionEnded()
        {
            GenerateQuests();
        }

        private void OnDestroy()
        {
            if (_globalEvents == null) return;

            _globalEvents.OnExpeditionEnded -= OnExpeditionEnded;
        }
    }
}
