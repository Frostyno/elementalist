﻿using Elementalist.Items;
using System;
using UnityEngine;

namespace Elementalist.Quests
{
    public class CollectGoal : IQuestGoal
    {
        // NOTE: CurrentCount is taken from Inventory upon CollectGoal creation to prevent issues
        // with load order (inventory vs questlog)
        [Serializable]
        public class CollectGoalSaveData : QuestGoalSaveData
        {
            public int TargetCount { get; set; }
            public Item TargetItem { get; set; }
            public string Description { get; set; }
        }

        private GlobalEvents _globalEvents = null;

        private int _totalCount;

        public Item TargetItem { get; private set; }
        public int TargetCount { get; private set; }
        public int CurrentCount { get; private set; }
        public bool Completed { get; private set; }

        private string _description;

        public event Action<IQuestGoal> OnGoalUpdated = null;

        public CollectGoal(GlobalEvents globalEvents, Item targetItem, int targetCount, int currentCount = 0, string description = null)
        {
            _globalEvents = globalEvents;
            _globalEvents.OnPlayerCollectedItem += OnPlayerCollectedItem;
            _globalEvents.OnPlayerRemovedItem += OnPlayerRemovedItem;

            TargetItem = targetItem;
            TargetCount = targetCount;
            _totalCount = currentCount;
            CurrentCount = Math.Min(_totalCount, TargetCount);
            Completed = _totalCount >= TargetCount;

            if (!string.IsNullOrEmpty(description)) _description = description;
            else _description = $"Collect {targetItem}." + "{0}/{1}";
        }

        ~CollectGoal()
        {
            _globalEvents.OnPlayerCollectedItem -= OnPlayerCollectedItem;
            _globalEvents.OnPlayerRemovedItem -= OnPlayerRemovedItem;
        }

        public string GetFormattedDescription()
        {
            try
            {
                return string.Format(_description, CurrentCount, TargetCount);
            }
            catch (Exception e)
            {
                Debug.LogError($"Invalid description string for collect goal: {_description}. Error: {e.Message}");
                return "Collect goal description error.";
            }
        }

        public QuestGoalSaveData GetState()
        {
            return new CollectGoalSaveData()
            {
                TargetCount = TargetCount,
                TargetItem = TargetItem,
                Description = _description
            };
        }

        private void OnPlayerCollectedItem(Item item, int amount)
        {
            if (item != TargetItem) return;

            _totalCount += amount;

            if (Completed) return;
            
            CurrentCount = Math.Min(_totalCount, TargetCount);
            Completed = CurrentCount >= TargetCount;
            OnGoalUpdated?.Invoke(this);
        }

        private void OnPlayerRemovedItem(Item item, int amount)
        {
            if (item != TargetItem) return;

            _totalCount -= amount;
            if (_totalCount >= TargetCount) return;

            CurrentCount = Math.Min(_totalCount, TargetCount);
            Completed = CurrentCount >= TargetCount;
            OnGoalUpdated?.Invoke(this);
        }
    }
}
