﻿using Elementalist.Combat;
using System;
using UnityEngine;

namespace Elementalist.Quests
{
    public class KillGoal : IQuestGoal
    {
        [Serializable]
        public class KillGoalSaveData : QuestGoalSaveData
        {
            public int CurrentCount { get; set; }
            public int TargetCount { get; set; }
            public Enemy TargetEnemy { get; set; }
            public string Description { get; set; }
        }

        private GlobalEvents _globalEvents;
        
        public Enemy TargetEnemy { get; private set; }
        public int TargetCount { get; private set; }
        public int CurrentCount { get; private set; }
        public bool Completed { get; private set; }

        private string _description;

        public event Action<IQuestGoal> OnGoalUpdated = null;

        public KillGoal(GlobalEvents globalEvents, Enemy targetEnemy, int targetCount, int currentCount = 0, string description = null)
        {
            _globalEvents = globalEvents;
            _globalEvents.OnEnemyDied += OnEnemyDied;

            TargetEnemy = targetEnemy;
            TargetCount = targetCount;
            CurrentCount = currentCount;
            Completed = false;

            if (!string.IsNullOrEmpty(description)) _description = description;
            else _description = $"Slay {TargetEnemy}." + " {0}/{1}";
        }

        ~KillGoal()
        {
            _globalEvents.OnEnemyDied -= OnEnemyDied;
        }

        public string GetFormattedDescription()
        {
            try
            {
                return string.Format(_description, CurrentCount, TargetCount);
            }
            catch (Exception e)
            {
                Debug.LogError($"Invalid description string for kill goal: {_description}. Error: {e.Message}");
                return "Kill goal description error.";
            }
        }

        public QuestGoalSaveData GetState()
        {
            return new KillGoalSaveData()
            {
                TargetEnemy = TargetEnemy,
                TargetCount = TargetCount,
                CurrentCount = CurrentCount,
                Description = _description
            };
        }

        private void OnEnemyDied(Enemy enemy)
        {
            if (enemy != TargetEnemy || CurrentCount >= TargetCount) return;

            CurrentCount++;
            Completed = CurrentCount >= TargetCount;
            OnGoalUpdated?.Invoke(this);
        }
    }
}