﻿using Elementalist.Items;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Elementalist.Quests
{
    public class QuestUtility : MonoBehaviour
    {
        [Tooltip("Specifies folder (with subfolders) containing all quest prototypes within Resources folder.")]
        [SerializeField] private string _questPrototypesFolderPath = "Quests";

        private Inventory _playerInventory = null;
        private GlobalEvents _globalEvents = null;

        private Dictionary<string, QuestPrototype> _questPrototypes = new Dictionary<string, QuestPrototype>();

        [Inject]
        private void Inject(
            Inventory playerInventory,
            GlobalEvents globalEvents)
        {
            _playerInventory = playerInventory;
            _globalEvents = globalEvents;

            Init();
        }

        private void Init()
        {
            var questPrototypes = Resources.LoadAll<QuestPrototype>(_questPrototypesFolderPath);

            foreach (var questPrototype in questPrototypes)
            {
                if (_questPrototypes.ContainsKey(questPrototype.QuestGUID))
                {
                    Debug.LogError($"Quests {questPrototype.QuestName} and {_questPrototypes[questPrototype.QuestGUID].QuestName} have the same GUID!");
                    continue;
                }

                _questPrototypes[questPrototype.QuestGUID] = questPrototype;
            }
        }

        public Quest CreateQuestFromSaveData(Quest.QuestSaveData saveData)
        {
            List<IQuestGoal> goals = new List<IQuestGoal>(saveData.Goals.Length);
            foreach (QuestGoalSaveData goal in saveData.Goals)
            {
                switch (goal)
                {
                    case KillGoal.KillGoalSaveData killGoal:
                        goals.Add(CreateKillGoalFromSaveData(killGoal));
                        break;
                    case CollectGoal.CollectGoalSaveData collectGoal:
                        goals.Add(CreateCollectGoalFromSaveData(collectGoal));
                        break;
                    default:
                        Debug.LogError($"Unexpected QuestGoalSaveData type: {goal.GetType().ToString()}.");
                        break;
                }
            }

            Quest quest = new Quest(goals.ToArray(), saveData.QuestName, saveData.Description, _questPrototypes[saveData.QuestGUID]);
            return quest;
        }

        private KillGoal CreateKillGoalFromSaveData(KillGoal.KillGoalSaveData saveData)
        {
            return new KillGoal(_globalEvents, saveData.TargetEnemy, saveData.TargetCount, saveData.CurrentCount, saveData.Description);
        }

        private CollectGoal CreateCollectGoalFromSaveData(CollectGoal.CollectGoalSaveData saveData)
        {
            var itemCount = _playerInventory.GetItemAmount(saveData.TargetItem);
            return new CollectGoal(_globalEvents, saveData.TargetItem, saveData.TargetCount, itemCount, saveData.Description);
        }
    }
}