﻿using Elementalist.Items;
using Elementalist.Utility;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Tooltip
{
    public class ItemDisplayer : GenericContentDisplayer<ItemType>
    {
        [SerializeField] private TextMeshProUGUI _nameText = null;
        [SerializeField] private TextMeshProUGUI _costText = null;
        [SerializeField] private TextMeshProUGUI _stackSizeText = null;
        [SerializeField] private TextMeshProUGUI _descriptionText = null;

        [Space]

        [SerializeField] private ColorsPreset _colorsPreset = null;

        private ImmediateEffectDisplayerPool _immediateEffectDisplayerPool = null;
        private TemporaryEffectDisplayerPool _temporaryEffectDisplayerPool = null;
        private StatModifierDisplayerPool _statModifierDisplayerPool = null;
        private SpellDisplayerPool _spellDisplayerPool = null;

        private List<ImmediateEffectDisplayer> _immediateEffectDisplayers = new List<ImmediateEffectDisplayer>();
        private List<TemporaryEffectDisplayer> _temporaryEffectDisplayers = new List<TemporaryEffectDisplayer>();
        private List<StatModifierDisplayer> _statModifierDisplayers = new List<StatModifierDisplayer>();
        private List<SpellDisplayer> _spellDisplayers = new List<SpellDisplayer>();

        [Inject]
        private void Inject(
            ImmediateEffectDisplayerPool immediateEffectDisplayerPool,
            TemporaryEffectDisplayerPool temporaryEffectDisplayerPool,
            StatModifierDisplayerPool statModifierDisplayerPool,
            SpellDisplayerPool spellDisplayerPool)
        {
            _immediateEffectDisplayerPool = immediateEffectDisplayerPool;
            _temporaryEffectDisplayerPool = temporaryEffectDisplayerPool;
            _statModifierDisplayerPool = statModifierDisplayerPool;
            _spellDisplayerPool = spellDisplayerPool;
        }

        private void Awake()
        {
            if (_colorsPreset == null)
            {
                Debug.LogError($"Colors preset missing in {name}.");
                return;
            }

            _costText.color = _colorsPreset.StandardColor;
            _nameText.color = _colorsPreset.StandardColor;
            _stackSizeText.color = _colorsPreset.StandardColor;
            _descriptionText.color = _colorsPreset.StandardColor;
        }

        protected override void SetCastedContent(ItemType content)
        {
            if (content == null) return;

            _nameText?.SetText(content.ItemName);
            string stackText = content.StackSize > 0 ? content.StackSize.ToString() : "Infinite";
            _stackSizeText?.SetText($"Stack size: {stackText}");
            _descriptionText?.SetText(content.Description);

            ResetDisplayer();

            switch (content)
            {
                case UsableItemType usableItem:
                    DisplayUsableItemInfo(usableItem);
                    break;
                case TrinketType trinket:
                    DisplayTrinketInfo(trinket);
                    break;
            }
        }

        private void ResetDisplayer()
        {
            _costText.gameObject.SetActive(false);

            foreach (var displayer in _immediateEffectDisplayers)
                _immediateEffectDisplayerPool.ReturnObject(displayer);
            _immediateEffectDisplayers.Clear();

            foreach (var displayer in _temporaryEffectDisplayers)
                _temporaryEffectDisplayerPool.ReturnObject(displayer);
            _temporaryEffectDisplayers.Clear();

            foreach (var displayer in _statModifierDisplayers)
                _statModifierDisplayerPool.ReturnObject(displayer);
            _statModifierDisplayers.Clear();

            foreach (var displayer in _spellDisplayers)
                _spellDisplayerPool.ReturnObject(displayer);
            _spellDisplayers.Clear();
        }

        private void DisplayUsableItemInfo(UsableItemType usableItem)
        {
            _costText.gameObject.SetActive(true);
            _costText?.SetText($"{usableItem.ActionPointsCost} AP"); // TODO: nahradit AP za obrazok
            // TODO: pridat targeting, targettype

            foreach (var immediateEffectProbPair in usableItem.ImmediateEffects)
            {
                var displayer = _immediateEffectDisplayerPool.GetObject();
                displayer.SetContent(immediateEffectProbPair.Item);
                SetUpDisplayer(displayer);
                _immediateEffectDisplayers.Add(displayer);
            }

            foreach (var temporaryEffectProbPair in usableItem.TemporaryEffects)
            {
                var displayer = _temporaryEffectDisplayerPool.GetObject();
                displayer.SetContent(temporaryEffectProbPair.Item);
                SetUpDisplayer(displayer);
                _temporaryEffectDisplayers.Add(displayer);
            }
        }

        private void DisplayTrinketInfo(TrinketType trinket)
        {
            foreach (var mod in trinket.StatModifiers)
            {
                var displayer = _statModifierDisplayerPool.GetObject();
                displayer.SetContent(mod);
                SetUpDisplayer(displayer);
                _statModifierDisplayers.Add(displayer);
            }

            foreach (var spell in trinket.Spells)
            {
                var displayer = _spellDisplayerPool.GetObject();
                displayer.SetContent(spell);
                SetUpDisplayer(displayer);
                _spellDisplayers.Add(displayer);
            }
        }

        private void SetUpDisplayer(ContentDisplayer displayer)
        {
            displayer.transform.SetParent(transform);
            displayer.transform.localScale = Vector3.one;
            displayer.gameObject.SetActive(true);
        }
    }
}
