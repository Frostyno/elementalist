﻿using Elementalist.Utility;
using TMPro;
using UnityEngine;

namespace Elementalist.UI.Tooltip.Displays
{
    [System.Serializable]
    public class LabelValueDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _valueText = null;
        [SerializeField] private TextMeshProUGUI _labelText = null;

        [SerializeField] private ColorsPreset _colorsPreset = null;

        private void Awake()
        {
            if (_colorsPreset == null)
            {
                Debug.LogError($"Color preset missing in {name}.");
                return;
            }

            _valueText.color = _colorsPreset.ValueColor;
            _labelText.color = _colorsPreset.StandardColor;
        }

        public void SetValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                gameObject.SetActive(false);
                return;
            }

            _valueText?.SetText(value);
            gameObject.SetActive(true);
        }
    }
}
