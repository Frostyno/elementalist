﻿using Elementalist.Utility;
using TMPro;
using UnityEngine;

namespace Elementalist.UI.Tooltip.Displays
{
    [System.Serializable]
    public class LabelElementValueDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _fireValueText = null;
        [SerializeField] private TextMeshProUGUI _waterValueText = null;
        [SerializeField] private TextMeshProUGUI _earthValueText = null;
        [SerializeField] private TextMeshProUGUI _airValueText = null;
        [SerializeField] private TextMeshProUGUI _labelText = null;

        [Space]

        [SerializeField] private ColorsPreset _colorsPreset = null;

        private void Awake()
        {
            if (_colorsPreset == null)
            {
                Debug.LogError($"Colors preset missing in {name}.");
                return;
            }

            _fireValueText.color = _colorsPreset.ElementColors.GetElementColor(ElementType.Fire);
            _waterValueText.color = _colorsPreset.ElementColors.GetElementColor(ElementType.Water);
            _earthValueText.color = _colorsPreset.ElementColors.GetElementColor(ElementType.Earth);
            _airValueText.color = _colorsPreset.ElementColors.GetElementColor(ElementType.Air);
            _labelText.color = _colorsPreset.StandardColor;
        }

        /// <summary>
        /// Sets values of texts accordint to passed values.
        /// </summary>
        /// <param name="fireValue">Value of fire text.</param>
        /// <param name="waterValue">Value of water text.</param>
        /// <param name="earthValue">Value of earth text.</param>
        /// <param name="airValue">Value of air text.</param>
        public void SetValues(string fireValue = "", string waterValue = "", string earthValue = "", string airValue = "")
        {
            if (string.IsNullOrEmpty(fireValue) && string.IsNullOrEmpty(waterValue) && string.IsNullOrEmpty(earthValue) && string.IsNullOrEmpty(airValue))
            {
                gameObject.SetActive(false);
                return;
            }

            SetTextValue(fireValue, _fireValueText);
            SetTextValue(waterValue, _waterValueText);
            SetTextValue(earthValue, _earthValueText);
            SetTextValue(airValue, _airValueText);
            gameObject.SetActive(true);
            return;
        }

        /// <summary>
        /// Sets value of the text. If passed value is null or empty sets
        /// text as inactive.
        /// </summary>
        /// <param name="value">Value to set into the text.</param>
        /// <param name="text">Text to set value of.</param>
        private void SetTextValue(string value, TextMeshProUGUI text)
        {
            if (string.IsNullOrEmpty(value))
            {
                text.gameObject.SetActive(false);
                return;
            }

            text?.SetText(value);
            text?.gameObject.SetActive(true);
        }
    }
}
