﻿using System;
using UnityEngine;

namespace Elementalist.UI.Tooltip
{
    public abstract class GenericContentDisplayer<T> : ContentDisplayer where T : class
    {
        public override void SetContent(object content)
        {
            T castedContent = content as T;
            if (castedContent == null)
            {
                Debug.LogError($"Invalid type passed to displayer for type {typeof(T).ToString()}. Passed type: {content.GetType().ToString()}");
                return;
            }

            SetCastedContent(castedContent);
        }

        public override Type GetHandledContentType() => typeof(T);

        protected abstract void SetCastedContent(T content);
    }
}
