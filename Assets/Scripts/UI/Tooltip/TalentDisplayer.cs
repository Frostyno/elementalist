﻿using Elementalist.Character.Stats;
using Elementalist.Character.Talents;
using Elementalist.Combat.Actions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TMPro;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Tooltip
{
    public class TalentDisplayer : GenericContentDisplayer<Talent>
    {
        [SerializeField] private TextMeshProUGUI _talentNameText = null;

        private StatModifierDisplayerPool _statModDisplayerPool = null;
        private SpellDisplayerPool _spellDisplayerPool = null;

        private List<StatModifierDisplayer> _statModDisplayers = new List<StatModifierDisplayer>();
        private List<SpellDisplayer> _spellDisplayers = new List<SpellDisplayer>();

        [Inject]
        private void Inject(
            StatModifierDisplayerPool statModDisplayerPool,
            SpellDisplayerPool spellDisplayerPool)
        {
            _statModDisplayerPool = statModDisplayerPool;
            _spellDisplayerPool = spellDisplayerPool;
        }

        protected override void SetCastedContent(Talent content)
        {
            if (content == null) return;

            if (content.StatModifiers.Count == 0 && content.Spells.Count > 0) _talentNameText.gameObject.SetActive(false);
            else _talentNameText.gameObject.SetActive(true);
            _talentNameText?.SetText(content.TalentName);

            ResetDisplayer();
            DisplayStatModifiers(content.StatModifiers);
            DisplaySpells(content.Spells);
        }

        /// <summary>
        /// Returns all StatModifierDisplayers to pool.
        /// </summary>
        private void ResetDisplayer()
        {
            foreach (var displayer in _statModDisplayers)
                _statModDisplayerPool.ReturnObject(displayer);
            _statModDisplayers.Clear();

            foreach (var displayer in _spellDisplayers)
                _spellDisplayerPool.ReturnObject(displayer);
            _spellDisplayers.Clear();
        }

        /// <summary>
        /// Uses StatModifierDisplayer for each of StatModifiers passed to display them.
        /// </summary>
        /// <param name="mods">Modifiers to display.</param>
        private void DisplayStatModifiers(IEnumerable<StatModifier> mods)
        {
            foreach (var mod in mods)
            {
                var displayer = _statModDisplayerPool.GetObject();
                displayer.SetContent(mod);
                displayer.transform.SetParent(transform);
                displayer.transform.localScale = Vector3.one;
                displayer.gameObject.SetActive(true);
                _statModDisplayers.Add(displayer);
            }
        }

        private void DisplaySpells(ReadOnlyCollection<SpellType> spells)
        {
            foreach (var spell in spells)
            {
                var displayer = _spellDisplayerPool.GetObject();
                displayer.SetContent(spell);
                displayer.transform.SetParent(transform);
                displayer.transform.localScale = Vector3.one;
                displayer.gameObject.SetActive(true);
                _spellDisplayers.Add(displayer);
            }
        }
    }
}
