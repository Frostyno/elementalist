﻿using Elementalist.Utility;

namespace Elementalist.UI.Tooltip
{
    public class ImmediateEffectDisplayerPool : ObjectPool<ImmediateEffectDisplayer> { }
}