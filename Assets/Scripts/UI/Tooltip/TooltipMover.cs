﻿using UnityEngine;

namespace Elementalist.UI.Tooltip
{
    public class TooltipMover : MonoBehaviour
    {
        [SerializeField] private RectTransform _rootCanvas = null;
        private RectTransform _rectTransform = null;

        private Vector2 _canvasExtends = Vector2.zero;

        private void Awake()
        {
            if (!TryGetComponent(out _rectTransform))
                Debug.LogError("TooltipMover requires to be on a GameObject with RectTransform component.");
        }

        private void Update()
        {
            if (!gameObject.activeInHierarchy) return;

            MoveToPosition(Input.mousePosition);
            CheckBounds();
        }

        public void UpdatePosition()
        {
            MoveToPosition(Input.mousePosition);
        }

        /// <summary>
        /// Moves the associated RectTransform to passed position.
        /// </summary>
        /// <param name="position">Position to move this GameObject to.</param>
        private void MoveToPosition(Vector2 position)
        {
            if ((Vector2)_rectTransform.localPosition == position) return;

            _rectTransform.position = position;
        }

        /// <summary>
        /// Checks if tooltip isn't out of screen bounds. If it is
        /// adjusts pivot position.
        /// </summary>
        private void CheckBounds()
        {
            var newPivotPosition = Vector2.zero; // default position

            // check if tooltip isn't out of screen bounds on x and y axis, if it is change pivot position
            if ((_rootCanvas.rect.width / 2) - _rectTransform.anchoredPosition.x < _rectTransform.rect.size.x)
                newPivotPosition.x = 1;
            if ((_rootCanvas.rect.height / 2) - _rectTransform.anchoredPosition.y < _rectTransform.rect.size.y)
                newPivotPosition.y = 1;

            if (_rectTransform.pivot == newPivotPosition) return;
            _rectTransform.pivot = newPivotPosition;
        }
    }
}
