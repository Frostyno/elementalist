﻿using Elementalist.Character.Stats;
using Elementalist.UI.Tooltip.Displays;
using Elementalist.Utility;
using System;
using System.Text;
using TMPro;
using UnityEngine;

namespace Elementalist.UI.Tooltip
{
    public class StatModifierDisplayer : GenericContentDisplayer<StatModifier>
    {
        [SerializeField] private LabelValueDisplay _healthDisplay = null;
        [SerializeField] private LabelValueDisplay _healthRegenDisplay = null;
        [SerializeField] private LabelValueDisplay _barrierDisplay = null;
        [SerializeField] private LabelValueDisplay _barrierRegenDisplay = null;

        [Space]

        [SerializeField] private LabelValueDisplay _actionPointsDisplay = null;
        [SerializeField] private LabelValueDisplay _actionPointsRegenDisplay = null;
        [SerializeField] private LabelElementValueDisplay _orbsDisplay = null;
        [SerializeField] private LabelElementValueDisplay _orbsRegenDisplay = null;
        [SerializeField] private LabelElementValueDisplay _resistancesDisplay = null;
        [SerializeField] private LabelElementValueDisplay _spellPowerDisplay = null;

        private StringBuilder _concater = new StringBuilder();

        /// <summary>
        /// Sets values of displays according to passed stat modifier.
        /// </summary>
        /// <param name="content">Stat modifier to display.</param>
        protected override void SetCastedContent(StatModifier content)
        {
            if (content == null) return;

            SetDisplayValue(content.GetStatModifier(StatType.Health), _healthDisplay, content.Type);
            SetDisplayValue(content.GetStatModifier(StatType.HealthRegen), _healthRegenDisplay, content.Type);
            SetDisplayValue(content.GetStatModifier(StatType.Barrier), _barrierDisplay, content.Type);
            SetDisplayValue(content.GetStatModifier(StatType.BarrierRegen), _barrierRegenDisplay, content.Type);

            SetDisplayValue(content.GetStatModifier(StatType.ActionPoints), _actionPointsDisplay, content.Type);
            SetDisplayValue(content.GetStatModifier(StatType.ActionPointsRegen), _actionPointsRegenDisplay, content.Type);

            SetElementDisplayValue(
                content.GetStatModifier(StatType.Orbs, ElementType.Fire),
                content.GetStatModifier(StatType.Orbs, ElementType.Water),
                content.GetStatModifier(StatType.Orbs, ElementType.Earth),
                content.GetStatModifier(StatType.Orbs, ElementType.Air),
                _orbsDisplay, content.Type
                );
            SetElementDisplayValue(
                content.GetStatModifier(StatType.OrbsRegen, ElementType.Fire),
                content.GetStatModifier(StatType.OrbsRegen, ElementType.Water),
                content.GetStatModifier(StatType.OrbsRegen, ElementType.Earth),
                content.GetStatModifier(StatType.OrbsRegen, ElementType.Air),
                _orbsRegenDisplay, content.Type
                );
            SetElementDisplayValue(
                content.GetStatModifier(StatType.Resistance, ElementType.Fire) * 100,
                content.GetStatModifier(StatType.Resistance, ElementType.Water) * 100,
                content.GetStatModifier(StatType.Resistance, ElementType.Earth) * 100,
                content.GetStatModifier(StatType.Resistance, ElementType.Air) * 100,
                _resistancesDisplay, content.Type, "%"
                );
            SetElementDisplayValue(
                content.GetStatModifier(StatType.SpellPower, ElementType.Fire),
                content.GetStatModifier(StatType.SpellPower, ElementType.Water),
                content.GetStatModifier(StatType.SpellPower, ElementType.Earth),
                content.GetStatModifier(StatType.SpellPower, ElementType.Air),
                _spellPowerDisplay, content.Type
                );
        }

        private void SetDisplayValue(double value, LabelValueDisplay display, StatModifierType statModType, string metricsUnit = null)
        {
            if (display == null) return;

            display.SetValue(ConvertValueToString(value, statModType, metricsUnit));
        }

        private void SetElementDisplayValue(double fire, double water, double earth, double air,
            LabelElementValueDisplay display, StatModifierType statModType, string metricsUnit = null)
        {
            if (display == null) return;

            display.SetValues(
                ConvertValueToString(fire, statModType, metricsUnit),
                ConvertValueToString(water, statModType, metricsUnit),
                ConvertValueToString(earth, statModType, metricsUnit),
                ConvertValueToString(air, statModType, metricsUnit));
        }

        /// <summary>
        /// Converts the value to string representation including sign and unit (either nothig, % or H)
        /// </summary>
        /// <param name="value">Value to be converted.</param>
        /// <param name="statModType">Type of modifier value is converted for.</param>
        /// <param name="metricsUnit">Override for metrics unit, default is null.</param>
        /// <returns></returns>
        private string ConvertValueToString(double value, StatModifierType statModType, string metricsUnit = null)
        {
            if (Math.Abs(value) < GlobalVariables.EPSILON)
                return "";

            _concater.Clear();

            // add sign if positive
            _concater.Append(value >= 0d ? "+" : "");
            _concater.Append((int)Math.Round(value));
            // determine metrics unit
            if (metricsUnit != null) _concater.Append(metricsUnit);
            else _concater.Append(statModType == StatModifierType.Additive ? "" : statModType == StatModifierType.Percentual ? "%" : "H");

            return _concater.ToString();
        }
    }
}
