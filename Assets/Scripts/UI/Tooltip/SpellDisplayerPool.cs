﻿using Elementalist.Utility;

namespace Elementalist.UI.Tooltip
{
    public class SpellDisplayerPool : ObjectPool<SpellDisplayer> { }
}