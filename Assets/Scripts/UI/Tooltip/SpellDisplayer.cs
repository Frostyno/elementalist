﻿using Elementalist.Combat.Actions;
using Elementalist.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Tooltip
{
    public class SpellDisplayer : GenericContentDisplayer<SpellType>
    {
        [SerializeField] private TextMeshProUGUI _nameText = null;
        [SerializeField] private TextMeshProUGUI _costText = null;

        [Space]

        [SerializeField] private ColorsPreset _colorsPreset = null;

        private ImmediateEffectDisplayerPool _immediateEffectDisplayerPool = null;
        private TemporaryEffectDisplayerPool _temporaryEffectDisplayerPool = null;

        private List<ImmediateEffectDisplayer> _immediateEffectDisplayers = new List<ImmediateEffectDisplayer>();
        private List<TemporaryEffectDisplayer> _temporaryEffectDisplayers = new List<TemporaryEffectDisplayer>();

        private StringBuilder _stringBuilder = new StringBuilder();

        [Inject]
        private void Inject(
            ImmediateEffectDisplayerPool immediateEffectDisplayerPool,
            TemporaryEffectDisplayerPool temporaryEffectDisplayerPool)
        {
            _immediateEffectDisplayerPool = immediateEffectDisplayerPool;
            _temporaryEffectDisplayerPool = temporaryEffectDisplayerPool;
        }

        private void Awake()
        {
            if (_colorsPreset == null)
            {
                Debug.LogError($"Colors preset is missing in {name}.");
                return;
            }

            _nameText.color = _colorsPreset.StandardColor;
            _costText.color = _colorsPreset.StandardColor;
        }

        protected override void SetCastedContent(SpellType content)
        {
            if (content == null) return;

            ResetDisplayer();

            _nameText.text = content.SpellName;
            _costText.text = BuildCostText(content);

            foreach (var immediateEffect in content.ImmediateEffects)
            {
                var displayer = _immediateEffectDisplayerPool.GetObject();
                displayer.transform.SetParent(transform);
                displayer.transform.localScale = Vector3.one;
                displayer.SetContent(immediateEffect.Item);
                displayer.gameObject.SetActive(true);
                _immediateEffectDisplayers.Add(displayer);
            }
            foreach (var temporaryEffect in content.TemporaryEffects)
            {
                var displayer = _temporaryEffectDisplayerPool.GetObject();
                displayer.transform.SetParent(transform);
                displayer.transform.localScale = Vector3.one;
                displayer.SetContent(temporaryEffect.Item);
                displayer.gameObject.SetActive(true);
                _temporaryEffectDisplayers.Add(displayer);
            }
        }

        private void ResetDisplayer()
        {
            foreach (var displayer in _immediateEffectDisplayers)
                _immediateEffectDisplayerPool.ReturnObject(displayer);
            _immediateEffectDisplayers.Clear();

            foreach (var displayer in _temporaryEffectDisplayers)
                _temporaryEffectDisplayerPool.ReturnObject(displayer);
            _temporaryEffectDisplayers.Clear();
        }

        private string BuildCostText(SpellType spellType)
        {
            _stringBuilder.Clear();

            if (Math.Abs(spellType.ActionPointsCost) >= GlobalVariables.EPSILON)
                _stringBuilder.Append($"{spellType.ActionPointsCost}AP");

            foreach (var element in EnumUtility.GetValues<ElementType>())
            {
                if (Math.Abs(spellType.OrbsCost.GetValue(element)) < GlobalVariables.EPSILON) continue;

                if (_stringBuilder.Length > 0) _stringBuilder.Append(", ");
                _stringBuilder.Append($"<#{ColorUtility.ToHtmlStringRGB(_colorsPreset.ElementColors.GetElementColor(element))}>");
                _stringBuilder.Append($"{spellType.OrbsCost.GetValue(element)}O");
                _stringBuilder.Append("</color>");
            }

            return _stringBuilder.ToString();
        }
    }
}
