﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Tooltip
{
    public class Tooltip : MonoBehaviour
    {
        private TooltipMover _tooltipMover = null;
        private ContentDisplayer[] _contentDisplayers = null;

        private ContentDisplayer _lastUsedDisplayer = null;

        [Inject]
        private void Inject(ContentDisplayer[] contentDisplayers)
        {
            _contentDisplayers = contentDisplayers;
        }

        private void Awake()
        {
            _tooltipMover = GetComponent<TooltipMover>();
        }

        /// <summary>
        /// Sets content to ContentDisplayer for content type and displays tooltip.
        /// </summary>
        /// <param name="contentToDisplay">Content to display in tooltip.</param>
        public void DisplayTooltip(object contentToDisplay)
        {
            ContentDisplayer displayer = GetDisplayerFor(contentToDisplay.GetType());
            if (displayer == null)
            {
                Debug.LogError($"No valid displayer present for {contentToDisplay.GetType().ToString()}.");
                return;
            }

            if (_lastUsedDisplayer != null)
                _lastUsedDisplayer.gameObject.SetActive(false);

            displayer.SetContent(contentToDisplay);
            displayer.gameObject.SetActive(true);
            _lastUsedDisplayer = displayer;

            _tooltipMover?.UpdatePosition();
            gameObject.SetActive(true); // show tooltip
        }

        public void HideTooltip()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Chooses ContentDisplayer that can display passed content type.
        /// If no such displayer is found returns null.
        /// </summary>
        /// <param name="contentType">Type of object to display.</param>
        /// <returns>ContentDisplayer for passed type.</returns>
        private ContentDisplayer GetDisplayerFor(Type contentType)
        {
            ContentDisplayer displayer = null;
            foreach (var contentDisplayer in _contentDisplayers) // determine which displayer to use
            {
                Type handledType = contentDisplayer.GetHandledContentType();
                if (contentType == handledType || contentType.IsSubclassOf(handledType)) // check if passed object is the handled type or its subclass
                {
                    displayer = contentDisplayer;
                    break;
                }
            }

            return displayer;
        }
    }
}
