﻿using Elementalist.Combat.Effects;
using Elementalist.Utility;
using System;
using System.Text;
using TMPro;
using UnityEngine;

namespace Elementalist.UI.Tooltip
{
    public class ImmediateEffectDisplayer : GenericContentDisplayer<IImmediateEffect>
    {
        [SerializeField] private TextMeshProUGUI _descriptionText = null;
        [SerializeField] private ColorsPreset _colorsPreset = null;

        [Space]
        [Header("Description format strings")]

        [SerializeField, TextArea] private string _damageEffectFormat = "";
        [SerializeField, TextArea] private string _healEffectFormat = "";
        [SerializeField, TextArea] private string _lifeStealEffectFormat = "";

        private StringBuilder _stringBuilder = new StringBuilder();
        
        private void Awake()
        {
            _descriptionText.color = _colorsPreset.StandardColor;
        }
        
        protected override void SetCastedContent(IImmediateEffect content)
        {
            if (content == null) return;

            string description = "";

            switch (content)
            {
                case DamageEffect damageEffect:
                    description = string.Format(_damageEffectFormat, BuildSpellPowerDescription(damageEffect));
                    break;
                case HealEffect healEffect:
                    description = string.Format(_healEffectFormat, BuildSpellPowerDescription(healEffect));
                    break;
                case LifeStealEffect lifeStealEffect:
                    description = string.Format(
                        _lifeStealEffectFormat,
                        BuildSpellPowerDescription(lifeStealEffect),
                        BuildLifeStealDescription(lifeStealEffect));
                    break;
                default:
                    Debug.LogError($"Unsupported type in ImmediateEffectDisplayer: {content.GetType().ToString()}.");
                    break;
            }

            _descriptionText?.SetText(description);
        }

        /// <summary>
        /// Creates a description for spell power of passed effect.
        /// </summary>
        /// <param name="effect">Effect to create description for.</param>
        /// <returns>Spell power description of passed effect</returns>
        private string BuildSpellPowerDescription(ImmediateEffect effect)
        {
            _stringBuilder.Clear();
            foreach (var element in EnumUtility.GetValues<ElementType>())
            {
                AddValueWithPercentage(effect.BaseSpellPower.GetValue(element),
                    effect.SpellPowerPercentage.GetValue(element),
                    _stringBuilder, _colorsPreset.ElementColors.GetElementColor(element));
            }

            return _stringBuilder.ToString();
        }

        /// <summary>
        /// Creates a description for life steal percentages.
        /// </summary>
        /// <param name="lifeStealEffect">Effect to create description for.</param>
        /// <returns>Life steal description of passed effect.</returns>
        private string BuildLifeStealDescription(LifeStealEffect lifeStealEffect)
        {
            if (string.IsNullOrEmpty(_lifeStealEffectFormat))
            {
                Debug.LogError($"Missing formatting string for LifeStealEffect.");
                return "";
            }

            _stringBuilder.Clear();
            foreach (var element in EnumUtility.GetValues<ElementType>())
            {
                AddPercentage(lifeStealEffect.StealPercentage.GetValue(element),
                    _stringBuilder, _colorsPreset.ElementColors.GetElementColor(element));
            }

            return _stringBuilder.ToString();
        }

        /// <summary>
        /// Adds value and percentage passed to the string builder. If color is specified adds a color tag.
        /// </summary>
        /// <param name="value">Value to add.</param>
        /// <param name="percentage">percentage to add.</param>
        /// <param name="builder">Builder to add value and percentage to.</param>
        /// <param name="color">Color of added text.</param>
        private void AddValueWithPercentage(double value, double percentage, StringBuilder builder, Color? color = null)
        {
            if (Math.Abs(value) < GlobalVariables.EPSILON && 
                Math.Abs(percentage) < GlobalVariables.EPSILON)
                return;

            if (builder.Length > 0) builder.Append(", ");

            bool usedColor = AddColor(color, builder);

            builder.Append($"{(int)Math.Round(value)}");
            if (Math.Abs(percentage) >= GlobalVariables.EPSILON)
                builder.Append($" (+{percentage}x SP)".Replace(',', '.'));

            if (usedColor) AddColor(null, builder, true);
        }

        /// <summary>
        /// Adds a percentage value to passed string builder. If color is specified adds a color tag.
        /// </summary>
        /// <param name="percentage">Percentage to add.</param>
        /// <param name="builder">Builder to add percentage to.</param>
        /// <param name="color">Color of added percentage text.</param>
        private void AddPercentage(double percentage, StringBuilder builder, Color? color = null)
        {
            if (Math.Abs(percentage) < GlobalVariables.EPSILON) return;

            if (builder.Length > 0) builder.Append(", ");

            bool usedColor = AddColor(color, builder);
            builder.Append($"{percentage}x".Replace(',', '.'));
            if (usedColor) AddColor(null, builder, true);
        }

        /// <summary>
        /// Adds a color tag to passed builder.
        /// </summary>
        /// <param name="color">Color tag to add.</param>
        /// <param name="builder">Builder to add tag to.</param>
        /// <param name="endTag">Specifies if end tag should be added (or start tag if false).</param>
        /// <returns></returns>
        private bool AddColor(Color? color, StringBuilder builder, bool endTag = false)
        {
            if (color == null && !endTag) return false;

            if (!endTag && color.HasValue)
                builder.Append($"<#{ColorUtility.ToHtmlStringRGB(color.Value)}>");
            else if (endTag)
                builder.Append("</color>");

            return true;
        }
    }
}
