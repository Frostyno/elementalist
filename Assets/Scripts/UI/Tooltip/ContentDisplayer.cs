﻿using System;
using UnityEngine;

namespace Elementalist.UI.Tooltip
{
    public abstract class ContentDisplayer : MonoBehaviour
    {
        /// <summary>
        /// Updates the ContentDisplayer to display information about
        /// content passed in parameter.
        /// </summary>
        /// <param name="content">Content to display info of.</param>
        public abstract void SetContent(object content);

        /// <summary>
        /// Gets the type of content that can be handled by this content displayer.
        /// </summary>
        /// <returns>Type of handled content.</returns>
        public abstract Type GetHandledContentType();
    }
}
