﻿using Elementalist.Utility;
using UnityEngine;

namespace Elementalist.UI.Tooltip
{
    public class StatModifierDisplayerPool : ObjectPool<StatModifierDisplayer> { }
}
