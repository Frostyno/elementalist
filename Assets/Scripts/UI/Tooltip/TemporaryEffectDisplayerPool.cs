﻿using Elementalist.Utility;

namespace Elementalist.UI.Tooltip
{
    public class TemporaryEffectDisplayerPool : ObjectPool<TemporaryEffectDisplayer> { }
}
