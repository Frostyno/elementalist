﻿using Elementalist.Character;
using TMPro;
using UnityEngine;

namespace Elementalist.UI.Tooltip
{
    public class EnemyDisplayer : GenericContentDisplayer<Elementalist.Character.Character>
    {
        [SerializeField] private TextMeshProUGUI _nameText = null;
        [SerializeField] private TextMeshProUGUI _descriptionText = null;

        protected override void SetCastedContent(Elementalist.Character.Character content)
        {
            if (content == null) return;

            // TODO: this is only a workaround because prefabs are not getting injected, figure out different way
            var essence = content.GetComponent<CharacterEssence>();
            _nameText?.SetText(essence.CharacterName);
            _descriptionText?.SetText(essence.CharacterDescription);
        }
    }
}
