﻿using System;
using System.Collections.Generic;
using Elementalist.Combat.Effects;
using Elementalist.Utility;
using TMPro;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Tooltip
{
    public class TemporaryEffectDisplayer : GenericContentDisplayer<ITemporaryEffect>
    {
        [SerializeField] private TextMeshProUGUI _nameText = null;
        [SerializeField] private TextMeshProUGUI _lengthText = null;
        [SerializeField] private TextMeshProUGUI _periodText = null;

        [Space]

        [SerializeField] private ColorsPreset _colorsPreset = null;

        private StatModifierDisplayerPool _statModDisplayerPool = null;
        private ImmediateEffectDisplayerPool _immediateEffectDisplayerPool = null;

        private List<StatModifierDisplayer> _statModifierDisplayers = new List<StatModifierDisplayer>();
        private List<ImmediateEffectDisplayer> _immediateEffectDisplayers = new List<ImmediateEffectDisplayer>();

        [Inject]
        private void Inject(
            StatModifierDisplayerPool statModDisplayerPool,
            ImmediateEffectDisplayerPool immediateEffectDisplayerPool)
        {
            _statModDisplayerPool = statModDisplayerPool;
            _immediateEffectDisplayerPool = immediateEffectDisplayerPool;
        }

        private void Awake()
        {
            if (_colorsPreset == null)
            {
                Debug.LogError($"Colors preset missing from {name}.");
                return;
            }

            _nameText.color = _colorsPreset.StandardColor;
            _lengthText.color = _colorsPreset.StandardColor;
            _periodText.color = _colorsPreset.StandardColor;
        }

        protected override void SetCastedContent(ITemporaryEffect content)
        {
            var effect = content as TemporaryEffect;

            if (effect == null)
            {
                Debug.LogError($"Unsupported type in TemporaryEffectDisplayer: {content.GetType().ToString()}.");
                return;
            }

            ResetDisplayer();
            SetInfo(effect);

            if (effect.StatModifier != null)
            {
                var statModDisplayer = _statModDisplayerPool.GetObject();
                statModDisplayer.transform.SetParent(transform);
                statModDisplayer.transform.localScale = Vector3.one;
                statModDisplayer.SetContent(effect.StatModifier);
                statModDisplayer.gameObject.SetActive(true);
                _statModifierDisplayers.Add(statModDisplayer);
            }

            DisplayImmediateEffects(effect);
        }

        /// <summary>
        /// Returns all dynamic content to its respective pools.
        /// </summary>
        private void ResetDisplayer()
        {
            foreach (var displayer in _statModifierDisplayers)
                _statModDisplayerPool.ReturnObject(displayer);
            _statModifierDisplayers.Clear();

            foreach (var displayer in _immediateEffectDisplayers)
                _immediateEffectDisplayerPool.ReturnObject(displayer);
            _immediateEffectDisplayers.Clear();
        }

        /// <summary>
        /// Sets the text of general information e.g. name, length and period.
        /// </summary>
        /// <param name="effect">Effect to display information of.</param>
        private void SetInfo(TemporaryEffect effect)
        {
            _nameText?.SetText(effect.EffectName);

            if (effect.Length == 1) _lengthText?.SetText("Duration: 1 round");
            else _lengthText?.SetText($"Duration: {effect.Length} rounds");

            if (effect.Period == 0)
            {
                _periodText.gameObject.SetActive(false);
                return;
            }
            
            _periodText.gameObject.SetActive(true);
            if (effect.Period == 1) _periodText?.SetText("Period: every round");
            else _periodText?.SetText($"Period: {effect.Period} rounds");
        }
            
        private void DisplayImmediateEffects(TemporaryEffect effect)
        {
            foreach (var immediateEffect in effect.ImmediateEffects)
            {
                var displayer = _immediateEffectDisplayerPool.GetObject();
                displayer.transform.SetParent(transform);
                displayer.transform.localScale = Vector3.one;
                displayer.SetContent(immediateEffect);
                displayer.gameObject.SetActive(true);
                _immediateEffectDisplayers.Add(displayer);
            }
        }
    }
}
