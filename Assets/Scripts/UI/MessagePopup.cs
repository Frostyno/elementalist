﻿using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace Elementalist.UI
{
    public class MessagePopup : PopupWindow
    {
        [SerializeField] private GameObject _title = null;
        [SerializeField] private TextMeshProUGUI _titleText = null;
        [SerializeField] private TextMeshProUGUI _messageText = null;

        private TaskCompletionSource<bool> _confirmWaiter = null;

        public async Task DisplayMessage(string message, string title = "")
        {
            if (_confirmWaiter != null) return;
            if (!_isPriorityWindow) _isPriorityWindow = true; // message popup is a priority window

            _messageText?.SetText(message);
            _title?.SetActive(!string.IsNullOrEmpty(title));
            _titleText?.SetText(title);

            OpenWindow();
            _confirmWaiter = new TaskCompletionSource<bool>();

            await _confirmWaiter.Task;
            _confirmWaiter = null;

            CloseWindow();
        }

        public void ConfirmMessage()
        {
            if (_confirmWaiter == null) return;

            _confirmWaiter.SetResult(true);
        }
    }
}
