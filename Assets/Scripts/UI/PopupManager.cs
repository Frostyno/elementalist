﻿using Elementalist.UI.Quests;
using UnityEngine;
using Zenject;
using System.Linq;

namespace Elementalist.UI
{
    public class PopupManager : MonoBehaviour
    {
        private PopupWindow[] _popups = null;

        private PopupWindow _openedWindow = null;
        private bool _isPriorityWindowOpened = false;

        [Inject]
        private void Inject(
            PopupWindow[] popups)
        {
            _popups = popups;
            foreach (var popup in _popups)
            {
                popup.OnPopupClosed += OnPopupClosed;
                popup.OnPopupOpened += OnPopupOpened;
            }
        }

        private void TogglePopup(PopupWindow popup)
        {
            if (_openedWindow == popup)
            {
                _openedWindow.CloseWindow();
                return;
            }

            popup.OpenWindow();
        }

        private void OnPopupOpened(PopupWindow popup, bool isPriorityWindow)
        {
            if (_isPriorityWindowOpened)
            {
                popup.CloseWindow();
                return;
            }

            if (_openedWindow != null) _openedWindow.CloseWindow();

            _openedWindow = popup;
            _isPriorityWindowOpened = isPriorityWindow;
        }

        private void OnPopupClosed(PopupWindow popup)
        {
            if (popup == _openedWindow)
            {
                _openedWindow = null;
                _isPriorityWindowOpened = false;
            }
        }

        private void OnDestroy()
        {
            foreach (var popup in _popups)
            {
                if (popup == null) continue;
                popup.OnPopupClosed -= OnPopupClosed;
                popup.OnPopupOpened -= OnPopupOpened;
            }
        }
    }
}
