﻿using Elementalist.Combat;
using Elementalist.Combat.Actions;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;
using UnityEngine.UI;
using Elementalist.Character.Stats;
using Elementalist.Character;

namespace Elementalist.UI.Actions
{
    public class SpellbookSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Image _spellIcon = null;
        [SerializeField] private Image _unusableGraphics = null;

        private PlayerCombatController _playerController = null;
        private Tooltip.Tooltip _tooltip = null;

        public SpellType Spell { get; private set; }

        [Inject]
        private void Inject(
            PlayerCombatController playerController,
            Tooltip.Tooltip tooltip)
        {
            _tooltip = tooltip;
            _playerController = playerController;

            var playerSources = _playerController.GetComponent<Sources>();
            playerSources.OnActionPointsChanged += OnPlayerSourcesChanged;
            playerSources.OnFireOrbsChanged += OnPlayerSourcesChanged;
            playerSources.OnWaterOrbsChanged += OnPlayerSourcesChanged;
            playerSources.OnEarthOrbsChanged += OnPlayerSourcesChanged;
            playerSources.OnAirOrbsChanged += OnPlayerSourcesChanged;

            if (_spellIcon == null)
            {
                Debug.LogError($"Spell icon image missing in {name}.");
                return;
            }
            
            _spellIcon.type = Image.Type.Simple;
            _spellIcon.preserveAspect = true;
        }

        private void CheckUsable()
        {
            if (Spell == null) return;

            bool unusable = !Spell.CanExecute(_playerController.Character);
            _unusableGraphics.gameObject.SetActive(unusable);
        }

        public void SetSpell(SpellType spell)
        {
            Spell = spell;

            if (_spellIcon == null) return;

            _spellIcon.sprite = Spell.Icon;
        }

        private void OnPlayerSourcesChanged(ResourceChangedArgs args)
        {
            CheckUsable();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (Spell == null) return;
            if (!Spell.CanExecute(_playerController.Character)) return;

            _playerController.SetAction(Spell);
        }

        private void OnDestroy()
        {
            if (_playerController == null) return;

            var playerSources = _playerController.GetComponent<Sources>();
            playerSources.OnActionPointsChanged -= OnPlayerSourcesChanged;
            playerSources.OnFireOrbsChanged -= OnPlayerSourcesChanged;
            playerSources.OnWaterOrbsChanged -= OnPlayerSourcesChanged;
            playerSources.OnEarthOrbsChanged -= OnPlayerSourcesChanged;
            playerSources.OnAirOrbsChanged -= OnPlayerSourcesChanged;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (Spell == null) return;

            _tooltip.DisplayTooltip(Spell);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _tooltip.HideTooltip();
        }

        public class SpellbookSlotFactory : PlaceholderFactory<SpellbookSlot> { }
    }
}
