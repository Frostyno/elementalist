﻿using Elementalist.Character;
using Elementalist.Combat.Actions;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Actions
{
    public class Spellbook : MonoBehaviour
    {
        private CharacterEvents _playerEvents = null;
        private SpellbookSlot.SpellbookSlotFactory _slotFactory = null;

        [Inject]
        private void Inject(
            CharacterEvents playerEvents,
            SpellbookSlot.SpellbookSlotFactory slotFactory)
        {
            _slotFactory = slotFactory;
            _playerEvents = playerEvents;
            _playerEvents.OnSpellsChanged += OnSpellsChanged;

            var playerSpells = _playerEvents.GetComponent<CharacterSpells>();
            if (playerSpells.Spells != null && playerSpells.Spells.Count > 0)
            {
                OnSpellsChanged(playerSpells.GetSpellsListClone());
            }
        }

        private void OnSpellsChanged(SpellType[] spells)
        {
            SpawnSlots(spells.Length);
            FillSlots(spells);
        }

        private void SpawnSlots(int slotsCount)
        {
            // calculate how many slots are to be removed/spawned
            slotsCount = slotsCount - transform.childCount;

            if (slotsCount > 0) // slots need to by spawned
            {
                for (int i = 0; i < slotsCount; i++)
                {
                    var slot = _slotFactory.Create();
                    slot.transform.SetParent(transform); // set spellbook as parent
                }
            }
            else
            {
                slotsCount = Mathf.Abs(slotsCount);
                for (int i = 0; i < slotsCount; i++)
                    Destroy(transform.GetChild(0));
            }
        }

        private void FillSlots(SpellType[] spells)
        {
            for (int i = 0; i < spells.Length; i++)
                transform.GetChild(i).GetComponent<SpellbookSlot>().SetSpell(spells[i]);
        }

        private void OnDestroy()
        {
            _playerEvents.OnSpellsChanged -= OnSpellsChanged;
        }
    }
}
