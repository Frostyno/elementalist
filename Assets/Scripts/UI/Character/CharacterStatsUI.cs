﻿using System;
using TMPro;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Character
{
    public class CharacterStatsUI : PopupWindow
    {
        [Header("Resources")]
        [SerializeField] private TextMeshProUGUI _healthText = null;
        [SerializeField] private TextMeshProUGUI _healthRegenText = null;
        [SerializeField] private TextMeshProUGUI _barrierText = null;
        [SerializeField] private TextMeshProUGUI _barrierRegenText = null;

        [Space]

        [SerializeField] private TextMeshProUGUI _actionPointsText = null;
        [SerializeField] private TextMeshProUGUI _actionPointsRegenText = null;
        [SerializeField] private TextMeshProUGUI _fireOrbsText = null;
        [SerializeField] private TextMeshProUGUI _fireOrbsRegenText = null;
        [SerializeField] private TextMeshProUGUI _waterOrbsText = null;
        [SerializeField] private TextMeshProUGUI _waterOrbsRegenText = null;
        [SerializeField] private TextMeshProUGUI _earthOrbsText = null;
        [SerializeField] private TextMeshProUGUI _earthOrbsRegenText = null;
        [SerializeField] private TextMeshProUGUI _airOrbsText = null;
        [SerializeField] private TextMeshProUGUI _airOrbsRegenText = null;

        [Header("Other stats")]
        [Space]

        [SerializeField] private TextMeshProUGUI _fireResistanceText = null;
        [SerializeField] private TextMeshProUGUI _waterResistanceText = null;
        [SerializeField] private TextMeshProUGUI _earthResistanceText = null;
        [SerializeField] private TextMeshProUGUI _airResistanceText = null;
        [SerializeField] private TextMeshProUGUI _fireSpellPower= null;
        [SerializeField] private TextMeshProUGUI _waterSpellPower = null;
        [SerializeField] private TextMeshProUGUI _earthSpellPower = null;
        [SerializeField] private TextMeshProUGUI _airSpellPower = null;

        private InputController _inputController = null;
        private Elementalist.Character.Character _player = null;

        [Inject]
        private void Inject(InputController inputController,
            Elementalist.Character.Character player)
        {
            _player = player;

            _inputController = inputController;
            _inputController.OnTogglePlayerStats += ToggleWindow;
        }

        private void OnEnable()
        {
            UpdateDisplay();
        }

        private void UpdateDisplay()
        {
            _healthText.text = _player.MaxHealth.ToString();
            _healthRegenText.text = _player.HealthRegeneration.ToString();
            _barrierText.text = _player.MaxBarrier.ToString();
            _barrierRegenText.text = _player.BarrierRegeneration.ToString();

            _actionPointsText.text = _player.MaxActionPoints.ToString();
            _actionPointsRegenText.text = _player.ActionPointsRegeneration.ToString();
            _fireOrbsText.text = _player.MaxOrbs(ElementType.Fire).ToString();
            _fireOrbsRegenText.text = _player.OrbsRegeneration(ElementType.Fire).ToString();
            _waterOrbsText.text = _player.MaxOrbs(ElementType.Water).ToString();
            _waterOrbsRegenText.text = _player.OrbsRegeneration(ElementType.Water).ToString();
            _earthOrbsText.text = _player.MaxOrbs(ElementType.Earth).ToString();
            _earthOrbsRegenText.text = _player.OrbsRegeneration(ElementType.Earth).ToString();
            _airOrbsText.text = _player.MaxOrbs(ElementType.Air).ToString();
            _airOrbsRegenText.text = _player.OrbsRegeneration(ElementType.Air).ToString();

            _fireResistanceText.text = FormatResistanceText(_player.Resistance(ElementType.Fire));
            _waterResistanceText.text = FormatResistanceText(_player.Resistance(ElementType.Water));
            _earthResistanceText.text = FormatResistanceText(_player.Resistance(ElementType.Earth));
            _airResistanceText.text = FormatResistanceText(_player.Resistance(ElementType.Air));

            _fireSpellPower.text = _player.SpellPower(ElementType.Fire).ToString();
            _waterSpellPower.text = _player.SpellPower(ElementType.Water).ToString();
            _earthSpellPower.text = _player.SpellPower(ElementType.Earth).ToString();
            _airSpellPower.text = _player.SpellPower(ElementType.Air).ToString();
        }

        private string FormatResistanceText(double value) => $"{(int)Math.Round(value * 100d)}%";

        private void OnDestroy()
        {
            if (_inputController == null) return;
            
            _inputController.OnTogglePlayerStats -= ToggleWindow;
        }
    }
}
