﻿using Elementalist.Combat;
using Elementalist.Character;
using UnityEngine;
using Zenject;
using TMPro;
using Elementalist.Character.Stats;
using System;

namespace Elementalist.UI.Character
{
    public class CharacterCombatDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _healthText = null;
        [SerializeField] private TextMeshProUGUI _barrierText = null;
        [SerializeField] private TextMeshProUGUI _actionPointsText = null;
        [SerializeField] private TextMeshProUGUI _fireOrbsText = null;
        [SerializeField] private TextMeshProUGUI _waterOrbsText = null;
        [SerializeField] private TextMeshProUGUI _earthOrbsText = null;
        [SerializeField] private TextMeshProUGUI _airOrbsText = null;

        [Space]

        [SerializeField] private CombatSide _combatSide = CombatSide.Left;

        private Combat.Combat _combat = null;
        private CombatController _combatant = null;

        [Inject]
        private void Inject(Combat.Combat combat)
        {
            _combat = combat;
            _combat.OnCombatInicialized += OnCombatInicialized;
        }

        private void SetCombatant(CombatController combatant)
        {
            if (combatant == null)
            {
                Debug.LogError($"Combatant is null in {name}.");
                return;
            }

            _combatant = combatant;

            var vitality = _combatant.GetComponent<Vitality>();
            vitality.OnHealthChanged += OnHealthChanged;
            vitality.OnBarrierChanged += OnBarrierChanged;

            var sources = _combatant.GetComponent<Sources>();
            sources.OnActionPointsChanged += OnActionPointsChanged;
            sources.OnFireOrbsChanged += OnFireOrbsChanged;
            sources.OnWaterOrbsChanged += OnWaterOrbsChanged;
            sources.OnEarthOrbsChanged += OnEarthOrbsChanged;
            sources.OnAirOrbsChanged += OnAirOrbsChanged;

            var character = _combatant.Character;
            UpdateDisplay(_healthText, character.CurrentHealth, character.MaxHealth);
            UpdateDisplay(_barrierText, character.CurrentBarrier, character.MaxBarrier);
            UpdateDisplay(_actionPointsText, character.CurrentActionPoints, character.MaxActionPoints);
            UpdateDisplay(_fireOrbsText, character.CurrentOrbs(ElementType.Fire), character.MaxOrbs(ElementType.Fire));
            UpdateDisplay(_waterOrbsText, character.CurrentOrbs(ElementType.Water), character.MaxOrbs(ElementType.Water));
            UpdateDisplay(_earthOrbsText, character.CurrentOrbs(ElementType.Earth), character.MaxOrbs(ElementType.Earth));
            UpdateDisplay(_airOrbsText, character.CurrentOrbs(ElementType.Air), character.MaxOrbs(ElementType.Air));
        }

        private void UpdateDisplay(TextMeshProUGUI displayText, double current, double max)
        {
            displayText.text = $"{current}/{max}";
        }

        private void OnCombatInicialized()
        {
            SetCombatant(_combat.CombatArea.GetFirstCombatantPosition(_combatSide).Combatant);
        }

        private void OnHealthChanged(ResourceChangedArgs args) => UpdateDisplay(_healthText, args.NewCurrent, args.NewMax);
        private void OnBarrierChanged(ResourceChangedArgs args) => UpdateDisplay(_barrierText, args.NewCurrent, args.NewMax);
        private void OnActionPointsChanged(ResourceChangedArgs args) => UpdateDisplay(_actionPointsText, args.NewCurrent, args.NewMax);
        private void OnFireOrbsChanged(ResourceChangedArgs args) => UpdateDisplay(_fireOrbsText, args.NewCurrent, args.NewMax);
        private void OnWaterOrbsChanged(ResourceChangedArgs args) => UpdateDisplay(_waterOrbsText, args.NewCurrent, args.NewMax);
        private void OnEarthOrbsChanged(ResourceChangedArgs args) => UpdateDisplay(_earthOrbsText, args.NewCurrent, args.NewMax);
        private void OnAirOrbsChanged(ResourceChangedArgs args) => UpdateDisplay(_airOrbsText, args.NewCurrent, args.NewMax);

        private void OnDestroy()
        {
            if (_combat != null) _combat.OnCombatInicialized -= OnCombatInicialized;

            if (_combatant == null) return;

            var vitality = _combatant.GetComponent<Vitality>();
            vitality.OnHealthChanged -= OnHealthChanged;
            vitality.OnBarrierChanged -= OnBarrierChanged;

            var sources = _combatant.GetComponent<Sources>();
            sources.OnActionPointsChanged -= OnActionPointsChanged;
            sources.OnFireOrbsChanged -= OnFireOrbsChanged;
            sources.OnWaterOrbsChanged -= OnWaterOrbsChanged;
            sources.OnEarthOrbsChanged -= OnEarthOrbsChanged;
            sources.OnAirOrbsChanged -= OnAirOrbsChanged;
        }
    }
}
