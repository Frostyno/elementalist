﻿using Elementalist.Quests;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Elementalist.UI.Quests
{
    [RequireComponent(typeof(Button))]
    public class QuestButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _buttonText = null;

        private Button _button = null;

        private Quest _quest = null;
        private IQuestDisplayer _questDisplayer = null;

        private void Awake()
        {
            _button = GetComponent<Button>();
        }

        private void DisplayQuest()
        {
            if (_quest == null || _questDisplayer == null) return;

            //TODO: add selected image
            _questDisplayer.DisplayQuest(_quest);
        }

        public void SetQuest(Quest quest, IQuestDisplayer questDisplayer)
        {
            SetInteractable(true);
            _button.onClick.RemoveAllListeners();

            _quest = quest;
            _questDisplayer = questDisplayer;

            _buttonText.text = quest.QuestName;
            _button.onClick.AddListener(() => DisplayQuest());
        }

        public void SetInteractable(bool state)
        {
            _button.interactable = state;
        }

        private void OnDisable()
        {
            _quest = null;
            _questDisplayer = null;
        }
    }
}
