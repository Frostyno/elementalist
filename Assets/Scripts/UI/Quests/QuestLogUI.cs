﻿using System;
using Elementalist.Quests;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Quests
{
    public class QuestLogUI : PopupWindow, IQuestDisplayer
    {
        [SerializeField] private RectTransform _activeQuestButtons = null;
        [SerializeField] private QuestDisplay _questDisplay = null;
        [SerializeField] private GameObject _noQuestsText = null;

        private QuestLog _playerQuestLog = null;
        private QuestButtonPool _questButtonPool = null;
        private InputController _inputController = null;

        [Inject]
        private void Inject(
            QuestLog playerQuestLog,
            QuestButtonPool questButtonPool,
            InputController inputController)
        {
            _playerQuestLog = playerQuestLog;
            _questButtonPool = questButtonPool;

            _inputController = inputController;
            _inputController.OnToggleQuestLog += ToggleWindow;
        }

        private void GenerateQuestButtons()
        {
            while (_activeQuestButtons.childCount > 0)
            {
                _questButtonPool.ReturnObject(_activeQuestButtons.GetChild(0).GetComponent<QuestButton>());
            }
            
            if (_playerQuestLog.Quests.Count > 0)
            {
                _noQuestsText.SetActive(false);
                foreach (var quest in _playerQuestLog.Quests)
                {
                    var qButton = _questButtonPool.GetObject();
                    qButton.transform.SetParent(_activeQuestButtons);
                    qButton.SetQuest(quest, this);
                    qButton.gameObject.SetActive(true);
                }
            }
            else _noQuestsText.SetActive(true);
        }

        public void DisplayQuest(Quest quest)
        {
            _questDisplay.SetQuest(quest);
            _questDisplay.gameObject.SetActive(true);
        }

        private void OnEnable()
        {
            GenerateQuestButtons();
        }

        private void OnDisable()
        {
            _questDisplay.gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            if (_inputController == null) return;

            _inputController.OnToggleQuestLog -= ToggleWindow;
        }
    }
}
