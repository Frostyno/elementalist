﻿using Elementalist.Quests;
using Elementalist.UI.Items;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Quests
{
    public class QuestDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _questNameText = null;
        [SerializeField] private TextMeshProUGUI _questDescriptionText = null;
        [SerializeField] private RectTransform _goalTrackersParent = null;
        [SerializeField] private RectTransform _itemDisplaysParent = null;

        private Quest _quest = null;

        private GoalTrackerPool _goalTrackersPool = null;
        private ItemDisplayPool _itemDisplaysPool = null;

        private List<GoalTracker> _goalTrackers = new List<GoalTracker>();
        private List<ItemDisplay> _itemDisplays = new List<ItemDisplay>();

        [Inject]
        private void Inject(
            GoalTrackerPool trackerPool,
            ItemDisplayPool itemDisplayPool)
        {
            _goalTrackersPool = trackerPool;
            _itemDisplaysPool = itemDisplayPool;
        }

        private void UpdateDisplay()
        {
            if (_quest == null) return;

            if (_quest.Completed) _questNameText.text = $"(Completed) {_quest.QuestName}";
            else _questNameText.text = _quest.QuestName;

            _questDescriptionText.text = _quest.Description;
        }

        private void ResetDisplay()
        {
            foreach (var tracker in _goalTrackers)
                _goalTrackersPool.ReturnObject(tracker);
            _goalTrackers.Clear();
            foreach (var display in _itemDisplays)
                _itemDisplaysPool.ReturnObject(display);
            _itemDisplays.Clear();
        }
        
        public void SetQuest(Quest quest)
        {
            if (quest == null) return;
            if (_quest != null)
            {
                _quest.OnQuestUpdated -= OnQuestUpdated;
                ResetDisplay();
            }

            _quest = quest;
            _quest.OnQuestUpdated += OnQuestUpdated;

            UpdateDisplay();

            // start tracking goals
            foreach (var goal in _quest.Goals)
            {
                var tracker = _goalTrackersPool.GetObject();
                tracker.transform.SetParent(_goalTrackersParent);
                tracker.SetTrackedGoal(goal);
                tracker.gameObject.SetActive(true);
                _goalTrackers.Add(tracker);
            }

            // create item displays
            foreach (var reward in _quest.QuestPrototype.Rewards)
            {
                var itemDisplay = _itemDisplaysPool.GetObject();
                itemDisplay.transform.SetParent(_itemDisplaysParent);
                itemDisplay.SetDisplayedItem(reward.ItemType, reward.Amount);
                itemDisplay.gameObject.SetActive(true);
                _itemDisplays.Add(itemDisplay);
            }
        }

        private void OnQuestUpdated(Quest quest)
        {
            if (quest != _quest)
            {
                quest.OnQuestUpdated -= OnQuestUpdated;
                return;
            }

            UpdateDisplay();
        }

        private void OnDestroy()
        {
            if (_quest == null) return;
            _quest.OnQuestUpdated -= OnQuestUpdated;
        }
    }
}
