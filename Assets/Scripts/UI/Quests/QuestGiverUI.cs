﻿using System;
using System.Collections.ObjectModel;
using Elementalist.Quests;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Elementalist.UI.Quests
{
    public class QuestGiverUI : MonoBehaviour, IQuestDisplayer
    {
        [Header("Quest giver panel dependencies")]
        [SerializeField] private RectTransform _activeQuestButtons = null;
        [SerializeField] private RectTransform _acceptableQuestButtons = null;
        [SerializeField] private GameObject _noActiveQuestsText = null;
        [SerializeField] private GameObject _noAcceptableQuestsText = null;

        [Space]

        [Header("Quest panel dependencies")]
        [SerializeField] private GameObject _questPanel = null;
        [SerializeField] private QuestDisplay _questDisplay = null;
        [SerializeField] private GameObject _inactiveQuestControls = null;
        [SerializeField] private GameObject _activeQuestControls = null;
        [SerializeField] private Button _turnInButton = null;

        private QuestGiver _questGiver = null;
        private QuestLog _playerQuestLog = null;
        private QuestButtonPool _questButtonPool = null;

        private Quest _displayedQuest = null;

        [Inject]
        private void Inject(
            QuestGiver questGiver,
            QuestLog playerQuestLog,
            QuestButtonPool questButtonPool)
        {
            _questButtonPool = questButtonPool;
            _questGiver = questGiver;
            _questGiver.OnGeneratedQuestsChanged += OnGeneratedQuestsChanged;
            _playerQuestLog = playerQuestLog;
            _playerQuestLog.OnQuestAdded += OnQuestAdded;
            _playerQuestLog.OnQuestRemoved += OnQuestRemoved;
        }

        #region DynamicButtonsGeneration

        private void GenerateQuestGiverButtons()
        {
            GenerateQuestButtons(_acceptableQuestButtons, _questGiver.GeneratedQuests, _noAcceptableQuestsText);
        }

        private void GenerateQuestLogButtons()
        {
            GenerateQuestButtons(_activeQuestButtons, _playerQuestLog.Quests, _noActiveQuestsText, true);
        }

        private void GenerateQuestButtons(RectTransform buttonsParent, ReadOnlyCollection<Quest> quests, GameObject noQuestsText, bool? interactable = null)
        {
            while (buttonsParent.childCount != 0)
            {
                _questButtonPool.ReturnObject(buttonsParent.GetChild(0).GetComponent<QuestButton>());
            }

            if (quests.Count > 0)
            {
                noQuestsText.SetActive(false);
                foreach (var quest in quests)
                {
                    var qButton = _questButtonPool.GetObject();
                    qButton.transform.SetParent(buttonsParent);
                    qButton.gameObject.SetActive(true);
                    qButton.SetQuest(quest, this);

                    if (interactable.HasValue) qButton.SetInteractable(interactable.Value);
                    else qButton.SetInteractable(_playerQuestLog.CanAddQuest());
                }
            }
            else noQuestsText.SetActive(true);
        }

        #endregion
        
        public void AcceptQuest()
        {
            _questGiver.AddQuestToPlayer(_displayedQuest);
            _questPanel.SetActive(false);
        }

        public void AbandonQuest()
        {
            _playerQuestLog.RemoveQuest(_displayedQuest);
            _questPanel.SetActive(false);
        }

        public void CompleteQuest()
        {
            _questGiver.CompleteQuest(_displayedQuest);
            _questPanel.SetActive(false);
        }

        public void DisplayQuest(Quest quest)
        {
            _displayedQuest = quest;
            _questDisplay.SetQuest(quest);

            bool isActiveQuest = false;
            foreach (var q in _playerQuestLog.Quests)
            {
                if (q == quest)
                {
                    isActiveQuest = true;
                    break;
                }
            }

            _inactiveQuestControls.SetActive(!isActiveQuest);
            _activeQuestControls.SetActive(isActiveQuest);
            _turnInButton.interactable = isActiveQuest && quest.Completed;

            _questPanel.SetActive(true);
        }

        #region EventHandlers

        private void OnGeneratedQuestsChanged()
        {
            GenerateQuestGiverButtons();
        }

        private void OnQuestAdded()
        {
            GenerateQuestGiverButtons();
            GenerateQuestLogButtons();
        }

        private void OnQuestRemoved()
        {
            GenerateQuestGiverButtons();
            GenerateQuestLogButtons();
        }

        private void OnEnable()
        {
            GenerateQuestGiverButtons();
            GenerateQuestLogButtons();
        }

        private void OnDisable()
        {
            _questPanel.SetActive(false);
            _displayedQuest = null;
        }

        private void OnDestroy()
        {
            if (_questGiver != null)
                _questGiver.OnGeneratedQuestsChanged -= OnGeneratedQuestsChanged;

            if (_playerQuestLog == null) return;

            _playerQuestLog.OnQuestAdded -= OnQuestAdded;
            _playerQuestLog.OnQuestRemoved -= OnQuestRemoved;
        }

        #endregion
    }
}
