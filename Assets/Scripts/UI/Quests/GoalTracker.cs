﻿using Elementalist.Quests;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Elementalist.UI.Quests
{
    public class GoalTracker : MonoBehaviour
    {
        [SerializeField] private Image _progressImage = null;
        [SerializeField] private TextMeshProUGUI _descriptionText = null;

        [Space]

        [SerializeField] private Sprite _inProgressSprite = null;
        [SerializeField] private Sprite _completedSprite = null;
        [SerializeField] private Color _inProgressColor = Color.yellow;
        [SerializeField] private Color _completedColor = Color.green;

        private IQuestGoal _trackedGoal = null;
        
        private void UpdateDisplay()
        {
            if (_trackedGoal == null) return;

            _progressImage.sprite = _trackedGoal.Completed ? _completedSprite : _inProgressSprite;
            _progressImage.color = _trackedGoal.Completed ? _completedColor : _inProgressColor;

            _descriptionText.text = _trackedGoal.GetFormattedDescription();
            _descriptionText.fontStyle = _trackedGoal.Completed ? FontStyles.Strikethrough : FontStyles.Normal;
        }

        public void SetTrackedGoal(IQuestGoal goal)
        {
            if (goal == null) return;
            if (_trackedGoal != null)
            {
                _trackedGoal.OnGoalUpdated -= OnGoalUpdated;
            }


            _trackedGoal = goal;
            _trackedGoal.OnGoalUpdated += OnGoalUpdated;

            UpdateDisplay();
        }

        private void OnGoalUpdated(IQuestGoal goal)
        {
            if (goal != _trackedGoal)
            {
                goal.OnGoalUpdated -= OnGoalUpdated;
                return;
            }

            UpdateDisplay();
        }

        private void OnDisable()
        {
            if (_trackedGoal == null) return;

            _trackedGoal.OnGoalUpdated -= OnGoalUpdated;
            _trackedGoal = null;
        }

        private void OnDestroy()
        {
            if (_trackedGoal == null) return;
            _trackedGoal.OnGoalUpdated -= OnGoalUpdated;
        }
    }
}
