﻿using Elementalist.Quests;

namespace Elementalist.UI.Quests
{
    public interface IQuestDisplayer
    {
        void DisplayQuest(Quest quest);
    }
}