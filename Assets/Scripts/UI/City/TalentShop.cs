﻿using Elementalist.Character;
using Elementalist.Character.Talents;
using Elementalist.Items;
using Elementalist.Saving;
using Elementalist.UI.Tooltip;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Elementalist.UI.City
{
    public class TalentShop : MonoBehaviour, ISavable
    {
        [SerializeField] private GameObject _talentSlotsPanel = null;
        [SerializeField] private GameObject _talentDisplayPanel = null;
        [SerializeField] private TalentDisplayer _talentDisplayer = null;
        [SerializeField] private RectTransform _slotsParent = null;
        [SerializeField] private Button _purchaseButton = null;
        [SerializeField] private TextMeshProUGUI _priceText = null;

        [Space]

        [SerializeField] private TalentSlot _slotPrefab = null;
        [SerializeField] private string _talentsFolder = "Talents";

        private Inventory _playerInventory = null;
        private CharacterTalents _playerTalents = null;

        private TalentSlot[] _slots = new TalentSlot[0];
        private Dictionary<string, List<TalentSlot>> _slotsByTalentGUID = new Dictionary<string, List<TalentSlot>>();

        private TalentSlot _previewedTalentSlot = null;

        [Inject]
        private void Inject(
            Inventory playerInventory,
            CharacterTalents playerTalents)
        {
            _playerInventory = playerInventory;
            _playerTalents = playerTalents;

            Init();
        }

        #region Initialization

        /// <summary>
        /// Initializes talent shop. Loads all talents from Resource folder, creates buttons for them
        /// and assignes them into lookup dictionary.
        /// </summary>
        private void Init()
        {
            if (_slotsParent == null)
                Debug.LogError($"Slots parent is missing ing {name}. This will result in incorrect display of TalentSlots.");

            var talents = Resources.LoadAll<Talent>(_talentsFolder); // TODO: consider if this shouldn't be made an inspector field
            var slots = CreateSlots(talents);

            if (slots == null) return;
            _slots = slots;

            foreach (var slot in _slots)
            {
                if (slot.Talent == null || string.IsNullOrEmpty(slot.Talent.TalentGUID))
                {
                    Debug.LogError($"Talent slot {slot.name} is missing talent or the talent has no GUID assigned.");
                    continue;
                }

                if (!_slotsByTalentGUID.ContainsKey(slot.Talent.TalentGUID)) _slotsByTalentGUID[slot.Talent.TalentGUID] = new List<TalentSlot>();
                _slotsByTalentGUID[slot.Talent.TalentGUID].Add(slot);
            }
        }

        /// <summary>
        /// Creates a talent slot for each of passed talents.
        /// </summary>
        /// <param name="talents">Talents to create slots for.</param>
        /// <returns>Array of created talent slots.</returns>
        private TalentSlot[] CreateSlots(Talent[] talents)
        {
            if (_slotPrefab == null)
            {
                Debug.LogError($"Cannot create TalentSlots, prefab is null.");
                return null;
            }

            TalentSlot[] slots = new TalentSlot[talents.Length];
            for (int i = 0; i < talents.Length; i++)
            {
                var slot = Instantiate(_slotPrefab);
                slot.transform.SetParent(_slotsParent);
                slot.Init(talents[i], this);
                slots[i] = slot;
                slot.gameObject.SetActive(false);
            }

            return slots;
        }

        #endregion

        private void OnEnable()
        {
            _previewedTalentSlot = null;

            _talentDisplayPanel?.SetActive(false);
            _talentSlotsPanel?.SetActive(false);
        }

        /// <summary>
        /// Resets all slots to initial state. Also removes purchased talents from player.
        /// </summary>
        private void ResetShop()
        {
            var talentsToRemove = new List<Talent>();

            foreach (var slot in _slots)
            {
                if (slot.Purchased)
                    talentsToRemove.Add(slot.Talent);
                slot.ResetSlot();
            }

            _playerTalents.RemoveMultipleTalents(talentsToRemove);
        }

        /// <summary>
        /// Displays talent slots for passed element type.
        /// NOTE: passed type is int because of UnityEvent compatibility
        /// </summary>
        /// <param name="elementTypeIndex">Index of ElementType to display talents for.</param>
        public void DisplayTalentSlotsFor(int elementTypeIndex) // used in button
        {
            if (!Enum.IsDefined(typeof(ElementType), elementTypeIndex))
            {
                Debug.LogError($"ElementType index is out of bounds in {name};");
                return;
            }
            ElementType elementType = (ElementType)elementTypeIndex;

            foreach (var slot in _slots)
                slot.gameObject.SetActive(slot.Talent.ElementType == elementType);

            if (_talentDisplayPanel.activeInHierarchy) _talentDisplayPanel.SetActive(false);
            if (!_talentSlotsPanel.activeInHierarchy) _talentSlotsPanel.SetActive(true);
        }

        /// <summary>
        /// Displays the Talent associated with passed TalentSlot.
        /// </summary>
        /// <param name="talentSlot">TalentSlot which talent is to be displayed.</param>
        public void DisplaySlotTalent(TalentSlot talentSlot)
        {
            _previewedTalentSlot = talentSlot;

            bool purchasable = !_previewedTalentSlot.Purchased;
            if (purchasable)
                purchasable = _playerInventory.GetItemAmount(Item.ElementShard) >= _previewedTalentSlot.Talent.Cost;

            _talentDisplayer?.SetContent(talentSlot.Talent);
            _priceText?.SetText($"{talentSlot.Talent.Cost} shards");
            _purchaseButton.interactable = purchasable;
            if (!_talentDisplayPanel.activeInHierarchy) _talentDisplayPanel.SetActive(true);
        }

        /// <summary>
        /// Purcheses previewed talent for player. Removes currency accordingly.
        /// </summary>
        public void PurchaseTalent() // used in button
        {
            if (_previewedTalentSlot == null) return;

            bool payed = _playerInventory.RemoveItem(Item.ElementShard, (int)_previewedTalentSlot.Talent.Cost);

            if (!payed)
            {
                Debug.LogError("Tried to purchase talent but player didn't have enough currency. Perhaps TalentSlot.CheckPurchasability wasn't called?");
                return;
            }

            _previewedTalentSlot.Purchase();
            _purchaseButton.interactable = false;
            _playerTalents.AddTalent(_previewedTalentSlot.Talent);
        }

        #region Saving

        public string GetKey() => "";

        /// <summary>
        /// Captures the current state of shop into a save data object.
        /// </summary>
        /// <returns>Current state of shop.</returns>
        public object GetState()
        {
            TalentSlot.TalentSlotSaveData[] slotsSaveData = new TalentSlot.TalentSlotSaveData[_slots.Length];

            for (int i = 0; i < _slots.Length; i++)
            {
                if (!_slots[i].Purchased) continue;
                slotsSaveData[i] = _slots[i].GetState();
            }

            return slotsSaveData;
        }

        /// <summary>
        /// Loads shop state based on passed data.
        /// </summary>
        /// <param name="state">Saved state of shop. Must be off type TalentSlotSaveData[].</param>
        public void LoadState(object state)
        {
            var slotsSaveData = state as TalentSlot.TalentSlotSaveData[];
            if (slotsSaveData == null)
            {
                Debug.LogError($"Invalid data passed to TalentShop when trying to LoadState. Passed type: {state.GetType().ToString()}. Expected type: {typeof(TalentSlot.TalentSlotSaveData).ToString()}.");
                return;
            }
            if (slotsSaveData.Length != _slots.Length)
                Debug.LogWarning($"Number of slots and saved data don't match. This might result in lost loaded data.");

            ResetShop();

            var talentsToAdd = new List<Talent>();
            for (int i = 0; i < _slots.Length; i++)
            {
                if (!slotsSaveData[i].Purchased) continue;
                if (string.IsNullOrEmpty(slotsSaveData[i].TalentGUID)) continue;

                var slotsWithTalent = _slotsByTalentGUID[slotsSaveData[i].TalentGUID];
                bool loaded = false;
                foreach (var slot in slotsWithTalent)
                {
                    if (slot.Purchased) continue;

                    slot.Purchase();
                    talentsToAdd.Add(slot.Talent);
                    loaded = true;
                    break;
                }

                if (!loaded)
                    Debug.LogWarning($"There wasn't enough slots to load purchased saved talent. TalentGUID: {slotsSaveData[i].TalentGUID}.");
            }
            
            _playerTalents.AddMultipleTalents(talentsToAdd);
        }

        #endregion
    }
}
