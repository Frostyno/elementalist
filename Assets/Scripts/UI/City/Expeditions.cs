﻿using Elementalist.Items;
using Elementalist.Map;
using Elementalist.Map.Generation;
using Elementalist.Saving;
using Elementalist.SceneManagement;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.City
{
    public class Expeditions : MonoBehaviour
    {
        [SerializeField] private Transform _slotsParent = null;
        [SerializeField] private DungeonDisplay _dungeonDisplay = null;

        [Space]

        [SerializeField] private DungeonSettings[] _dungeonSettings = new DungeonSettings[0];

        [Space]

        [Tooltip("Specifies how much element shards player loses for dying on expedition.")]
        [SerializeField, Range(0f, 1f)] private float _playerDeathSanction = 0.5f;

        private ExpeditionSlotPool _expeditionSlotPool = null;
        private Canvas _cityUI = null;
        private GlobalEvents _globaEvents = null;
        private Inventory _playerInventory = null;
        private SaveManager _saveManager = null;

        private List<ExpeditionSlot> _expeditionSlots = new List<ExpeditionSlot>();
        private DungeonSettings _displayedDungeon = null;

        private TaskCompletionSource<bool> _mapWaiter = null;
        private Dungeon _dungeon = null;

        [Inject]
        private void Inject(
            Canvas cityUI,
            ExpeditionSlotPool expeditionSlotPool,
            GlobalEvents globalEvents,
            Inventory playerInventory,
            SaveManager saveManager)
        {
            _expeditionSlotPool = expeditionSlotPool;
            _cityUI = cityUI;
            _globaEvents = globalEvents;
            _playerInventory = playerInventory;
            _saveManager = saveManager;
        }

        private void Start()
        {
            GenerateExpeditions();
        }

        private void GenerateExpeditions()
        {
            if (_slotsParent == null)
                Debug.LogWarning("Expedition slots parent shouldn't be null.");

            foreach (var slot in _expeditionSlots)
                _expeditionSlotPool.ReturnObject(slot);
            _expeditionSlots.Clear();

            // TODO: in future, do some random choice from generated dungeons
            foreach (var dungeonSettings in _dungeonSettings)
            {
                var slot = _expeditionSlotPool.GetObject();
                slot.transform.SetParent(_slotsParent);
                slot.SetDungeon(dungeonSettings);
                slot.transform.gameObject.SetActive(true);
                _expeditionSlots.Add(slot);
            }
        }

        public void DisplayDungeonInfo(DungeonSettings dungeonSettings)
        {
            if (!_dungeonDisplay.gameObject.activeInHierarchy)
                _dungeonDisplay.gameObject.SetActive(true);

            _dungeonDisplay.SetDisplayedDungeon(dungeonSettings);
            _displayedDungeon = dungeonSettings;
        }

        public async void ExecuteExpedition()
        {
            if (_mapWaiter != null)
            {
                Debug.LogError("Expedition is already running or wasn't ended properly.");
                return;
            }

            if (_displayedDungeon == null) return;

            _mapWaiter = new TaskCompletionSource<bool>();

            await SceneManager.LoadScene(Scenes.MapScene);

            var dungeonGenerator = FindObjectOfType<DungeonGenerator>();
            _dungeon = FindObjectOfType<Dungeon>();

            dungeonGenerator.GenerateDungeon(_displayedDungeon);
            _dungeon.OnDungeonCompleted += OnDungeonCompleted;

            _cityUI.gameObject.SetActive(false);
            await _mapWaiter.Task;

            await SceneManager.UnloadScene(Scenes.MapScene);
            _cityUI.gameObject.SetActive(true);
            _mapWaiter = null;

            _saveManager.SaveGameState();
            
            _globaEvents.SignalExpeditionEnded();
        }

        /// <summary>
        /// Sanctions player. Used when player dies in dungeon
        /// </summary>
        private void SanctionPlayer()
        {
            if (_playerInventory == null)
            {
                Debug.LogError($"Player inventory is missing in {name}.");
                return;
            }

            int shardsAmount = _playerInventory.GetItemAmount(Item.ElementShard);
            if (shardsAmount == 0) return;

            shardsAmount = Mathf.RoundToInt(shardsAmount * _playerDeathSanction);
            _playerInventory.RemoveItem(Item.ElementShard, shardsAmount);
        }

        private void OnDungeonCompleted(bool playerDied)
        {
            if (_mapWaiter == null)
            {
                Debug.LogError("Dungeon completed but map waiter was null.");
                return;
            }

            if (playerDied)
                SanctionPlayer();

            _dungeon.OnDungeonCompleted -= OnDungeonCompleted;
            _mapWaiter.SetResult(true);
        }

        private void OnDisable()
        {
            if (_dungeonDisplay == null) return;
            _dungeonDisplay.gameObject.SetActive(false);   
        }
    }
}