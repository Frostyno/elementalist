﻿using Elementalist.Character.Talents;
using Elementalist.Items;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Elementalist.UI.City
{
    [RequireComponent(typeof(Button))]
    public class TalentSlot : MonoBehaviour
    {
        [System.Serializable]
        public struct TalentSlotSaveData
        {
            public string TalentGUID { get; set; }
            public bool Purchased { get; set; }
        }
        
        private TalentShop _talentShop = null;
        private TextMeshProUGUI _buttonText = null;

        public bool Purchased { get; private set; }
        public Talent Talent { get; private set; }

        public void Init(Talent talent, TalentShop talentShop)
        {
            if (talent == null || talentShop == null)
            {
                Debug.LogError($"Talent or TalentShop is null. Cannot init TalentSlot.");
                return;
            }

            Talent = talent;
            _talentShop = talentShop;

            if (!TryGetComponent<Button>(out var button))
            {
                Debug.LogError($"TalentSlot on {name} doesn't have Button component.");
                return;
            }
            
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => _talentShop.DisplaySlotTalent(this));

            var buttonText = GetComponentInChildren<TextMeshProUGUI>();
            if (buttonText == null)
            {
                Debug.LogError($"Texts are missing for TalentSlot.");
                return;
            }

            _buttonText = buttonText;
            buttonText.text = Talent.TalentName;
        }

        public void ResetSlot()
        {
            Purchased = false;
            if (Talent != null) _buttonText?.SetText(Talent.TalentName);
        }

        public void Purchase()
        {
            if (Purchased) return;

            _buttonText?.SetText("(Purchased)" + Talent.TalentName);
            Purchased = true;
        }

        public TalentSlotSaveData GetState()
        {
            return new TalentSlotSaveData()
            {
                TalentGUID = Talent != null ? Talent.TalentGUID : "",
                Purchased = Purchased
            };
        }
    }
}
