﻿using UnityEngine;
using TMPro;
using Elementalist.Items;
using Zenject;
using System;
using Elementalist.Utility;

namespace Elementalist.UI.City
{
    public class CurrencyDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _labelText = null;
        [SerializeField] private TextMeshProUGUI _elementShardsText = null;
        [Space]
        [SerializeField] private ColorsPreset _colorsPreset = null;

        private Inventory _playerInventory = null;

        [Inject]
        private void Inject(Inventory playerInventory)
        {
            _playerInventory = playerInventory;

            _playerInventory.OnItemCollected += OnItemCollected;
            _playerInventory.OnItemRemoved += OnItemRemoved;
        }

        private void Awake()
        {
            _labelText.color = _colorsPreset.StandardColor;
            _elementShardsText.color = _colorsPreset.ValueColor;
        }

        private void UpdateCurrencyAmount(Item item)
        {
            switch (item)
            {
                case Item.ElementShard:
                    _elementShardsText.text = _playerInventory.GetItemAmount(item).ToString();
                    break;
            }
        }

        private void OnEnable()
        {
            UpdateCurrencyAmount(Item.ElementShard);
        }

        private void OnItemCollected(Item item)
        {
            UpdateCurrencyAmount(item);
        }

        private void OnItemRemoved(Item item)
        {
            UpdateCurrencyAmount(item);
        }

        private void OnDestroy()
        {
            _playerInventory.OnItemCollected -= OnItemCollected;
            _playerInventory.OnItemRemoved -= OnItemRemoved;
        }
    }
}
