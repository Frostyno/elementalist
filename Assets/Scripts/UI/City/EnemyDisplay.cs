﻿using Elementalist.Character;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Elementalist.UI.City
{
    public class EnemyDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Image _iconImage = null;

        private Tooltip.Tooltip _tooltip = null;
        private Elementalist.Character.Character _displayedEnemy = null;

        [Inject]
        private void Inject(Tooltip.Tooltip tooltip)
        {
            _tooltip = tooltip;
        }

        public void SetDisplayedEnemy(
            Elementalist.Character.Character enemy)
        {
            if (enemy == null) return;

            _displayedEnemy = enemy;
            // TODO: this is only a workaround for prefabs not getting injected, figure out different way to go about it
            _iconImage.sprite = _displayedEnemy.GetComponent<CharacterEssence>().CharacterIcon;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (_displayedEnemy == null) return;
            _tooltip?.DisplayTooltip(_displayedEnemy);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _tooltip?.HideTooltip();
        }
    }
}
