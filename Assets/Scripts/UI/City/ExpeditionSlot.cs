﻿using Elementalist.Map.Generation;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Elementalist.UI.City
{
    public class ExpeditionSlot : MonoBehaviour
    {
        private TextMeshProUGUI _dungeonNameText = null;

        private DungeonSettings _dungeonSettings = null;

        [Inject]
        private void Inject(
            TextMeshProUGUI dungeonNameText,
            Expeditions expeditions,
            Button button)
        {
            _dungeonNameText = dungeonNameText;
            button.onClick.AddListener(() =>
                {
                    if (_dungeonSettings == null) return;
                    expeditions.DisplayDungeonInfo(_dungeonSettings);
                });
        }

        public void SetDungeon(DungeonSettings dungeonSettings)
        {
            if (_dungeonSettings != null) return;

            _dungeonSettings = dungeonSettings;
            _dungeonNameText.text = _dungeonSettings.DungeonName;
        }
    }
}
