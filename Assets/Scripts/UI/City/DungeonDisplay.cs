﻿using Elementalist.Map.Generation;
using Elementalist.UI.Items;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.City
{
    public class DungeonDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _nameText = null;
        [SerializeField] private TextMeshProUGUI _descriptionText = null;
        [SerializeField] private RectTransform _enemies = null;
        [SerializeField] private RectTransform _drops = null;

        private ItemDisplayPool _itemDisplayPool = null;
        private EnemyDisplayPool _enemyDisplayPool = null;

        private List<ItemDisplay> _itemDisplays = new List<ItemDisplay>();
        private List<EnemyDisplay> _enemyDisplays = new List<EnemyDisplay>();

        [Inject]
        private void Inject(
            ItemDisplayPool itemDisplayPool,
            EnemyDisplayPool enemyDisplayPool)
        {
            _itemDisplayPool = itemDisplayPool;
            _enemyDisplayPool = enemyDisplayPool;
        }

        public void SetDisplayedDungeon(DungeonSettings dungeon)
        {
            ResetDisplay();

            _nameText?.SetText(dungeon.DungeonName);
            _descriptionText?.SetText(dungeon.DungeonDescription);

            foreach (var itemsByRarity in dungeon.ItemsByRarity)
            {
                foreach (var item in itemsByRarity.Items)
                {
                    var display = _itemDisplayPool.GetObject();
                    display.transform.SetParent(_drops);
                    display.SetDisplayedItem(item.ItemType);
                    display.gameObject.SetActive(true);
                    _itemDisplays.Add(display);
                }
            }

            foreach (var enemy in dungeon.EnemyPrefabs)
            {
                if (!enemy.Item.TryGetComponent<Elementalist.Character.Character>(out var enemyCharacter))
                    continue;

                var display = _enemyDisplayPool.GetObject();
                display.transform.SetParent(_enemies);
                display.SetDisplayedEnemy(enemyCharacter);
                display.gameObject.SetActive(true);
                _enemyDisplays.Add(display);
            }
        }

        private void ResetDisplay()
        {
            foreach (var display in _itemDisplays)
                _itemDisplayPool.ReturnObject(display);
            _itemDisplays.Clear();
            foreach (var display in _enemyDisplays)
                _enemyDisplayPool.ReturnObject(display);
            _enemyDisplays.Clear();
        }
    }
}
