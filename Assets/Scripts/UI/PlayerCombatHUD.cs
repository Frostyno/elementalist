﻿using Elementalist.Combat;
using UnityEngine;
using Zenject;

namespace Elementalist.UI
{
    public class PlayerCombatHUD : MonoBehaviour
    {
        private PlayerCombatController _playerController = null;

        [Inject]
        private void Inject(PlayerCombatController playerController)
        {
            _playerController = playerController;
        }

        public void EndPlayerTurn()
        {
            if (_playerController == null)
            {
                Debug.LogError("Player is missing in PlayerCombatHUD. Can't end turn.");
                return;
            }
            
            _playerController.EndTurn();
        }
    }
}
