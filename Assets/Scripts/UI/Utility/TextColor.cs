﻿namespace Elementalist.UI.Utility
{
    public enum TextColor
    {
        Text,
        Value,
        Fire,
        Water,
        Earth,
        Air
    }
}