﻿using Elementalist.Utility;
using TMPro;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Utility
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class ColorTextMarker : MonoBehaviour
    {
        [SerializeField] private TextColor _textColor = TextColor.Text;

        public TextColor TextColor => _textColor;
        public TextMeshProUGUI MarkedText { get; private set; }

        private TextColorPresetHolder _textColorPresetHolder = null;

        [Inject]
        private void Inject(
            TextMeshProUGUI text,
            TextColorPresetHolder textColorPresetHolder)
        {
            MarkedText = text;
            _textColorPresetHolder = textColorPresetHolder;
        }

        private void Awake()
        {
            MarkedText.color = _textColorPresetHolder.GetTextColor(_textColor);
        }
    }
}
