﻿using Elementalist.Utility;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Utility
{
    public class TextColorPresetHolder : MonoBehaviour
    {
        [SerializeField] private ColorsPreset _colorsPreset = null;

        private ColorTextMarker[] _markers = null;

        [Inject]
        private void Inject(ColorTextMarker[] markers)
        {
            _markers = markers;
        }

        public Color GetTextColor(TextColor color) => _colorsPreset.GetTextColor(color);
    }
}
