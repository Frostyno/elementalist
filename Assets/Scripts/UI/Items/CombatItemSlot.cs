﻿using System;
using Elementalist.Character;
using Elementalist.Character.Stats;
using Elementalist.Combat;
using Elementalist.Combat.Actions;
using Elementalist.Items;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Elementalist.UI.Items
{
    public class CombatItemSlot : ItemSlot
    {
        [Space]
        [SerializeField] private Image _unusableGraphics = null;

        private PlayerCombatController _playerController = null;
        
        [Inject]
        private void Inject(PlayerCombatController playerController)
        {
            _playerController = playerController;

            _playerController.GetComponent<Sources>().OnActionPointsChanged += OnPlayerActionPointsChanged;
            _playerController.OnActionExecuted += OnActionExecuted;
        }

        /// <summary>
        /// Checks if attached item is UsableItemType and if player can use it.
        /// If he can then it is set as active action in player controller.
        /// </summary>
        private void UseItem()
        {
            if (ItemStack.ItemType == null) return;
            if (ItemStack.ItemType.GetType() != typeof(UsableItemType)) return;

            var usableItem = ItemStack.ItemType as UsableItemType;
            if (!usableItem.CanExecute(_playerController.Character)) return;

            _playerController.SetAction(usableItem, this);
        }

        /// <summary>
        /// Checks if attached item is UsableItemType and if player has enough sources to use it in such a case.
        /// Updates UnusableGraphics accordingly.
        /// </summary>
        private void CheckUsable()
        {
            _unusableGraphics.gameObject.SetActive(false);

            if (ItemStack.ItemType == null) return;
            UsableItemType usableItem = ItemStack.ItemType as UsableItemType;
            if (usableItem == null) return;
            
            _unusableGraphics.gameObject.SetActive(!usableItem.CanExecute(_playerController.Character));
        }

        protected override void OnItemTypeChanged()
        {
            base.OnItemTypeChanged();

            CheckUsable();
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (ItemStack == null)
            {
                Debug.LogError($"ItemStack is null in {name}.");
                return;
            }

            base.OnPointerClick(eventData);

            UseItem();
        }

        private void OnActionExecuted(IAction action, object actionSource, bool success)
        {
            if (!success) return;
            if ((object)actionSource != this || (object)action != ItemStack.ItemType) return;
            
            ItemStack.RemoveItemsFromStack(1);
        }

        private void OnPlayerActionPointsChanged(ResourceChangedArgs args)
        {
            CheckUsable();
        }
        
        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (_playerController == null) return;

            _playerController.GetComponent<Sources>().OnActionPointsChanged -= OnPlayerActionPointsChanged;
            _playerController.OnActionExecuted -= OnActionExecuted;
        }
    }

    public class CombatItemSlotFactory : PlaceholderFactory<CombatItemSlot> { }
}
