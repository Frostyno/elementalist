﻿using Elementalist.Items;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Items
{
    public class PlayerInventoryUI : MonoBehaviour
    {
        private Inventory _playerInventory = null;
        private ItemSlot[] _slots = null;

        [Inject]
        private void Inject(
            Inventory inventory,
            ItemSlot[] slots)
        {
            _playerInventory = inventory;
            _slots = slots;
        }

        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            if (_playerInventory.ItemStacks.Count != _slots.Length)
                Debug.LogError($"Incorrect ammount of inventory slots in {name}. This will result in some slots not having attached item stack or some item stacks from inventory not displaying.");

            for (int i = 0; i < _playerInventory.ItemStacks.Count; i++)
            {
                if (i >= _slots.Length) break;
                _slots[i].ItemStack = _playerInventory.ItemStacks[i];
            }
        }
    }
}
