﻿using Elementalist.Items;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Elementalist.UI.Items
{
    /// <summary>
    /// Displays attached ItemType and activates tooltip when hovered over.
    /// </summary>
    public class ItemDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Image _iconImage = null;
        [SerializeField] private TextMeshProUGUI _amountText = null;

        private Tooltip.Tooltip _tooltip = null;
        private ItemType _displayedItem = null;
        private int? _displayedItemAmount = null;


        [Inject]
        private void Inject(Tooltip.Tooltip tooltip)
        {
            _tooltip = tooltip;
        }
        
        public void SetDisplayedItem(ItemType itemType, int? amount = null)
        {
            if (itemType == null) return;

            _displayedItem = itemType;
            _displayedItemAmount = amount;
            UpdateDisplay();
        }

        private void UpdateDisplay()
        {
            _iconImage.sprite = _displayedItem.Icon;
            _amountText.gameObject.SetActive(_displayedItemAmount.HasValue);
            if (!_displayedItemAmount.HasValue) return;
            _amountText?.SetText(_displayedItemAmount.Value.ToString());
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (_displayedItem == null) return;
            _tooltip?.DisplayTooltip(_displayedItem);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _tooltip?.HideTooltip();
        }
    }
}
