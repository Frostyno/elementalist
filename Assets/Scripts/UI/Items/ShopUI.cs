﻿using System;
using Elementalist.Items;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Items
{
    public class ShopUI : MonoBehaviour
    {
        [SerializeField] private RectTransform _shopItemSlotsParent = null;

        private Shop _shop = null;
        private Inventory _playerInventory = null;
        private ShopSliderPopup _sliderPopup = null;
        private MessagePopup _messagePopup = null;

        [Inject]
        private void Inject(
            Shop shop,
            Inventory playerInventory,
            ShopSliderPopup sliderPopup,
            MessagePopup messagePopup)
        {
            _shop = shop;
            _shop.OnItemsGenerated += OnItemsGenerated;

            _playerInventory = playerInventory;
            _sliderPopup = sliderPopup;
            _messagePopup = messagePopup;
            
            if (_shopItemSlotsParent == null)
            {
                Debug.LogError($"Shop item slots parent is missing in {name}.");
                return;
            }

            foreach (RectTransform child in _shopItemSlotsParent)
            {
                if (!child.TryGetComponent<ShopItemSlot>(out var slot)) continue;
                slot.IsShopSideSlot = true;
                slot.ItemStack = new ItemStack();
            }
        }

        private void UpdateShopItemSlots()
        {
            for (int i = 0; i < _shopItemSlotsParent.childCount; i++)
            {
                if (!_shopItemSlotsParent.GetChild(i).TryGetComponent<ShopItemSlot>(out var slot)) continue;
                slot.ItemStack.Clear();

                if (i >= _shop.GeneratedItems.Count) continue;

                slot.ItemStack.ItemType = _shop.GeneratedItems[i].ItemType;
                slot.ItemStack.AddItemsToStack(_shop.GeneratedItems[i].Amount);
            }
        }

        private int GetShopItemAmount(ItemType itemType)
        {
            int amount = 0;
            foreach (RectTransform child in _shopItemSlotsParent)
            {
                if (!child.TryGetComponent<ShopItemSlot>(out var slot)) continue;

                if (slot.ItemStack.ItemType == itemType) amount += slot.ItemStack.Amount;
            }

            return amount;
        }

        private bool RemoveShopItem(ItemType itemType, int amount)
        {
            foreach (RectTransform child in _shopItemSlotsParent)
            {
                if (!child.TryGetComponent<ShopItemSlot>(out var slot)) continue;
                if (slot.ItemStack.ItemType != itemType) continue;
                if (amount == 0) break;

                amount = slot.ItemStack.RemoveItemsFromStack(amount);
            }

            return amount == 0;
        }

        public async void DisplaySellingPopup(ItemType itemType)
        {
            int maxSliderValue = _playerInventory.GetItemAmount(itemType.Item);

            (int amount, bool confirmed) = await _sliderPopup.DisplayPopup(itemType, 0, maxSliderValue, true);

            if (!confirmed) return;

            _shop.SellItem(itemType, amount);
        }

        public async void DisplayBuyingPopup(ItemType itemType)
        {
            int maxSliderValue = GetShopItemAmount(itemType);
            int freeSpace = _playerInventory.GetFreeSpace(itemType);
            if (freeSpace >= 0) maxSliderValue = Math.Min(freeSpace, maxSliderValue);
            if (maxSliderValue > 0) maxSliderValue = Math.Min(maxSliderValue, _playerInventory.GetItemAmount(Item.ElementShard) / itemType.BuyPrice);

            if (maxSliderValue == 0)
            {
                await _messagePopup?.DisplayMessage($"You don't have enough shards to buy {itemType.ItemName}.");
                return;
            }

            (int amount, bool confirmed) = await _sliderPopup.DisplayPopup(itemType, 0, maxSliderValue, false);

            if (!confirmed) return;

            bool success = _shop.BuyItem(itemType, amount);

            if (!success) return;

            RemoveShopItem(itemType, amount);
        }

        private void OnItemsGenerated()
        {
            UpdateShopItemSlots();
        }

        private void OnEnable()
        {
            UpdateShopItemSlots();
        }

        private void OnDestroy()
        {
            if (_shop != null) _shop.OnItemsGenerated -= OnItemsGenerated;
        }
    }
}
