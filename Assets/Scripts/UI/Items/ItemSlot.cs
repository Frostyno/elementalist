﻿using Elementalist.Items;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Elementalist.UI.Items
{
    public class ItemSlot : MonoBehaviour, IPointerClickHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [Header("Dependencies")]
        [SerializeField] private RectTransform _dragGraphicsPivot = null;
        [SerializeField] private Canvas _dragGraphicsCanvas = null;
        [SerializeField] private Image _iconImage = null;
        [SerializeField] private TextMeshProUGUI _amountText = null;

        [Space]
        [Tooltip("Specifies what sorting order should dragged graphics element have.")]
        [SerializeField] private int _dragSortingOrder = 100;

        private GraphicRaycaster _raycaster = null;
        private Tooltip.Tooltip _tooltip = null;
        private List<RaycastResult> _raycastHits = new List<RaycastResult>();
        private ReadOnlyCollection<RaycastResult> _observableHits = null;

        private ItemStack _itemStack = null;
        public ItemStack ItemStack
        {
            get => _itemStack;
            set
            {
                if (_itemStack == null)
                {
                    _itemStack = value;
                    _itemStack.OnItemTypeChanged += OnItemTypeChanged;
                    _itemStack.OnAmountChanged += OnAmountChanged;
                    UpdateDisplay();
                }
                else
                    Debug.LogWarning($"Trying to set allready occupied ItemStack in {name}.");
            }
        }

        private Vector2 _iconImageInitialPosition = Vector2.zero;
        private int _originalSortingOrder = 0;
        private bool _isDragged = false;

        [Inject]
        private void Inject(GraphicRaycaster raycaster, Tooltip.Tooltip tooltip)
        {
            _raycaster = raycaster;
            _tooltip = tooltip;

            _observableHits = new ReadOnlyCollection<RaycastResult>(_raycastHits);
        }

        /// <summary>
        /// Updates graphic components attacheed to this item slot, such as
        /// amount text and icon image.
        /// </summary>
        private void UpdateDisplay()
        {
            if (_iconImage == null || _amountText == null) return;

            bool hasItem = _itemStack.ItemType != null;

            _iconImage.gameObject.SetActive(hasItem);
            _amountText.gameObject.SetActive(hasItem);

            if (!hasItem) return;

            if (_itemStack.ItemType.GetType() == typeof(TrinketType))
                _amountText.gameObject.SetActive(false);

            _iconImage.sprite = _itemStack.ItemType.Icon;
            _amountText.text = _itemStack.Amount.ToString();
        }

        /// <summary>
        /// Exchanges contents of this item slot with the other slot.
        /// </summary>
        /// <param name="otherSlot">Slot to exchange contents with.</param>
        private void ExchangeContents(ItemSlot otherSlot)
        {
            // store contents of the other slot
            ItemTypeAmountPair tmp = new ItemTypeAmountPair();
            tmp.ItemType = otherSlot.ItemStack.ItemType;
            tmp.Amount = otherSlot.ItemStack.Amount;
            otherSlot.ItemStack.Clear();

            // if this slot has an item, transfer it to the other slot
            if (ItemStack.ItemType != null)
            {
                (ItemType itemType, int amount) = (ItemStack.ItemType, ItemStack.Amount);
                ItemStack.Clear();
                otherSlot.ItemStack.ItemType = itemType;
                otherSlot.ItemStack.AddItemsToStack(amount);
            }

            // if there was an item in the other slot, transfer it to this slot
            if (tmp.ItemType != null)
            {
                ItemStack.ItemType = tmp.ItemType;
                ItemStack.AddItemsToStack(tmp.Amount);
            }
        }

        /// <summary>
        /// Merges contents of this item slot with the other slot.
        /// </summary>
        /// <param name="otherSlot">Slot to merge contents with.</param>
        private void MergeContents(ItemSlot otherSlot)
        {
            int amountToTransfer = -1;
            // if ItemType's stack size is infinite
            if (ItemStack.ItemType.StackSize <= 0)
            {
                amountToTransfer = ItemStack.Amount;
                ItemStack.Clear();
                otherSlot.ItemStack.AddItemsToStack(amountToTransfer);
                return;
            }

            // find out how much items can be transfered to other slot
            var freeSpace = otherSlot.ItemStack.ItemType.StackSize - otherSlot.ItemStack.Amount;
            amountToTransfer = freeSpace > ItemStack.Amount ? ItemStack.Amount : freeSpace;

            // transfer
            ItemStack.RemoveItemsFromStack(amountToTransfer);
            otherSlot.ItemStack.AddItemsToStack(amountToTransfer);
            return;
        }

        #region EventCallbacks

        protected virtual void OnItemTypeChanged()
        {
            UpdateDisplay();
        }

        private void OnAmountChanged()
        {
            UpdateDisplay();
        }

        public virtual void OnBeginDrag(PointerEventData eventData)
        {
            _isDragged = true;
            _tooltip.HideTooltip();

            _iconImageInitialPosition = _dragGraphicsPivot.localPosition;

            // override sorting order so that dragged images are always on top of other UI elements
            _originalSortingOrder = _dragGraphicsCanvas.sortingOrder;
            _dragGraphicsCanvas.overrideSorting = true;
            _dragGraphicsCanvas.sortingOrder = _dragSortingOrder;
        }

        public virtual void OnDrag(PointerEventData eventData)
        {
            // move graphic components around
            _dragGraphicsPivot.position = Input.mousePosition;
        }

        public virtual void OnEndDrag(PointerEventData eventData)
        {
            _isDragged = false;

            // reset graphic components position and sorting order
            _dragGraphicsPivot.localPosition = _iconImageInitialPosition;
            _dragGraphicsCanvas.sortingOrder = _originalSortingOrder;
            _dragGraphicsCanvas.overrideSorting = false;

            // get all UI elements on position where user stopped dragging
            _raycastHits.Clear();
            _raycaster.Raycast(eventData, _raycastHits);

            foreach (var hit in _raycastHits)
            {
                var otherSlot = hit.gameObject.GetComponent<ItemSlot>();
                if (otherSlot == null) continue;
                if (otherSlot == this) break;

                if (ItemStack.ItemType == otherSlot.ItemStack.ItemType)
                    MergeContents(otherSlot);
                else
                    ExchangeContents(otherSlot);

                break;
            }
        }

        public virtual void OnPointerClick(PointerEventData eventData) { }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (ItemStack.ItemType == null) return;
            if (_isDragged) return;

            _tooltip.DisplayTooltip(ItemStack.ItemType);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _tooltip.HideTooltip();
        }

        protected virtual void OnDestroy()
        {
            if (_itemStack == null) return;

            _itemStack.OnItemTypeChanged -= OnItemTypeChanged;
            _itemStack.OnAmountChanged -= OnAmountChanged;
        }

        #endregion
    }
}
