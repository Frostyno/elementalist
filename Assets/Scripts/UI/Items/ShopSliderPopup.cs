﻿using Elementalist.Items;
using System;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Elementalist.UI.Items
{
    public class ShopSliderPopup : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _popupText = null;
        [SerializeField] private TextMeshProUGUI _minValueText = null;
        [SerializeField] private TextMeshProUGUI _maxValueText = null;
        [SerializeField] private Slider _slider = null;

        [Space]

        [SerializeField, TextArea] private string _buyingDescription = "Buy {0} {1} for {2} element shards";
        [SerializeField, TextArea] private string _sellingDescription = "Sell {0} {1} for {2} element shards";

        private TaskCompletionSource<bool> _confirmWaiter = null;

        private ItemType _currentItemType = null;
        private bool _selling = false;

        private void Awake()
        {
            _slider.onValueChanged.AddListener((currentValue) => OnValueChanged(currentValue));
        }

        private void UpdateDisplay()
        {
            string format = _selling ? _sellingDescription : _buyingDescription;

            _popupText.text = string.Format(_sellingDescription, Mathf.RoundToInt(_slider.value), _currentItemType.ItemName, CalculatePrice());
        }

        private int CalculatePrice()
        {
            if (_currentItemType == null) return 0;

            int price = _selling ? _currentItemType.SellPrice : _currentItemType.BuyPrice;
            return Mathf.RoundToInt(price * _slider.value);
        }

        public async Task<(int, bool)> DisplayPopup(ItemType itemType, int minValue, int maxValue, bool selling)
        {
            if (_confirmWaiter != null) return (0, false);
            if (minValue < 0 || maxValue < 0 || minValue > maxValue) return (0, false);

            gameObject.SetActive(true);

            _currentItemType = itemType;
            _selling = selling;

            _minValueText.text = minValue.ToString();
            _maxValueText.text = maxValue.ToString();
            _slider.minValue = minValue;
            _slider.maxValue = maxValue;
            _slider.value = minValue;
            UpdateDisplay();

            _confirmWaiter = new TaskCompletionSource<bool>();

            bool confirmed = await _confirmWaiter.Task;
            _confirmWaiter = null;

            gameObject.SetActive(false);

            return (Mathf.RoundToInt(_slider.value), confirmed);
        }

        public void Confirm()
        {
            if (_confirmWaiter == null) return;
            _confirmWaiter.SetResult(true);
        }

        public void Cancel()
        {
            if (_confirmWaiter == null) return;
            _confirmWaiter.SetResult(false);
        }

        private void OnValueChanged(float currentValue)
        {
            UpdateDisplay();
        }
    }
}
