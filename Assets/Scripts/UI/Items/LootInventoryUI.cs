﻿using Elementalist.Items;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Items
{
    public class LootInventoryUI : MonoBehaviour
    {
        private ItemSlot[] _slots = null;

        [Inject]
        private void Inject(ItemSlot[] slots)
        {
            _slots = slots;

            foreach (var slot in _slots)
            {
                slot.ItemStack = new ItemStack();
            }
        }

        public void SetLootItems(ItemTypeAmountPair[] items)
        {
            ClearLoot();

            if (items.Length > _slots.Length)
                Debug.LogError($"Not enough slots for loot in {name}. Not all loot will be displayed.");

            for (int i = 0; i < items.Length; i++)
            {
                if (i >= _slots.Length) break;
                
                _slots[i].ItemStack.ItemType = items[i].ItemType;
                _slots[i].ItemStack.AddItemsToStack(items[i].Amount);
            }
        }

        public void ClearLoot()
        {
            foreach (var slot in _slots)
                slot.ItemStack.Clear();
        }
    }
}
