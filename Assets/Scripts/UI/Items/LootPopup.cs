﻿using Elementalist.Items;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Elementalist.UI.Items
{
    // TODO: consider merging with LootInventoryUI
    public class LootPopup : MonoBehaviour
    {
        private LootInventoryUI _lootInventory = null;

        private TaskCompletionSource<bool> _tcs = null;

        [Inject]
        private void Inject(LootInventoryUI lootInventory)
        {
            _lootInventory = lootInventory;
        }

        public async Task DisplayPopup(ItemTypeAmountPair[] items)
        {
            _lootInventory.SetLootItems(items);
            gameObject.SetActive(true);

            _tcs = new TaskCompletionSource<bool>();

            await _tcs.Task;

            _tcs = null;
        }

        public void HidePopup()
        {
            if (_tcs == null) return;

            _lootInventory.ClearLoot();
            _tcs.SetResult(true);

            gameObject.SetActive(false);
        }
    }
}
