﻿using Elementalist.Items;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Elementalist.UI.Items
{
    public class ShopItemSlot : ItemSlot
    {
        private ShopUI _shopUI = null;

        public bool IsShopSideSlot { get; set; }

        [Inject]
        private void Inject(ShopUI shopUI)
        {
            _shopUI = shopUI;
        }

        // override drag functionality
        public override void OnBeginDrag(PointerEventData eventData) { }
        public override void OnDrag(PointerEventData eventData) { }
        public override void OnEndDrag(PointerEventData eventData) { }

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (ItemStack.ItemType == null) return;
            if (ItemStack.ItemType.Item == Item.ElementShard) return;

            if (IsShopSideSlot)
                _shopUI.DisplayBuyingPopup(ItemStack.ItemType);
            else
                _shopUI.DisplaySellingPopup(ItemStack.ItemType);
        }
    }
}
