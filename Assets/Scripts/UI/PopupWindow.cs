﻿using System;
using UnityEngine;

namespace Elementalist.UI
{
    public abstract class PopupWindow : MonoBehaviour
    {
        protected bool _isPriorityWindow = false;

        public event Action<PopupWindow, bool> OnPopupOpened = null;
        public event Action<PopupWindow> OnPopupClosed = null;

        public void OpenWindow()
        {
            gameObject.SetActive(true);
            OnPopupOpened?.Invoke(this, _isPriorityWindow);
        }

        public void CloseWindow()
        {
            gameObject.SetActive(false);
            OnPopupClosed.Invoke(this);
        }

        public void ToggleWindow()
        {
            if (!gameObject.activeInHierarchy)
                OpenWindow();
            else
                CloseWindow();
        }
    }
}