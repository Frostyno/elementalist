﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Elementalist.AI.BehaviorTree;

namespace Elementalist.AI.Testing
{
    [Serializable]
    [DataContract]
    public class BTMockUpNode : BTNode
    {
        [DataMember(Name = "TestResult")]
        private BTProcessResult _testResult = BTProcessResult.Failure;

        public BTMockUpNode(BTProcessResult result) => _testResult = result;

        public override Task<BTProcessResult> Process(BTContext context)
        {
            return Task.FromResult(_testResult);
        }
    }
}
