﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Elementalist.AI.BehaviorTree;
using Elementalist.Character.Stats;
using Elementalist.Combat;
using UnityEngine;

namespace Elementalist.AI.CombatNodes
{
    [Serializable]
    [DataContract, KnownType(typeof(ConditionTarget)), KnownType(typeof(StatType)),
        KnownType(typeof(ElementType?))]
    public class StatConditionNode : NumericConditionNode
    {
        [DataMember(Name = "StatType")]
        private StatType _statType = StatType.Health;
        [DataMember(Name = "ElementType")]
        private ElementType? _elementType = null;
        [DataMember(Name = "UseMaxValue")]
        private bool _useMaxValue = false;

        [DataMember(Name = "TargetValue")]
        private double _targetValue = 0d;
        [DataMember(Name = "ConditionTarget")]
        private ConditionTarget _targetType = ConditionTarget.Target;

        public StatConditionNode() { }
        public StatConditionNode(
            StatType statType,
            double targetValue,
            ConditionTarget targetType,
            ComparisonType comparisonType,
            ElementType? elementType = null,
            bool maxValue = false
            )
        {
            _statType = statType;
            _elementType = elementType;
            _useMaxValue = maxValue;
            _targetValue = targetValue;
            _targetType = targetType;
            this.comparisonType = comparisonType;
        }

        /// <summary>
        /// Evaluates if condition defined in fields is met.
        /// </summary>
        /// <param name="context">Context of behavior tree.</param>
        /// <returns>Success if condition is true.</returns>
        public override Task<BTProcessResult> Process(BTContext context)
        {
            var combatContext = context.GetDataItem<CombatContext>();
            if (combatContext == null)
            {
                Debug.LogError("Missing combat context when checking for stats.");
                return Task.FromResult(BTProcessResult.Failure);
            }

            CombatController condTarget = null;
            if (_targetType == ConditionTarget.Source) condTarget = combatContext.Source;
            else condTarget = combatContext.Target;

            if (condTarget == null || condTarget.Character == null)
            {
                Debug.LogError("Missing condition target when checking stats.");
                return Task.FromResult(BTProcessResult.Failure);
            }

            var conditionResult = Compare(condTarget.Character.GetStatValue(_statType, _elementType, _useMaxValue), _targetValue);
            var result = conditionResult ? BTProcessResult.Success : BTProcessResult.Failure;

            return Task.FromResult(result);
        }
    }
}
