﻿using Elementalist.AI.BehaviorTree;
using System;
using System.Runtime.Serialization;

namespace Elementalist.AI.CombatNodes
{
    [Serializable]
    [DataContract, KnownType(typeof(ComparisonType))]
    public abstract class NumericConditionNode : BTNode
    {
        [DataMember(Name = "ComparisonType")]
        protected ComparisonType comparisonType = ComparisonType.Less;

        /// <summary>
        /// Compares values in parameters and returns comparison result.
        /// Comparison type is defined in comparisonType field of NumericConditionNode.
        /// </summary>
        /// <param name="value">Actual value.</param>
        /// <param name="targetValue">Condition target value.</param>
        /// <param name="epsilon">Floating point inaccuracy tolerance in ComparisonType.Equal.</param>
        /// <returns></returns>
        protected bool Compare(double value, double targetValue, double epsilon = 0.001d)
        {
            switch (comparisonType)
            {
                case ComparisonType.Equal:
                    return Math.Abs(targetValue - value) <= epsilon;
                case ComparisonType.Greater:
                    return targetValue < value;
                case ComparisonType.GreaterOrEqual:
                    return targetValue <= value;
                case ComparisonType.Less:
                    return targetValue > value;
                case ComparisonType.LessOrEqual:
                    return targetValue >= value;
                default:
                    return false;
            }
        }
    }
}
