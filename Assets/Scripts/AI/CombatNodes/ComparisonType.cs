﻿using System;
using System.Runtime.Serialization;

namespace Elementalist.AI.CombatNodes
{
    [Serializable]
    [DataContract]
    public enum ComparisonType
    {
        [EnumMember]Equal,
        [EnumMember]Greater,
        [EnumMember]GreaterOrEqual,
        [EnumMember]Less,
        [EnumMember]LessOrEqual
    }
}