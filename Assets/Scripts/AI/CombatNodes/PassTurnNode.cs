﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Elementalist.AI.BehaviorTree;
using Elementalist.Combat;

namespace Elementalist.AI.CombatNodes
{
    [Serializable]
    [DataContract]
    public class PassTurnNode : BTNode
    {
        public override Task<BTProcessResult> Process(BTContext context)
        {
            CombatContext combatContext = context.GetDataItem<CombatContext>();

            if (combatContext == null) return Task.FromResult(BTProcessResult.Failure);

            combatContext.Source.EndTurn();

            return Task.FromResult(BTProcessResult.Success);
        }
    }
}
