﻿using System;
using System.Runtime.Serialization;

namespace Elementalist.AI.CombatNodes
{
    [Serializable]
    [DataContract]
    public enum ConditionTarget
    {
        [EnumMember]Source,
        [EnumMember]Target
    }
}