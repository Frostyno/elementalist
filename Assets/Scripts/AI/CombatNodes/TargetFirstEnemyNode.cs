﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Elementalist.AI.BehaviorTree;
using Elementalist.Combat;
using UnityEngine;

namespace Elementalist.AI.CombatNodes
{
    [Serializable]
    [DataContract]
    public class TargetFirstEnemyNode : BTNode
    {
        public override Task<BTProcessResult> Process(BTContext context)
        {
            var combatContext = context.GetDataItem<CombatContext>();
            // Check if nothing required is missing
            if (combatContext == null || combatContext.CombatArea == null || combatContext.Source == null)
            {
                Debug.LogError("CombatArea or Source is null when trying to target first enemy.");
                return Task.FromResult(BTProcessResult.Failure);
            }
            else if (combatContext.Source.Team == null)
            {
                Debug.LogError("Team is null when trying to target first enemy.");
                return Task.FromResult(BTProcessResult.Failure);
            }

            // Find position that holds enemy closest to source combatant
            var enemySide = combatContext.Source.Team.Side == CombatSide.Left ? CombatSide.Right : CombatSide.Left;
            var targetPosition = combatContext.CombatArea.GetFirstCombatantPosition(enemySide);
            if (targetPosition == null)
            {
                Debug.LogError("No enemy was found when tryinng to target first enemy.");
                return Task.FromResult(BTProcessResult.Failure);
            }

            combatContext.Target = targetPosition.Combatant;

            return Task.FromResult(BTProcessResult.Success);
        }
    }
}
