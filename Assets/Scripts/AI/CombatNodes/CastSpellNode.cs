﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Elementalist.AI.BehaviorTree;
using Elementalist.Combat;
using Elementalist.Combat.Actions;
using UnityEngine;

namespace Elementalist.AI.CombatNodes
{
    [Serializable]
    [DataContract, KnownType(typeof(Spell?))]
    public class CastSpellNode : BTNode
    {
        [DataMember(Name = "SpellToCast")]
        private Spell? _spellToCast = null;

        public CastSpellNode() { }
        public CastSpellNode(Spell spellToCast) => _spellToCast = spellToCast;

        public async override Task<BTProcessResult> Process(BTContext context)
        {
            var combatContext = context.GetDataItem<CombatContext>();
            if (combatContext == null || combatContext.CombatArea == null || 
                combatContext.Source == null || combatContext.Target == null)
            {
                Debug.LogError("References missing when casting spell.");
                return BTProcessResult.Failure;
            }
            else if (_spellToCast == null)
            {
                Debug.LogError(string.Format("Spell is not set when trying to cast spell for {0}", combatContext.Source.name));
                return BTProcessResult.Failure;
            }

            if (!combatContext.Source.Character.HasSpell(_spellToCast.Value))
            {
                Debug.LogError(string.Format("{0} does not have {1} spell when trying to cast spell.", 
                    combatContext.Source.name, _spellToCast.Value));
                return BTProcessResult.Failure;
            }
            
            var spell = combatContext.Source.Character.GetSpell(_spellToCast.Value);

            var executeResult = await spell.Execute(combatContext.Source, combatContext.Target, combatContext.CombatArea);

            var result = executeResult ? BTProcessResult.Success : BTProcessResult.Failure;

            return result;
        }
    }
}
