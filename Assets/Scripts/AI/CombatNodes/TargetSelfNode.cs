﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Elementalist.AI.BehaviorTree;
using Elementalist.Combat;
using UnityEngine;

namespace Elementalist.AI.CombatNodes
{
    [Serializable]
    [DataContract]
    public class TargetSelfNode : BTNode
    {
        public override Task<BTProcessResult> Process(BTContext context)
        {
            var combatContext = context.GetDataItem<CombatContext>();
            // Check if nothing required is missing
            if (combatContext == null || combatContext.Source == null)
            {
                Debug.LogError("Source is null when trying to target self.");
                return Task.FromResult(BTProcessResult.Failure);
            }

            combatContext.Target = combatContext.Source;

            return Task.FromResult(BTProcessResult.Success);
        }
    }
}
