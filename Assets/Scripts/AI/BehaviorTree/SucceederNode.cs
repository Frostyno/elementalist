﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Elementalist.AI.BehaviorTree
{
    [Serializable]
    [DataContract]
    public class SucceederNode : DecoratorBTNode
    {
        public SucceederNode() { }
        public SucceederNode(BTNode child) => this.child = child;

        public async override Task<BTProcessResult> Process(BTContext context)
        {
            await child.Process(context);
            return BTProcessResult.Success;
        }
    }
}
