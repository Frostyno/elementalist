﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Elementalist.AI.BehaviorTree
{
    [Serializable]
    [DataContract]
    public class ChanceSucceederNode : DecoratorBTNode
    {
        private Random _chanceGenerator = null;
        [DataMember(Name = "SucceedChance")]
        private double _succeedChance = 1d;
        
        public ChanceSucceederNode() { }
        public ChanceSucceederNode(BTNode child, double succeedChance)
        {
            this.child = child;
            _succeedChance = succeedChance;
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            _chanceGenerator = new Random();
        }

        public async override Task<BTProcessResult> Process(BTContext context)
        {
            var result = await child.Process(context);
            if (result == BTProcessResult.Success) return result;

            if (_chanceGenerator == null) _chanceGenerator = new Random();

            return _chanceGenerator.NextDouble() < _succeedChance ? BTProcessResult.Success : result;
        }
    }
}
