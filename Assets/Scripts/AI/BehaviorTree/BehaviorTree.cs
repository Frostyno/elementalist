﻿using Elementalist.AI.CombatNodes;
using Elementalist.Character.Stats;
using Elementalist.Combat.Actions;
using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Elementalist.AI.BehaviorTree
{
    [Serializable]
    [DataContract, KnownType("GetKnownTypes")]
    public class BehaviorTree
    {
        /// <summary>
        /// Gets an array of types consisting of BTNode and all of it's subclasses.
        /// </summary>
        /// <returns>Array of types consisting of BTNode and all of it's subclasses</returns>
        private static Type[] GetKnownTypes()
        {
            var types = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsSubclassOf(typeof(BTNode))).ToList();
            types.Add(typeof(BTNode));
            return types.ToArray();
        }

        [DataMember(Name = "Root")]
        private BTNode _root = null;

        public BehaviorTree() { }
        public BehaviorTree(BTNode root) => _root = root;

        public void Inicialize()
        {
            if (_root != null)
                _root.Initialize();
        }

        public async Task<BTProcessResult> Execute(BTContext context)
        {
            var result = BTProcessResult.Failure;

            if (_root != null)
                result = await _root.Process(context);

            return result;
        }
    }
}
