﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Elementalist.AI.BehaviorTree
{
    [Serializable]
    [DataContract]
    public class SequenceNode : CompositeBTNode
    {
        public SequenceNode() { }
        public SequenceNode(List<BTNode> children) => this.children = children;

        /// <summary>
        /// Processes child nodes until all are processed or a child returns Failure.
        /// </summary>
        /// <param name="context">Context of the behavior tree.</param>
        /// <returns>Success if all of the children return Success.</returns>
        public async override Task<BTProcessResult> Process(BTContext context)
        {
            foreach (var child in children)
            {
                var result = await child.Process(context);
                if (result == BTProcessResult.Failure) return result;
            }

            return BTProcessResult.Success;
        }
    }
}
