﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Elementalist.AI.BehaviorTree
{
    [Serializable]
    [DataContract]
    public class InverterNode : DecoratorBTNode
    {
        public InverterNode() { }
        public InverterNode(BTNode child) => this.child = child;

        public async override Task<BTProcessResult> Process(BTContext context)
        {
            var result = await child.Process(context);
            result = result == BTProcessResult.Success ? BTProcessResult.Failure : BTProcessResult.Success;

            return result;
        }
    }
}
