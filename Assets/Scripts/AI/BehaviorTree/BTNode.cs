﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Elementalist.AI.BehaviorTree
{
    [Serializable]
    [DataContract]
    public abstract class BTNode
    {
        public virtual void Initialize() { }
        public abstract Task<BTProcessResult> Process(BTContext context);
    }
}
