﻿using System;
using System.Runtime.Serialization;

namespace Elementalist.AI.BehaviorTree
{
    [Serializable]
    [DataContract]
    public enum BTProcessResult
    {
        [EnumMember]Success,
        [EnumMember]Failure
    }
}
