﻿using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Elementalist.AI.BehaviorTree
{
    [Serializable]
    [DataContract]
    public class ChanceNode : BTNode
    {
        [DataMember(Name = "SuccessProbability")]
        private double _successProbability = 0.5d;

        public override Task<BTProcessResult> Process(BTContext context)
        {
            double chance = UnityEngine.Random.Range(0f, 1f);
            
            return Task.FromResult(chance < _successProbability ? BTProcessResult.Success : BTProcessResult.Failure);
        }
    }
}
