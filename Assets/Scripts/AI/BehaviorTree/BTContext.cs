﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Elementalist.AI.BehaviorTree
{
    public class BTContext
    {
        private Dictionary<(Type, string), object> _data = null;

        public int ItemsCount { get => _data.Count; }

        public BTContext()
        {
            _data = new Dictionary<(Type, string), object>();
        }

        public void SetDataItem<TData>(TData value, string key = "")
            where TData : class
        {
            _data[(typeof(TData), key)] = value;
        }

        /// <summary>
        /// Gets a data item from context.
        /// </summary>
        /// <typeparam name="TData">Type of data to get.</typeparam>
        /// <param name="key">Optional key if multiple instances are present.</param>
        /// <returns>Found data item or null if not found.</returns>
        public TData GetDataItem<TData>(string key = "")
            where TData : class
        {
            if (_data.TryGetValue((typeof(TData), key), out var ret))
                return (TData)ret;

            return null;
        }
    }
}
