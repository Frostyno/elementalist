﻿using System;
using System.Runtime.Serialization;

namespace Elementalist.AI.BehaviorTree
{
    [Serializable]
    [DataContract]
    public abstract class DecoratorBTNode : BTNode
    {
        [DataMember(Name = "Child")]
        protected BTNode child = null;

        public override void Initialize()
        {
            base.Initialize();

            if (child != null) child.Initialize();
        }
    }
}
