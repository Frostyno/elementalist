﻿using System;
using System.IO;
using System.Runtime.Serialization;
using UnityEngine;

namespace Elementalist.AI.BehaviorTree
{
    public class BehaviorTreeLoader
    {
        private string _xmlFolderPath = null;

        public BehaviorTreeLoader()
        {
            _xmlFolderPath = Path.Combine(Application.streamingAssetsPath, "BehaviorTreeXMLs");
        }

        public BehaviorTree LoadBehaviorTree(string fileName)
        {
            BehaviorTree ret = null;

            var files = Directory.GetFiles(_xmlFolderPath, fileName + ".xml", SearchOption.AllDirectories);
            if (files.Length == 0)
            {
                Debug.LogError(string.Format("BehaviorTree xml file with name \"{0}\" not found in \"{1}\" or subdirectories.", fileName, _xmlFolderPath));
                return null;
            }
            else if (files.Length > 1)
                Debug.LogWarning(string.Format("Multiple BehaviorTree xml files with name \"{0}\" found in \"{1}\" or subdirectories. Using \"{2}\".",
                    fileName, _xmlFolderPath, files[0]));

            using (FileStream stream = new FileStream(files[0], FileMode.Open))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(BehaviorTree));
                try
                {
                    ret = serializer.ReadObject(stream) as BehaviorTree;
                }
                catch (Exception e)
                {
                    Debug.LogError(string.Format("Loading BehaviorTree \"{0}\" failed. Error: {1}", files[0], e.Message));
                    ret = null;
                }
            }

            return ret;
        }
    }
}
