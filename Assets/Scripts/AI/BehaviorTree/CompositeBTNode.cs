﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Elementalist.AI.BehaviorTree
{
    [Serializable]
    [DataContract]
    public abstract class CompositeBTNode : BTNode
    {
        [DataMember(Name = "Children")]
        protected List<BTNode> children = new List<BTNode>();

        public override void Initialize()
        {
            base.Initialize();

            foreach (var child in children)
                child.Initialize();
        }
    }
}
