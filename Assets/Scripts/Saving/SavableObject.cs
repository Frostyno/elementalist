﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Elementalist.Saving
{
    public class SavableObject : MonoBehaviour
    {
        [SerializeField] private string _key = "";
        public string Key => _key;

        public Dictionary<(Type, string), object> State => CreateState();

#if UNITY_EDITOR
        /// <summary>
        /// Used to automatically generate GUID when object is assigned to game object.
        /// Only called in editor.
        /// </summary>
        private void OnValidate()
        {
            AssignGUID();
        }

        /// <summary>
        /// Generates GUID for this savable object.
        /// </summary>
        private void AssignGUID()
        {
            if (!string.IsNullOrEmpty(_key)) return;

            _key = GUID.Generate().ToString();
        }
#endif

        /// <summary>
        /// Creates state dictionary that stores data for all ISavable components attached
        /// to this game object.
        /// NOTE: All objects passed intoo the dictionary as states must be Serializable.
        /// </summary>
        /// <returns>Dictionary containing state of game object this component is attached to</returns>
        private Dictionary<(Type, string), object> CreateState()
        {
            Dictionary<(Type, string), object> state = new Dictionary<(Type, string), object>();

            foreach (var savable in GetComponents<ISavable>())
            {
                if (state.ContainsKey((savable.GetType(), savable.GetKey())))
                {
                    Debug.LogError($"Object {name} contains two components with same type {savable.GetType().ToString()} and key {savable.GetKey()}.");
                    continue;
                }

                state[(savable.GetType(), savable.GetKey())] = savable.GetState();
            }

            return state;
        }

        /// <summary>
        /// Loads state for all ISavable components attached to this game object.
        /// State of individual components must be present in passed object to be loaded.
        /// </summary>
        /// <param name="stateObject">State object containing this game object's state.</param>
        public void LoadState(object stateObject)
        {
            var state = stateObject as Dictionary<(Type, string), object>;
            if (state == null)
            {
                Debug.LogError($"Invalid object passed as parameter to {name}'s LoadState function. Object's type: {stateObject.GetType().ToString()}. Expected Dictionary<(Type, string), object>");
                return;
            }

            foreach (var savable in GetComponents<ISavable>())
            {
                if (!state.TryGetValue((savable.GetType(), savable.GetKey()), out var savedState))
                {
                    Debug.LogWarning($"Component with type {savable.GetType().ToString()} and key {savable.GetKey()} on {name} is missing saved state.");
                    continue;
                }

                savable.LoadState(savedState);
            }
        }
    }
}
