﻿namespace Elementalist.Saving
{
    public interface ISavable
    {
        string GetKey();
        object GetState();
        void LoadState(object state);
    }
}