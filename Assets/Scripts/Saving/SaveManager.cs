﻿using Elementalist.Utility;
using UnityEngine;

namespace Elementalist.Saving
{
    public class SaveManager : MonoBehaviour
    {
        [SerializeField] private string _saveFileName = "save.bin";

        private void Awake()
        {
            LoadGameState();
        }

        public void SaveGameState()
        {
            var savables = SearchUtility.GetSceneObjectsOfType<SavableObject>();

            SaveSystem.Save(savables, _saveFileName);
        }

        public void LoadGameState()
        {
            var savables = SearchUtility.GetSceneObjectsOfType<SavableObject>();

            SaveSystem.Load(savables, _saveFileName);
        }

        private void OnApplicationQuit()
        {
            SaveGameState();
        }
    }
}
