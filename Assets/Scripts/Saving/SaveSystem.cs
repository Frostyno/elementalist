﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;

namespace Elementalist.Saving
{
    public static class SaveSystem
    {
        /// <summary>
        /// Collects the state of all SavableObject's passed and then saves them as
        /// binary file in persistent file path under save file name passed.
        /// NOTE: All state objects must by Serializable.
        /// </summary>
        /// <param name="savables">Array of all SavableObjects that should have their state saved.</param>
        /// <param name="saveFileName">Name of save file.</param>
        public static void Save(SavableObject[] savables, string saveFileName)
        {
            if (savables == null)
            {
                Debug.LogError($"Savables cannot be null when trying to save game state.");
                return;
            }

            Dictionary<string, object> gameState = new Dictionary<string, object>();
            foreach (var savable in savables)
            {
                if (gameState.ContainsKey(savable.Key))
                {
                    Debug.LogError($"Duplicate GUIDs when trying to save game state. Duplicate GUID: {savable.Key}.");
                    continue;
                }

                gameState[savable.Key] = savable.State;
            }

            string filePath = Path.Combine(Application.persistentDataPath, saveFileName);
            BinaryFormatter binFormatter = new BinaryFormatter();
            using (FileStream saveFile = new FileStream(filePath, FileMode.Create))
            {
                try
                {
                    binFormatter.Serialize(saveFile, gameState);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Failed to serialize game's state. Error: {e.Message}.");
                }
            }
        }

        /// <summary>
        /// Loads game state from file in persisntant file path under save file name passed
        /// Afterwards loads state for all SavableObjects passed.
        /// </summary>
        /// <param name="savables">Array of savable object that should have state loaded.</param>
        /// <param name="saveFileName">Name of the save file.</param>
        public static void Load(SavableObject[] savables, string saveFileName)
        {
            if (savables == null)
            {
                Debug.LogError($"Savables cannot be null when trying to load game state.");
                return;
            }
            if (string.IsNullOrEmpty(saveFileName))
            {
                Debug.LogError($"File name must be defined when trying to laod game state.");
                return;
            }

            string filePath = Path.Combine(Application.persistentDataPath, saveFileName);
            if (!File.Exists(filePath)) return;

            BinaryFormatter binFormatter = new BinaryFormatter();
            using (FileStream saveFile = new FileStream(filePath, FileMode.Open))
            {
                try
                {
                    var gameState = (Dictionary<string, object>)binFormatter.Deserialize(saveFile);

                    foreach (var savable in savables)
                    {
                        if (!gameState.TryGetValue(savable.Key, out var state))
                        {
                            Debug.LogWarning($"Savable object {savable.name}'s state is missing from saved game state.");
                            continue;
                        }

                        savable.LoadState(state);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError($"Failed to deserialize save file. Error: {e.Message}");
                }
            }
        }
    }
}
