﻿using System;
using System.Runtime.Serialization;

namespace Elementalist
{
    [Serializable]
    [DataContract]
    public enum ElementType
    {
        [EnumMember]Fire,
        [EnumMember]Water,
        [EnumMember]Earth,
        [EnumMember]Air
    }
}
