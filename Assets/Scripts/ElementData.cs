﻿using UnityEngine;

namespace Elementalist
{
    // Concrete subclasses for use in inspector
    [System.Serializable] public class ElementDataDouble : ElementData<double> { }
    [System.Serializable] public class ElementDataUint : ElementData<uint> { }

    [System.Serializable]
    public class ElementData<T>
    {
        [SerializeField] private T _fire = default;
        [SerializeField] private T _water = default;
        [SerializeField] private T _earth = default;
        [SerializeField] private T _air = default;

        public ElementData() { }
        // Parameter constructor used only for unit testing
        public ElementData(T f = default, T w = default, T e = default, T a = default)
        {
            _fire = f;
            _water = w;
            _earth = e;
            _air = a;
        }

        public T GetValue(ElementType type)
        {
            switch (type)
            {
                case ElementType.Fire: return _fire;
                case ElementType.Water: return _water;
                case ElementType.Earth: return _earth;
                case ElementType.Air: return _air;
            }

            return default;
        }
    }
}
