﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Zenject;

namespace Elementalist.Items
{

    public class Shop : MonoBehaviour
    {
        [Serializable]
        private class ShopItemTypeArgs
        {
            [SerializeField] private ItemType _itemType = null;
            [SerializeField, Range(0f, 1f)] private float _probability = 0f;

            public ItemType ItemType { get => _itemType; }
            public float Probability { get => _probability; }
        }
        
        [SerializeField] private int _itemsToGenerate = 5;

        [Space]

        [SerializeField] private ShopItemTypeArgs[] _possibleItems = new ShopItemTypeArgs[0];

        private GlobalEvents _globalEvents = null;
        private Inventory _playerInventory = null;
        private ItemTypeProvider _itemTypeProvider = null;
        private ItemType _elementShardType = null;

        private List<ItemTypeAmountPair> _generatedItems = new List<ItemTypeAmountPair>();

        public ReadOnlyCollection<ItemTypeAmountPair> GeneratedItems { get; private set; }

        public event Action OnItemsGenerated = null;


        [Inject]
        public void Inject(
            GlobalEvents globalEvents,
            Inventory playerInventory,
            ItemTypeProvider itemTypeProvider)
        {
            _globalEvents = globalEvents;
            _globalEvents.OnExpeditionEnded += OnExpeditionEnded;

            _playerInventory = playerInventory;
            _itemTypeProvider = itemTypeProvider;
            _elementShardType = itemTypeProvider.GetItemType(Item.ElementShard);

            GeneratedItems = new ReadOnlyCollection<ItemTypeAmountPair>(_generatedItems);
        }

        private void Awake()
        {
            if (_possibleItems.Length == 0)
                Debug.LogError($"No possible items provided in {name}.");

            float sum = 0f;
            foreach (var item in _possibleItems) sum += item.Probability;
            if (1f - sum > 0.01f)
                Debug.LogError($"Sum of individual possible item probabilities needs to be equal to 1.");
        }

        private void Start()
        {
            if (_elementShardType == null) _elementShardType = _itemTypeProvider.GetItemType(Item.ElementShard);
            GenerateItems();
        }

        private void GenerateItems()
        {
            _generatedItems.Clear();
            if (_possibleItems.Length == 0) return;

            for (int i = 0; i < _itemsToGenerate; i++)
            {
                float prob = UnityEngine.Random.Range(0f, 1f);
                float sum = 0f;

                ItemType itemType = null;
                foreach (var args in _possibleItems)
                {
                    sum += args.Probability;
                    if (prob < sum)
                    {
                        itemType = args.ItemType;
                        break;
                    }
                }
                if (itemType == null) itemType = _possibleItems[0].ItemType;

                ItemTypeAmountPair item;
                item.ItemType = itemType;
                item.Amount = UnityEngine.Random.Range(1, itemType.StackSize + 1);

                _generatedItems.Add(item);
            }

            OnItemsGenerated?.Invoke();
        }

        private void RemoveGeneratedItem(ItemType itemType, int amount)
        {
            for (int i = 0; i < _generatedItems.Count; i++)
            {
                if (_generatedItems[i].ItemType != itemType) continue;
                if (amount == 0) break;
                if (_generatedItems[i].Amount > amount)
                {
                    var tmp = _generatedItems[i];
                    tmp.Amount -= amount;
                    _generatedItems[i] = tmp;
                    break;
                }

                amount -= _generatedItems[i].Amount;
                _generatedItems.RemoveAt(i--);
            }
        }

        public void SellItem(ItemType itemType, int amount)
        {
            if (_playerInventory.GetFreeSpace(_elementShardType) == 0) return; // no free space for element shards

            bool removed = _playerInventory.RemoveItem(itemType, amount);
            if (!removed)
            {
                Debug.LogError($"Shop failed to remove items from player inventory when selling. Item: {itemType.ItemName}. Amount: {amount}.");
                return;
            }

            _playerInventory.AddItem(_elementShardType, amount * itemType.SellPrice);
        }

        public bool BuyItem(ItemType itemType, int amount)
        {
            if (_playerInventory.GetFreeSpace(itemType) < amount)
            {
                Debug.LogError($"Player didn't havee enough free space to buy item. Item: {itemType.ItemName}. Amount: {amount}.");
                return false;
            }

            bool removed = _playerInventory.RemoveItem(_elementShardType, itemType.BuyPrice * amount);
            if (!removed)
            {
                Debug.LogError($"Player didn't have enough element shards to buy. Item: {itemType.ItemName}. Amount: {amount}.");
                return false;
            }

            RemoveGeneratedItem(itemType, amount);
            _playerInventory.AddItem(itemType, amount);
            return true;
        }

        private void OnExpeditionEnded()
        {
            GenerateItems();
        }

        private void OnDestroy()
        {
            if (_globalEvents != null) _globalEvents.OnExpeditionEnded -= OnExpeditionEnded;
        }
    }
}
