﻿using System;
using UnityEngine;

namespace Elementalist.Items
{
    [Serializable]
    public class ItemStack
    {
        private ItemType _itemType = null;
        public ItemType ItemType
        {
            get => _itemType;
            set
            {
                _itemType = value;
                OnItemTypeChanged?.Invoke();
            }
        }
        
        public int Amount { get; private set; }

        public event Action<Item, int> OnItemAdded = null;
        public event Action<Item, int> OnItemRemoved = null;
        public event Action OnItemTypeChanged = null;
        public event Action OnAmountChanged = null;

        /// <summary>
        /// Adds items to stack~.
        /// </summary>
        /// <param name="amount">Amount of items to add.</param>
        /// <returns>Amount of items that didn't fit into the stack.</returns>
        public int AddItemsToStack(int amount)
        {
            if (_itemType == null)
            {
                Debug.LogWarning("Cannot add items to stack with no item atttached.");
                return amount;
            }

            int freeSpace = _itemType.StackSize - Amount;
            if (_itemType.StackSize <= 0 || amount <= freeSpace)
            {
                Amount += amount;
                if (ItemType != null) OnItemAdded?.Invoke(ItemType.Item, amount);
                OnAmountChanged?.Invoke();
                return 0;
            }

            Amount = _itemType.StackSize;
            if (ItemType != null) OnItemAdded?.Invoke(ItemType.Item, freeSpace);
            OnAmountChanged?.Invoke();
            return amount - freeSpace;
        }

        /// <summary>
        /// Removes items to the stack.
        /// </summary>
        /// <param name="amount">Amount of items to remove.</param>
        /// <returns>Amount of items that couldn't be removed because there wasn't enough items.</returns>
        public int RemoveItemsFromStack(int amount)
        {
            int ret = Math.Max(0, amount - Amount);
            Amount = Math.Max(0, Amount - amount);

            if (ItemType != null)  OnItemRemoved?.Invoke(ItemType.Item, amount - ret);
            if (Amount == 0) ItemType = null;
            OnAmountChanged?.Invoke();
            return ret;
        }

        public void Clear()
        {
            if (ItemType != null)  OnItemRemoved?.Invoke(ItemType.Item, Amount);
            Amount = 0;
            OnAmountChanged?.Invoke();
            ItemType = null;
        }
    }
}
