﻿using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Items
{
    public class ItemTypeProvider : MonoBehaviour
    {
        [Tooltip("Specifies folder (with subfolders) containing all items within Resources folder.")]
        [SerializeField] private string _itemsFolderPath = "Items";

        private Dictionary<Item, ItemType> _items = new Dictionary<Item, ItemType>();

        private void Awake()
        {
            var itemTypes = Resources.LoadAll<ItemType>(_itemsFolderPath);

            foreach (var itemType in itemTypes)
            {
                if (_items.ContainsKey(itemType.Item))
                {
                    Debug.LogWarning($"Multiple items with same enum value found. These are {_items[itemType.Item].ItemName} and {itemType.ItemName}.");
                    continue;
                }

                _items.Add(itemType.Item, itemType);
            }
        }

        public ItemType GetItemType(Item item)
        {
            if (_items.TryGetValue(item, out ItemType itemType))
                return itemType;

            return null;
        }
    }
}
