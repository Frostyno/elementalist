﻿namespace Elementalist.Items
{
    public enum ItemRarity
    {
        Common,
        Rare,
        Legendary
    }
}