﻿using Elementalist.Character;
using Elementalist.Character.Stats;
using Elementalist.Combat.Actions;
using Elementalist.Saving;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Zenject;

namespace Elementalist.Items
{
    public class Inventory : MonoBehaviour, ISpellProvider, IStatModifierProvider, ISavable
    {
        [Serializable]
        private class InventorySaveData
        {
            public List<(Item?, int)> StackContents = new List<(Item?, int)>();
        }

        [Tooltip("Amount of stacks this inventory should consist of.")]
        [Range(0, 1000)]
        [SerializeField] private int _inventorySize = 1;

        [Space]
        [Tooltip("Items that character should possess. Note that in case of player these will be overwritten with save data. If more items than fit the inventory, they will be ignored.")]
        [SerializeField] private ItemTypeAmountPair[] _inspectorItems = new ItemTypeAmountPair[0];

        // TODO: figure out where to put this together with signnalling adding and removing items
        private bool _isPlayerInventory = false;
        private GlobalEvents _globalEvents = null;
        private ItemTypeProvider _itemProvider = null;
        private List<ItemStack> _itemStacks = new List<ItemStack>();

        public ReadOnlyCollection<ItemStack> ItemStacks { get; private set; }

        public event Action<Item> OnItemCollected = null;
        public event Action<Item> OnItemRemoved = null;
        public event Action OnStatModifiersChanged = null;
        public event Action OnSpellsChanged = null;
        
        [Inject]
        private void Inject(GlobalEvents globalEvents, ItemTypeProvider itemProvider)
        {
            _isPlayerInventory = gameObject.tag == "Player";
            _globalEvents = globalEvents;
            _itemProvider = itemProvider;
            Initialize();
        }

        public void Initialize()
        {
            if (_itemStacks.Count != 0) return;

            for (int i = 0; i < _inventorySize; i++)
            {
                var stack = new ItemStack();

                if (_isPlayerInventory)
                {
                    stack.OnItemAdded += OnStackItemAdded;
                    stack.OnItemRemoved += OnStackItemRemoved;
                }

                _itemStacks.Add(stack);
            }

            foreach (var pair in _inspectorItems)
                AddItem(pair.ItemType, pair.Amount);

            ItemStacks = new ReadOnlyCollection<ItemStack>(_itemStacks);
        }

        private void TrySignalProviderChanged(ItemType itemType)
        {
            if (itemType.GetType() != typeof(TrinketType)) return;
            var trinket = itemType as TrinketType;

            if (trinket.StatModifiers.Count > 0) OnStatModifiersChanged?.Invoke();
            if (trinket.Spells.Count > 0) OnSpellsChanged?.Invoke();
        }

        /// <summary>
        /// Removes all items from inventory.
        /// </summary>
        public void Clear()
        {
            foreach (var stack in _itemStacks)
            {
                if (stack.ItemType == null) continue;
                Item item = stack.ItemType.Item;
                stack.Clear();
                OnItemRemoved?.Invoke(item);
            }
        }

        /// <summary>
        /// Gets amount of items contained within the inventory.
        /// </summary>
        /// <param name="item">Item amount of which should be get.</param>
        /// <returns>Amount of item in inventory.</returns>
        public int GetItemAmount(Item item)
        {
            int amount = 0;

            foreach (var stack in _itemStacks)
            {
                if (stack.ItemType == null) continue;
                if (stack.ItemType.Item == item) amount += stack.Amount;
            }

            return amount;
        }

        /// <summary>
        /// Gets how many units of passed ItemType can be inserted into inventory.
        /// For items with infinite stack size returns negative value if space is awailable.
        /// </summary>
        /// <param name="itemType">ItemType for which to get free space.</param>
        /// <returns>Free space for passed ItemType.</returns>
        public int GetFreeSpace(ItemType itemType)
        {
            if (itemType == null)
            {
                Debug.LogWarning("Cannot calculate free space for null.");
                return 0;
            }

            int freeSpace = 0;

            foreach (var stack in _itemStacks)
            {
                if (stack.ItemType == itemType) freeSpace += itemType.StackSize - stack.Amount;
                else if (stack.ItemType == null) freeSpace += itemType.StackSize;
            }

            return freeSpace;
        }

        /// <summary>
        /// Adds amount of items of passed ItemType into the inventory.
        /// </summary>
        /// <param name="itemType">ItemType of added item.</param>
        /// <param name="amount">Amount of added items.</param>
        /// <returns>Items that coundn't be added because there was no space left.</returns>
        public int AddItem(ItemType itemType, int amount)
        {
            // try to add items to stacks containing passed item type
            foreach (var stack in _itemStacks)
            {
                if (amount == 0) break;
                if (stack.ItemType != itemType) continue;

                amount = stack.AddItemsToStack(amount);
            }

            // try to add items to empty stacks
            foreach (var stack in _itemStacks)
            {
                if (amount == 0) break;
                if (stack.ItemType != null) continue;

                stack.ItemType = itemType;
                amount = stack.AddItemsToStack(amount);
            }

            OnItemCollected?.Invoke(itemType.Item);
            TrySignalProviderChanged(itemType);
            
            // return amount that couldn't be added (there wasn't ennough free space
            return amount;
        }

        /// <summary>
        /// First checks if there is enough items of passed ItemType to remove from inventory.
        /// If there is, removes them from inventory.
        /// </summary>
        /// <param name="itemType">ItemType to remove from inventory.</param>
        /// <param name="amount">Amount of items to remove.</param>
        /// <returns>Whether the items were removed from inventory.</returns>
        public bool RemoveItem(Item item, int amount)
        {
            if (amount < 0)
            {
                Debug.LogError($"Trying to remove {amount} of {item}.");
                return false;
            }
            if (amount == 0) return true;
            if (GetItemAmount(item) < amount) return false;

            ItemType itemType = null;

            foreach (var stack in _itemStacks)
            {
                if (amount == 0) break;
                if (stack.ItemType == null) continue;
                if (stack.ItemType.Item != item) continue;
                if (itemType == null) itemType = stack.ItemType;

                amount = stack.RemoveItemsFromStack(amount);
            }

            if (amount != 0)
                Debug.LogError("Not all items were removed.");

            OnItemRemoved?.Invoke(item);
            TrySignalProviderChanged(itemType);
            
            return true;
        }

        /// <summary>
        /// First checks if there is enough items of passed ItemType to remove from inventory.
        /// If there is, removes them from inventory.
        /// </summary>
        /// <param name="itemType">ItemType to remove from inventory.</param>
        /// <param name="amount">Amount of items to remove.</param>
        /// <returns>Whether the items were removed from inventory.</returns>
        public bool RemoveItem(ItemType itemType, int amount)
        {
            if (GetItemAmount(itemType.Item) < amount) return false;

            foreach (var stack in _itemStacks)
            {
                if (amount == 0) break;
                if (stack.ItemType == null) continue;
                if (stack.ItemType != itemType) continue;

                amount = stack.RemoveItemsFromStack(amount);
            }

            if (amount != 0)
                Debug.LogError("Not all items were removed.");

            OnItemRemoved?.Invoke(itemType.Item);
            TrySignalProviderChanged(itemType);
            
            return true;
        }
        
        public string GetKey() => ""; // only one inventory per game object, so no additional key needed
        
        /// <summary>
        /// Creates save data for inventory contents.
        /// </summary>
        /// <returns>Inventory save data.</returns>
        public object GetState()
        {
            InventorySaveData saveData = new InventorySaveData();

            foreach (var stack in _itemStacks)
            {
                if (stack.ItemType != null)
                    saveData.StackContents.Add((stack.ItemType.Item, stack.Amount));
                else
                    saveData.StackContents.Add((null, 0));
            }

            return saveData;
        }

        public void LoadState(object state)
        {
            var saveData = state as InventorySaveData;
            if (saveData == null)
            {
                Debug.LogError($"Expected {typeof(InventorySaveData).ToString()} when loading inventory for {name} but got {state.GetType().ToString()}.");
                return;
            }

            Clear();

            if (saveData.StackContents.Count != _itemStacks.Count)
                Debug.LogWarning($"Different number of stacks in save data and inventory in {name}. This may result in loss of data.");

            for (int i = 0; i < saveData.StackContents.Count; i++) // StackContents is a list of (Item?, int)
            {
                if (i >= _itemStacks.Count) break;
                if (saveData.StackContents[i].Item1 == null) continue;

                var itemType = _itemProvider.GetItemType(saveData.StackContents[i].Item1.Value);
                if (itemType == null)
                {
                    Debug.LogError($"ItemType for {saveData.StackContents[i].Item1.Value} is missing from ItemTypeProvider.");
                    continue;
                }

                _itemStacks[i].ItemType = itemType;
                _itemStacks[i].AddItemsToStack(saveData.StackContents[i].Item2);
                OnItemCollected(_itemStacks[i].ItemType.Item);
            }
        }

        public StatModifier[] GetStatModifiers()
        {
            var mods = new List<StatModifier>();
            foreach (var stack in _itemStacks)
            {
                if (stack.ItemType == null) continue;
                if (stack.ItemType.GetType() == typeof(TrinketType))
                    mods.AddRange((stack.ItemType as TrinketType).StatModifiers);
            }

            return mods.ToArray();
        }

        public SpellType[] GetSpells()
        {
            var spells = new List<SpellType>();
            foreach (var stack in _itemStacks)
            {
                if (stack.ItemType == null) continue;
                if (stack.ItemType.GetType() == typeof(TrinketType))
                    spells.AddRange((stack.ItemType as TrinketType).Spells);
            }

            return spells.ToArray();
        }
        
        private void OnStackItemAdded(Item item, int amount)
        {
            _globalEvents.SignalPlayerCollectedItem(item, amount);
        }

        private void OnStackItemRemoved(Item item, int amount)
        {
            _globalEvents.SignalPlayerRemovedItem(item, amount);
        }

        private void OnDestroy()
        {
            if (!_isPlayerInventory) return;

            foreach (var stack in _itemStacks)
            {
                if (stack == null) continue;

                stack.OnItemAdded -= OnStackItemAdded;
                stack.OnItemRemoved -= OnStackItemRemoved;
            }
        }
    }
}
