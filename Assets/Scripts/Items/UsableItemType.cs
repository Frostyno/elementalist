﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Elementalist.Combat;
using Elementalist.Combat.Actions;
using Elementalist.Combat.Effects;
using Elementalist.Combat.Targeting;
using UnityEngine;

namespace Elementalist.Items
{
    [CreateAssetMenu(fileName = "UsableItem", menuName = "Elementalist/Items/UsableItem")]
    public class UsableItemType : ItemType, IAction
    {
        [Space]

        [SerializeField] private GameObject _actionDisplayerPrefab = null;

        [Space]

        [SerializeField] private double _actionPointsCost = 0d;
        [SerializeField] private SOTargeting _targeting = null;
        [SerializeField] private ActionTargetType[] _targetTypes = null;

        [SerializeField] private TemporaryEffectProbPair[] _temporaryEffects = null;
        [SerializeField] private ImmediateEffectProbPair[] _immediateEffects = null;

        private System.Random _effectProcChanceGenerator = new System.Random();
        private HashSet<ActionTargetType> _hashTargetTypes = null;

        public double ActionPointsCost { get => _actionPointsCost; }
        public ReadOnlyCollection<ImmediateEffectProbPair> ImmediateEffects { get; private set; }
        public ReadOnlyCollection<TemporaryEffectProbPair> TemporaryEffects { get; private set; }

        private void OnEnable()
        {
            if (_actionDisplayerPrefab == null ||
                !_actionDisplayerPrefab.TryGetComponent<IActionDisplayer>(out var displayer))
            {
                Debug.LogWarning($"Item {ItemName} on {name} is missing action displayer prefab. Either prefab is not attached or is missing IActionDisplayer component.");
            }

            ImmediateEffects = new ReadOnlyCollection<ImmediateEffectProbPair>(_immediateEffects);
            TemporaryEffects = new ReadOnlyCollection<TemporaryEffectProbPair>(_temporaryEffects);

            if (_targetTypes == null)
            {
                Debug.LogWarning(string.Format("Target types are null on usable item {0}", name));
                return;
            }
            _hashTargetTypes = new HashSet<ActionTargetType>(_targetTypes);
        }

        /// <summary>
        /// Checks if character who is executing the usable item has enough resources to do so.
        /// </summary>
        /// <param name="source">Character who is executing the usable item.</param>
        /// <returns>True if source can execute the usable item.</returns>
        public bool CanExecute(Character.Character source)
        {
            if (source.CurrentActionPoints < _actionPointsCost) return false;

            return true;
        }

        private bool UseResources(Character.Character source)
        {
            if (!CanExecute(source)) return false;

            source.UseActionPoints(_actionPointsCost);

            return true;
        }

        /// <summary>
        /// Checks conditions for usable item execution. Then executes all immediate and temporary effects (if they proc).
        /// </summary>
        /// <param name="source">Caster of the usable item.</param>
        /// <param name="target">Target of the usable item.</param>
        /// <param name="area">Combat area of the current combat.</param>
        /// <returns>True if usable item executed properly.</returns>
        public async Task<bool> Execute(CombatController source, CombatController target, CombatArea area)
        {
            if (!UseResources(source.Character)) return false;
            if (!_targeting.IsTargetablePosition(source, target, area, _hashTargetTypes)) return false;

            if (_actionDisplayerPrefab != null) await DisplayEffect(source, target, area);

            if (_immediateEffects != null)
            {
                foreach (var effect in _immediateEffects)
                {
                    double prob = _effectProcChanceGenerator.NextDouble();
                    if (prob < effect.Probability) effect.Item.Execute(target.Character, source.Character);
                }
            }

            if (_temporaryEffects != null)
            {
                foreach (var effect in _temporaryEffects)
                {
                    double prob = _effectProcChanceGenerator.NextDouble();
                    if (prob < effect.Probability) target.Character.AddTemporaryEffect(effect.Item, target.Character, source.Character);
                }
            }

            // TODO: after addyng async usable item spawn change to return true;
            return true;
        }
        
        private async Task DisplayEffect(CombatController source, CombatController target, CombatArea area)
        {
            if (_actionDisplayerPrefab == null ||
                !_actionDisplayerPrefab.TryGetComponent<IActionDisplayer>(out var d)) return;

            var displayer = Instantiate(_actionDisplayerPrefab);
            displayer.transform.position = area.GetCombatantPosition(source).Center;
            var position = area.GetCombatantPosition(target);
            await displayer.GetComponent<IActionDisplayer>().Display(position.Center, position.Size);

            Destroy(displayer);
        }

        /// <summary>
        /// Makes valid combat positions (positions that can be hit by this usable item) targetable.
        /// </summary>
        /// <param name="source">Caster of the usable item.</param>
        /// <param name="area">Combat area of the current combat.</param>
        public void MarkTargetablePositions(CombatController source, CombatArea area)
        {
            if (_targeting == null)
            {
                Debug.LogError("Targeting is null. Cannot mark targetable positions.");
                return;
            }

            _targeting.MarkTargetablePositions(source, area, _hashTargetTypes);
        }
    }
}
