﻿using Elementalist.Character.Stats;
using Elementalist.Combat.Actions;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Elementalist.Items
{
    [CreateAssetMenu(fileName = "Trinket", menuName = "Elementalist/Items/Trinket")]
    public class TrinketType : ItemType
    {
        [SerializeField] private StatModifier[] _statModifiers = new StatModifier[0];
        [SerializeField] private SpellType[] _spells = new SpellType[0];

        public ReadOnlyCollection<StatModifier> StatModifiers { get; private set; }
        public ReadOnlyCollection<SpellType> Spells { get; private set; }

        private void OnEnable()
        {
            StatModifiers = new ReadOnlyCollection<StatModifier>(_statModifiers);
            Spells = new ReadOnlyCollection<SpellType>(_spells);
        }
    }
}
