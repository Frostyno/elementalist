﻿namespace Elementalist.Items
{
    public enum Item
    {
        TestItem,
        TestingTrinketOfLife,
        TestingHealthPotiion,
        ElementShard,
        Emerald,
        HealthPotion,
        WaterBall,
        HeartOfFire
    }
}