﻿using System;
using UnityEngine;

namespace Elementalist.Items
{
    [Serializable]
    public struct ItemTypeAmountPair
    {
        [SerializeField] public ItemType ItemType;
        [Range(1, 1000)]
        [SerializeField] public int Amount;
    }

    [CreateAssetMenu(fileName = "Item", menuName = "Elementalist/Items/Item")]
    public class ItemType : ScriptableObject
    {
        [SerializeField] private string _itemName = "";
        [SerializeField] private Item _item = Item.TestItem;
        [SerializeField] private ItemRarity _itemRarity = ItemRarity.Common;
        [SerializeField] private Sprite _icon = null;
        [TextArea]
        [SerializeField] private string _description = "";
        [SerializeField] private int _stackSize = 1;
        [SerializeField] private int _buyPrice = 1;
        [SerializeField] private int _sellPrice = 1;

        public string ItemName { get => _itemName; }
        public Item Item { get => _item; }
        public ItemRarity ItemRarity { get => _itemRarity; }
        public Sprite Icon { get => _icon; }
        public string Description { get => _description; }
        public int StackSize { get => _stackSize; }
        public int BuyPrice { get => _buyPrice; }
        public int SellPrice { get => _sellPrice; }
    }
}
