﻿using Elementalist.Combat;
using UnityEngine;
using Zenject;

namespace Elementalist.Injection
{
    public class CombatantFromPrefabFactory : IFactory<CombatController>
    {
        private DiContainer _container = null;
        private CombatantFactoryContext _context = null;

        public CombatantFromPrefabFactory(DiContainer container, CombatantFactoryContext context)
        {
            _container = container;
            _context = context;
        }

        public CombatController Create()
        {
            if (_context.CombatantPrefab == null)
            {
                Debug.LogError("Cannot instantiate combatant from null");
                return null;
            }

            return _container.InstantiatePrefabForComponent<CombatController>(_context.CombatantPrefab);
        }
    }
}
