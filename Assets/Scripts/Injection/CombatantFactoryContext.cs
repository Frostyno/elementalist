﻿using Elementalist.Combat;
using UnityEngine;

namespace Elementalist.Injection
{
    public class CombatantFactoryContext
    {
        public GameObject CombatantPrefab { get; private set; }

        public void SetCombatantPrefab(GameObject prefab)
        {
            if (prefab == null)
            {
                Debug.LogError("Combatant prefab is null.");
                return;
            }

            if (prefab.GetComponent<CombatController>() == null)
            {
                Debug.LogError("Combatant prefab is missing CombatController.");
                return;
            }

            CombatantPrefab = prefab;
        }
    }
}
