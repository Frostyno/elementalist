using Zenject;
using Elementalist.Character;
using Elementalist.Combat;
using UnityEngine;
using Elementalist.Map;

namespace Elementalist.Injection
{
    public class CharacterInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<CharacterEssence>().FromComponentSibling();
            Container.Bind<CharacterEvents>().FromComponentSibling();
            Container.Bind<IStatModifierProvider>().FromComponentsSibling();
            Container.Bind<ISpellProvider>().FromComponentsSibling();
            Container.Bind<Resistance>().FromComponentSibling();
            Container.Bind<Sources>().FromComponentSibling();
            Container.Bind<Vitality>().FromComponentSibling();
            Container.Bind<SpellPower>().FromComponentSibling();
            Container.Bind<CharacterTemporaryEffects>().FromComponentSibling();
            Container.Bind<CharacterSpells>().FromComponentSibling();

            Container.Bind<Character.Character>().FromComponentSibling().AsTransient()
                .WhenInjectedInto<CombatController>();

            var playerGO = GameObject.FindGameObjectWithTag("Player");
            Container.Bind<Character.Character>()
                .FromComponentOn(playerGO)
                .AsCached();

            //Container.Bind<StatusUtility>().AsSingle();
        }
    }
}
