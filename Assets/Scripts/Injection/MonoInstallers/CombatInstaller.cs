using Elementalist.AI.BehaviorTree;
using Elementalist.Combat;
using Elementalist.Combat.Utility;
using UnityEngine;
using Zenject;

namespace Elementalist.Injection
{
    public class CombatInstaller : MonoInstaller
    {
        [SerializeField] private GameObject _combatPositionPrefab = null;

        public override void InstallBindings()
        {
            Container.Bind<PlayerCombatController>()
                .FromComponentOn(GameObject.FindGameObjectWithTag("Player"))
                .AsCached();

            Container.Bind<CombatPositionSpawner>()
                .FromComponentSibling().AsSingle();

            Container.Bind<SpriteRenderer>().FromComponentsInChildren().AsTransient()
                .WhenInjectedInto<CombatPosition>();
            Container.Bind<BoxCollider2D>().FromComponentSibling().AsTransient()
                .WhenInjectedInto<CombatPosition>();

            Container.Bind<Combat.Combat>().FromComponentInHierarchy().AsCached();

            Container.Bind<BehaviorTreeLoader>().FromNew().AsCached();

            //Factories
            Container.BindFactory<CombatPosition, CombatPosition.CombatPositionFactory>()
                .FromComponentInNewPrefab(_combatPositionPrefab);

            Container.Bind<CombatantFactoryContext>().AsSingle();
            Container.BindFactory<CombatController, CombatController.CombatControllerFactory>()
                .FromFactory<CombatantFromPrefabFactory>();
        }
    }
}