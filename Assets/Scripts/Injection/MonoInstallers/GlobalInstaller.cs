using Elementalist.Combat;
using Elementalist.Items;
using Elementalist.Quests;
using Elementalist.Saving;
using UnityEngine;
using Zenject;

namespace Elementalist.Injection
{
    public class GlobalInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<InputController>()
                .FromComponentInHierarchy()
                .AsCached();

            Container.Bind<ItemTypeProvider>()
                .FromComponentInHierarchy()
                .AsCached();

            Container.Bind<EnemyPrefabProvider>()
                .FromComponentInHierarchy()
                .AsCached();

            Container.Bind<GlobalEvents>()
                .FromComponentInHierarchy()
                .AsCached();

            Container.Bind<QuestLog>()
                .FromComponentOn(GameObject.FindGameObjectWithTag("Player"))
                .AsCached();
            Container.Bind<QuestGiver>()
                .FromComponentInHierarchy()
                .AsCached();
            Container.Bind<QuestUtility>()
                .FromComponentInHierarchy()
                .AsCached();

            Container.Bind<Shop>()
                .FromComponentInHierarchy()
                .AsCached();

            Container.Bind<SaveManager>()
                .FromComponentInHierarchy()
                .AsCached();
        }
    }
}