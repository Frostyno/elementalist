using Elementalist.Character;
using Elementalist.Items;
using Elementalist.UI.City;
using Elementalist.UI.Items;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Elementalist.UI.Quests;
using Elementalist.UI.Tooltip;
using Elementalist.UI;
using Elementalist.UI.Utility;

namespace Elementalist.Injection
{
    public class GlobalUIInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<Inventory>()
                .FromComponentOn(GameObject.FindGameObjectWithTag("Player"))
                .AsCached();
            Container.Bind<CharacterTalents>()
                .FromComponentOn(GameObject.FindGameObjectWithTag("Player"))
                .AsCached();

            // TalentShop
            Container.Bind<TalentSlot>()
                .FromComponentsInChildren()
                .AsSingle()
                .WhenInjectedInto<TalentShop>();
            Container.Bind<TalentShop>()
                .FromComponentInHierarchy()
                .AsCached();

            // PlayerInventoryUI
            Container.Bind<ItemSlot>()
                .FromComponentsInChildren()
                .AsTransient()
                .WhenInjectedInto<PlayerInventoryUI>();

            // Items
            Container.Bind<ItemDisplayPool>()
                .FromComponentInHierarchy()
                .AsCached();

            // Enemies
            Container.Bind<EnemyDisplayPool>()
                .FromComponentInHierarchy()
                .AsCached();

            // Expeditions
            Container.Bind<Canvas>()
                .FromComponentInHierarchy()
                .AsCached()
                .WhenInjectedInto<Expeditions>();
            Container.Bind<ExpeditionSlotPool>()
                .FromComponentInHierarchy()
                .AsCached();

            // Expedition slot
            Container.Bind<Expeditions>()
                .FromComponentInHierarchy()
                .AsCached()
                .WhenInjectedInto<ExpeditionSlot>();
            Container.Bind<TextMeshProUGUI>()
                .FromComponentInChildren()
                .AsTransient()
                .WhenInjectedInto<ExpeditionSlot>();
            Container.Bind<Button>()
                .FromComponentSibling()
                .AsTransient()
                .WhenInjectedInto<ExpeditionSlot>();
            
            // Quests
            Container.Bind<GoalTrackerPool>()
                .FromComponentInHierarchy()
                .AsCached();
            Container.Bind<QuestButtonPool>()
                .FromComponentInHierarchy()
                .AsCached();

            // Utility
            Container.Bind<GraphicRaycaster>()
                .FromComponentInParents()
                .AsTransient();
            Container.Bind<ColorTextMarker>()
                .FromComponentsInHierarchy()
                .AsTransient()
                .WhenInjectedInto<TextColorPresetHolder>();
            Container.Bind<TextMeshProUGUI>()
                .FromComponentSibling()
                .AsTransient()
                .WhenInjectedInto<ColorTextMarker>();
            Container.Bind<TextColorPresetHolder>()
                .FromComponentInHierarchy()
                .AsCached();

            // Popups
            Container.Bind<PopupWindow>()
                .FromComponentsInHierarchy()
                .AsCached();
            Container.Bind<MessagePopup>()
                .FromComponentInHierarchy()
                .AsCached();

            // LootPopup
            Container.Bind<LootInventoryUI>().FromComponentInChildren().AsSingle();
            Container.Bind<LootPopup>().FromComponentInHierarchy().AsCached();
            Container.Bind<ItemSlot>().FromComponentsInChildren().WhenInjectedInto<LootInventoryUI>();

            // Shop
            Container.Bind<ShopUI>()
                .FromComponentInHierarchy()
                .AsCached();
            Container.Bind<ShopSliderPopup>()
                .FromComponentInHierarchy()
                .AsCached();

            // Tooltip
            Container.Bind<StatModifierDisplayerPool>()
                .FromComponentInHierarchy()
                .AsCached();
            Container.Bind<ImmediateEffectDisplayerPool>()
                .FromComponentInHierarchy()
                .AsCached();
            Container.Bind<TemporaryEffectDisplayerPool>()
                .FromComponentInHierarchy()
                .AsCached();
            Container.Bind<SpellDisplayerPool>()
                .FromComponentInHierarchy()
                .AsCached();
            Container.Bind<ContentDisplayer>()
                .FromComponentsInChildren()
                .AsCached()
                .WhenInjectedInto<Tooltip>();
            Container.Bind<Tooltip>()
                .FromComponentInHierarchy()
                .AsCached();
        }
    }
}