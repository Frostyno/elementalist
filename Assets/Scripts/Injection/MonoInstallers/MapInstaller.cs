using Elementalist.Character;
using Elementalist.Combat;
using Elementalist.Map;
using Elementalist.Map.Generation;
using Elementalist.UI.Items;
using UnityEngine;
using Zenject;

namespace Elementalist.Injection
{
    public class MapInstaller : MonoInstaller
    {
        [SerializeField] private GameObject _dungeonRoomSlotPrefab = null;

        [Space]

        [SerializeField] private GameObject _mapPivot = null;

        public override void InstallBindings()
        {
            // Map Instances
            Container.Bind<CombatHandler>().FromComponentInHierarchy().AsCached();
            Container.Bind<Dungeon>().FromComponentInHierarchy().AsCached();
            Container.Bind<PlayerDungeonMover>().FromComponentInHierarchy().AsCached();
            Container.Bind<DungeonGenerator>().FromComponentInHierarchy().AsCached();
            Container.Bind<MapMover>().FromComponentInHierarchy().AsCached();

            // Map
            Container.Bind<GameObject>().FromInstance(_mapPivot)
                .AsCached().WhenInjectedInto<CombatHandler>();

            // Marker
            Container.Bind<PlayerMapMarker>()
                .FromComponentInHierarchy()
                .AsCached();
            Container.Bind<CharacterEssence>()
                .FromComponentOn(GameObject.FindGameObjectWithTag("Player"))
                .AsCached()
                .WhenInjectedInto<PlayerMapMarker>();

            // Factories
            Container.BindFactory<DungeonRoomSlot, DungeonRoomSlot.DungeonRoomSlotFactory>()
                .FromComponentInNewPrefab(_dungeonRoomSlotPrefab);
        }
    }
} 