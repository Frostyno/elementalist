using Elementalist.Character;
using Elementalist.Combat;
using Elementalist.UI.Actions;
using UnityEngine;
using Zenject;
using TMPro;
using Elementalist.UI.Character;
using Elementalist.UI.Items;

namespace Elementalist.Injection
{
    public class CombatUIInstaller : MonoInstaller
    {
        [SerializeField] private GameObject _spellbookSlotPrefab = null;

        public override void InstallBindings()
        {
            // CombatPosition
            Container.Bind<CombatPosition>().FromComponentInParents().AsTransient()
                .WhenInjectedInto<CharacterCombatDisplay>();

            // Spellbook
            Container.Bind<CharacterEvents>() // maybe move this binding to CombatInstaller
                .FromComponentOn(GameObject.FindGameObjectWithTag("Player"))
                .AsCached()
                .WhenInjectedInto<Spellbook>();

            // SpellbookSlot
            // NOTE: PlayerCombatController binding is supplied in CombatInstaller

            // Factories
            Container.BindFactory<SpellbookSlot, SpellbookSlot.SpellbookSlotFactory>()
                .FromComponentInNewPrefab(_spellbookSlotPrefab);
        }
    }
}