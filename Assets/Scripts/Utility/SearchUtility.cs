﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Elementalist.Utility
{
    public static class SearchUtility
    {
        public static T[] GetSceneObjectsOfType<T>()
            where T : Component
        {
            List<T> foundObjects = new List<T>();

            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                var rootObjects = SceneManager.GetSceneAt(i).GetRootGameObjects();
                foreach (var rootObject in rootObjects)
                    foundObjects.AddRange(rootObject.GetComponentsInChildren<T>(true));
            }

            return foundObjects.ToArray();
        }
    }
}