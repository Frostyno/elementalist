﻿using Elementalist.UI.Utility;
using UnityEngine;

namespace Elementalist.Utility
{
    [System.Serializable]
    public class ElementColors
    {
        [SerializeField] private Color _fireColor = Color.white;
        [SerializeField] private Color _waterColor = Color.white;
        [SerializeField] private Color _earthColor = Color.white;
        [SerializeField] private Color _airColor = Color.white;

        public Color GetElementColor(ElementType elementType)
        {
            switch (elementType)
            {
                case ElementType.Fire: return _fireColor;
                case ElementType.Water: return _waterColor;
                case ElementType.Earth: return _earthColor;
                case ElementType.Air: return _airColor;
            }
            return Color.white;
        }
    }

    [CreateAssetMenu(fileName = "ColorPreset", menuName = "Elementalist/ColorPreset")]
    public class ColorsPreset : ScriptableObject
    {
        [SerializeField] private Color _textColor = Color.white;
        [SerializeField] private Color _valueColor = Color.white;

        [SerializeField] private ElementColors _elementColors = null;

        public Color StandardColor => _textColor;
        public Color ValueColor => _valueColor;
        public ElementColors ElementColors => _elementColors;
        
        public Color GetTextColor(TextColor color)
        {
            switch (color)
            {
                case TextColor.Text: return StandardColor;
                case TextColor.Value: return ValueColor;
                case TextColor.Fire: return ElementColors.GetElementColor(ElementType.Fire);
                case TextColor.Water: return ElementColors.GetElementColor(ElementType.Water);
                case TextColor.Earth: return ElementColors.GetElementColor(ElementType.Earth);
                case TextColor.Air: return ElementColors.GetElementColor(ElementType.Air);
                default: return Color.white;
            }
        }
    }
}
