﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Elementalist.Utility
{
    public class ObjectPool<T> : MonoBehaviour where T : Component
    {
        [SerializeField] private Transform _objectsParent = null;
        [SerializeField] private T _prefab = null;

        private DiContainer _container = null;

        private Queue<T> _pool = new Queue<T>();

        [Inject]
        private void Inject(DiContainer container)
        {
            _container = container;
        }

        private void Awake()
        {
            if (_prefab == null)
            {
                Debug.LogError($"Prefab is missing in object pool for {typeof(T).Name}.");
                return;
            }

            if (_objectsParent == null) _objectsParent = new GameObject($"[{_prefab.name}s]").transform;
        }

        private void CreateResource()
        {
            var newResource = Instantiate(_prefab);
            _container.InjectGameObject(newResource.gameObject);
            newResource.gameObject.SetActive(false);
            newResource.transform.SetParent(_objectsParent);
            _pool.Enqueue(newResource);
        }

        public T GetObject()
        {
            if (_pool.Count == 0) CreateResource();

            return _pool.Dequeue();
        }

        public void ReturnObject(T released)
        {
            released.transform.SetParent(_objectsParent);
            released.gameObject.SetActive(false);
            _pool.Enqueue(released);
        }
    }
}
