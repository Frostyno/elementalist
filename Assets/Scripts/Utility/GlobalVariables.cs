﻿namespace Elementalist.Utility
{
    public static class GlobalVariables
    {
        /// <summary>
        /// Used when comparing equality of two doubles.
        /// </summary>
        public static readonly double EPSILON = 0.001d;

        /// <summary>
        /// Specifies how many Unity units one dungeon distance unit is.
        /// </summary>
        public static readonly float DISTANCE_SCALE = 1.5f;
    }
}