﻿using Elementalist.Combat;
using Elementalist.Items;
using System;
using UnityEngine;

namespace Elementalist
{
    public class GlobalEvents : MonoBehaviour
    {
        public event Action<Enemy> OnEnemyDied = null;
        public event Action<Item, int> OnPlayerCollectedItem = null;
        public event Action<Item, int> OnPlayerRemovedItem = null;

        public event Action OnExpeditionEnded = null;

        public void SignalEnemyDied(Enemy enemy)
        {
            OnEnemyDied?.Invoke(enemy);
        }

        public void SignalPlayerCollectedItem(Item item, int amount)
        {
            OnPlayerCollectedItem?.Invoke(item, amount);
        }

        public void SignalPlayerRemovedItem(Item item, int amount)
        {
            OnPlayerRemovedItem?.Invoke(item, amount);
        }

        public void SignalExpeditionEnded()
        {
            OnExpeditionEnded?.Invoke();
        }
    }
}
