﻿using System;
using UnityEngine;

namespace Elementalist
{
    public class InputController : MonoBehaviour
    {
        public event Action OnToggleQuestLog = null;
        public event Action OnTogglePlayerStats = null;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                OnToggleQuestLog?.Invoke();
            }
            
            if (Input.GetKeyDown(KeyCode.C))
            {
                OnTogglePlayerStats?.Invoke();   
            }
        }
    }
}
