﻿namespace Elementalist.Combat
{
    public enum Enemy
    {
        TestingEnemy,
        LesserFireElemental,
        EarthElemental
    }
}