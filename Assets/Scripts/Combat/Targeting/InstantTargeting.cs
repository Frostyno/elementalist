﻿using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Combat.Targeting
{
    [CreateAssetMenu(fileName = "InstantTargeting", menuName = "Elementalist/Combat/InstantTargeting", order = 0)]
    public class InstantTargeting : SOTargeting
    {
        protected override List<CombatPosition> GetTargetablePositions(CombatController source, CombatArea area, HashSet<ActionTargetType> targetTypes)
        {
            List<CombatPosition> targetablePositions = new List<CombatPosition>();

            foreach (var targetType in targetTypes)
            {
                switch (targetType)
                {
                    case ActionTargetType.Self:
                        targetablePositions.Add(area.GetCombatantPosition(source));
                        break;
                    case ActionTargetType.Ally:
                        foreach (var position in area.GetCombatSide(source.Team.Side))
                            if (position.Combatant != null && position.Combatant != source)
                                targetablePositions.Add(position);
                        break;
                    case ActionTargetType.Enemy:
                        CombatSide enemySide = source.Team.Side == CombatSide.Left ? CombatSide.Right : CombatSide.Left;
                        foreach (var position in area.GetCombatSide(enemySide))
                            if (position.Combatant != null)
                                targetablePositions.Add(position);
                        break;
                }
            }

            return targetablePositions;
        }
    }
}
