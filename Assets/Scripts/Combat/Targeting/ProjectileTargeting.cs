﻿using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Combat.Targeting
{
    [CreateAssetMenu(fileName = "ProjectileTargeting", menuName = "Elementalist/Combat/ProjectileTargeting", order = 0)]
    public class ProjectileTargeting : SOTargeting
    {
        protected override List<CombatPosition> GetTargetablePositions(CombatController source, CombatArea area, HashSet<ActionTargetType> targetTypes)
        {
            List<CombatPosition> targetablePositions = new List<CombatPosition>();

            foreach (var targetType in targetTypes)
            {
                switch (targetType)
                {
                    case ActionTargetType.Self:
                        // Cannot target self with projectile
                        break;
                    case ActionTargetType.Ally:
                        targetablePositions.AddRange(area.GetCombatantNeighboursPositions(source));
                        break;
                    case ActionTargetType.Enemy:
                        CombatSide enemySide = source.Team.Side == CombatSide.Left ? CombatSide.Right : CombatSide.Left;
                        var enemyPosition = area.GetFirstCombatantPosition(enemySide);
                        if (enemyPosition != null) targetablePositions.Add(enemyPosition);
                        break;
                }
            }

            return targetablePositions;
        }
    }
}
