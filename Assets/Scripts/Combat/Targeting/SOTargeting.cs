﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Combat.Targeting
{
    public abstract class SOTargeting : ScriptableObject, ITargetingStrategy
    {
        protected abstract List<CombatPosition> GetTargetablePositions(CombatController source, CombatArea area, HashSet<ActionTargetType> targetTypes);

        public bool IsTargetablePosition(CombatController source, CombatController target, CombatArea area, HashSet<ActionTargetType> targetTypes)
        {
            var validPositions = GetTargetablePositions(source, area, targetTypes);
            foreach (var position in validPositions)
                if (position.Combatant == target)
                    return true;

            return false;
        }

        public void MarkTargetablePositions(CombatController source, CombatArea area, HashSet<ActionTargetType> targetTypes)
        {
            var validPositions = GetTargetablePositions(source, area, targetTypes);
            foreach (var position in validPositions)
                position.SetTargetable(true);
        }
    }
}
