﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Combat.Targeting
{
    public interface ITargetingStrategy
    {
        void MarkTargetablePositions(CombatController source, CombatArea area, HashSet<ActionTargetType> targetTypes);
        bool IsTargetablePosition(CombatController source, CombatController target, CombatArea area, HashSet<ActionTargetType> targetTypes);
    }
}
