﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Elementalist.Character;
using Elementalist.Combat.Utility;
using Elementalist.Injection;
using Elementalist.UI;

namespace Elementalist.Combat
{
    public class Combat : MonoBehaviour
    {
        #region AttributesAndProperties

        [SerializeField, TextArea] private string _winMessage = "";
        [SerializeField, TextArea] private string _loseMessage = "";

        [Space]

        [SerializeField] private Transform _enemiesParent = null;

        [Space]

        private PlayerCombatController _playerController = null;
        private CombatPositionSpawner _combatPositionSpawner = null;
        private CombatController.CombatControllerFactory _npcFactory = null;
        private CombatantFactoryContext _npcFactoryContext = null;
        private MessagePopup _messagePopup = null;
        private GlobalEvents _globalEvents = null;

        public CombatArea CombatArea { get; private set; }

        private CombatController _activeCombatant = null;
        private bool _combatEnded = false;
        /// <summary>
        /// Used to check when the round is beginning and when the round is ending.
        /// Contains combatants in the order they take turns but is immutable (except for when combatant dies).
        /// </summary>
        private List<CombatController> _roundOrder = new List<CombatController>();
        /// <summary>
        /// Used to get next combatant when active combatant is changing.
        /// </summary>
        private Queue<CombatController> _combatantOrder = new Queue<CombatController>();

        [Tooltip("Specifies, whether all combatants should get combat events together on round began/end, or individually as they become active.")]
        [SerializeField] private bool _useGlobalCombatEvents = false;

        public event Action OnCombatInicialized = null;

        /// <summary>
        /// Fired when combat ends. Parameter is CombatSide that won the combat.
        /// </summary>
        public event Action<CombatSide> OnCombatEnded = null;

        #endregion

        [Inject]
        private void Inject(
            PlayerCombatController playerController,
            CombatPositionSpawner spawner,
            CombatController.CombatControllerFactory factory,
            CombatantFactoryContext context,
            [InjectOptional] MessagePopup messagePopup,
            [InjectOptional] GlobalEvents globalEvents) // to prevent test failing
        {
            _combatPositionSpawner = spawner;
            _playerController = playerController;

            _npcFactory = factory;
            _npcFactoryContext = context;

            _messagePopup = messagePopup;
            _globalEvents = globalEvents;
        }

        private void Awake()
        {
            _combatEnded = false;
            if (_globalEvents == null)
                Debug.LogError("Global events are null in combat. This will result in some global events not being fired.");
        }

        #region InicializationFunctions

        /// <summary>
        /// Initializes combat by creating NPCs and creating a combat order.
        /// After that starts the combat by beginning the first combatant's turn.
        /// </summary>
        /// <param name="combatParams">Parameters of the combat.]</param>
        public void Initialize(CombatParameters combatParams)
        {
            if (_playerController == null)
            {
                Debug.LogError("Player game object is missing from Combat.");
                return;
            }

            List<CombatController> leftSideCombatants = new List<CombatController>();
            List<CombatController> rightSideCombatants = new List<CombatController>();

            // set up player
            _playerController.Team = new CombatTeam(combatParams.PlayerCombatSide);
            _playerController.GetComponent<Vitality>().OnCharacterDied += OnCharacterDied;
            if (combatParams.PlayerCombatSide == CombatSide.Left) leftSideCombatants.Add(_playerController);
            else rightSideCombatants.Add(_playerController);
            _playerController.SetCombat(this);

            CreateNPC(combatParams, leftSideCombatants, rightSideCombatants);
            CreateCombatOrder(leftSideCombatants, rightSideCombatants);

            CombatArea = new CombatArea(
                _combatPositionSpawner, 
                leftSideCombatants,
                rightSideCombatants, 
                combatParams 
                );

            BeginCombat(leftSideCombatants, rightSideCombatants);

            OnCombatInicialized?.Invoke();

            //TODO: Consider putting this into StartCombat() method
            BeginNextCombatantTurn();
        }

        /// <summary>
        /// Creates instances of NPCs based on prefabs present in CombatParameters.
        /// </summary>
        /// <param name="combatParams">Parameters of the combat (containing NPC prefabs).</param>
        /// <param name="leftSideCombatants">List to be filled with combatants generated on the left side.</param>
        /// <param name="rightSideCombatants">List to be filled with combatants generated on the right side.</param>
        private void CreateNPC(
            CombatParameters combatParams, 
            List<CombatController> leftSideCombatants, 
            List<CombatController> rightSideCombatants)
        {
            foreach(var enemyPrefab in combatParams.EnemyPrefabs)
            {
                _npcFactoryContext.SetCombatantPrefab(enemyPrefab);
                var enemyController = _npcFactory.Create();
                if (_enemiesParent != null) enemyController.transform.parent = _enemiesParent;
                enemyController.Team = new CombatTeam(combatParams.EnemyCombatSide);

                if (combatParams.EnemyCombatSide == CombatSide.Left) leftSideCombatants.Add(enemyController);
                else rightSideCombatants.Add(enemyController);

                enemyController.GetComponent<Vitality>().OnCharacterDied += OnCharacterDied;

                enemyController.BeginCombat();
            }
        }

        /// <summary>
        /// Creates an order in which combatants take turns within combat.
        /// </summary>
        /// <param name="leftSideCombatants">Combatants on the left side.</param>
        /// <param name="rightSideCombatants">Combatants on the right side.</param>
        private void CreateCombatOrder(
            List<CombatController> leftSideCombatants,
            List<CombatController> rightSideCombatants)
        {
            // generate order in which combatants will take turns here
            // for now just put them in order they were created

            foreach (var combatant in leftSideCombatants)
            {
                _roundOrder.Add(combatant);
                _combatantOrder.Enqueue(combatant);
            }
            foreach (var combatant in rightSideCombatants)
            {
                _roundOrder.Add(combatant);
                _combatantOrder.Enqueue(combatant);
            }
        }



        /// <summary>
        /// Begins combat for all combatants.
        /// </summary>
        /// <param name="leftSideCombatants">Combatants on the left side.</param>
        /// <param name="rightSideCombatants">Combatants on the right side.</param>
        private void BeginCombat(
            List<CombatController> leftSideCombatants,
            List<CombatController> rightSideCombatants)
        {
            foreach (var combatant in leftSideCombatants)
                combatant.BeginCombat();
            foreach (var combatant in rightSideCombatants)
                combatant.BeginCombat();
        }

        #endregion

        #region CombatManagementFunctions

        /// <summary>
        /// Starts thee turn for next combatant in combatant order.
        /// Also signals RoundBegan event either locally for combatant or globally
        /// for all combatants if next combatant is the first one in combatant order.
        /// </summary>
        private void BeginNextCombatantTurn()
        {
            if (_combatEnded) return;

            _activeCombatant = _combatantOrder.Dequeue();
            if (_useGlobalCombatEvents && _activeCombatant == _roundOrder[0])
            {
                foreach (var comb in _roundOrder)
                    comb.BeginRound();
            }
            else if (!_useGlobalCombatEvents)
            {
                _activeCombatant.BeginRound();
            }
            
            _activeCombatant.BeginTurn();
        }

        /// <summary>
        /// Ends the turn for a combatant given in event payload.
        /// Also signals RoundEnded event either locally for combatant or globally
        /// for all combatants if last combatant in combatant order has ended his turn.
        /// </summary>
        /// <param name="combatant">Combatant that ended turn.</param>
        public void EndCombatantTurn(CombatController combatant, bool enqueue = true)
        {
            if (combatant == null)
            {
                Debug.LogError(string.Format("Combatant is null in {0}.", name));
                return;
            }
            else if (combatant != _activeCombatant)
            {
                Debug.LogError(string.Format("Invalid combatant ended turn in {0}.", name));
                return;
            }

            if (_useGlobalCombatEvents && combatant == _roundOrder[_roundOrder.Count - 1])
            {
                foreach (var comb in _roundOrder)
                    comb.EndRound();
            }
            else if (!_useGlobalCombatEvents)
            {
                combatant.EndRound();
            }
            
            if (enqueue)
                _combatantOrder.Enqueue(combatant);
            _activeCombatant = null;

            BeginNextCombatantTurn();
        }

        private (bool, CombatSide?) CheckCombatEndConditions()
        {
            // find out if there are combatants alive on both sides
            (bool leftCombatantFound, bool rightCombatantFound) = (false, false);
            foreach (var combatant in _roundOrder)
            {
                if (combatant.Team.Side == CombatSide.Left) leftCombatantFound = true;
                else if (combatant.Team.Side == CombatSide.Right) rightCombatantFound = true;
            }

            // if combatants on one side are dead
            if (!(leftCombatantFound && rightCombatantFound))
            {
                return (true, leftCombatantFound ? CombatSide.Left : CombatSide.Right);
            }

            return (false, null);
        }

        private async void EndCombat(CombatSide winnerSide)
        {
            _combatEnded = true;
            CombatArea.MakePositionsUntargetable();
            
            if (winnerSide == _playerController.Team.Side)
            {
                _playerController.GetComponent<Vitality>().OnCharacterDied -= OnCharacterDied;
                await _messagePopup.DisplayMessage(_winMessage, "Combat result");
            }
            else
            {
                _playerController.EndCombat();
                await _messagePopup.DisplayMessage(_loseMessage, "Combat result");
            }

            foreach (var combatant in _roundOrder)
                combatant.EndCombat();
            
            OnCombatEnded?.Invoke(winnerSide);
        }

        #endregion

        #region EventCallbacks

        private void OnCharacterDied(Vitality vitality)
        {
            vitality.OnCharacterDied -= OnCharacterDied;

            var combatant = vitality.GetComponent<CombatController>();
            _roundOrder.Remove(combatant);

            // check if character that died was an enemy, if so signal enemy died
            if (combatant is AICombatController)
                _globalEvents.SignalEnemyDied((combatant as AICombatController).Enemy);
            
            // check if combat ended
            (var combatEnded, var winnerSide) = CheckCombatEndConditions();
            if (combatEnded)
            {
                if (winnerSide == null) Debug.LogError("Winner side cannot be null when ending combat.");
                EndCombat(winnerSide.Value);
                return;
            }

            CombatArea.GetCombatantPosition(combatant).Combatant = null;

            if (_activeCombatant == combatant)
            {
                EndCombatantTurn(_activeCombatant, enqueue: false);
            }
            else
            {
                List<CombatController> combatants = new List<CombatController>(_combatantOrder.ToArray());
                combatants.Remove(combatant);
                _combatantOrder.Clear();
                foreach (var comb in combatants) _combatantOrder.Enqueue(comb);
            }
        }

        #endregion
    }
}
