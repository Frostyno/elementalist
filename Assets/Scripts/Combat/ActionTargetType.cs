﻿namespace Elementalist.Combat
{
    public enum ActionTargetType
    {
        Self,
        Ally,
        Enemy
    }
}