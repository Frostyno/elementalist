﻿using Elementalist.Character;
using Elementalist.Utility;
using UnityEngine;

namespace Elementalist.Combat.Effects
{
    [CreateAssetMenu(fileName = "DamageEffect", menuName = "Elementalist/Effects/Immediate/Damage Effect", order = 0)]
    public class DamageEffect : ImmediateEffect
    {
        [SerializeField] private bool bypassBarrier = false;

        public override void Execute(Character.Character target, Character.Character source = null)
        {
            base.Execute(target, source);

            foreach (var elementType in EnumUtility.GetValues<ElementType>())
            {
                var spellPower = ComputeTotalSpellPower(elementType, source);
                if (spellPower > 0d)
                    target.TakeDamage(spellPower, elementType, bypassBarrier);
            }
        }
    }
}
