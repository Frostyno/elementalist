﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Elementalist.Character;
using Elementalist.Character.Stats;
using UnityEngine;

namespace Elementalist.Combat.Effects
{
    [System.Serializable] public class TemporaryEffectProbPair : ItemProbabilityPair<TemporaryEffect> { }

    public abstract class TemporaryEffect : ScriptableObject, ITemporaryEffect
    {
        [SerializeField] private string _effectName = "";

        [Space]

        [SerializeField] private bool _executeWhenAdded = false;
        [Tooltip("Specifies whether the temporary effect should be updated either when round begins (true) or ends (false).")]
        [SerializeField] private bool _processOnRoundBegan = true;
        [SerializeField] private uint _length = 0;
        [Tooltip("Specifies how often should effect be executed (in rounds). Value of 0 means no period.")]
        [SerializeField] private uint _period = 0;

        [Space]

        [SerializeField] private StatModifier _statModifier = null;
        // for use in inspector, this version of Unity doesn't support interfaces in inspector
        [SerializeField] private ImmediateEffect[] _scriptableImmediateEffects = null;

        private List<IImmediateEffect> _effects = new List<IImmediateEffect>();

        public string EffectName { get => _effectName; }

        public bool ExecuteWhenAdded { get => _executeWhenAdded; }
        public bool ProcessOnRoundBegan { get => _processOnRoundBegan; }
        public uint Length { get => _length; }
        public uint Period { get => _period; }

        public StatModifier StatModifier { get => _statModifier; }
        public ReadOnlyCollection<IImmediateEffect> ImmediateEffects { get; private set; }

        private void OnEnable()
        {
            // effects list is built here
            _effects = new List<IImmediateEffect>();

            if (_scriptableImmediateEffects != null)
                foreach (var effect in _scriptableImmediateEffects)
                    if (effect != null) _effects.Add(effect);

            ImmediateEffects = new ReadOnlyCollection<IImmediateEffect>(_effects);
        }

        public virtual void Execute(Character.Character target, Character.Character source = null)
        {
            foreach (var effect in _effects) effect.Execute(target, source);
        }
    }
}
