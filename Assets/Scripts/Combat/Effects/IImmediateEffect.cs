﻿namespace Elementalist.Combat.Effects
{
    public interface IImmediateEffect
    {
        void Execute(Character.Character target, Character.Character source = null);
    }
}
