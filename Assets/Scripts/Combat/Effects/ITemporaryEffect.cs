﻿namespace Elementalist.Combat.Effects
{
    public interface ITemporaryEffect
    {
        uint Length { get; }
        uint Period { get; }
        bool ExecuteWhenAdded { get; }
        bool ProcessOnRoundBegan { get; }
        void Execute(Character.Character target, Character.Character source = null);
    }
}
