﻿using UnityEngine;

namespace Elementalist.Combat.Effects
{
    [CreateAssetMenu(fileName = "ActionEffect", menuName = "Elementalist/Effects/Temporary/Action Effect", order = 0)]
    public class ActionEffect : TemporaryEffect
    {
        public override void Execute(Character.Character target, Character.Character source = null)
        {
            base.Execute(target, source);
        }
    }
}
