﻿using System;
using System.Collections;
using System.Collections.Generic;
using Elementalist.Character;

namespace Elementalist.Combat.Effects
{
    public class TemporaryEffectHandler<T> where T : class, ITemporaryEffect
    {
        private uint _roundsLeft = 0;
        private uint _roundsSinceLastTrigger = 0;

        private ITemporaryEffect _tempEffect = null;

        private Character.Character _target = null;
        private Character.Character _source = null;

        public uint RoundsLeft { get => _roundsLeft; }
        public T Effect { get => _tempEffect as T; }

        public event Action<TemporaryEffectHandler<T>> OnEffectEnded = null;
        
        public TemporaryEffectHandler(ITemporaryEffect tempEffect, Character.Character target, Character.Character source = null)
        {
            _target = target;
            _source = source;

            _tempEffect = tempEffect;

            _roundsLeft = _tempEffect.Length;

            // TODO: consider if this shouldnt be handled in a different way
            if (_tempEffect.ExecuteWhenAdded) _tempEffect.Execute(target, source);
        }


        public void Update(uint roundsCount)
        {
            if (roundsCount == 0) return;

            if (_roundsLeft < roundsCount) roundsCount = _roundsLeft;
            
            // TODO: consider making this more performance friendly
            while (roundsCount > 0)
            {
                _roundsSinceLastTrigger++;
                if (_roundsSinceLastTrigger == _tempEffect.Period)
                {
                    _roundsSinceLastTrigger = 0;
                    _tempEffect.Execute(_target, _source);
                }

                _roundsLeft--;
                roundsCount--;
            }

            if (_roundsLeft == 0) OnEffectEnded?.Invoke(this);
        }
    }
}
