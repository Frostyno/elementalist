﻿namespace Elementalist.Combat.Effects
{
    public enum StatusType
    {
        Burning,
        Stunned,
        Electrified,
        Wet,
        Chilled
    }
}
