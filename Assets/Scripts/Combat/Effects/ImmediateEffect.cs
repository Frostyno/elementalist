﻿using Elementalist.Character;
using UnityEngine;

namespace Elementalist.Combat.Effects
{
    [System.Serializable] public class ImmediateEffectProbPair : ItemProbabilityPair<ImmediateEffect> { }

    public abstract class ImmediateEffect : ScriptableObject, IImmediateEffect
    {
        [SerializeField] private string _effectName = "Effect";

        [Space]

        [SerializeField] private ElementDataDouble _baseSpellPower = new ElementDataDouble();
        [Tooltip("Represents what percentage of effect's source character's spell power should be used to strenghten effect.")]
        [SerializeField] private ElementDataDouble _spellPowerPercentage = new ElementDataDouble();

        public string EffectName { get => _effectName; }

        public ElementDataDouble BaseSpellPower { get => _baseSpellPower; }
        public ElementDataDouble SpellPowerPercentage { get => _spellPowerPercentage; }

        public virtual void Execute(Character.Character target, Character.Character source = null)
        {
        }

        /// <summary>
        /// Computes total spell power for a given element.
        /// </summary>
        /// <param name="elementType">Type of element to compute spell power for.</param>
        /// <param name="source">Source of the effect.</param>
        /// <returns>Total spell power.</returns>
        protected double ComputeTotalSpellPower(ElementType elementType, Character.Character source = null)
        {
            double spellPower = BaseSpellPower.GetValue(elementType);

            if (source != null)
            {
                double percentage = SpellPowerPercentage.GetValue(elementType);
                if (percentage > 0d) spellPower += source.SpellPower(elementType) * percentage;
            }

            return spellPower;
        }
    }
}
