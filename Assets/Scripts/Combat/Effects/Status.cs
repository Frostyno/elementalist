﻿using System.Collections;
using System.Collections.Generic;
using Elementalist.Character;
using UnityEngine;

namespace Elementalist.Combat.Effects
{
    [CreateAssetMenu(fileName = "Status", menuName = "Elementalist/Effects/Temporary/Status", order = 0)]
    public class Status : TemporaryEffect
    {
        [SerializeField] private StatusType _statusType = StatusType.Burning;

        public StatusType StatusType { get => _statusType; }

        public override void Execute(Character.Character target, Character.Character source = null)
        {
            base.Execute(target, source);
        }
    }
}
