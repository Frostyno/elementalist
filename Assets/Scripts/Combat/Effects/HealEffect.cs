﻿using Elementalist.Character;
using Elementalist.Utility;
using UnityEngine;

namespace Elementalist.Combat.Effects
{
    [CreateAssetMenu(fileName = "HealEffect", menuName = "Elementalist/Effects/Immediate/Heal Effect", order = 0)]
    public class HealEffect : ImmediateEffect
    {
        public override void Execute(Character.Character target, Character.Character source = null)
        {
            base.Execute(target, source);

            foreach (var elementType in EnumUtility.GetValues<ElementType>())
            {
                var spellPower = ComputeTotalSpellPower(elementType, source);
                if (spellPower > 0d)
                    target.RestoreHealth(spellPower, elementType);
            }
        }
    }
}
