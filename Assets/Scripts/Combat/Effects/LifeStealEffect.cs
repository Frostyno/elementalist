﻿using Elementalist.Character;
using Elementalist.Utility;
using UnityEngine;

namespace Elementalist.Combat.Effects
{
    /// <summary>
    /// Deals damage to target character and heals source character.
    /// </summary>
    [CreateAssetMenu(fileName = "LifestealEffect", menuName = "Elementalist/Effects/Immediate/Lifesteal Effect", order = 0)]
    public class LifeStealEffect : ImmediateEffect
    {
        [SerializeField] private ElementDataDouble _stealPercentage = new ElementDataDouble();
        [SerializeField] private bool bypassBarrier = false;

        public ElementDataDouble StealPercentage => _stealPercentage;
        
        public override void Execute(Character.Character target, Character.Character source = null)
        {
            base.Execute(target, source);

            foreach (var elementType in EnumUtility.GetValues<ElementType>())
            {
                var spellPower = ComputeTotalSpellPower(elementType, source);
                if (spellPower > 0d)
                {
                    double damageGiven = target.TakeDamage(spellPower, elementType, bypassBarrier);
                    if (damageGiven > 0d && source != null)
                        source.RestoreHealth(damageGiven * _stealPercentage.GetValue(elementType), elementType);
                }
            }
        }
    }
}
