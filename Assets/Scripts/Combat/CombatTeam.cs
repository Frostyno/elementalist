﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Combat
{
    public class CombatTeam
    {
        public CombatSide Side { get; private set; }

        public CombatTeam(CombatSide side)
        {
            Side = side;
        }
    }
}
