﻿using Elementalist.Combat.Actions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Combat
{
    public class PlayerCombatController : CombatController
    {
        public event Action OnPlayerTurnBegan = null;
        public event Action OnPlayerTurnEnded = null;

        private IAction _activeAction = null;
        private object _activeActionSource = null;
        private bool _isUsingAction = false;

        public event Action<IAction, object, bool> OnActionExecuted = null;

        public void SetCombat(Combat combat) => this.combat = combat;

        public void SetAction(IAction action, object actionSource = null)
        {
            if (!OnTurn) return;
            if (_isUsingAction) return;

            if (_activeAction != null)
                ClearActiveAction();

            _activeAction = action;
            _activeActionSource = actionSource;

            _activeAction.MarkTargetablePositions(this, combat.CombatArea);
        }

        public void ClearActiveAction()
        {
            _activeAction = null;
            _activeActionSource = null;

            // to prevent null ptr exception when combat ends by player killing enemy
            // TODO: consider resolving in a different way
            if (combat != null)
                combat.CombatArea.MakePositionsUntargetable();
        }

        /// <summary>
        /// Hooks up events to detect when targetable combat positions are clicked
        /// by the player.
        /// </summary>
        public override void BeginCombat()
        {
            base.BeginCombat();

            foreach (var position in combat.CombatArea.LeftSidePositions)
                position.OnPositionTargeted += OnCombatPositionTargeted;
            foreach (var position in combat.CombatArea.RightSidePositions)
                position.OnPositionTargeted += OnCombatPositionTargeted;
        }


        /// <summary>
        /// Unhooks up events to detect when targetable combat positions are clicked
        /// by the player.
        /// </summary>
        public override void EndCombat()
        {
            foreach (var position in combat.CombatArea.LeftSidePositions)
                position.OnPositionTargeted -= OnCombatPositionTargeted;
            foreach (var position in combat.CombatArea.RightSidePositions)
                position.OnPositionTargeted -= OnCombatPositionTargeted;

            base.EndCombat();
        }

        public override void BeginTurn()
        {
            base.BeginTurn();
            OnPlayerTurnBegan?.Invoke();
        }

        public override void EndTurn()
        {
            if (!OnTurn) return;

            base.EndTurn();
            if (_activeAction != null)
                ClearActiveAction();

            OnPlayerTurnEnded?.Invoke();
        }

        protected override void ResetController()
        {
            base.ResetController();

            OnPlayerTurnBegan = null;
            _activeAction = null;
        }

        /// <summary>
        /// Executes active action on target and clears it.
        /// Triggered when a targetable combat position is clicked on.
        /// </summary>
        /// <param name="target">Target of the active action.</param>
        private async void OnCombatPositionTargeted(CombatController target)
        {
            _isUsingAction = true;
            combat.CombatArea.MakePositionsUntargetable();
            var success = await _activeAction.Execute(this, target, combat.CombatArea);
            if (!success)
            {
                Debug.LogError(string.Format("Player's action {0} did not execute. Conditions for action's validity should be checked before setting it as active " +
                    "and executing it.", _activeAction.ToString()));
            }

            OnActionExecuted?.Invoke(_activeAction, _activeActionSource, success);
            ClearActiveAction();

            _isUsingAction = false;
        }
    }
}
