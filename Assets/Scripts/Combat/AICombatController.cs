﻿using Elementalist.AI.BehaviorTree;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Elementalist.Combat
{
    public class AICombatController : CombatController
    {
        [SerializeField] private string _enemyName = "";
        [SerializeField] private Enemy _enemy = Enemy.TestingEnemy;

        [Space]

        [SerializeField] private string _behaviorFileName = "";

        private BehaviorTree _behavior = null;

        public string EnemyName { get => _enemyName; }
        public Enemy Enemy { get => _enemy; }

        [Inject]
        private void Inject(BehaviorTreeLoader loader)
        {
            _behavior = loader.LoadBehaviorTree(_behaviorFileName);
            if (_behavior == null)
                Debug.LogError(string.Format("Failed to laod behavior for {0}. Behavior name \"{1}\".", name, _behaviorFileName));
        }

        public override void BeginTurn()
        {
            base.BeginTurn();
            
            RunAIBehavior();
        }

        public override void EndCombat()
        {
            base.EndCombat();

            if (OnTurn) EndTurn();
        }

        /// <summary>
        /// Runs AI's attached behavior while AI is on turn.
        /// If AI's behavior executes without succeess (AI can't do anything more this turn),
        /// ends turn for AI.
        /// </summary>
        private async void RunAIBehavior()
        {
            if (_behavior == null)
            {
                Debug.LogError(string.Format("AI controller is missing it's behavior in {0}", name));
                EndTurn();
                return;
            }
            
            while (OnTurn)
            {
                // Added so AI won't start spamming spells the moment it get's on turn.
                await Task.Delay(500);

                var result = await _behavior.Execute(CreateBehaviorContext());
                if (result == BTProcessResult.Failure) EndTurn();
            }

            await Task.Delay(500);
        }

        /// <summary>
        /// Creates new context for behavior tree.
        /// </summary>
        /// <returns>New initializec context.</returns>
        private BTContext CreateBehaviorContext()
        {
            BTContext context = new BTContext();
            CombatContext combatContext = new CombatContext();

            combatContext.Combat = combat;
            combatContext.CombatArea = combat.CombatArea;
            combatContext.Source = this;

            context.SetDataItem(combatContext);

            return context;
        }
    }
}
