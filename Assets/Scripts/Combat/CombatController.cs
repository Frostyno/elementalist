﻿using Elementalist.Character;
using System;
using UnityEngine;
using Zenject;

namespace Elementalist.Combat
{
    public abstract class CombatController : MonoBehaviour
    {
        protected Combat combat = null;
        private CharacterEvents _characterEvents = null;
        
        public bool OnTurn { get; private set; }

        public Character.Character Character { get; private set; }
        public CombatTeam Team { get; set; }
        
        [Inject]
        private void Inject(
            Character.Character character,
            CharacterEvents characterEvents,
            [InjectOptional] Combat combat // inject is optional to simplify some tests
            ) 
        {
            Character = character;
            _characterEvents = characterEvents;
            this.combat = combat;
        }

        public virtual void BeginCombat() { }

        public virtual void EndCombat()
        {
            _characterEvents.SignalCombatEnded();
            ResetController();
        }

        public void BeginRound()
        {
            _characterEvents.SignalRoundBegan();
        }

        public void EndRound()
        {
            _characterEvents.SignalRoundEnded();
        }

        public virtual void BeginTurn()
        {
            OnTurn = true;
        }

        public virtual void EndTurn()
        {
            OnTurn = false;
            combat.EndCombatantTurn(this);
        }

        protected virtual void ResetController()
        {
            Team = null;
            combat = null;
            OnTurn = false;
        }

        public class CombatControllerFactory : PlaceholderFactory<CombatController> { }
    }
}
