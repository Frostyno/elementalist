﻿using UnityEngine;
using Zenject;

namespace Elementalist.Combat.Utility
{
    public class CombatPositionSpawner : MonoBehaviour
    {
        private CombatPosition.CombatPositionFactory _combatPositionFactory = null;
        
        [SerializeField] private Transform _positionParent = null;

        [Space]

        [Tooltip("Specifies x distance between first position and position (0, 0)")]
        [SerializeField] private float _distanceFromMiddle = 0.25f;
        [Tooltip("Specifies x space between two positions on the same side.")]
        [SerializeField] private float _spaceBetweenPositions = 0.25f;

        private uint _leftPositionsSpawned = 0;
        private uint _rightPositionSpawned = 0;

        [Inject]
        private void Inject(CombatPosition.CombatPositionFactory factory)
        {
            _combatPositionFactory = factory;
        }

        /// <summary>
        /// Spawns a new CombatPosition and sets it's position.
        /// </summary>
        /// <param name="side">Side of CombatArea where position should be spawned.</param>
        /// <returns>Spawned CombatPosition.</returns>
        public CombatPosition SpawnCombatPosition(CombatSide side)
        {
            var combatPosition = _combatPositionFactory.Create();
            if (_positionParent != null) combatPosition.transform.parent = _positionParent;

            float combatPositionWidth = combatPosition.GetComponent<Collider2D>().bounds.size.x;

            // Calculate x position of CombatPosition
            Vector2 spacePosition = Vector2.zero;
            uint positionsSpawned = side == CombatSide.Left ? _leftPositionsSpawned : _rightPositionSpawned;

            spacePosition.x = _distanceFromMiddle + (combatPositionWidth / 2);
            spacePosition.x += (combatPositionWidth * positionsSpawned) + (_spaceBetweenPositions * positionsSpawned);

            if (side == CombatSide.Left)
            {
                // positions on left combat side are in negative X axis
                spacePosition.x *= -1;
                _leftPositionsSpawned++;
            }
            else _rightPositionSpawned++;

            combatPosition.transform.position = spacePosition;

            return combatPosition;
        }
    }
}
