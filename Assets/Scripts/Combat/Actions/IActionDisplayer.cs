﻿using System.Threading.Tasks;
using UnityEngine;

namespace Elementalist.Combat.Actions
{
    public interface IActionDisplayer
    {
        Task Display(Vector3 targetPosition, Vector2? size = null);
    }
}