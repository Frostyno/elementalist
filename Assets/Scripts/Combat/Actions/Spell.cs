﻿using System;
using System.Runtime.Serialization;

namespace Elementalist.Combat.Actions
{
    [Serializable]
    [DataContract]
    public enum Spell
    {
        [EnumMember]TestSpell,
        [EnumMember]TestingFireball,
        [EnumMember]TestingQuickHeal,
        [EnumMember]BoulderToss,
        [EnumMember]Fortitude,
        [EnumMember]AvatarOfWater,
        [EnumMember]LesserHeal,
        [EnumMember]WrathOfThunder,
        [EnumMember]Haste,
        [EnumMember]Fireball,
        [EnumMember]LesserShielding
    }
}