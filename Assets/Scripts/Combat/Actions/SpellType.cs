﻿using Elementalist.Character;
using Elementalist.Combat.Effects;
using Elementalist.Combat.Targeting;
using Elementalist.Utility;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using UnityEngine;

namespace Elementalist.Combat.Actions
{
    [CreateAssetMenu(fileName = "NewSpell", menuName = "Elementalist/Actions/Spell", order = 0)]
    public class SpellType : ScriptableObject, IAction
    {
        [SerializeField] private string _spellName = "NewSpell";
        [SerializeField] private Spell _spellEnum = Spell.TestSpell;
        [SerializeField] private Sprite _icon = null;
        [SerializeField] private GameObject _actionDisplayerPrefab = null;

        [Space]
        [Header("Spell costs")]

        [SerializeField] private double _actionPointsCost = 1d;
        [SerializeField] private ElementDataDouble _orbsCost = new ElementDataDouble();

        [Space]
        [Header("Targeting")]

        [SerializeField] private SOTargeting _targeting = null;
        [SerializeField] private ActionTargetType[] _targetTypes = null;
        //[SerializeField] private uint _backAreaOfEffect = 0;
        //[SerializeField] private uint _frontAreaOfEffect = 0;

        [Space]
        [Header("Effects")]

        [SerializeField] private TemporaryEffectProbPair[] _temporaryEffects = null;
        [SerializeField] private ImmediateEffectProbPair[] _immediateEffects = null;

        private System.Random _effectProcChanceGenerator = new System.Random();
        private HashSet<ActionTargetType> _hashTargetTypes = null;

        public string SpellName { get => _spellName; }
        public Spell SpellEnum { get => _spellEnum; }
        public Sprite Icon { get => _icon; }

        public double ActionPointsCost => _actionPointsCost;
        public ElementDataDouble OrbsCost => _orbsCost;

        public ReadOnlyCollection<ImmediateEffectProbPair> ImmediateEffects { get; private set; }
        public ReadOnlyCollection<TemporaryEffectProbPair> TemporaryEffects { get; private set; }

        private void OnEnable()
        {
            if (_actionDisplayerPrefab == null ||
                !_actionDisplayerPrefab.TryGetComponent<IActionDisplayer>(out var displayer))
            {
                Debug.LogWarning($"Spell {_spellName} on {name} is missing action displayer prefab. Either prefab is not attached or is missing IActionDisplayer component.");
            }

            ImmediateEffects = new ReadOnlyCollection<ImmediateEffectProbPair>(_immediateEffects);
            TemporaryEffects = new ReadOnlyCollection<TemporaryEffectProbPair>(_temporaryEffects);

            if (_targetTypes == null)
            {
                Debug.LogWarning(string.Format("Target types are null on spell {0}", name));
                return;
            }
            _hashTargetTypes = new HashSet<ActionTargetType>(_targetTypes);
        }

        /// <summary>
        /// Checks if character who is executing the spell has enough resources to do so.
        /// </summary>
        /// <param name="source">Character who is executing the spell.</param>
        /// <returns>True if source can execute the spell.</returns>
        public bool CanExecute(Character.Character source)
        {
            if (source.CurrentActionPoints < _actionPointsCost) return false;

            foreach (var element in EnumUtility.GetValues<ElementType>())
                if (source.CurrentOrbs(element) < _orbsCost.GetValue(element))
                    return false;

            return true;
        }

        private bool UseResources(Character.Character source)
        {
            if (!CanExecute(source)) return false;

            source.UseActionPoints(_actionPointsCost);

            foreach (var element in EnumUtility.GetValues<ElementType>())
                source.UseOrbs(_orbsCost.GetValue(element), element);

            return true;
        }

        /// <summary>
        /// Checks conditions for spell execution. Then executes all immediate and temporary effects (if they proc).
        /// </summary>
        /// <param name="source">Caster of the spell.</param>
        /// <param name="target">Target of the spell.</param>
        /// <param name="area">Combat area of the current combat.</param>
        /// <returns>True if spell executed properly.</returns>
        public async Task<bool> Execute(CombatController source, CombatController target, CombatArea area)
        {
            if (!UseResources(source.Character)) return false;
            if (!_targeting.IsTargetablePosition(source, target, area, _hashTargetTypes)) return false;

            if (_actionDisplayerPrefab != null) await DisplaySpell(source, target, area);

            if (_immediateEffects != null)
            {
                foreach (var effect in _immediateEffects)
                {
                    double prob = _effectProcChanceGenerator.NextDouble();
                    if (prob < effect.Probability) effect.Item.Execute(target.Character, source.Character);
                }
            }

            if (_temporaryEffects != null)
            {
                foreach (var effect in _temporaryEffects)
                {
                    double prob = _effectProcChanceGenerator.NextDouble();
                    if (prob < effect.Probability) target.Character.AddTemporaryEffect(effect.Item, target.Character, source.Character);
                }
            }

            // TODO: after addyng async spell spawn change to return true;
            return true;
        }

        private async Task DisplaySpell(CombatController source, CombatController target, CombatArea area)
        {
            if (_actionDisplayerPrefab == null ||
                !_actionDisplayerPrefab.TryGetComponent<IActionDisplayer>(out var d)) return;

            var displayer = Instantiate(_actionDisplayerPrefab);
            displayer.transform.position = area.GetCombatantPosition(source).Center;
            var position = area.GetCombatantPosition(target);
            await displayer.GetComponent<IActionDisplayer>().Display(position.Center, position.Size);

            Destroy(displayer);
        }

        /// <summary>
        /// Makes valid combat positions (positions that can be hit by this spell) targetable.
        /// </summary>
        /// <param name="source">Caster of the spell.</param>
        /// <param name="area">Combat area of the current combat.</param>
        public void MarkTargetablePositions(CombatController source, CombatArea area)
        {
            if (_targeting == null)
            {
                Debug.LogError("Targeting is null. Cannot mark targetable positions.");
                return;
            }

            _targeting.MarkTargetablePositions(source, area, _hashTargetTypes);
        }
    }
}
