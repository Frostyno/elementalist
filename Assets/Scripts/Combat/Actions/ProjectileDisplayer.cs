﻿using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

namespace Elementalist.Combat.Actions
{
    public class ProjectileDisplayer : MonoBehaviour, IActionDisplayer
    {
        [SerializeField, Range(0.1f, 10f)] private float _speed = 2f;

        private TaskCompletionSource<bool> _targetReachedWaiter = null;

        private Vector3? _targetPosition = null;

        public async Task Display(Vector3 targetPosition, Vector2? size = null)
        {
            if (_targetReachedWaiter != null) return;

            _targetPosition = targetPosition;
            var scale = transform.localScale;
            scale.x *= targetPosition.x > transform.position.x ? 1 : -1;
            transform.localScale = scale;

            _targetReachedWaiter = new TaskCompletionSource<bool>();

            await _targetReachedWaiter.Task;

            _targetReachedWaiter = null;
            // TODO: spawn particles
        }

        private void FixedUpdate()
        {
            if (_targetReachedWaiter == null) return;

            MoveTowardsTarget();
            CheckReachedTarget();
        }

        private void MoveTowardsTarget()
        {
            if (!_targetPosition.HasValue) return;
            if (_targetPosition.Value == transform.position) return;

            transform.position = Vector3.MoveTowards(transform.position, _targetPosition.Value, _speed * Time.fixedDeltaTime);
        }

        private void CheckReachedTarget()
        {
            if (_targetReachedWaiter == null) return;
            if (!_targetPosition.HasValue) return;
            if (_targetPosition.Value != transform.position) return;

            _targetReachedWaiter.SetResult(true);
        }
    }
}
