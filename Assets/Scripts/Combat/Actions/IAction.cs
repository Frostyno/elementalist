﻿using System.Threading.Tasks;

namespace Elementalist.Combat.Actions
{
    public interface IAction
    {
        bool CanExecute(Character.Character source);
        void MarkTargetablePositions(CombatController source, CombatArea area);
        Task<bool> Execute(CombatController source, CombatController target, CombatArea area);
    }
}
