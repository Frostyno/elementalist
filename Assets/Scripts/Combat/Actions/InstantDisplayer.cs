﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Elementalist.Combat.Actions
{
    public class InstantDisplayer : MonoBehaviour, IActionDisplayer
    {
        [SerializeField] private ParticleSystem _particleSystem = null;

        [SerializeField] private float _length = 1f;
        [SerializeField] private Vector2 _offset = Vector2.zero;

        private TaskCompletionSource<bool> _displayWaiter = null;

        private float _timeSinceStart = 0f;

        public async Task Display(Vector3 targetPosition, Vector2? size = null)
        {
            if (_displayWaiter != null) return;

            transform.position = targetPosition + (Vector3)_offset;
            _displayWaiter = new TaskCompletionSource<bool>();
            if (_particleSystem != null && size.HasValue)
            {
                var shape = _particleSystem.shape;
                shape.scale = size.Value;
            }

            await _displayWaiter.Task;

            _displayWaiter = null;
        }

        private void FixedUpdate()
        {
            if (_displayWaiter == null) return;

            if (_timeSinceStart >= _length)
            {
                _timeSinceStart = 0f;
                _displayWaiter.SetResult(true);
                return;
            }

            _timeSinceStart += Time.fixedDeltaTime;
        }
    }
}
