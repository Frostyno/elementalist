﻿using Elementalist.Combat.Actions;

namespace Elementalist.Combat
{
    public class CombatContext
    {
        public Combat Combat { get; set; }
        public CombatArea CombatArea { get; set; }
        public CombatController Source { get; set; }
        public CombatController Target { get; set; }
    }
}
