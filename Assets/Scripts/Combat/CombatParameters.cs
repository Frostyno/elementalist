﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using Zenject;

namespace Elementalist.Combat
{
    public class CombatParameters
    {
        public CombatSide PlayerCombatSide { get; private set; }
        public CombatSide EnemyCombatSide
        {
            get => PlayerCombatSide == CombatSide.Left ? CombatSide.Right : CombatSide.Left;
        }

        public ReadOnlyCollection<GameObject> EnemyPrefabs { get; private set; }

        private List<GameObject> _enemyPrefabs = new List<GameObject>();

        private uint _leftCombatAreaSize;
        private uint _rightCombatAreaSize;

        public CombatParameters(
            uint leftCombatAreaSize,
            uint rightCombatAreaSize,
            CombatSide playerCombatSide = CombatSide.Left)
        {
            if (leftCombatAreaSize == 0 || rightCombatAreaSize == 0)
                Debug.LogError("Combat area size cannot be 0.");

            PlayerCombatSide = playerCombatSide;

            _leftCombatAreaSize = leftCombatAreaSize;
            _rightCombatAreaSize = rightCombatAreaSize;

            EnemyPrefabs = new ReadOnlyCollection<GameObject>(_enemyPrefabs);
        }

        public uint GetCombatAreaSize(CombatSide side) => side == CombatSide.Left? _leftCombatAreaSize : _rightCombatAreaSize;

        public void AddEnemyPrefab(GameObject enemyPrefab)
        {
            if (EnemyPrefabs.Count == GetCombatAreaSize(EnemyCombatSide))
            {
                Debug.LogWarning("Cannot add more enemy prefabs in CombatParametes. Combat area is too small.");
                return;
            }

            _enemyPrefabs.Add(enemyPrefab);
        }
    }
}
