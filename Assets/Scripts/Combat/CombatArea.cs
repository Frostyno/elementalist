﻿using Elementalist.Combat.Utility;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Elementalist.Combat
{
    public class CombatArea
    {
        private List<CombatPosition> _leftSidePositions = new List<CombatPosition>();
        private List<CombatPosition> _rightSidePositions = new List<CombatPosition>();

        public readonly ReadOnlyCollection<CombatPosition> LeftSidePositions;
        public readonly ReadOnlyCollection<CombatPosition> RightSidePositions;

        public CombatArea(
            CombatPositionSpawner spawner,
            List<CombatController> leftSideCombatants,
            List<CombatController> rightSideCombatants,
            CombatParameters combatParams)
        {
            if (spawner == null)
            {
                Debug.LogError("CombatPositionSpawner is null.");
            }
            else
            {
                LeftSidePositions = new ReadOnlyCollection<CombatPosition>(_leftSidePositions);
                RightSidePositions = new ReadOnlyCollection<CombatPosition>(_rightSidePositions);

                SpawnPositions(spawner, combatParams);
                EmplaceCombatants(leftSideCombatants, rightSideCombatants);
            }
        }

        /// <summary>
        /// Spawns combat positions in the scene based on combat area size
        /// definec in combat parameters.
        /// </summary>
        /// <param name="spawner">Spawner of combat positions.</param>
        /// <param name="combatParams">Parameters of the combat containing combat area size.</param>
        private void SpawnPositions(CombatPositionSpawner spawner, CombatParameters combatParams)
        {
            for (uint i = 0; i < combatParams.GetCombatAreaSize(CombatSide.Left); i++)
            {
                _leftSidePositions.Add(spawner.SpawnCombatPosition(CombatSide.Left));
            }

            for (uint i = 0; i < combatParams.GetCombatAreaSize(CombatSide.Right); i++)
            {
                _rightSidePositions.Add(spawner.SpawnCombatPosition(CombatSide.Right));
            }
        }

        /// <summary>
        /// Places combatants into their respective combat positions.
        /// </summary>
        /// <param name="leftSideCombatants">Combatants on the left side.</param>
        /// <param name="rightSideCombatants">Combatants on the right side.</param>
        private void EmplaceCombatants(
            List<CombatController> leftSideCombatants,
            List<CombatController> rightSideCombatants)
        {
            if (_leftSidePositions.Count < leftSideCombatants.Count)
            {
                Debug.LogError("Not enough combat positions for spawned combatants on the left side.");
                return;
            }
            else if (_rightSidePositions.Count < rightSideCombatants.Count)
            {
                Debug.LogError("Not enough combat positions for spawned combatants on the right side.");
            }

            for (int i = 0; i < leftSideCombatants.Count; i++)
                _leftSidePositions[i].Combatant = leftSideCombatants[i];
            for (int i = 0; i < rightSideCombatants.Count; i++)
                _rightSidePositions[i].Combatant = rightSideCombatants[i];
        }

        /// <summary>
        /// Finds the combat position containing the combatant on specified combat area side.
        /// </summary>
        /// <param name="combatant">Combatant to find.</param>
        /// <param name="side">Combat side where combatant should be looked for.</param>
        /// <returns>Combat position of the combatant (or null if not found).</returns>
        private CombatPosition FindCombatantInSide(CombatController combatant, CombatSide side)
        {
            List<CombatPosition> positions = side == CombatSide.Left ? _leftSidePositions : _rightSidePositions;
            CombatPosition position = null;

            foreach (var pos in positions)
            {
                if (pos.Combatant == combatant)
                {
                    position = pos;
                    break;
                }
            }

            return position;
        }

        private int GetCombatantPositionIndex(CombatController combatant)
        {
            var sidePositions = GetCombatSide(combatant.Team.Side);
            int combatantIndex = -1;
            for (int i = 0; i < sidePositions.Count; i++)
            {
                if (sidePositions[i].Combatant == combatant)
                {
                    combatantIndex = i;
                    break;
                }
            }
            if (combatantIndex == -1)
                Debug.LogError("Combatant not found.");

            return combatantIndex;
        }

        /// <summary>
        /// Returns a (readonly) collection of combat positions on specified combat area side.
        /// </summary>
        /// <param name="side">Combat side to get combat positions for.</param>
        /// <returns>Returns a (readonly) collection of combat positions on specified combat area side.</returns>
        public ReadOnlyCollection<CombatPosition> GetCombatSide(CombatSide side)
        {
            return side == CombatSide.Left ? LeftSidePositions : RightSidePositions;
        }

        /// <summary>
        /// Returns the first combat position that contains a combatant on specified combat area side.
        /// </summary>
        /// <param name="side">Combat side to return combat position for.</param>
        /// <returns>First non empty combat position on given side.</returns>
        public CombatPosition GetFirstCombatantPosition(CombatSide side)
        {
            List<CombatPosition> sidePositions = side == CombatSide.Left ? _leftSidePositions : _rightSidePositions;

            CombatPosition position = null;
            foreach (var pos in sidePositions)
            {
                if (pos.Combatant != null)
                {
                    position = pos;
                    break;
                }
            }

            if (position == null)
                Debug.LogError(string.Format("No combatant present on {0} side of CombatArea.", side));

            return position;
        }

        public List<CombatPosition> GetCombatantNeighboursPositions(CombatController combatant)
        {
            List<CombatPosition> positions = new List<CombatPosition>();

            var sidePositions = GetCombatSide(combatant.Team.Side);
            int combatantIndex = GetCombatantPositionIndex(combatant);
            if (combatantIndex == -1) return positions;

            // get left neighbour
            if (combatantIndex > 0)
            {
                for (int i = combatantIndex - 1; i >= 0; i--)
                {
                    if (sidePositions[i].Combatant != null)
                    {
                        positions.Add(sidePositions[i]);
                        break;
                    }
                }
            }
            // get right neighbour
            if (combatantIndex < sidePositions.Count - 1)
            {
                for (int i = combatantIndex + 1; i < sidePositions.Count; i++)
                {
                    if (sidePositions[i].Combatant != null)
                    {
                        positions.Add(sidePositions[i]);
                        break;
                    }
                }
            }
            
            return positions;
        }

        /// <summary>
        /// Finds the combat position containing combatant.
        /// </summary>
        /// <param name="combatant">Combatant to find.</param>
        /// <returns>Combat position of the combatant (or null if not found).</returns>
        public CombatPosition GetCombatantPosition(CombatController combatant)
        {
            if (combatant == null)
            {
                Debug.LogError("CombatArea::GetCombatantPosition called with combatant = null.");
                return null;
            }

            CombatPosition position = FindCombatantInSide(combatant, CombatSide.Left);
            if (position == null) position = FindCombatantInSide(combatant, CombatSide.Right);

            if (position == null) Debug.LogWarning(string.Format("Combatant {0} not found in CombatArea.", combatant.name));

            return position;
        }

        public void MakePositionsUntargetable()
        {
            foreach (var position in _leftSidePositions)
                position.SetTargetable(false);
            foreach (var position in _rightSidePositions)
                position.SetTargetable(false);
        }
    }
}
