﻿using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Combat
{ 
    public class EnemyPrefabProvider : MonoBehaviour
    {
        [Tooltip("Specifies folder (with subfolders) containing all enemy prefabs within Resources folder.")]
        [SerializeField] private string _enemiesFolderPath = "Enemies";

        private Dictionary<Enemy, AICombatController> _enemies = new Dictionary<Enemy, AICombatController>();

        private void Awake()
        {
            var enemies = Resources.LoadAll<AICombatController>(_enemiesFolderPath);

            foreach (var enemy in enemies)
            {
                if (_enemies.ContainsKey(enemy.Enemy))
                {
                    Debug.LogWarning($"Multiple enemies with same enum value found. These are {_enemies[enemy.Enemy].EnemyName} and {enemy.EnemyName}.");
                    continue;
                }

                _enemies.Add(enemy.Enemy, enemy);
            }
        }

        public AICombatController GetEnemyPrefab(Enemy enemy)
        {
            if (_enemies.TryGetValue(enemy, out AICombatController enemyPrefab))
                return enemyPrefab;

            return null;
        }
    }
}
