﻿using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using Elementalist.SceneManagement;
using Zenject;

namespace Elementalist.Combat
{
    public class CombatHandler : MonoBehaviour
    {
        private GameObject _mapPivot = null;

        private TaskCompletionSource<CombatSide?> _combatWaiter = null;

        [Inject]
        private void Inject(GameObject mapPivot)
        {
            _mapPivot = mapPivot;
        }

        /// <summary>
        /// Loads combat scene, initializes combat and then waits for it to end.
        /// </summary>
        /// <param name="combatParams">Parameters of the combat.</param>
        /// <returns>Winner side.</returns>
        public async Task<CombatSide?> HandleCombat(CombatParameters combatParams)
        {
            if (_combatWaiter != null)
            {
                Debug.LogError("Combat scene is already running or wasn't ended properly.");
                return null;
            }

            await SceneManager.LoadScene(Scenes.CombatScene);
            _mapPivot.SetActive(false);

            var combat = FindObjectOfType<Combat>();
            _combatWaiter = new TaskCompletionSource<CombatSide?>();
            
            combat.OnCombatEnded += OnCombatEnded;
            combat.Initialize(combatParams);

            var winnerSide = await _combatWaiter.Task;
            await SceneManager.UnloadScene(Scenes.CombatScene);

            if (_mapPivot != null)
                _mapPivot?.SetActive(true);
            _combatWaiter = null;

            return winnerSide;
        }

        private void OnCombatEnded(CombatSide winnerSide)
        {
            _combatWaiter.SetResult(winnerSide);
        }

        private void OnDestroy()
        {
            if (_combatWaiter == null) return;
            _combatWaiter.SetResult(null);
        }
    }
}
