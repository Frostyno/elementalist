﻿using Elementalist.Character;
using System;
using UnityEngine;
using Zenject;

namespace Elementalist.Combat
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class CombatPosition : MonoBehaviour
    {
        private CombatController _combatant = null;
        public CombatController Combatant { get => _combatant; set => SetCombatant(value); }
        /// <summary>
        /// Specifies whether the position should react to mouse click event.
        /// </summary>
        public bool Targetable { get; private set; }
        public Vector2 Center => _collider == null ? Vector2.zero : (Vector2)_collider.bounds.center;
        public Vector2 Size => _collider == null ? Vector2.zero : (Vector2)_collider.bounds.size;

        public event Action<CombatController, CombatController> OnCombatantChanged = null;
        public event Action<CombatController> OnPositionTargeted = null;

        private BoxCollider2D _collider = null;
        private SpriteRenderer _renderer = null;
        private SpriteRenderer _targetableHighlightRenderer = null;

        [Inject]
        private void Inject(SpriteRenderer[] renderers, BoxCollider2D collider)
        {
            if (renderers.Length != 2)
            {
                Debug.LogError(string.Format("Invalid number of renderers ({0}) in {1}.", renderers.Length, name));
                return;
            }

            _collider = collider as BoxCollider2D;
            _renderer = renderers[0];
            _targetableHighlightRenderer = renderers[1];
        }

        public void SetTargetable(bool state)
        {
            Targetable = state;
            _targetableHighlightRenderer.enabled = state;
        }

        private void SetCombatant(CombatController combatant)
        {
            OnCombatantChanged?.Invoke(combatant, Combatant);

            if (combatant != null)
            {
                var characterSprite = combatant.Character.CharacterIcon;
                if (characterSprite != null)
                {
                    _renderer.sprite = characterSprite;

                    // Set position of renderer object so that character's sprite bottom touches y = 0
                    Vector2 rendererPos = _renderer.transform.position;
                    rendererPos.y = characterSprite.bounds.extents.y;
                    _renderer.transform.position = rendererPos;

                    var interactebleSize = new Vector2(1f, characterSprite.bounds.size.y + characterSprite.bounds.extents.y);
                    // Set position and size of target highlight
                    _targetableHighlightRenderer.size = interactebleSize;
                    _targetableHighlightRenderer.transform.localPosition = new Vector3(0f, characterSprite.bounds.extents.y);

                    // Set position and size of collider
                    _collider.size = interactebleSize;
                    _collider.offset = new Vector2(0f, characterSprite.bounds.extents.y);
                }
                else Debug.LogWarning(string.Format("Character sprite is missing for character {0}.", combatant.name));
            }
            else
            {
                _renderer.sprite = null;
            }

            _combatant = combatant;
        }

        private void OnMouseDown()
        {
            if (!Targetable) return;
            if (Combatant == null)
            {
                Debug.LogWarning(string.Format("Targeted a position with no combatant attached on {0}. This isn't allowed.", name));
                return;
            }

            OnPositionTargeted?.Invoke(Combatant);
        }

        public class CombatPositionFactory : PlaceholderFactory<CombatPosition> { }
    }
}
