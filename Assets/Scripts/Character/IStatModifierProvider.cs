﻿using System;
using Elementalist.Character.Stats;

namespace Elementalist.Character
{
    public interface IStatModifierProvider
    {
        StatModifier[] GetStatModifiers();
        event Action OnStatModifiersChanged;
    }
}
