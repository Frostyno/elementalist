﻿using Elementalist.Character.Stats;
using UnityEngine;
using Zenject;

namespace Elementalist.Character
{
    public abstract class ElementStat : MonoBehaviour
    {
        #region AttributesAndProperties

        private ElementData<Stat> _stats = null;

        private CharacterEssence _characterEssence = null;
        private CharacterEvents _characterEvents = null;

        #endregion

        /// <summary>
        /// Inicializes ElementStat.
        /// Needs to be called in child constructor, inject method or awake/start function.
        /// </summary>
        protected void Initialize(CharacterEssence charEss, CharacterEvents charEvents, StatType statType)
        {
            _stats = new ElementData<Stat>(
                new Stat(statType, ElementType.Fire),
                new Stat(statType, ElementType.Water),
                new Stat(statType, ElementType.Earth),
                new Stat(statType, ElementType.Air)
                );

            _characterEssence = charEss;
            _characterEvents = charEvents;

            _characterEvents.OnStatModifiersChanged += OnStatModifiersChanged;
            OnStatModifiersChanged(null);
        }

        #region PublicFunctions

        public double GetValue(ElementType elementType)
        {
            var stat = _stats.GetValue(elementType);
            return stat.Value;
        }

        #endregion

        #region EventCallbacks

        /// <summary>
        /// Recalculates all of the stats contained.
        /// </summary>
        /// <param name="mods">Active stat modifiers.</param>
        private void OnStatModifiersChanged(StatModifier[] mods)
        {
            var charType = _characterEssence.CharacterType;

            _stats.GetValue(ElementType.Fire).Recalculate(charType, mods);
            _stats.GetValue(ElementType.Water).Recalculate(charType, mods);
            _stats.GetValue(ElementType.Earth).Recalculate(charType, mods);
            _stats.GetValue(ElementType.Air).Recalculate(charType, mods);
        }

        private void OnDestroy()
        {
            if (_characterEvents != null)
                _characterEvents.OnStatModifiersChanged -= OnStatModifiersChanged;
        }

        #endregion
    }
}
