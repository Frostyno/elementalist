﻿using System.Collections.Generic;
using UnityEngine;
using Elementalist.Character.Talents;
using Elementalist.Character.Stats;
using System;
using Elementalist.Combat.Actions;

namespace Elementalist.Character
{
    /// <summary>
    /// Contains all of character's talents.
    /// </summary>
    public class CharacterTalents : MonoBehaviour, IStatModifierProvider, ISpellProvider
    {
        [SerializeField] private Talent[] _inspectorTalents = null;

        private List<Talent> _talents = new List<Talent>();
        private List<StatModifier> _statModifiers = new List<StatModifier>();
        private List<SpellType> _spells = new List<SpellType>();

        public event Action OnStatModifiersChanged = null;
        public event Action OnSpellsChanged = null;

        private void Awake()
        {
            if (_inspectorTalents != null)
            {
                AddMultipleTalents(_inspectorTalents);
            }
        }

        public void AddMultipleTalents(IEnumerable<Talent> talentsToAdd)
        {
            (bool addedSpells, bool addedMods) = (false, false);

            _talents.AddRange(talentsToAdd);
            foreach (var talent in talentsToAdd)
            {
                foreach (var statMod in talent.StatModifiers)
                {
                    if (!addedMods) addedMods = true;
                    _statModifiers.Add(statMod);
                }
                foreach (var spell in talent.Spells)
                {
                    if (!addedSpells) addedSpells = true;
                    _spells.Add(spell);
                }
            }

            if (addedMods) OnStatModifiersChanged?.Invoke();
            if (addedSpells) OnSpellsChanged?.Invoke();
        }

        /// <summary>
        /// Adds a new talent and if the talent has a StatModifier
        /// notifies all observers that modifiers changed.
        /// </summary>
        /// <param name="talent">Talent to add.</param>
        public void AddTalent(Talent talent)
        {
            if (talent == null) return;

            _talents.Add(talent);

            if (talent.StatModifiers.Count > 0)
            {
                foreach (var statMod in talent.StatModifiers)
                    _statModifiers.Add(statMod);
                OnStatModifiersChanged?.Invoke();
            }
            if (talent.Spells.Count > 0)
            {
                foreach (var spell in talent.Spells)
                    _spells.Add(spell);
                OnSpellsChanged?.Invoke();
            }
        }

        /// <summary>
        /// Removes a talent and if that talent had a StatModifier
        /// notifies all observers that modifiers changed.
        /// </summary>
        /// <param name="talent">Talent to remove.</param>
        /// <returns></returns>
        public bool RemoveTalent(Talent talent)
        {
            bool removed = _talents.Remove(talent);

            if (removed && talent.StatModifiers.Count > 0)
            {
                foreach (var statMod in talent.StatModifiers)
                    _statModifiers.Remove(statMod);
                OnStatModifiersChanged?.Invoke();
            }
            if (removed && talent.Spells.Count > 0)
            {
                foreach (var spell in talent.Spells)
                    _spells.Remove(spell);
                OnSpellsChanged?.Invoke();
            }

            return removed;
        }

        public void RemoveMultipleTalents(IEnumerable<Talent> talentsToRemove)
        {
            (bool removedSpells, bool removedStatMods) = (false, false);

            foreach (var talent in talentsToRemove)
            {
                _talents.Remove(talent);
                if (talent.StatModifiers.Count > 0)
                {
                    if (!removedStatMods) removedStatMods = true;
                    foreach (var stat in talent.StatModifiers) _statModifiers.Remove(stat);
                }
                if (talent.Spells.Count > 0)
                {
                    if (!removedSpells) removedSpells = true;
                    foreach (var spell in talent.Spells) _spells.Remove(spell);
                }
            }

            if (removedStatMods) OnStatModifiersChanged?.Invoke();
            if (removedSpells) OnSpellsChanged?.Invoke();
        }

        /// <summary>
        /// Returns all modifiers contained within active talents.
        /// </summary>
        /// <returns>Array of active modifiers.</returns>
        public StatModifier[] GetStatModifiers()
        {
            return _statModifiers.ToArray();
        }

        /// <summary>
        /// Returns all spells contained within active talents.
        /// </summary>
        /// <returns>Array of active spells.</returns>
        public SpellType[] GetSpells()
        {
            return _spells.ToArray();
        }
    }
}
