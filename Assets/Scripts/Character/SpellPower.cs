﻿using Elementalist.Character.Stats;
using Zenject;

namespace Elementalist.Character
{
    public class SpellPower : ElementStat
    {
        [Inject]
        private void Inject(CharacterEssence charEss, CharacterEvents charEvents)
        {
            Initialize(charEss, charEvents, StatType.SpellPower);
        }
    }
}
