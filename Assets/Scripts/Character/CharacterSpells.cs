﻿using Elementalist.Combat.Actions;
using System.Collections.ObjectModel;
using UnityEngine;
using Zenject;

namespace Elementalist.Character
{
    public class CharacterSpells : MonoBehaviour
    {
        private CharacterEvents _characterEvents = null;

        private SpellType[] _spells = null;

        public ReadOnlyCollection<SpellType> Spells { get; private set; }

        [Inject]
        private void Inject(CharacterEvents characterEvents)
        {
            _characterEvents = characterEvents;
            _characterEvents.OnSpellsChanged += OnSpellsChanged;
        }

        private void OnSpellsChanged(SpellType[] spells)
        {
            _spells = spells.Clone() as SpellType[];
            Spells = new ReadOnlyCollection<SpellType>(_spells);
        }

        public SpellType[] GetSpellsListClone()
        {
            return _spells.Clone() as SpellType[];
        }
        
        public SpellType GetSpell(Spell spellEnum)
        {
            foreach (var spell in _spells)
                if (spell.SpellEnum == spellEnum) return spell;

            return null;
        }

        public bool HasSpell(Spell spellEnum)
        {
            foreach (var spell in _spells)
                if (spell.SpellEnum == spellEnum) return true;

            return false;
        }
    }
}
