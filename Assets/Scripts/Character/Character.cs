﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Elementalist.Character.Stats;
using Elementalist.Combat.Effects;
using Elementalist.Combat.Actions;

namespace Elementalist.Character
{
    /// <summary>
    /// Facade for Character GameObject.
    /// It is used by other GameObject to access Character.
    /// </summary>
    public class Character : MonoBehaviour
    {
        private SpellPower _spellpower = null;
        private Resistance _resistance = null;
        private Vitality _vitality = null;
        private Sources _sources = null;
        private CharacterEssence _characterEssence = null;
        private CharacterTemporaryEffects _temporaryEffects = null;
        private CharacterSpells _characterSpells = null;

        [Inject]
        private void Inject(
            SpellPower sp, Resistance res, 
            Vitality vt, Sources sc,
            CharacterEssence chess, 
            CharacterTemporaryEffects tmpEffects,
            CharacterSpells spells)
        {
            _spellpower = sp;
            _resistance = res;
            _vitality = vt;
            _sources = sc;
            _characterEssence = chess;
            _temporaryEffects = tmpEffects;
            _characterSpells = spells;
        }

        public string CharacterName => _characterEssence.CharacterName;
        public string CharacterDescription => _characterEssence.CharacterDescription;
        public Sprite CharacterIcon => _characterEssence.CharacterIcon;
        public CharacterType CharacterType { get => _characterEssence.CharacterType; }

        public double CurrentHealth { get => _vitality.CurrentHealth; }
        public double HealthRegeneration { get => _vitality.HealthRegeneration; }
        public double MaxHealth { get => _vitality.MaxHealth; }
        public double CurrentBarrier { get => _vitality.CurrentBarrier; }
        public double BarrierRegeneration { get => _vitality.BarrierRegeneration; }
        public double MaxBarrier { get => _vitality.MaxBarrier; }

        public double CurrentActionPoints { get => _sources.CurrentActionPoints; }
        public double ActionPointsRegeneration { get => _sources.ActionPointsRegeneration; }
        public double MaxActionPoints { get => _sources.MaxActionPoints; }
        public double CurrentOrbs(ElementType elementType) => _sources.GetCurrentOrbs(elementType);
        public double OrbsRegeneration(ElementType elementType) => _sources.GetOrbsRegeneration(elementType);
        public double MaxOrbs(ElementType elementType) => _sources.GetMaxOrbs(elementType);

        public double SpellPower(ElementType elementType) => _spellpower.GetValue(elementType);
        public double Resistance(ElementType elementType) => _resistance.GetValue(elementType);

        public double TakeDamage(double amount, ElementType elementType, bool bypassBarrier = false) => _vitality.TakeDamage(amount, elementType, bypassBarrier);

        public bool UseActionPoints(double amount) => _sources.UseActionPoints(amount);
        public bool UseOrbs(double amount, ElementType elementType) => _sources.UseOrbs(amount, elementType);

        public void RestoreHealth(double amount, ElementType elementType) => _vitality.Restore(amount, StatType.Health, elementType);
        public void RestoreBarrier(double amount, ElementType elementType) => _vitality.Restore(amount, StatType.Barrier, elementType);
        public void RestoreActionPoints(double amount) => _sources.RestoreActionPoints(amount);
        public void RestoreOrbs(double amount, ElementType elementType) => _sources.RestoreOrbs(amount, elementType);

        public void RefillResources()
        {
            _vitality.Refill();
            _sources.Refill();
        }

        public int ActiveActionEffectsCount { get => _temporaryEffects.ActiveEffectsCount; }

        public bool HasSpell(Spell spellEnum) => _characterSpells.HasSpell(spellEnum);
        public SpellType GetSpell(Spell spellEnum) => _characterSpells.GetSpell(spellEnum);
        
        public void AddTemporaryEffect(ITemporaryEffect effect, Character target, Character source)
        {
            switch (effect)
            {
                case ActionEffect actionEffect:
                    _temporaryEffects.AddEffect(actionEffect, target, source);
                    break;
                case Status statusEffect:
                    // TODO: implement adding status effect
                    break;
                default:
                    Debug.LogWarning(string.Format("Trying to add unsupported temporary effect {0} to character {1}.", effect, name));
                    break;
            }
        }
        
        /// <summary>
        /// Gets the value of stat specified by parameters.
        /// </summary>
        /// <param name="statType">Stat type, value of which should be returned.</param>
        /// <param name="elementType">Element type of stat, value of which should be returned.</param>
        /// <param name="maxValue">Specifies whether max or current value should be returned. Ignored on stats without current value.</param>
        /// <returns>Value of specified stat.</returns>
        public double GetStatValue(StatType statType, ElementType? elementType = null, bool maxValue = false)
        {
            switch (statType)
            {
                case StatType.Health:
                    return maxValue ? MaxHealth : CurrentHealth;
                case StatType.HealthRegen:
                    return HealthRegeneration;
                case StatType.Barrier:
                    return maxValue ? MaxBarrier : CurrentBarrier;
                case StatType.BarrierRegen:
                    return BarrierRegeneration;
                case StatType.ActionPoints:
                    return maxValue ? MaxActionPoints : CurrentActionPoints;
                case StatType.ActionPointsRegen:
                    return ActionPointsRegeneration;
                case StatType.Orbs:
                case StatType.OrbsRegen:
                case StatType.Resistance:
                case StatType.SpellPower:
                    return GetElementalStatValue(statType, elementType, maxValue);
                default:
                    Debug.LogError(string.Format("Trying to get stat value for unsupported stat type from {0}. StatType: {1}", name, statType));
                    return 0d;
            }
        }

        /// <summary>
        /// Helper function for GetStatValue. Checks if element type is specified and if it is
        /// returns corresponding stat value.
        /// </summary>
        /// <param name="statType">Stat type, value of which should be returned.</param>
        /// <param name="elementType">Element type of stat, value of which should be returned.</param>
        /// <param name="maxValue">Specifies whether max or current value should be returned. Ignored on stats without current value.</param>
        /// <returns>Value of specified stat.</returns>
        private double GetElementalStatValue(StatType statType, ElementType? elementType, bool maxValue = false)
        {
            if (elementType == null)
            {
                Debug.LogError(string.Format("Can't get {0} value without specifying element type.", statType));
                return 0d;
            }

            switch (statType)
            {
                case StatType.Orbs:
                    return maxValue ? MaxOrbs(elementType.Value) : CurrentOrbs(elementType.Value);
                case StatType.OrbsRegen:
                    return OrbsRegeneration(elementType.Value);
                case StatType.Resistance:
                    return Resistance(elementType.Value);
                case StatType.SpellPower:
                    return SpellPower(elementType.Value);
                default:
                    Debug.LogError(string.Format("Trying to get stat value for unsupported element stat type from {0}. StatType: {1}", name, statType));
                    return 0d;
            }
        }
    }
}
