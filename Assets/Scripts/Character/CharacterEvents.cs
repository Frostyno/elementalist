﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Elementalist.Character.Stats;
using Zenject;
using Elementalist.Combat.Actions;

namespace Elementalist.Character
{
    /// <summary>
    /// Provides all events within a character.
    /// </summary>
    public class CharacterEvents : MonoBehaviour
    {
        private IStatModifierProvider[] _statModsProviders = null;
        private ISpellProvider[] _spellProviders = null;

        public event Action<StatModifier[]> OnStatModifiersChanged = null;
        public event Action<SpellType[]> OnSpellsChanged = null;

        public event Action OnRoundBegan = null;
        public event Action OnRoundEnded = null;
        public event Action OnCombatEnded = null;

        [Inject]
        private void Inject(IStatModifierProvider[] statModifierProviders, ISpellProvider[] spellProviders)
        {
            _statModsProviders = statModifierProviders;
            _spellProviders = spellProviders;
        
            foreach (var provider in _statModsProviders)
            {
                provider.OnStatModifiersChanged += OnProviderStatModifiersChanged;
            }
            foreach (var provider in _spellProviders)
            {
                provider.OnSpellsChanged += OnProviderSpellsChanged;
            }

            OnProviderStatModifiersChanged();
            OnProviderSpellsChanged();
        }

        private void OnProviderStatModifiersChanged()
        {
            List<StatModifier> mods = new List<StatModifier>();

            foreach (var provider in _statModsProviders)
                mods.AddRange(provider.GetStatModifiers());

            OnStatModifiersChanged?.Invoke(mods.ToArray());
        }

        private void OnProviderSpellsChanged()
        {
            List<SpellType> spells = new List<SpellType>();

            foreach (var provider in _spellProviders)
                spells.AddRange(provider.GetSpells());

            OnSpellsChanged?.Invoke(spells.ToArray());
        }

        public void SignalRoundBegan() => OnRoundBegan?.Invoke();
        public void SignalRoundEnded() => OnRoundEnded?.Invoke();
        public void SignalCombatEnded() => OnCombatEnded?.Invoke();
    }
}
