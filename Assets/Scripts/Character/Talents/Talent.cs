﻿using UnityEngine;
using Elementalist.Character.Stats;
using Elementalist.Combat.Actions;
using System.Collections.ObjectModel;
using UnityEditor;

namespace Elementalist.Character.Talents
{
    [CreateAssetMenu(fileName = "Talent", menuName = "Elementalist/Talents/Talent", order = 0)]
    public class Talent : ScriptableObject
    {
        [SerializeField] private string _talentGUID = "";
        [SerializeField] private string _talentName = "";
        [SerializeField] private uint _cost = 0;
        [SerializeField] private ElementType _elementType = ElementType.Fire;
        [SerializeField] private ElementDataUint _elementLevels = new ElementDataUint();
        [SerializeField] private StatModifier[] _statModifiers = new StatModifier[0];
        [SerializeField] private SpellType[] _spells = new SpellType[0];

        public string TalentGUID { get => _talentGUID; }
        public string TalentName { get => _talentName; }
        public ElementType ElementType { get => _elementType; }
        public uint Cost { get => _cost; }
        public ReadOnlyCollection<StatModifier> StatModifiers { get; private set; }
        public ReadOnlyCollection<SpellType> Spells { get; private set; }

        public uint GetElementLevel(ElementType elementType) => _elementLevels.GetValue(elementType);

        private void OnEnable()
        {
            StatModifiers = new ReadOnlyCollection<StatModifier>(_statModifiers);
            Spells = new ReadOnlyCollection<SpellType>(_spells);

#if UNITY_EDITOR
            AssignGUID();
#endif
        }

#if UNITY_EDITOR
        private void AssignGUID()
        {
            if (!string.IsNullOrEmpty(_talentGUID)) return;

            _talentGUID = GUID.Generate().ToString();
        }
#endif
    }
}
