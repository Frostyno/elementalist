﻿using UnityEngine;
namespace Elementalist.Character.Stats
{
    public class Stat
    {
        private readonly StatType _statType;
        private readonly ElementType? _elementType = null;
        private double _value = 0d;

        public StatType StatType { get => _statType; }
        public ElementType? ElementType { get => _elementType; }
        public double Value { get => _value; }
        
        public Stat(StatType statType, ElementType? elementType = null)
        {
            _statType = statType;
            _elementType = elementType;
        }

        /// <summary>
        /// Recalculates the value of this stat based on parameters.
        /// </summary>
        /// <param name="characterType">Contains character's base stats.</param>
        /// <param name="mods">Stat modifiers to add on top of base stats.</param>
        public virtual void Recalculate(CharacterType characterType, StatModifier[] mods)
        {
            double baseValue = 0d;

            if (characterType != null)
                baseValue = characterType.GetStatBaseValue(_statType, _elementType);
            else
                Debug.LogWarning(string.Format("CharacterType was null when recalculating stat {0}.", StatType));
            
            double additive = 0d;
            double percentage = 0d;
            double handicap = 0d;

            if (mods != null)
            {
                foreach (var mod in mods)
                {
                    double modValue = mod.GetStatModifier(_statType, _elementType);
                    switch (mod.Type)
                    {
                        case StatModifierType.Additive:
                            additive += modValue;
                            break;
                        case StatModifierType.Percentual:
                            percentage += modValue;
                            break;
                        case StatModifierType.Handicap:
                            handicap += modValue;
                            break;
                    }
                }
            }
            
            double newValue = baseValue + additive;
            newValue += newValue * percentage;
            newValue += newValue * handicap;

            _value = newValue;
        }
    }
}
