﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Character.Stats
{
    public struct ResourceChangedArgs
    {
        public double OldCurrent { get; private set; }
        public double NewCurrent { get; private set; }
        public double OldMax { get; private set; }
        public double NewMax { get; private set; }
        public double OldRegeneration { get; private set; }
        public double NewRegeneration { get; private set; }

        public ResourceChangedArgs(
            double oldCurr, double newCurr,
            double oldMax, double newMax,
            double oldRegen, double newRegen)
        {
            OldCurrent = oldCurr;
            NewCurrent = newCurr;
            OldMax = oldMax;
            NewMax = newMax;
            OldRegeneration = oldRegen;
            NewRegeneration = newRegen;
        }
    }

    public class Resource
    {
        protected double _currentValue = 0d;
        protected Stat _maxValueStat = null;
        protected Stat _regenStat = null;

        public double CurrentValue { get => _currentValue; }
        public double MaxValue { get => _maxValueStat.Value; }
        public double Regeneration { get => _regenStat.Value; }
        public StatType StatType { get => _maxValueStat.StatType; }
        public StatType RegenerationStatType { get => _regenStat.StatType; }
        public ElementType? ElementType { get => _maxValueStat.ElementType; }
        public ElementType? RegenerationElementType { get => _regenStat.ElementType; }

        public event Action<ResourceChangedArgs> OnResourceChanged = null;

        public Resource(StatType statType, StatType regenStatType, ElementType? elementType = null)
        {
            _maxValueStat = new Stat(statType, elementType);
            _regenStat = new Stat(regenStatType, elementType);
        }

        private ResourceChangedArgs CreateResourceChangedArgs(double oldCurrent, double oldMax, double oldRegen)
        {
            return new ResourceChangedArgs(
                oldCurrent, CurrentValue,
                oldMax, MaxValue,
                oldRegen, Regeneration
                );
        }

        /// <summary>
        /// Recalculates max and current value as well as regeneration value.
        /// </summary>
        /// <param name="charType">Contains character's base stats.</param>
        /// <param name="mods">Array of stat modifiers that are added on top of base stats.</param>
        public void Recalculate(CharacterType charType, StatModifier[] mods)
        {
            (double oldCurrent, double oldMax, double oldRegen) = (CurrentValue, MaxValue, Regeneration);

            _maxValueStat.Recalculate(charType, mods);

            double diff = MaxValue - oldMax;

            if (diff > 0d)
            {
                _currentValue += diff;
            }
            else
            {
                _currentValue = Math.Min(_currentValue, MaxValue);
            }

            _regenStat.Recalculate(charType, mods);
            OnResourceChanged?.Invoke(CreateResourceChangedArgs(oldCurrent, oldMax, oldRegen));
        }

        /// <summary>
        /// Increases current value by roundsCount * Regeneration.
        /// </summary>
        /// <param name="roundsCount">Number of rounds to regenerate.</param>
        public void Regenerate(uint roundsCount)
        {
            (double oldCurrent, double oldMax, double oldRegen) = (CurrentValue, MaxValue, Regeneration);

            _currentValue += Regeneration * roundsCount;
            _currentValue = Math.Min(_currentValue, MaxValue);
            OnResourceChanged?.Invoke(CreateResourceChangedArgs(oldCurrent, oldMax, oldRegen));
        }

        /// <summary>
        /// Increases current value.
        /// </summary>
        /// <param name="amount">Increment.</param>
        public void Restore(double amount)
        {
            (double oldCurrent, double oldMax, double oldRegen) = (CurrentValue, MaxValue, Regeneration);

            _currentValue = Math.Min(_currentValue + amount, MaxValue);
            OnResourceChanged?.Invoke(CreateResourceChangedArgs(oldCurrent, oldMax, oldRegen));
        }

        /// <summary>
        /// Decreases resource's current value (with minimum being 0).
        /// Cannot use more than current value of resource.
        /// </summary>
        /// <param name="amount">Decrement.</param>
        /// <param name="allowNegativeValue">Allow negative values in special cases.</param>
        /// <returns>Whether there was enough resource to use, or usage failed.</returns>
        public bool Use(double amount, bool allowNegativeValue = false)
        {
            if (!allowNegativeValue && amount > _currentValue) return false;

            (double oldCurrent, double oldMax, double oldRegen) = (CurrentValue, MaxValue, Regeneration);

            _currentValue -= amount;
            OnResourceChanged?.Invoke(CreateResourceChangedArgs(oldCurrent, oldMax, oldRegen));

            return true;
        }

        public void Refill()
        {
            double oldCurr = CurrentValue;
            _currentValue = MaxValue;
            OnResourceChanged?.Invoke(CreateResourceChangedArgs(oldCurr, MaxValue, Regeneration));
        }
    }
}
