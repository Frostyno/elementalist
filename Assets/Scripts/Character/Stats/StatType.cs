﻿using System;
using System.Runtime.Serialization;

namespace Elementalist.Character.Stats
{
    [Serializable]
    [DataContract]
    public enum StatType
    {
        [EnumMember]Health,
        [EnumMember]HealthRegen,
        [EnumMember]Barrier,
        [EnumMember]BarrierRegen,
        [EnumMember]ActionPoints,
        [EnumMember]ActionPointsRegen,
        [EnumMember]Orbs,
        [EnumMember]OrbsRegen,
        [EnumMember]Resistance,
        [EnumMember]SpellPower
    }
}
