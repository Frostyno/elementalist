﻿using UnityEngine;

namespace Elementalist.Character.Stats
{
    [CreateAssetMenu(fileName = "StatModifier", menuName = "Elementalist/Stats/StatModifier", order = 0)]
    public class StatModifier : ScriptableObject
    {
        [SerializeField] private StatModifierType _type = StatModifierType.Additive;
        [SerializeField] private StatData _stats = null;

        public StatModifierType Type { get => _type; }

        private void OnEnable()
        {
            if (_type != StatModifierType.Additive)
            {
                (double fire, double water, double earth, double air) = GetElementStatModifiers(StatType.Resistance);
                if (fire != 0d || water != 0d || earth != 0d || air != 0d)
                    Debug.LogWarning($"Resistance is non 0 value in {name} modifier. Only additive modifiers should be affecting resistance.");
            }
        }

        /// <summary>
        /// Returns value of this stat modifier for selected stat type and element type.
        /// </summary>
        /// <param name="statType">Type of stat.</param>
        /// <param name="elementType">Type of element.</param>
        /// <returns>Value of stat modifier for selected parameters.</returns>
        public double GetStatModifier(StatType statType, ElementType? elementType = null)
        {
            return _stats.GetStatValue(statType, elementType);
        }

        /// <summary>
        /// Gets values of passed element based stat broken down to individual elements.
        /// For non-element based stat returns tuple of 0s. To get non-element based stat valuee use GetStatValue.
        /// </summary>
        /// <param name="statType">Type of element based stat to get values for.</param>
        /// <returns>Tuple of values of passed element based stat.</returns>
        public (double, double, double, double) GetElementStatModifiers(StatType statType) => _stats.GetElementStatValues(statType);
    }
}
