﻿namespace Elementalist.Character.Stats
{
    public enum StatModifierType
    {
        Additive,
        Percentual,
        Handicap
    }
}