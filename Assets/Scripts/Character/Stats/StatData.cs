﻿using System.Text;
using UnityEngine;

namespace Elementalist.Character.Stats
{
    /// <summary>
    /// Contains data for all possible stats.
    /// </summary>
    [System.Serializable]
    public class StatData
    {
        [SerializeField] private double _health = 0d;
        [SerializeField] private double _healthRegen = 0d;
        [SerializeField] private double _barrier = 0d;
        [SerializeField] private double _barrierRegen = 0d;
        [SerializeField] private double _actionPoints = 0d;
        [SerializeField] private double _actionPointsRegen = 0d;
        [SerializeField] private ElementDataDouble _orbs = new ElementDataDouble();
        [SerializeField] private ElementDataDouble _orbsRegen = new ElementDataDouble();
        [SerializeField] private ElementDataDouble _resistances = new ElementDataDouble();
        [SerializeField] private ElementDataDouble _spellPower = new ElementDataDouble();

        /// <summary>
        /// Returns value of this stat modifier for selected stat type and element type.
        /// </summary>
        /// <param name="statType">Type of stat.</param>
        /// <param name="elementType">Type of element.</param>
        /// <returns>Value of stat modifier for selected parameters.</returns>
        public double GetStatValue(StatType statType, ElementType? elementType = null)
        {
            switch (statType)
            {
                case StatType.Health: return _health;
                case StatType.HealthRegen: return _healthRegen;
                case StatType.Barrier: return _barrier;
                case StatType.BarrierRegen: return _barrierRegen;
                case StatType.ActionPoints: return _actionPoints;
                case StatType.ActionPointsRegen: return _actionPointsRegen;
                case StatType.Orbs: return _orbs != null && elementType.HasValue ? _orbs.GetValue(elementType.Value) : 0d;
                case StatType.OrbsRegen: return _orbsRegen != null && elementType.HasValue ? _orbsRegen.GetValue(elementType.Value) : 0d;
                case StatType.Resistance: return _resistances != null && elementType.HasValue ? _resistances.GetValue(elementType.Value) : 0d;
                case StatType.SpellPower: return _spellPower != null && elementType.HasValue ? _spellPower.GetValue(elementType.Value) : 0d;
            }

            return 0d;
        }

        /// <summary>
        /// Gets values of passed element based stat broken down to individual elements.
        /// For non-element based stat returns tuple of 0s. To get non-element based stat valuee use GetStatValue.
        /// </summary>
        /// <param name="statType">Type of element based stat to get values for.</param>
        /// <returns>Tuple of values of passed element based stat.</returns>
        public (double, double, double, double) GetElementStatValues(StatType statType)
        {
            switch (statType)
            {
                case StatType.Orbs:
                    return (_orbs.GetValue(ElementType.Fire), _orbs.GetValue(ElementType.Water), 
                        _orbs.GetValue(ElementType.Earth), _orbs.GetValue(ElementType.Air));
                case StatType.OrbsRegen:
                    return (_orbsRegen.GetValue(ElementType.Fire), _orbsRegen.GetValue(ElementType.Water),
                        _orbsRegen.GetValue(ElementType.Earth), _orbsRegen.GetValue(ElementType.Air));
                case StatType.Resistance:
                    return (_resistances.GetValue(ElementType.Fire), _resistances.GetValue(ElementType.Water),
                        _resistances.GetValue(ElementType.Earth), _resistances.GetValue(ElementType.Air));
                case StatType.SpellPower:
                    return (_spellPower.GetValue(ElementType.Fire), _spellPower.GetValue(ElementType.Water),
                        _spellPower.GetValue(ElementType.Earth), _spellPower.GetValue(ElementType.Air));
                default:
                    return (0d, 0d, 0d, 0d);
            }
        }
    }
}