﻿using Elementalist.Combat.Actions;
using System;

namespace Elementalist.Character
{
    public interface ISpellProvider
    {
        SpellType[] GetSpells();
        event Action OnSpellsChanged;
    }
}
