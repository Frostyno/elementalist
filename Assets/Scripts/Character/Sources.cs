﻿using Elementalist.Character.Stats;
using System;
using UnityEngine;
using Zenject;

namespace Elementalist.Character
{
    /// <summary>
    /// Contains all usable resources for the character.
    /// </summary>
    public class Sources : MonoBehaviour
    {
        #region AttributesAndProperties

        private Resource _fireOrbs = new Resource(StatType.Orbs, StatType.OrbsRegen, ElementType.Fire);
        private Resource _waterOrbs = new Resource(StatType.Orbs, StatType.OrbsRegen, ElementType.Water);
        private Resource _earthOrbs = new Resource(StatType.Orbs, StatType.OrbsRegen, ElementType.Earth);
        private Resource _airOrbs = new Resource(StatType.Orbs, StatType.OrbsRegen, ElementType.Air);
        private Resource _actionPoints = new Resource(StatType.ActionPoints, StatType.ActionPointsRegen);

        private CharacterEssence _characterEssence = null;
        private CharacterEvents _characterEvents = null;

        public double CurrentActionPoints { get => _actionPoints.CurrentValue; }
        public double MaxActionPoints { get => _actionPoints.MaxValue; }
        public double ActionPointsRegeneration { get => _actionPoints.Regeneration; }

        public event Action<ResourceChangedArgs> OnFireOrbsChanged = null;
        public event Action<ResourceChangedArgs> OnWaterOrbsChanged = null;
        public event Action<ResourceChangedArgs> OnEarthOrbsChanged = null;
        public event Action<ResourceChangedArgs> OnAirOrbsChanged = null;
        public event Action<ResourceChangedArgs> OnActionPointsChanged = null;

        #endregion

        [Inject]
        private void Inject(CharacterEssence charEss, CharacterEvents charEvents)
        {
            _fireOrbs.OnResourceChanged += OnFireOrbsResourceChanged;
            _waterOrbs.OnResourceChanged += OnWaterOrbsResourceChanged;
            _earthOrbs.OnResourceChanged += OnEarthOrbsResourceChanged;
            _airOrbs.OnResourceChanged += OnAirOrbsResourceChanged;
            _actionPoints.OnResourceChanged += OnActionPointsResourceChanged;

            _characterEssence = charEss;
            _characterEvents = charEvents;

            _characterEvents.OnRoundBegan += OnRoundBegan;
            _characterEvents.OnStatModifiersChanged += OnStatModifiersChanged;
            OnStatModifiersChanged(null);
        }

        #region PrivateFunctions

        private Resource GetOrbsByElement(ElementType elementType)
        {
            switch (elementType)
            {
                case ElementType.Fire: return _fireOrbs;
                case ElementType.Water: return _waterOrbs;
                case ElementType.Earth: return _earthOrbs;
                case ElementType.Air: return _airOrbs;
            }

            return null;
        }

        #endregion
        
        #region PublicFunctions

        public double GetCurrentOrbs(ElementType elementType)
        {
            return GetOrbsByElement(elementType).CurrentValue;
        }

        public double GetMaxOrbs(ElementType elementType)
        {
            return GetOrbsByElement(elementType).MaxValue;
        }

        public double GetOrbsRegeneration(ElementType elementType)
        {
            return GetOrbsByElement(elementType).Regeneration;
        }

        public bool UseOrbs(double amount, ElementType elementType)
        {
            return GetOrbsByElement(elementType).Use(amount);
        }

        public bool UseActionPoints(double amount)
        {
            return _actionPoints.Use(amount);
        }

        public void RestoreOrbs(double amount, ElementType elementType)
        {
            GetOrbsByElement(elementType).Restore(amount);
        }

        public void RestoreActionPoints(double amount)
        {
            _actionPoints.Restore(amount);
        }

        public void Refill()
        {
            _actionPoints.Refill();
            _fireOrbs.Refill();
            _waterOrbs.Refill();
            _earthOrbs.Refill();
            _airOrbs.Refill();
        }

        #endregion
        
        #region EventCallbacks

        private void OnRoundBegan()
        {
            _fireOrbs.Regenerate(1);
            _waterOrbs.Regenerate(1);
            _earthOrbs.Regenerate(1);
            _airOrbs.Regenerate(1);
            _actionPoints.Regenerate(1);
        }

        private void OnStatModifiersChanged(StatModifier[] mods)
        {
            var charType = _characterEssence.CharacterType;

            _fireOrbs.Recalculate(charType, mods);
            _waterOrbs.Recalculate(charType, mods);
            _earthOrbs.Recalculate(charType, mods);
            _airOrbs.Recalculate(charType, mods);
            _actionPoints.Recalculate(charType, mods);
        }

        private void OnFireOrbsResourceChanged(ResourceChangedArgs args) => OnFireOrbsChanged?.Invoke(args);
        private void OnWaterOrbsResourceChanged(ResourceChangedArgs args) => OnWaterOrbsChanged?.Invoke(args);
        private void OnEarthOrbsResourceChanged(ResourceChangedArgs args) => OnEarthOrbsChanged?.Invoke(args);
        private void OnAirOrbsResourceChanged(ResourceChangedArgs args) => OnAirOrbsChanged?.Invoke(args);
        private void OnActionPointsResourceChanged(ResourceChangedArgs args) => OnActionPointsChanged?.Invoke(args);

        private void OnDestroy()
        {
            if (_characterEvents == null) return;

            _characterEvents.OnRoundBegan -= OnRoundBegan;
            _characterEvents.OnStatModifiersChanged -= OnStatModifiersChanged;
        }

        #endregion
    }
}
