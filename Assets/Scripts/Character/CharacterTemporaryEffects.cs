﻿using Elementalist.Character;
using Elementalist.Character.Stats;
using Elementalist.Combat.Effects;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Elementalist.Character
{
    /// <summary>
    /// Contains all temporary effects present on character with the exception
    /// of statuses.
    /// </summary>
    public class CharacterTemporaryEffects : MonoBehaviour, IStatModifierProvider
    {
        private CharacterEvents _characterEvents = null;

        private List<TemporaryEffectHandler<ActionEffect>> _effectHandlers = new List<TemporaryEffectHandler<ActionEffect>>();
        private List<TemporaryEffectHandler<ActionEffect>> _effectHandlersToRemove = new List<TemporaryEffectHandler<ActionEffect>>();

        public event Action OnStatModifiersChanged = null;

        public int ActiveEffectsCount { get => _effectHandlers.Count; }

        [Inject]
        public void Inject(CharacterEvents charEvents)
        {
            _characterEvents = charEvents;

            _characterEvents.OnRoundBegan += OnRoundBegan;
            _characterEvents.OnRoundEnded += OnRoundEnded;
            _characterEvents.OnCombatEnded += OnCombatEnded;
        }

        /// <summary>
        /// Creates a new ActionEffect handler.
        /// If added action effect has a StatModifier notifies all
        /// observers that modifiers changed.
        /// </summary>
        /// <param name="effect">ActionEffect to add.</param>
        /// <param name="target">Target of the effect.</param>
        /// <param name="source">Source of the effect.</param>
        public void AddEffect(ActionEffect effect, Character target, Character source)
        {
            var effectHandler = new TemporaryEffectHandler<ActionEffect>(effect, target, source);
            effectHandler.OnEffectEnded += OnEffectEnded;

            _effectHandlers.Add(effectHandler);

            if (effect.StatModifier != null) OnStatModifiersChanged?.Invoke();
        }

        /// <summary>
        /// Returns all StatModifiers contained within active TemporaryEffects.
        /// </summary>
        /// <returns>Array of active modifiers.</returns>
        public StatModifier[] GetStatModifiers()
        {
            // TODO: consider making this list a field and change it in AddEffect and RemoveEffectHandler
            List<StatModifier> mods = new List<StatModifier>();

            foreach (var effectHandler in _effectHandlers)
                if (effectHandler.Effect.StatModifier != null)
                    mods.Add(effectHandler.Effect.StatModifier);

            return mods.ToArray();
        }

        /// <summary>
        /// Removes specified action handler.
        /// </summary>
        /// <param name="effectHandler"></param>
        private void RemoveEffectHandler(TemporaryEffectHandler<ActionEffect> effectHandler)
        {
            effectHandler.OnEffectEnded -= OnEffectEnded;
            _effectHandlers.Remove(effectHandler);
        }

        /// <summary>
        /// Updates active temporary effects.
        /// If any effects with StatModifiers ended, notifies observers that modifiers changed.
        /// </summary>
        /// <param name="roundBeganEffects">Specifies what type of effects should be updated (round began or end).</param>
        private void UpdateEffects(bool roundBeganEffects)
        {
            foreach (var effectHandler in _effectHandlers)
            {
                if (roundBeganEffects == effectHandler.Effect.ProcessOnRoundBegan)
                    effectHandler.Update(1);
            }

            bool removedWithStatModifier = false;
            foreach (var effectHandler in _effectHandlersToRemove)
            {
                if (effectHandler.Effect.StatModifier != null) removedWithStatModifier = true;
                RemoveEffectHandler(effectHandler);
            }

            _effectHandlersToRemove.Clear();
            if (removedWithStatModifier) OnStatModifiersChanged?.Invoke();
        }

        private void OnRoundBegan()
        {
            UpdateEffects(true);
        }

        private void OnRoundEnded()
        {
            UpdateEffects(false);
        }

        private void OnCombatEnded()
        {
            if (_effectHandlers.Count > 0)
                _effectHandlers.Clear();

            OnStatModifiersChanged?.Invoke();
        }

        private void OnEffectEnded(TemporaryEffectHandler<ActionEffect> effectHandler)
        {
            _effectHandlersToRemove.Add(effectHandler);
        }

        private void OnDestroy()
        {
            if (_characterEvents == null) return;

            _characterEvents.OnCombatEnded -= OnCombatEnded;
            _characterEvents.OnRoundBegan -= OnRoundBegan;
            _characterEvents.OnRoundEnded -= OnRoundEnded;
        }
    }
}
