﻿using UnityEngine;
using Elementalist.Character.Stats;

namespace Elementalist.Character
{
    [CreateAssetMenu(fileName = "CharacterType", menuName = "Elementalist/Stats/CharacterType", order = 0)]
    public class CharacterType : ScriptableObject
    {
        [SerializeField] private StatData _stats = null;

        public void Construct(StatData stats) => _stats = stats;

        public double GetStatBaseValue(StatType statType, ElementType? elementType = null)
        {
            return _stats.GetStatValue(statType, elementType);
        }
    }
}
