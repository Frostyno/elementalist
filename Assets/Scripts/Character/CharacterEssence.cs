﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Elementalist.Character
{
    /// <summary>
    /// Contains basic information about character.
    /// </summary>
    public class CharacterEssence : MonoBehaviour
    {
        [SerializeField] private string _characterName = "";
        [SerializeField, TextArea] private string _characterDescription = "";

        [Space]

        [SerializeField] private CharacterType _characterType = null;
        [SerializeField] private Sprite _characterIcon = null;

        public string CharacterName => _characterName;
        public string CharacterDescription => _characterDescription;

        public CharacterType CharacterType { get => _characterType; }
        public Sprite CharacterIcon { get => _characterIcon; }
    }
}
