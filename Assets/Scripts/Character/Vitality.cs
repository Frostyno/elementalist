﻿using System;
using UnityEngine;
using Elementalist.Character.Stats;
using Zenject;

namespace Elementalist.Character
{
    public class Vitality : MonoBehaviour
    {
        #region AttributesAndProperties

        private Resource _health = new Resource(StatType.Health, StatType.HealthRegen);
        private Resource _barrier = new Resource(StatType.Barrier, StatType.BarrierRegen);

        private Resistance _resistance = null;
        private CharacterEssence _characterEssence = null;
        private CharacterEvents _characterEvents = null;

        public double MaxHealth { get => _health.MaxValue; }
        public double CurrentHealth { get => _health.CurrentValue; }
        public double HealthRegeneration { get => _health.Regeneration; }
        public double MaxBarrier { get => _barrier.MaxValue; }
        public double CurrentBarrier { get => _barrier.CurrentValue; }
        public double BarrierRegeneration { get => _barrier.Regeneration; }

        public event Action<ResourceChangedArgs> OnHealthChanged = null;
        public event Action<ResourceChangedArgs> OnBarrierChanged = null;
        public event Action<Vitality> OnCharacterDied = null;

        #endregion

        [Inject]
        private void Inject(Resistance resistance, CharacterEssence charEss, CharacterEvents charEvents)
        {
            _health.OnResourceChanged += OnHealthResourceChanged;
            _barrier.OnResourceChanged += OnBarrierResourceChanged;

            _resistance = resistance;
            _characterEssence = charEss;
            _characterEvents = charEvents;

            _characterEvents.OnRoundBegan += OnRoundBegan;
            _characterEvents.OnStatModifiersChanged += OnStatModifiersChanged;
            OnStatModifiersChanged(null); // Recalculate value only from CharacterType
        }

        #region PublicFunctions

        /// <summary>
        /// Reduces character's vitality by using up barrier and health.
        /// If character's resistance is greater than 1 (100%), he is healed instead of damaged.
        /// </summary>
        /// <param name="damageToTake">Damage that should be taken by character.</param>
        /// <param name="elementType">Element type of damage source.</param>
        /// <param name="bypassBarrier">Specifies whether the barrier should be ignored when taking damage.</param>
        /// <returns>Ammount of health damage taken (barrier is not included).</returns>
        public double TakeDamage(double damageToTake, ElementType elementType, bool bypassBarrier = false)
        {
            double resistance = _resistance.GetValue(elementType);

            // TODO: maybe heal only after barrier is destroyed?
            if (resistance > 1) // if resistance is big enough, target is healed instead of damaged
            {
                double healAmount = damageToTake * (resistance - 1);
                Restore(healAmount, StatType.Health, elementType);
                return 0d;
            }

            if (!bypassBarrier && CurrentBarrier > 0d)
            {
                if (CurrentBarrier >= damageToTake)
                {
                    _barrier.Use(damageToTake);
                    return 0d;
                }

                damageToTake -= CurrentBarrier;
                _barrier.Use(CurrentBarrier);
            }

            damageToTake -= damageToTake * resistance;
            _health.Use(damageToTake, allowNegativeValue: true);

            if (CurrentHealth <= 0) OnCharacterDied?.Invoke(this);

            return damageToTake;
        }

        /// <summary>
        /// Restores targeted resource.
        /// </summary>
        /// <param name="amount">Amount to be restored.</param>
        /// <param name="statType">Targeted resource type.</param>
        /// <param name="elementType">Restore's source element type (e.g. spell).</param>
        public void Restore(double amount, StatType statType, ElementType elementType)
        {
            switch (statType)
            {
                case StatType.Health:
                    _health.Restore(amount);
                    break;
                case StatType.Barrier:
                    _barrier.Restore(amount);
                    break;
            }
        }

        public void Refill()
        {
            _health.Refill();
            _barrier.Refill();
        }

        #endregion

        #region EventCallbacks

        private void OnRoundBegan()
        {
            _health.Regenerate(1);
            _barrier.Regenerate(1);
        }

        /// <summary>
        /// Recalculates health and barrier values.
        /// </summary>
        /// <param name="mods">Active stat modifiers.</param>
        private void OnStatModifiersChanged(StatModifier[] mods)
        {
            var charType = _characterEssence.CharacterType;

            _health.Recalculate(charType, mods);
            _barrier.Recalculate(charType, mods);
        }

        private void OnHealthResourceChanged(ResourceChangedArgs args) => OnHealthChanged?.Invoke(args);
        private void OnBarrierResourceChanged(ResourceChangedArgs args) => OnBarrierChanged?.Invoke(args);

        private void OnDestroy()
        {
            if (_characterEvents == null) return;

            _characterEvents.OnRoundEnded -= OnRoundBegan;
            _characterEvents.OnStatModifiersChanged -= OnStatModifiersChanged;
        }

        #endregion
    }
}
