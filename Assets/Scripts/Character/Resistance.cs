﻿using Elementalist.Character.Stats;
using Zenject;

namespace Elementalist.Character
{
    public class Resistance : ElementStat
    {
        [Inject]
        private void Inject(CharacterEssence charEss, CharacterEvents charEvents)
        {
            Initialize(charEss, charEvents, StatType.Resistance);
        }
    }
}
