﻿using System;
using UnityEngine;

namespace Elementalist
{
    [Serializable]
    public class PrefabProbabilityPair : ItemProbabilityPair<GameObject> { }

    [Serializable]
    public class ItemProbabilityPair<T>
    {
        [SerializeField] private T _item = default;
        [Range(0f, 1f)]
        [SerializeField] private double _probability = 1d;

        public T Item { get => _item; }
        public double Probability { get => _probability; }
    }
}
